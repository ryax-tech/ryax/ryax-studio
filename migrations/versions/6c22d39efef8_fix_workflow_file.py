"""fix-workflow-file

Revision ID: 6c22d39efef8
Revises: 60575b14d236
Create Date: 2023-03-23 18:51:34.177195

"""
from alembic import op

# revision identifiers, used by Alembic.
revision = '6c22d39efef8'
down_revision = '60575b14d236'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('workflow_action_addons_input_workflow_file_id_fkey', 'workflow_action_addons_input', type_='foreignkey')
    op.create_foreign_key(None, 'workflow_action_addons_input', 'workflow_file', ['workflow_file_id'], ['id'], ondelete='SET NULL')
    op.drop_constraint('workflow_action_input_workflow_file_id_fkey', 'workflow_action_input', type_='foreignkey')
    op.create_foreign_key(None, 'workflow_action_input', 'workflow_file', ['workflow_file_id'], ['id'], ondelete='SET NULL')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint(None, 'workflow_action_input', type_='foreignkey')
    op.create_foreign_key('workflow_action_input_workflow_file_id_fkey', 'workflow_action_input', 'workflow_file', ['workflow_file_id'], ['id'], ondelete='CASCADE')
    op.drop_constraint(None, 'workflow_action_addons_input', type_='foreignkey')
    op.create_foreign_key('workflow_action_addons_input_workflow_file_id_fkey', 'workflow_action_addons_input', 'workflow_file', ['workflow_file_id'], ['id'], ondelete='CASCADE')
    # ### end Alembic commands ###
