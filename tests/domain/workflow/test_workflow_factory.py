import uuid
from unittest import mock

import pytest

from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.workflow_factory import WorkflowFactory
from ryax.studio.domain.workflow.workflow_values import CreateWorkflowData


@pytest.fixture(scope="function")
def app_container():
    container = ApplicationContainer()
    yield container


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_workflow_id"))
def test_from_create_workflow_data(
    app_container: ApplicationContainer,
):
    """From create workflow data should return workflow"""
    create_workflow_data = CreateWorkflowData(
        name="Workflow name", description="Workflow description"
    )
    project_id = "88090135-0a30-45ab-b4e3-69739a583bad"

    workflow_factory: WorkflowFactory = app_container.workflow_factory()
    assert workflow_factory.from_create_workflow_data(
        create_workflow_data, "user", project_id
    ) == Workflow(
        id=str("new_workflow_id"),
        name=create_workflow_data.name,
        description=create_workflow_data.description,
        owner="user",
        project=project_id,
    )
    uuid.uuid4.assert_called_once()
