from datetime import datetime
from typing import List
from unittest import mock

import pytest

from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.domain.addon.addon_entities import AddOns
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_constraints import (
    WorkflowActionConstraints,
)
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionAddonInput,
    WorkflowActionInput,
)
from ryax.studio.domain.workflow.entities.workflow_action_link import WorkflowActionLink
from ryax.studio.domain.workflow.entities.workflow_action_objectives import (
    WorkflowActionObjectives,
)
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.entities.workflow_action_resources import (
    WorkflowActionResources,
)
from ryax.studio.domain.workflow.entities.workflow_error import WorkflowError
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.entities.workflow_result import WorkflowResult
from ryax.studio.domain.workflow.workflow_events import (
    WorkflowDeployEvent,
    WorkflowUndeployEvent,
)
from ryax.studio.domain.workflow.workflow_exceptions import (
    WorkflowActionLinkNotFoundException,
    WorkflowActionNotFoundException,
    WorkflowActionOutputNotFoundException,
    WorkflowHasNoTriggerException,
    WorkflowNotDeployable,
    WorkflowNotUpdatableException,
    WorkflowNotValidException,
)
from ryax.studio.domain.workflow.workflow_values import (
    AddWorkflowAction,
    AddWorkflowActionDynamicOutput,
    CopyWorkflowData,
    UpdateWorkflowAction,
    UpdateWorkflowActionDynamicOutput,
    UpdateWorkflowActionInputValue,
    WorkflowActionDeployStatus,
    WorkflowActionValidityStatus,
    WorkflowDeploymentStatus,
    WorkflowErrorCode,
    WorkflowInformations,
    WorkflowValidityStatus,
)


def test_update_informations() -> None:
    """Update informations should update workflow informations"""
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    workflow_informations = WorkflowInformations(
        name="Workflow name", description="Workflow description"
    )
    workflow.update_informations(workflow_informations, "user")
    assert workflow.name == workflow_informations.name
    assert workflow.description == workflow_informations.description
    assert workflow.owner == "user"


def test_get_actions() -> None:
    workflow_action_1 = WorkflowAction(
        id="workflow_action_1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_2 = WorkflowAction(
        id="workflow_action_2",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action_1, workflow_action_2],
    )
    assert workflow.get_actions() == [workflow_action_1, workflow_action_2]


def test_get_action() -> None:
    """Get workflow action from workflow"""
    workflow_action_id = "workflow_action_id"
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[
            WorkflowAction(
                id=workflow_action_id,
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            )
        ],
    )
    assert workflow.get_action(workflow_action_id) == WorkflowAction(
        id=workflow_action_id,
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )


def test_get_action_when_not_exist() -> None:
    """Get workflow action from workflow should return error when it is not found"""
    workflow_action_id = "workflow_action_id"
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    with pytest.raises(WorkflowActionNotFoundException):
        assert workflow.get_action(workflow_action_id)


@pytest.mark.parametrize(
    "workflow,expected_added_error,expected_removed_error",
    [
        (
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
            ),
            None,
            WorkflowErrorCode.WORKFLOW_ACTIONS_UPPER_LIMIT_REACHED,
        ),
        (
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id=f"wfm{i}",
                        name="wfmname",
                        technical_name="wfmname",
                        description="wfmname",
                        version="wfmname",
                        kind=ActionKind.PROCESSOR,
                        has_dynamic_outputs=False,
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    )
                    for i in (range(129))
                ],
            ),
            WorkflowErrorCode.WORKFLOW_ACTIONS_UPPER_LIMIT_REACHED,
            None,
        ),
    ],
    ids=[
        "With less than 128",
        "With more than 128 ",
    ],
)
def test_check_actions_limit(
    workflow: Workflow,
    expected_added_error: WorkflowErrorCode,
    expected_removed_error: WorkflowErrorCode,
) -> None:
    """Workflows may contain a maximum of 128 actions"""
    workflow.add_error = mock.MagicMock()
    workflow.remove_error = mock.MagicMock()
    workflow.check_actions_limit()
    if expected_added_error is not None:
        workflow.add_error.assert_called_with(expected_added_error)
    elif expected_removed_error is not None:
        workflow.remove_error.assert_called_with(expected_removed_error)
    else:
        workflow.add_error.assert_not_called()
        workflow.remove_error.assert_not_called()


@pytest.mark.parametrize(
    "workflow,expected_added_error,expected_removed_error",
    [
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        kind=ActionKind.PROCESSOR,
                        id="wfm1",
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        has_dynamic_outputs=False,
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        kind=ActionKind.PUBLISHER,
                        id="wfm2",
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        has_dynamic_outputs=False,
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        kind=ActionKind.PROCESSOR,
                        id="wfm3",
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        has_dynamic_outputs=False,
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
            ),
            WorkflowErrorCode.WORKFLOW_SOURCES_LOWER_LIMIT_REACHED,
            None,
            id="With no source",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        kind=ActionKind.SOURCE,
                        id="wfm1",
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        has_dynamic_outputs=False,
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    )
                ],
            ),
            None,
            WorkflowErrorCode.WORKFLOW_SOURCES_LOWER_LIMIT_REACHED,
            id="With no source",
        ),
    ],
)
def test_check_sources_limit(
    workflow: Workflow,
    expected_added_error: WorkflowErrorCode,
    expected_removed_error: WorkflowErrorCode,
) -> None:
    """Workflows may contain a maximum of 128 actions"""
    workflow.add_error = mock.MagicMock()
    workflow.remove_error = mock.MagicMock()
    workflow.check_sources_limit()
    if expected_added_error is not None:
        workflow.add_error.assert_called_with(expected_added_error)
    elif expected_removed_error is not None:
        workflow.remove_error.assert_called_with(expected_removed_error)
    else:
        workflow.add_error.assert_not_called()
        workflow.remove_error.assert_not_called()


@pytest.mark.parametrize(
    "workflow,expected_validity_status",
    [
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
            ),
            WorkflowValidityStatus.VALID,
            id="With no errors and with no actions",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        validity_status=WorkflowActionValidityStatus.VALID,
                        id="wfm1",
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        kind=ActionKind.PROCESSOR,
                        has_dynamic_outputs=False,
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        validity_status=WorkflowActionValidityStatus.VALID,
                        id="wfm1",
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        kind=ActionKind.PROCESSOR,
                        has_dynamic_outputs=False,
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
            ),
            WorkflowValidityStatus.VALID,
            id="With no errors and with all actions valid",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        validity_status=WorkflowActionValidityStatus.INVALID,
                        id="wfm1",
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        kind=ActionKind.PROCESSOR,
                        has_dynamic_outputs=False,
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        validity_status=WorkflowActionValidityStatus.VALID,
                        id="wfm1",
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        kind=ActionKind.PROCESSOR,
                        has_dynamic_outputs=False,
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
            ),
            WorkflowValidityStatus.INVALID,
            id="With no errors and with one of actions invalid",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                errors=[
                    WorkflowError(
                        id="1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
                    ),
                    WorkflowError(
                        id="2", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
                    ),
                ],
            ),
            WorkflowValidityStatus.INVALID,
            id="With errors",
        ),
    ],
)
def test_update_validity_status(
    workflow: Workflow, expected_validity_status: WorkflowValidityStatus
) -> None:
    workflow.update_validity_status()
    assert workflow.validity_status == expected_validity_status


def test_check_all() -> None:
    workflow_action_1 = WorkflowAction(
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=False,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_1.check_action_links_inputs = mock.MagicMock()
    workflow_action_1.check_action_inputs = mock.MagicMock()
    workflow_action_1.update_validity_status = mock.MagicMock()

    workflow_action_2 = WorkflowAction(
        id="wfm2",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=False,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_2.check_action_links_inputs = mock.MagicMock()
    workflow_action_2.check_action_inputs = mock.MagicMock()
    workflow_action_2.update_validity_status = mock.MagicMock()

    workflow_action_link_1 = mock.MagicMock(WorkflowActionLink)
    workflow_action_link_1.has_output_action = mock.MagicMock(return_value=True)

    workflow_action_link_2 = mock.MagicMock(WorkflowActionLink)
    workflow_action_link_2.has_output_action = mock.MagicMock(return_value=False)

    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action_1, workflow_action_2],
        actions_links=[workflow_action_link_1, workflow_action_link_2],
    )
    workflow.check_graph_cyclicity = mock.MagicMock()
    workflow.check_graph_connexity = mock.MagicMock()
    workflow.check_actions_limit = mock.MagicMock()
    workflow.check_sources_limit = mock.MagicMock()
    workflow.update_validity_status = mock.MagicMock()

    workflow.check_all()
    workflow.check_graph_cyclicity.assert_called()
    workflow.check_graph_connexity.assert_called()
    workflow.check_actions_limit.assert_called()
    workflow.check_sources_limit.assert_called()
    for item in workflow.actions:
        item.check_action_links_inputs.assert_called()
        item.check_action_inputs.assert_called()
        item.update_validity_status.assert_called()
    workflow.update_validity_status.assert_called()


@pytest.mark.parametrize(
    "workflow,expected_added_error,expected_removed_error",
    [
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="1",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        has_dynamic_outputs=False,
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="2",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        has_dynamic_outputs=False,
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="3",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        has_dynamic_outputs=False,
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="4",
                        kind=ActionKind.PUBLISHER,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        has_dynamic_outputs=False,
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
                actions_links=[
                    WorkflowActionLink(
                        upstream_action_id="1", downstream_action_id="2"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="2", downstream_action_id="3"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="3", downstream_action_id="4"
                    ),
                ],
            ),
            None,
            WorkflowErrorCode.WORKFLOW_HAS_CYCLES,
            id="With classical workflow",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="1",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="2",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="3",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="4",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="5",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="6",
                        kind=ActionKind.PUBLISHER,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
                actions_links=[
                    WorkflowActionLink(
                        upstream_action_id="1", downstream_action_id="2"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="3", downstream_action_id="4"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="2", downstream_action_id="5"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="4", downstream_action_id="5"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="5", downstream_action_id="6"
                    ),
                ],
            ),
            None,
            WorkflowErrorCode.WORKFLOW_HAS_CYCLES,
            id="With complex workflow",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="1",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
            ),
            None,
            WorkflowErrorCode.WORKFLOW_HAS_CYCLES,
            id="With one source",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="1",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="2",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="3",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="4",
                        kind=ActionKind.PUBLISHER,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
                actions_links=[
                    WorkflowActionLink(
                        upstream_action_id="1", downstream_action_id="2"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="3", downstream_action_id="4"
                    ),
                ],
            ),
            None,
            WorkflowErrorCode.WORKFLOW_HAS_CYCLES,
            id="With disjointed actions",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="1",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="2",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="3",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="4",
                        kind=ActionKind.PUBLISHER,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
                actions_links=[
                    WorkflowActionLink(
                        upstream_action_id="1", downstream_action_id="2"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="2", downstream_action_id="3"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="3", downstream_action_id="2"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="3", downstream_action_id="4"
                    ),
                ],
            ),
            WorkflowErrorCode.WORKFLOW_HAS_CYCLES,
            None,
            id="With classical when cycled actions",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="1",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="2",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="3",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="4",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="5",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="6",
                        kind=ActionKind.PUBLISHER,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
                actions_links=[
                    WorkflowActionLink(
                        upstream_action_id="1", downstream_action_id="3"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="2", downstream_action_id="4"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="3", downstream_action_id="5"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="4", downstream_action_id="5"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="5", downstream_action_id="2"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="5", downstream_action_id="6"
                    ),
                ],
            ),
            WorkflowErrorCode.WORKFLOW_HAS_CYCLES,
            None,
            id="With complex when cycled actions",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="1",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="2",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="3",
                        kind=ActionKind.PUBLISHER,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="4",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="5",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="6",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="7",
                        kind=ActionKind.PUBLISHER,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
                actions_links=[
                    WorkflowActionLink(
                        upstream_action_id="1", downstream_action_id="2"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="2", downstream_action_id="3"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="4", downstream_action_id="5"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="5", downstream_action_id="6"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="6", downstream_action_id="5"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="6", downstream_action_id="7"
                    ),
                ],
            ),
            WorkflowErrorCode.WORKFLOW_HAS_CYCLES,
            None,
            id="With disjointed and cycled workflow design",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="1",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="2",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="3",
                        kind=ActionKind.PUBLISHER,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
                actions_links=[
                    WorkflowActionLink(
                        upstream_action_id="1", downstream_action_id="2"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="2", downstream_action_id="2"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="2", downstream_action_id="3"
                    ),
                ],
            ),
            WorkflowErrorCode.WORKFLOW_HAS_CYCLES,
            None,
            id="With action auto loop workflow design",
        ),
    ],
)
def test_check_workflow_graph_cyclicity(
    workflow: Workflow,
    expected_added_error: WorkflowErrorCode,
    expected_removed_error: WorkflowErrorCode,
) -> None:
    """A connected graph is no problem even if invalid"""
    workflow.add_error = mock.MagicMock()
    workflow.remove_error = mock.MagicMock()
    workflow.check_graph_cyclicity()
    if expected_added_error is not None:
        workflow.add_error.assert_called_with(expected_added_error)
    elif expected_removed_error is not None:
        workflow.remove_error.assert_called_with(expected_removed_error)
    else:
        workflow.add_error.assert_not_called()
        workflow.remove_error.assert_not_called()


@pytest.mark.parametrize(
    "status,expected",
    [
        (WorkflowDeploymentStatus.NONE, True),
        (WorkflowDeploymentStatus.DEPLOYING, False),
        (WorkflowDeploymentStatus.DEPLOYED, False),
        (WorkflowDeploymentStatus.ERROR, True),
    ],
)
def test_is_deletable(status: WorkflowDeploymentStatus, expected: bool) -> None:
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        deployment_status=status,
    )
    assert workflow.is_deletable() is expected


@pytest.mark.parametrize(
    "status,expected",
    [
        (WorkflowDeploymentStatus.NONE, True),
        (WorkflowDeploymentStatus.DEPLOYING, False),
        (WorkflowDeploymentStatus.DEPLOYED, False),
        (WorkflowDeploymentStatus.ERROR, True),
    ],
)
def test_is_updatable(status: WorkflowDeploymentStatus, expected: bool) -> None:
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        deployment_status=status,
    )
    assert workflow.is_updatable() is expected


@pytest.mark.parametrize(
    "workflow,expected_added_error,expected_removed_error",
    [
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="1",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="2",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="3",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="4",
                        kind=ActionKind.PUBLISHER,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
                actions_links=[
                    WorkflowActionLink(
                        upstream_action_id="1", downstream_action_id="2"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="2", downstream_action_id="3"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="3", downstream_action_id="4"
                    ),
                ],
            ),
            None,
            WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
            id="With classical workflow",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="1",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="2",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="3",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="4",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="5",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="6",
                        kind=ActionKind.PUBLISHER,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
                actions_links=[
                    WorkflowActionLink(
                        upstream_action_id="1", downstream_action_id="2"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="3", downstream_action_id="4"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="2", downstream_action_id="5"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="4", downstream_action_id="5"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="5", downstream_action_id="6"
                    ),
                ],
            ),
            None,
            WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
            id="With complex workflow",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="1",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
            ),
            None,
            WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
            id="With one action workflow",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="1",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="2",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="3",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="4",
                        kind=ActionKind.PUBLISHER,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
                actions_links=[
                    WorkflowActionLink(
                        upstream_action_id="1", downstream_action_id="2"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="3", downstream_action_id="4"
                    ),
                ],
            ),
            WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
            None,
            id="With classical when disjointed actions",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="1",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="2",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="3",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="4",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="5",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="6",
                        kind=ActionKind.PUBLISHER,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
                actions_links=[
                    WorkflowActionLink(
                        upstream_action_id="1", downstream_action_id="3"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="2", downstream_action_id="4"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="3", downstream_action_id="5"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="5", downstream_action_id="6"
                    ),
                ],
            ),
            WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
            None,
            id="With complex when disjointed actions",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="1",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="2",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="3",
                        kind=ActionKind.PUBLISHER,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="4",
                        kind=ActionKind.SOURCE,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="5",
                        kind=ActionKind.PROCESSOR,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                    WorkflowAction(
                        id="6",
                        kind=ActionKind.PUBLISHER,
                        name="wfmname",
                        technical_name="wfmtname",
                        description="wfmdescr",
                        version="wfmver",
                        position_x=2,
                        position_y=2,
                        action_id="action_id",
                    ),
                ],
                actions_links=[
                    WorkflowActionLink(
                        upstream_action_id="1", downstream_action_id="2"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="2", downstream_action_id="3"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="4", downstream_action_id="5"
                    ),
                    WorkflowActionLink(
                        upstream_action_id="5", downstream_action_id="6"
                    ),
                ],
            ),
            WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
            None,
            id="With symetric disjointed workflow design",
        ),
    ],
)
def test_check_workflow_graph_connexity(
    workflow: Workflow,
    expected_added_error: WorkflowErrorCode,
    expected_removed_error: WorkflowErrorCode,
) -> None:
    """A connected graph is no problem even if invalid"""
    workflow.add_error = mock.MagicMock()
    workflow.remove_error = mock.MagicMock()
    workflow.check_graph_connexity()
    if expected_added_error is not None:
        workflow.add_error.assert_called_with(expected_added_error)
    elif expected_removed_error is not None:
        workflow.remove_error.assert_called_with(expected_removed_error)
    else:
        workflow.add_error.assert_not_called()
        workflow.remove_error.assert_not_called()


def test_get_all_ios() -> None:
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="1",
                        technical_name="technical_name",
                        display_name="display_name",
                        help="help",
                        type=ActionIOType.STRING,
                    )
                ],
                addons_inputs=[
                    WorkflowActionAddonInput(
                        optional=False,
                        id="2",
                        technical_name="addon1",
                        display_name="display_name",
                        help="help",
                        type=ActionIOType.STRING,
                    ),
                    WorkflowActionAddonInput(
                        optional=False,
                        id="3",
                        technical_name="addon_not_editable",
                        display_name="display_name",
                        help="help",
                        type=ActionIOType.STRING,
                        editable=False,
                    ),
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="2",
                        technical_name="technical_name",
                        display_name="display_name",
                        help="help",
                        type=ActionIOType.STRING,
                    )
                ],
                outputs=[
                    WorkflowActionOutput(
                        optional=False,
                        id="1",
                        technical_name="technical_name",
                        display_name="display_name",
                        help="help",
                        type=ActionIOType.STRING,
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
        ],
    )
    assert all(
        item
        in [
            WorkflowActionInput(
                optional=False,
                id="1",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
            ),
            WorkflowActionInput(
                optional=False,
                id="2",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
            ),
            WorkflowActionAddonInput(
                optional=False,
                id="2",
                technical_name="addon1",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
            ),
            WorkflowActionAddonInput(
                optional=False,
                id="3",
                technical_name="addon_not_editable",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
                editable=False,
            ),
            WorkflowActionOutput(
                optional=False,
                id="1",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
            ),
        ]
        for item in workflow.get_all_ios()
    )


def test_get_all_inputs() -> None:
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="1",
                        technical_name="technical_name",
                        display_name="display_name",
                        help="help",
                        type=ActionIOType.STRING,
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="2",
                        technical_name="technical_name",
                        display_name="display_name",
                        help="help",
                        type=ActionIOType.STRING,
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
        ],
    )
    assert workflow.get_all_inputs() == [
        WorkflowActionInput(
            optional=False,
            id="1",
            technical_name="technical_name",
            display_name="display_name",
            help="help",
            type=ActionIOType.STRING,
        ),
        WorkflowActionInput(
            optional=False,
            id="2",
            technical_name="technical_name",
            display_name="display_name",
            help="help",
            type=ActionIOType.STRING,
        ),
    ]


def test_get_all_errors() -> None:
    workflow_action_1_error = WorkflowError(
        id="workflow_action_error_1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
    )
    workflow_action_1 = WorkflowAction(
        errors=[workflow_action_1_error],
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_2_error = WorkflowError(
        id="workflow_action_error_2", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
    )
    workflow_action_2 = WorkflowAction(
        errors=[workflow_action_2_error],
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_error = WorkflowError(
        id="workflow_error", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
    )
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action_1, workflow_action_2],
        errors=[workflow_error],
    )
    assert workflow.get_all_errors() == [
        workflow_action_1_error,
        workflow_action_2_error,
        workflow_error,
    ]


@pytest.mark.parametrize(
    "workflow_copy_data",
    [
        pytest.param(
            CopyWorkflowData(
                from_workflow="workflow_id",
            ),
            id="Without info",
        ),
        pytest.param(
            CopyWorkflowData(
                from_workflow="workflow_id",
                name="Workflow copy name",
            ),
            id="With name",
        ),
        pytest.param(
            CopyWorkflowData(
                from_workflow="workflow_id",
                description="Workflow copy description",
            ),
            id="With description",
        ),
        pytest.param(
            CopyWorkflowData(
                from_workflow="workflow_id",
                name="Workflow copy name",
                description="Workflow copy description",
            ),
            id="With name and description",
        ),
    ],
)
@mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_workflow_id"))
def test_copy(workflow_copy_data: CopyWorkflowData) -> None:
    """Copy workflow data should return workflow"""
    workflow_action_input_bis = WorkflowActionInput(
        optional=False,
        id="workflow_action_input_bis",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="workflow_action_input",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_input.copy = mock.MagicMock(return_value=workflow_action_input_bis)

    workflow_action_output_bis = WorkflowActionOutput(
        optional=False,
        id="workflow_action_output_bis",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_output = WorkflowActionOutput(
        optional=False,
        id="workflow_action_output",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_output.copy = mock.MagicMock(
        return_value=workflow_action_output_bis
    )

    workflow_error_bis = WorkflowError(
        id="workflow_error_bis", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
    )
    workflow_error = WorkflowError(
        id="workflow_error", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
    )
    workflow_error.copy = mock.MagicMock(return_value=workflow_error_bis)

    workflow_action_bis = WorkflowAction(
        id="workflow_action_bis",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action = WorkflowAction(
        id="workflow_action",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.copy = mock.MagicMock(return_value=workflow_action_bis)

    workflow_action_link_bis = WorkflowActionLink(
        id="workflow_action_link_bis",
        upstream_action_id="workflow_action1",
        downstream_action_id="workflow_action2",
    )
    workflow_action_link = WorkflowActionLink(
        id="workflow_action_link",
        upstream_action_id="workflow_action1",
        downstream_action_id="workflow_action2",
    )
    workflow_action_link.copy = mock.MagicMock(return_value=workflow_action_link_bis)

    workflow = Workflow(
        id="workflow",
        name="workflow_name",
        description="workflow_description",
        owner="wfowner",
        project="wfproject",
        validity_status=WorkflowValidityStatus.INVALID,
        deployment_status=WorkflowDeploymentStatus.DEPLOYING,
        actions=[workflow_action],
        actions_links=[workflow_action_link],
        errors=[workflow_error],
    )
    workflow.sort_actions = mock.MagicMock()
    workflow.get_all_inputs = mock.MagicMock(return_value=[workflow_action_input])
    workflow.get_all_outputs = mock.MagicMock(return_value=[workflow_action_output])
    workflow.get_all_errors = mock.MagicMock(return_value=[workflow_error])

    workflow_owner = "workflow_owner"
    assert workflow.copy(workflow_copy_data, workflow_owner) == Workflow(
        id="new_workflow_id",
        name=workflow_copy_data.name if workflow_copy_data.name else workflow.name,
        description=workflow_copy_data.description
        if workflow_copy_data.description
        else workflow.description,
        owner=workflow_owner,
        project="wfproject",
        actions=[workflow_action_bis],
        actions_links=[workflow_action_link_bis],
        deployment_status=WorkflowDeploymentStatus.NONE,
        errors=[workflow_error_bis],
        validity_status=workflow.validity_status,
    )
    workflow_action_output.copy.assert_called()
    workflow_action_input.copy.assert_called()
    workflow_error.copy.assert_called()
    workflow_action.copy.assert_called()
    workflow_action_link.copy.assert_called()


def test_is_valid_when_empty() -> None:
    """An empty workflow should be valid"""
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    assert workflow.validity_status is WorkflowValidityStatus.VALID


def test_add_action_dynamic_output() -> None:
    workflow_action_id = "workflow_action_id"
    workflow_action = WorkflowAction(
        id=workflow_action_id,
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.add_dynamic_output = mock.MagicMock()
    data = AddWorkflowActionDynamicOutput(
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action],
    )
    workflow.add_action_dynamic_output(workflow_action_id, data)
    workflow_action.add_dynamic_output.assert_called_once_with(data, 0)


def test_add_action_dynamic_output_when_action_not_exists() -> None:
    workflow_action_id = "workflow_action_id"
    workflow_action = WorkflowAction(
        id="other_workflow_action_id",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    data = AddWorkflowActionDynamicOutput(
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action],
    )
    with pytest.raises(WorkflowActionNotFoundException):
        workflow.add_action_dynamic_output(workflow_action_id, data)


def test_update_action_dynamic_output() -> None:
    workflow_action_id = "workflow_action_id"
    workflow_action_output_id = "workflow_action_output_id"
    data = UpdateWorkflowActionDynamicOutput(
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action = WorkflowAction(
        id=workflow_action_id,
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.update_dynamic_output = mock.MagicMock()
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action],
    )
    workflow.update_action_dynamic_output(
        workflow_action_id, workflow_action_output_id, data
    )
    workflow_action.update_dynamic_output.assert_called_once_with(
        workflow_action_output_id, data
    )


def test_update_action_dynamic_output_when_action_not_exists() -> None:
    workflow_action_id = "workflow_action_id"
    workflow_action_output_id = "workflow_action_output_id"
    data = UpdateWorkflowActionDynamicOutput(
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action = WorkflowAction(
        id="other_workflow_action_id",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action],
    )
    with pytest.raises(WorkflowActionNotFoundException):
        workflow.update_action_dynamic_output(
            workflow_action_id, workflow_action_output_id, data
        )


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="workflow_error_id"))
@pytest.mark.parametrize(
    "workflow,input_code,expected_errors",
    [
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                errors=[
                    WorkflowError(
                        id="wfe1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
                    )
                ],
            ),
            WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
            [WorkflowError(id="wfe1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED)],
            id="When code exists",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                errors=[
                    WorkflowError(
                        id="wfe1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
                    )
                ],
            ),
            WorkflowErrorCode.WORKFLOW_HAS_CYCLES,
            [
                WorkflowError(id="wfe1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED),
                WorkflowError(
                    id="workflow_error_id", code=WorkflowErrorCode.WORKFLOW_HAS_CYCLES
                ),
            ],
            id="When code not exists",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
            ),
            WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
            [
                WorkflowError(
                    id="workflow_error_id",
                    code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
                )
            ],
            id="When no code",
        ),
    ],
)
def test_add_error(
    workflow: Workflow,
    input_code: WorkflowErrorCode,
    expected_errors: List[WorkflowError],
) -> None:
    """Workflow.add_error should add an error to the action"""
    workflow.add_error(input_code)
    assert workflow.errors == expected_errors


@pytest.mark.parametrize(
    "workflow,input_code,expected_errors",
    [
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                errors=[
                    WorkflowError(
                        id="wfe1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
                    )
                ],
            ),
            WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
            [],
            id="When code exists",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
                errors=[
                    WorkflowError(
                        id="wfe1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
                    )
                ],
            ),
            WorkflowErrorCode.WORKFLOW_HAS_CYCLES,
            [WorkflowError(id="wfe1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED)],
            id="When code not exists",
        ),
        pytest.param(
            Workflow(
                name="wfname",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
            ),
            WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
            [],
            id="When no code",
        ),
    ],
)
def test_remove_error(
    workflow: Workflow,
    input_code: WorkflowErrorCode,
    expected_errors: List[WorkflowError],
) -> None:
    """Workflow.add_error should add an error to the action"""
    workflow.remove_error(input_code)
    assert workflow.errors == expected_errors


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_link_id"))
def test_add_action_link() -> None:
    """Add action link to workflow"""
    input_action_id = "workflow_action_1_id"
    input_action = WorkflowAction(
        id=input_action_id,
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    output_action_id = "workflow_action_2_id"
    output_action = WorkflowAction(
        id=output_action_id,
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    workflow.get_action = mock.MagicMock(side_effect=[input_action, output_action])
    workflow.add_action_link(input_action_id, output_action_id)
    assert workflow.actions_links == [
        WorkflowActionLink(
            id="new_link_id",
            upstream_action_id=input_action.id,
            downstream_action_id=output_action.id,
        )
    ]
    workflow.get_action.assert_has_calls(
        [mock.call(input_action_id), mock.call(output_action_id)]
    )


def test_add_action_link_when_input_action_not_found() -> None:
    input_action_id = "workflow_action_1_id"
    output_action_id = "workflow_action_2_id"
    output_action = WorkflowAction(
        id=output_action_id,
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    workflow.get_action = mock.MagicMock(
        side_effect=[WorkflowActionNotFoundException(), output_action]
    )
    with pytest.raises(WorkflowActionNotFoundException):
        workflow.add_action_link(input_action_id, output_action_id)
        workflow.get_action.assert_has_calls(
            [
                mock.call(input_action_id),
            ]
        )


def test_add_action_link_when_output_action_not_found() -> None:
    input_action_id = "workflow_action_1_id"
    input_action = WorkflowAction(
        id=input_action_id,
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    output_action_id = "workflow_action_2_id"
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    workflow.get_action = mock.MagicMock(
        side_effect=[input_action, WorkflowActionNotFoundException()]
    )
    with pytest.raises(WorkflowActionNotFoundException):
        workflow.add_action_link(input_action_id, output_action_id)
        workflow.get_action.assert_has_calls(
            [
                mock.call(input_action_id),
                mock.call(output_action_id),
            ]
        )


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="static_link_id"))
def test_patch_all_links() -> None:
    workflow_action_1 = WorkflowAction(
        id="workflow_action_1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_2 = WorkflowAction(
        id="workflow_action_2",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_3 = WorkflowAction(
        id="workflow_action_3",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_4 = WorkflowAction(
        id="workflow_action_4",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions_links=[
            WorkflowActionLink.create_from_workflow_actions(
                upstream_action=workflow_action_1, downstream_action=workflow_action_2
            ),
            WorkflowActionLink.create_from_workflow_actions(
                upstream_action=workflow_action_2, downstream_action=workflow_action_3
            ),
        ],
    )

    workflow.transfer_workflow_action_links(workflow_action_2, workflow_action_4)
    assert workflow.actions_links == [
        WorkflowActionLink.create_from_workflow_actions(
            upstream_action=workflow_action_1, downstream_action=workflow_action_4
        ),
        WorkflowActionLink.create_from_workflow_actions(
            upstream_action=workflow_action_4, downstream_action=workflow_action_3
        ),
    ]


def test_update_action() -> None:
    """Add action should add new action in workflow from existing action"""
    workflow_action = WorkflowAction(
        id="workflow_action_id",
        custom_name="Custom action name",
        position_x=1,
        position_y=1,
        action_id="action_id_ref",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
    )
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action],
    )
    data = UpdateWorkflowAction(
        custom_name="Different custom name", position_y=0, position_x=2
    )
    workflow.update_action("workflow_action_id", data)
    assert workflow.actions == [
        WorkflowAction(
            id="workflow_action_id",
            custom_name=data.custom_name,
            position_x=data.position_x,
            position_y=data.position_y,
            action_id="action_id_ref",
            name="wfmname",
            technical_name="wfmtname",
            description="wfmdescr",
            version="wfmver",
            kind=ActionKind.PROCESSOR,
        )
    ]


def test_delete_action() -> None:
    workflow_action_id = "workflow_action_id"
    workflow_action = WorkflowAction(
        id="workflow_action_id",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_link_1 = mock.MagicMock(
        WorkflowActionLink, id="workflow_action_link_1"
    )
    workflow_action_link_1.has_action = mock.MagicMock(return_value=False)
    workflow_action_link_2 = mock.MagicMock(
        WorkflowActionLink, id="workflow_action_link_2"
    )
    workflow_action_link_2.has_action = mock.MagicMock(return_value=True)

    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action],
        actions_links=[workflow_action_link_1, workflow_action_link_2],
    )
    workflow.get_action = mock.MagicMock(return_value=workflow_action)
    workflow.get_links_to_workflow_action = mock.MagicMock(return_value=[])
    workflow.get_links_from_workflow_action = mock.MagicMock(return_value=[])

    workflow.delete_action(workflow_action_id)
    assert workflow.actions == []
    assert workflow.actions_links == [workflow_action_link_1]
    workflow.get_action.assert_called_with(workflow_action_id)
    workflow_action_link_1.has_action.assert_called_with(workflow_action.id)
    workflow_action_link_2.has_action.assert_called_with(workflow_action.id)


def test_delete_action_when_not_exists() -> None:
    """Delete action should remove action from workflow"""
    workflow_action_id = "workflow_action_id"
    workflow_action = WorkflowAction(
        id="workflow_action",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_link = mock.MagicMock(WorkflowActionLink, id="workflow_action_link")
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action],
        actions_links=[workflow_action_link],
    )
    workflow.get_action = mock.MagicMock(side_effect=WorkflowActionNotFoundException())
    with pytest.raises(WorkflowActionNotFoundException):
        workflow.delete_action(workflow_action_id)


def test_delete_dynamic_output_when_no_workflow_action() -> None:
    """Delete workflow action dynamic output should return error when workflow action is not found"""
    workflow = Workflow(
        id="workflow_id",
        name="workflow_name",
        description="workflow_description",
        owner="wfowner",
        project="wfproject",
        actions=[],
    )
    with pytest.raises(WorkflowActionNotFoundException) as err:
        workflow.delete_action_dynamic_output(
            "workflow_action_id", "workflow_action_output_id"
        )
    assert err.value == WorkflowActionNotFoundException(
        "Action not found in the workflow"
    )


def test_delete_dynamic_output() -> None:
    """Delete workflow action dynamic output"""
    workflow_action_id = "workflow_action_id"
    workflow_action_output_id = "workflow_action_output_id"
    workflow_action = WorkflowAction(
        id=workflow_action_id,
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.delete_dynamic_output = mock.MagicMock()
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action],
    )
    workflow.delete_action_dynamic_output(workflow_action_id, workflow_action_output_id)
    workflow_action.delete_dynamic_output.assert_called_once_with(
        workflow_action_output_id
    )


def test_delete_dynamic_output_when_action_not_exists() -> None:
    """Delete workflow action dynamic output"""
    workflow_action_id = "workflow_action_id"
    workflow_action_output_id = "workflow_action_output_id"
    workflow_action = WorkflowAction(
        id="other_workflow_action_id",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.delete_dynamic_output = mock.MagicMock()
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action],
    )
    with pytest.raises(WorkflowActionNotFoundException):
        workflow.delete_action_dynamic_output(
            workflow_action_id, workflow_action_output_id
        )


def test_replace_workflow_action_reference_value() -> None:
    """The reference input should be replaced by another one"""
    output_1 = WorkflowActionOutput(
        optional=False,
        id="output_1",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    output_2 = WorkflowActionOutput(
        optional=False,
        id="output_2",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_1 = WorkflowAction(
        id="workflow_action_1",
        outputs=[output_1],
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_2 = WorkflowAction(
        id="workflow_action_2",
        inputs=[
            WorkflowActionInput(
                optional=False,
                id="ref-input",
                reference_value=output_1,
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
            )
        ],
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_3 = WorkflowAction(
        id="workflow_action_3",
        outputs=[output_2],
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action_1, workflow_action_2, workflow_action_3],
    )
    workflow.transfer_actions_references(workflow_action_1, workflow_action_3)
    assert workflow == Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[
            workflow_action_1,
            WorkflowAction(
                id="workflow_action_2",
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="ref-input",
                        reference_value=output_2,
                        technical_name="technical_name",
                        display_name="display_name",
                        help="help",
                        type=ActionIOType.STRING,
                    )
                ],
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            workflow_action_3,
        ],
    )


def test_delete_link_action() -> None:
    """Delete action link should remove link from workflow action_links"""
    workflow_action_link_1 = WorkflowActionLink(
        id="ebcc54f3-ed5d-4d18-968a-06602bcc80e2",
        upstream_action_id="",
        downstream_action_id="",
    )
    workflow_action_link_2 = WorkflowActionLink(
        id="d5ca5593-6ba8-4c2f-bf7b-a5cf40baac22",
        upstream_action_id="",
        downstream_action_id="",
    )
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions_links=[workflow_action_link_1, workflow_action_link_2],
    )
    workflow.delete_action_link(workflow_action_link_2.id)
    assert workflow.actions_links == [workflow_action_link_1]


def test_delete_action_link_when_not_exists() -> None:
    """Delete action link should raise error when link not found"""
    workflow_action_link_1 = WorkflowActionLink(
        id="ebcc54f3-ed5d-4d18-968a-06602bcc80e2",
        upstream_action_id="",
        downstream_action_id="",
    )
    workflow_action_link_2 = WorkflowActionLink(
        id="d5ca5593-6ba8-4c2f-bf7b-a5cf40baac22",
        upstream_action_id="",
        downstream_action_id="",
    )
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions_links=[workflow_action_link_2],
    )
    with pytest.raises(WorkflowActionLinkNotFoundException):
        workflow.delete_action_link(workflow_action_link_1.id)
    assert workflow.actions_links == [workflow_action_link_2]


def test_update_action_input_with_static_value() -> None:
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_action_input_id"
    data = UpdateWorkflowActionInputValue(static_value="static_value")
    workflow_action = mock.MagicMock(WorkflowAction)
    workflow_action.id = workflow_action_id
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action],
    )
    workflow.update_action_input(workflow_action.id, workflow_action_input_id, data)
    workflow_action.update_input_static_value.assert_called_with(
        workflow_action_input_id, data.static_value
    )


def test_update_action_input_when_reset_values() -> None:
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_action_input_id"
    data = UpdateWorkflowActionInputValue(static_value=None, reference_value=None)
    workflow_action = mock.MagicMock(WorkflowAction)
    workflow_action.id = workflow_action_id
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action],
    )
    workflow.update_action_input(workflow_action_id, workflow_action_input_id, data)
    workflow_action.reset_input_value.assert_called_once_with(workflow_action_input_id)


def test_update_action_input_with_reference_value() -> None:
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_action_input_id"
    data = UpdateWorkflowActionInputValue(reference_value="workflow_action_output")
    workflow_action = mock.MagicMock(WorkflowAction)
    workflow_action.id = workflow_action_id
    workflow_action.has_output.return_value = False
    workflow_action.get_output.return_value = None
    other_workflow_action = mock.MagicMock(WorkflowAction)
    other_workflow_action_output = mock.MagicMock(WorkflowActionOutput)
    other_workflow_action.has_output.return_value = True
    other_workflow_action.get_output.return_value = other_workflow_action_output
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action, other_workflow_action],
    )
    workflow.update_action_input(workflow_action_id, workflow_action_input_id, data)
    workflow_action.update_input_reference_value.assert_called_with(
        workflow_action_input_id, other_workflow_action_output
    )


def test_update_action_input_when_action_not_exists() -> None:
    data = UpdateWorkflowActionInputValue()
    workflow_action_input_id = "workflow_action_input_id"
    workflow_action_id = "workflow_action_input_id"
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    with pytest.raises(WorkflowActionNotFoundException):
        workflow.update_action_input(workflow_action_id, workflow_action_input_id, data)


def test_update_action_input_with_reference_value_when_output_not_exists() -> None:
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_action_input_id"
    data = UpdateWorkflowActionInputValue(reference_value="workflow_action_output")
    workflow_action = mock.MagicMock(WorkflowAction)
    workflow_action.id = workflow_action_id
    workflow_action.has_output.return_value = False
    workflow_action.get_output.side_effect = WorkflowActionOutputNotFoundException()
    other_workflow_action = mock.MagicMock(WorkflowAction)
    other_workflow_action.has_output.return_value = False
    other_workflow_action.get_output.side_effect = (
        WorkflowActionOutputNotFoundException()
    )
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action, other_workflow_action],
    )
    with pytest.raises(WorkflowActionOutputNotFoundException):
        workflow.update_action_input(workflow_action_id, workflow_action_input_id, data)


def test_delete_input_file() -> None:
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_action_input_id"
    workflow_action = WorkflowAction(
        id="workflow_action_id",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    workflow.get_action = mock.MagicMock(return_value=workflow_action)
    workflow_action.delete_input_file = mock.MagicMock()
    workflow.delete_input_file(workflow_action_id, workflow_action_input_id)
    workflow.get_action.assert_called_once_with(workflow_action_id)
    workflow_action.delete_input_file.assert_called_once_with(workflow_action_input_id)


def test_get_workflow_action_streams_to() -> None:
    workflow_action_link_1 = mock.MagicMock(WorkflowActionLink)
    workflow_action_link_1.has_input_action = mock.MagicMock(return_value=True)
    workflow_action_link_1.get_output_action_id = mock.MagicMock(
        return_value="output_action_id"
    )
    workflow_action_link_2 = mock.MagicMock(WorkflowActionLink)
    workflow_action_link_2.has_input_action = mock.MagicMock(return_value=False)

    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[
            WorkflowAction(
                id="output_action_id",
                technical_name="test-1",
                name="wfmname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            )
        ],
        actions_links=[
            workflow_action_link_1,
            workflow_action_link_2,
        ],
    )
    assert workflow.get_workflow_action_streams_to("workflow_action_1") == [
        "output_action_id",
    ]


def test_deploy() -> None:
    """Test deploying a workflow with many actions and different inputs and outputs"""
    workflow_action_1 = WorkflowAction(
        id="workflow_action_source_id",
        version="1.0",
        kind=ActionKind.SOURCE,
        technical_name="techname1",
        name="myname",
        description="descr",
        has_dynamic_outputs=False,
        action_id="action_id_1",
        position_x=1,
        position_y=1,
        endpoint="endpoint",
        inputs=[
            WorkflowActionInput(
                optional=False,
                id="input_1_id",
                technical_name="input_1_technical_name",
                display_name="input_1_display_name",
                help="input_1_help",
                type=ActionIOType.INTEGER,
                enum_values=[],
            )
        ],
        outputs=[
            WorkflowActionOutput(
                optional=False,
                id="output_1_id",
                technical_name="output_1_technical_name",
                display_name="output_1_display_name",
                help="output_1_help",
                type=ActionIOType.STRING,
                enum_values=[],
            )
        ],
        dynamic_outputs=[
            WorkflowActionOutput(
                optional=False,
                id="dyn_output_1_id",
                technical_name="dyn_output_1_technical_name",
                display_name="dyn_output_1_display_name",
                help="output_1_help",
                type=ActionIOType.STRING,
                enum_values=[],
            )
        ],
        addons_inputs=[
            WorkflowActionAddonInput(
                optional=False,
                id="input_addon_1_id",
                technical_name="input_addon_1_technical_name",
                display_name="input_addon_1_display_name",
                help="input_addon_1_help",
                type=ActionIOType.INTEGER,
                enum_values=[],
                addon_name="123",
            )
        ],
        addons={},
        resources=WorkflowActionResources(id="1"),
    )
    workflow_action_1.deploy = mock.MagicMock()

    workflow_action_2 = WorkflowAction(
        id="workflow_action_processor_id",
        version="2.0",
        kind=ActionKind.PROCESSOR,
        technical_name="techname2",
        name="myname",
        description="descr",
        has_dynamic_outputs=True,
        position_x=1,
        position_y=1,
        endpoint=None,
        action_id="action_id_2",
        inputs=[
            WorkflowActionInput(
                optional=False,
                id="input_2_id",
                technical_name="input_2_technical_name",
                display_name="input_2_display_name",
                help="input_2_help",
                type=ActionIOType.STRING,
                enum_values=[],
            ),
            WorkflowActionInput(
                optional=False,
                id="input_3_id",
                technical_name="input_3_technical_name",
                display_name="input_3_display_name",
                help="input_3_help",
                type=ActionIOType.ENUM,
                enum_values=["value1"],
            ),
        ],
        outputs=[
            WorkflowActionOutput(
                optional=False,
                id="output_2_id",
                display_name="disp_name",
                help="help",
                type=ActionIOType.STRING,
                technical_name="output_2_technical_name",
            )
        ],
        addons={},
        resources=WorkflowActionResources(id="1"),
    )
    workflow_action_2.deploy = mock.MagicMock()
    results = [
        WorkflowResult(
            id="foo",
            key="bar",
            workflow_id="baz",
            workflow_action_input_id=None,
            workflow_action_output_id="output_1_id",
        )
    ]
    workflow = Workflow(
        id="workflow_id",
        project="project_id",
        name="myname",
        description="wfdescr",
        owner="wfowner",
        actions=[
            workflow_action_1,
            workflow_action_2,
        ],
        results=results,
    )
    workflow.get_workflow_action_streams_to = mock.MagicMock(
        side_effect=[["workflow_action_processor_id"], []]
    )
    workflow.deploy()
    assert workflow.events == [
        WorkflowDeployEvent(
            workflow_id=workflow.id,
            workflow_project_id="project_id",
            workflow_name="myname",
            workflow_actions=[
                WorkflowDeployEvent.WorkflowAction(
                    id="workflow_action_source_id",
                    action_id="action_id_1",
                    technical_name="techname1",
                    human_name="myname",
                    description="descr",
                    action_version="1.0",
                    action_kind=ActionKind.SOURCE,
                    has_dynamic_outputs=False,
                    action_inputs=[
                        WorkflowDeployEvent.WorkflowActionIO(
                            technical_name="input_1_technical_name",
                            display_name="input_1_display_name",
                            help="input_1_help",
                            type=ActionIOType.INTEGER,
                            enum_values=[],
                            optional=False,
                        )
                    ],
                    action_addons_inputs=[
                        WorkflowDeployEvent.WorkflowActionIO(
                            technical_name="input_addon_1_technical_name",
                            display_name="input_addon_1_display_name",
                            help="input_addon_1_help",
                            type=ActionIOType.INTEGER,
                            enum_values=[],
                            addon_name="123",
                            optional=False,
                        )
                    ],
                    action_outputs=[
                        WorkflowDeployEvent.WorkflowActionIO(
                            technical_name="output_1_technical_name",
                            display_name="output_1_display_name",
                            help="output_1_help",
                            type=ActionIOType.STRING,
                            enum_values=[],
                            optional=False,
                        )
                    ],
                    dynamic_outputs=[
                        WorkflowDeployEvent.WorkflowActionIO(
                            technical_name="dyn_output_1_technical_name",
                            display_name="dyn_output_1_display_name",
                            help="output_1_help",
                            type=ActionIOType.STRING,
                            enum_values=[],
                            optional=False,
                        )
                    ],
                    static_inputs_values={},
                    reference_inputs_values={},
                    file_inputs_values={},
                    streams_to=["workflow_action_processor_id"],
                    endpoint="endpoint",
                    addons={},
                    resources=WorkflowDeployEvent.WorkflowActionResources(),
                    objectives=WorkflowActionObjectives(),
                    constraints=WorkflowActionConstraints(),
                ),
                WorkflowDeployEvent.WorkflowAction(
                    id="workflow_action_processor_id",
                    action_id="action_id_2",
                    action_version="2.0",
                    technical_name="techname2",
                    human_name="myname",
                    description="descr",
                    action_kind=ActionKind.PROCESSOR,
                    has_dynamic_outputs=True,
                    action_inputs=[
                        WorkflowDeployEvent.WorkflowActionIO(
                            technical_name="input_2_technical_name",
                            display_name="input_2_display_name",
                            help="input_2_help",
                            type=ActionIOType.STRING,
                            enum_values=[],
                            optional=False,
                        ),
                        WorkflowDeployEvent.WorkflowActionIO(
                            technical_name="input_3_technical_name",
                            display_name="input_3_display_name",
                            help="input_3_help",
                            type=ActionIOType.ENUM,
                            enum_values=["value1"],
                            optional=False,
                        ),
                    ],
                    action_addons_inputs=[],
                    static_inputs_values={},
                    reference_inputs_values={},
                    file_inputs_values={},
                    action_outputs=[
                        WorkflowDeployEvent.WorkflowActionIO(
                            technical_name="output_2_technical_name",
                            display_name="disp_name",
                            help="help",
                            type=ActionIOType.STRING,
                            enum_values=[],
                            optional=False,
                        )
                    ],
                    dynamic_outputs=[],
                    streams_to=[],
                    endpoint=None,
                    addons={},
                    resources=WorkflowDeployEvent.WorkflowActionResources(),
                    objectives=WorkflowActionObjectives(),
                    constraints=WorkflowActionConstraints(),
                ),
            ],
            results=[
                WorkflowDeployEvent.WorkflowResult(
                    id="foo",
                    key="bar",
                    workflow_action_io_id="output_1_id",
                    technical_name="output_1_technical_name",
                    action_id="workflow_action_source_id",
                )
            ],
        ),
    ]
    assert workflow.deployment_status == WorkflowDeploymentStatus.DEPLOYING


def test_deploy_workflow_success() -> None:
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
    )
    workflow.deploy_workflow_success()
    assert workflow.deployment_status == WorkflowDeploymentStatus.DEPLOYED


def test_deploy_invalid_workflow() -> None:
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        validity_status=WorkflowValidityStatus.INVALID,
    )
    with pytest.raises(WorkflowNotValidException):
        workflow.deploy()


def test_deploy_not_deployable_workflow() -> None:
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        deployment_status=WorkflowDeploymentStatus.DEPLOYED,
    )
    with pytest.raises(WorkflowNotDeployable):
        workflow.deploy()


def test_undeploy() -> None:
    workflow_action_1 = WorkflowAction(
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_2 = WorkflowAction(
        id="wfm2",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow = Workflow(
        id="workflow_id",
        project="project1",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        deployment_status=WorkflowDeploymentStatus.DEPLOYED,
        actions=[workflow_action_1, workflow_action_2],
    )
    workflow.undeploy(graceful=False)
    assert workflow.events == [
        WorkflowUndeployEvent(
            workflow_id="workflow_id", workflow_project_id="project1", graceful=False
        )
    ]


def test_undeploy_success() -> None:
    workflow_action_1 = WorkflowAction(
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_1.undeployed = mock.MagicMock()
    workflow_action_2 = WorkflowAction(
        id="wfm2",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_2.undeployed = mock.MagicMock()
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        deployment_status=WorkflowDeploymentStatus.DEPLOYED,
        actions=[workflow_action_1, workflow_action_2],
    )
    workflow.undeploy_success()
    assert workflow.deployment_status == WorkflowDeploymentStatus.NONE


def test_add_input_file() -> None:
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_action_input_id"
    filename = "filename"
    extension = ".csv"
    path = "path"
    size = 1.0
    workflow_action = WorkflowAction(
        id="workflow_action_id",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    workflow.get_action = mock.MagicMock(return_value=workflow_action)
    workflow_action.add_input_file = mock.MagicMock()

    workflow.add_input_file(
        workflow_action_id, workflow_action_input_id, filename, extension, path, size
    )
    workflow.get_action.assert_called_once_with(workflow_action_id)
    workflow_action.add_input_file.assert_called_once_with(
        workflow_action_input_id, filename, extension, path, size
    )


def test_add_input_file_when_action_not_exists() -> None:
    workflow_action_id = "workflow_action_id"
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    with pytest.raises(WorkflowActionNotFoundException):
        assert workflow.add_input_file(
            workflow_action_id, "input_id", "name", "extension", "path", 1.0
        )


def test_add_input_file_when_workflow_not_updatable() -> None:
    workflow_action_id = "workflow_action_id"
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    workflow.deployment_status = WorkflowDeploymentStatus.DEPLOYED

    with pytest.raises(WorkflowNotUpdatableException):
        assert workflow.add_input_file(
            workflow_action_id, "input_id", "name", "extension", "path", 1.0
        )


def test_get_action_input_file_path() -> None:
    path = "/path/test"
    workflow_action_id = "workflow_action_id"
    workflow_input_id = "workflow_input_id"
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    action = WorkflowAction(
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_input = WorkflowActionInput(
        optional=False,
        file_value=WorkflowFile(
            path=path, id="123", name="123", extension="jpg", size=42.3
        ),
        id="id",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    action.get_input = mock.MagicMock(return_value=workflow_input)
    workflow.get_action = mock.MagicMock(return_value=action)
    assert (
        workflow.get_action_input_file_path(workflow_action_id, workflow_input_id)
        == path
    )
    workflow.get_action.assert_called_once_with(workflow_action_id)
    action.get_input.assert_called_once_with(workflow_input_id)


def test_get_action_static_files() -> None:
    workflow_action_id = "workflow_action_id"
    workflow_action = WorkflowAction(
        id=workflow_action_id,
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    static_files = [
        WorkflowFile(id="file1", name="123", extension="jpg", path="a", size=42.3),
        WorkflowFile(id="file2", name="123", extension="jpg", path="a", size=42.3),
    ]
    workflow_action.get_static_files = mock.MagicMock(return_value=static_files)
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action],
    )
    assert workflow.get_action_static_files(workflow_action_id) == static_files


@mock.patch(
    "ryax.studio.domain.workflow.entities.workflow_action.WorkflowAction.get_static_files",
)
def test_get_all_static_files(get_static_files_mock) -> None:
    workflow_input_1 = WorkflowActionInput(
        optional=False,
        id="input1",
        file_value=WorkflowFile(id="1", name="1", extension="jpg", path="a", size=42.3),
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_input_2 = WorkflowActionInput(
        optional=False,
        id="input2",
        file_value=WorkflowFile(id="2", name="2", extension="jpg", path="a", size=42.3),
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_input_3 = WorkflowActionInput(
        optional=False,
        id="input4",
        file_value=WorkflowFile(id="3", name="3", extension="jpg", path="a", size=42.3),
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action = WorkflowAction(
        inputs=[workflow_input_1, workflow_input_2],
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_2 = WorkflowAction(
        inputs=[workflow_input_3],
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    get_static_files_mock.side_effect = [
        WorkflowFile(id="1", name="1", extension="jpg", path="a", size=42.3),
        WorkflowFile(id="2", name="2", extension="jpg", path="a", size=42.3),
    ], [WorkflowFile(id="3", name="3", extension="jpg", path="a", size=42.3)]
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action, workflow_action_2],
    )
    assert workflow.get_all_static_files() == [
        WorkflowFile(id="1", name="1", extension="jpg", path="a", size=42.3),
        WorkflowFile(id="2", name="2", extension="jpg", path="a", size=42.3),
        WorkflowFile(id="3", name="3", extension="jpg", path="a", size=42.3),
    ]


@pytest.mark.parametrize(
    "has_static_value_input1,has_static_value_input2",
    [
        (True, True),
        (False, False),
        (True, False),
        (False, True),
    ],
)
def test_get_inputs_static_values_map(
    has_static_value_input1: bool, has_static_value_input2: bool
) -> None:
    """Generate two inputs and simulate the combinations of them having static values or not.
    Make sure the map returned is correct"""
    wf_mod_input1 = WorkflowActionInput(
        optional=False,
        technical_name="input1",
        static_value="foo",
        id="id",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    wf_mod_input2 = WorkflowActionInput(
        optional=False,
        technical_name="input2",
        static_value="bar",
        id="id",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    wf_mod_input1.has_static_value = mock.MagicMock(
        return_value=has_static_value_input1
    )
    wf_mod_input2.has_static_value = mock.MagicMock(
        return_value=has_static_value_input2
    )
    workflow_action = WorkflowAction(
        inputs=[wf_mod_input1, wf_mod_input2],
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    static_values = workflow.get_inputs_static_values_map(workflow_action)
    if has_static_value_input1:
        assert static_values[wf_mod_input1.technical_name] == wf_mod_input1.static_value
    if has_static_value_input2:
        assert static_values[wf_mod_input2.technical_name] == wf_mod_input2.static_value
    if not (has_static_value_input1 or has_static_value_input2):
        assert static_values == {}


@pytest.mark.parametrize(
    "has_reference_value_input1,has_reference_value_input2",
    [
        (True, True),
        (False, False),
        (True, False),
        (False, True),
    ],
)
def test_get_inputs_reference_values_map(
    has_reference_value_input1: bool, has_reference_value_input2: bool
) -> None:
    reference_value_1 = WorkflowActionOutput(
        optional=False,
        id="qux",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    reference_value_2 = WorkflowActionOutput(
        optional=False,
        id="fred",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    wf_mod_input1 = WorkflowActionInput(
        optional=False,
        technical_name="input1",
        reference_value=reference_value_1,
        id="id",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    wf_mod_input2 = WorkflowActionInput(
        optional=False,
        technical_name="input2",
        reference_value=reference_value_2,
        id="id",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    wf_mod_input1.has_reference_value = mock.MagicMock(
        return_value=has_reference_value_input1
    )
    wf_mod_input2.has_reference_value = mock.MagicMock(
        return_value=has_reference_value_input2
    )
    workflow_action = WorkflowAction(
        inputs=[wf_mod_input1, wf_mod_input2],
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    outputs_map = {
        "qux": WorkflowActionOutput(
            optional=False,
            technical_name="corge",
            id="id",
            display_name="display_name",
            help="help",
            type=ActionIOType.STRING,
        ),
        "fred": WorkflowActionOutput(
            optional=False,
            technical_name="xyzzy",
            id="id",
            display_name="display_name",
            help="help",
            type=ActionIOType.STRING,
        ),
    }
    outputs_parents_map = {
        "qux": WorkflowAction(
            technical_name="corge2",
            id="wfm1",
            name="wfmname1",
            description="wfmdescr",
            version="wfmver",
            kind=ActionKind.PROCESSOR,
            position_x=2,
            position_y=2,
            action_id="action_id",
        ),
        "fred": WorkflowAction(
            technical_name="xyzzy2",
            id="wfm2",
            name="wfmname2",
            description="wfmdescr",
            version="wfmver",
            kind=ActionKind.PROCESSOR,
            position_x=2,
            position_y=2,
            action_id="action_id",
        ),
    }

    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    workflow.get_action_outputs_maps = mock.MagicMock()
    workflow.get_action_outputs_maps.return_value = (outputs_map, outputs_parents_map)

    reference_values = workflow.get_inputs_reference_values_map(workflow_action)
    if has_reference_value_input1:
        assert reference_values[wf_mod_input1.technical_name] == {
            "workflow_action_id": "wfm1",
            "output_technical_name": "corge",
        }
    if has_reference_value_input2:
        assert reference_values[wf_mod_input2.technical_name] == {
            "workflow_action_id": "wfm2",
            "output_technical_name": "xyzzy",
        }
    if not (has_reference_value_input1 or has_reference_value_input2):
        assert reference_values == {}


def test_sort_actions():
    action = Action(
        technical_name="foo",
        inputs=[],
        outputs=[],
        categories=[],
        id="action1",
        project="project_id",
        name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.fromtimestamp(0),
    )
    data = AddWorkflowAction(
        action_id=action.id,
        custom_name="action custom name",
        position_x=0,
        position_y=1,
    )

    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    addons = AddOns()
    wfm1 = workflow.add_action(action, data, addons)
    wfm3 = workflow.add_action(action, data, addons)
    wfm2 = workflow.add_action(action, data, addons)
    wfm4 = workflow.add_action(action, data, addons)

    workflow.add_action_link(wfm1.id, wfm2.id)
    workflow.add_action_link(wfm2.id, wfm3.id)
    workflow.add_action_link(wfm3.id, wfm4.id)

    assert [action.id for action in workflow.get_actions()] == [
        wfm1.id,
        wfm2.id,
        wfm3.id,
        wfm4.id,
    ]

    workflow.delete_action(wfm4.id)
    assert [action.id for action in workflow.get_actions()] == [
        wfm1.id,
        wfm2.id,
        wfm3.id,
    ]

    workflow.delete_action(wfm1.id)
    assert [action.id for action in workflow.get_actions()] == [wfm2.id, wfm3.id]

    wfm1 = workflow.add_action(action, data, addons)
    workflow.add_action_link(wfm1.id, wfm2.id)
    wfm4 = workflow.add_action(action, data, addons)

    workflow.transfer_workflow_action_links(wfm2, wfm4)
    workflow.delete_action(wfm2.id)
    assert [action.id for action in workflow.get_actions()] == [
        wfm1.id,
        wfm4.id,
        wfm3.id,
    ]


def test_sort_actions_two_children():
    action = Action(
        technical_name="foo",
        inputs=[],
        outputs=[],
        categories=[],
        id="action1",
        project="project_id",
        name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.fromtimestamp(0),
    )
    data = AddWorkflowAction(
        action_id=action.id,
        custom_name="action custom name",
        position_x=0,
        position_y=1,
    )

    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    addons = AddOns()
    wfm1 = workflow.add_action(action, data, addons)
    wfm3 = workflow.add_action(action, data, addons)
    wfm2 = workflow.add_action(action, data, addons)
    wfm4 = workflow.add_action(action, data, addons)

    workflow.add_action_link(wfm1.id, wfm2.id)
    workflow.add_action_link(wfm2.id, wfm3.id)
    workflow.add_action_link(wfm2.id, wfm4.id)

    assert [action.id for action in workflow.get_actions()][:2] == [
        wfm1.id,
        wfm2.id,
    ]


def test_get_trigger_action():
    trigger = WorkflowAction(
        id="workflow_action_1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.SOURCE,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )

    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[trigger],
    )

    assert workflow.get_trigger_action() == trigger


def test_get_trigger_action_when_has_none():
    processor = WorkflowAction(
        id="workflow_action_1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )

    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[processor],
    )

    with pytest.raises(WorkflowHasNoTriggerException):
        workflow.get_trigger_action()


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="mocked_link_id"))
def test_get_links_from_workflow_action():
    workflow_action_1 = WorkflowAction(
        id="workflow_action_1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_2 = WorkflowAction(
        id="workflow_action_2",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_3 = WorkflowAction(
        id="workflow_action_3",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_4 = WorkflowAction(
        id="workflow_action_4",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions_links=[
            WorkflowActionLink.create_from_workflow_actions(
                upstream_action=workflow_action_1, downstream_action=workflow_action_2
            ),
            WorkflowActionLink.create_from_workflow_actions(
                upstream_action=workflow_action_2, downstream_action=workflow_action_3
            ),
            WorkflowActionLink.create_from_workflow_actions(
                upstream_action=workflow_action_2, downstream_action=workflow_action_4
            ),
        ],
    )
    result_links = workflow.get_links_from_workflow_action(workflow_action_2.id)
    assert len(result_links) == 2
    assert all(
        link in result_links
        for link in [
            WorkflowActionLink(
                id="mocked_link_id",
                upstream_action_id=workflow_action_2.id,
                downstream_action_id=workflow_action_3.id,
            ),
            WorkflowActionLink(
                id="mocked_link_id",
                upstream_action_id=workflow_action_2.id,
                downstream_action_id=workflow_action_4.id,
            ),
        ]
    )


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="mocked_link_id"))
def test_get_links_to_workflow_action():
    workflow_action_1 = WorkflowAction(
        id="workflow_action_1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_2 = WorkflowAction(
        id="workflow_action_2",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions_links=[
            WorkflowActionLink.create_from_workflow_actions(
                upstream_action=workflow_action_1, downstream_action=workflow_action_2
            ),
        ],
    )
    result_links = workflow.get_links_to_workflow_action(workflow_action_2.id)
    assert len(result_links) == 1
    assert all(
        link in result_links
        for link in [
            WorkflowActionLink(
                id="mocked_link_id",
                upstream_action_id=workflow_action_1.id,
                downstream_action_id=workflow_action_2.id,
            ),
        ]
    )


def test_has_form_property() -> None:
    workflow_test = Workflow(
        id="workflow_1",
        name="workflow_1",
        description="descr",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.DEPLOYED,
        project="80f2c9fc-0377-4002-82f3-45bf55b96f7a",
        owner="",
    )
    assert not workflow_test.has_form
    workflow_action_with_form = WorkflowAction(
        technical_name="postgw",
        id="id",
        name="name",
        description="description",
        version="version",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=False,
        validity_status=WorkflowActionValidityStatus.VALID,
        deployment_status=WorkflowActionDeployStatus.NONE,
        action_id="action_id",
        custom_name="custom_name",
        position_x=0,
        position_y=0,
        workflow_id="workflow_id",
        categories=[],
        endpoint="",
    )
    workflow_test.actions.append(workflow_action_with_form)
    assert workflow_test.has_form
