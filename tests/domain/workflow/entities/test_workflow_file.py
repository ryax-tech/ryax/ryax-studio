from unittest import mock

from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="workflow_file_id"))
def test_copy():
    workflow_file = WorkflowFile(
        id="id",
        name="workflow_file_name",
        extension="workflow_file_extension",
        size=10.2,
        path="workflow_file_path",
    )
    assert workflow_file.copy() == WorkflowFile(
        id="workflow_file_id",
        name=workflow_file.name,
        extension=workflow_file.extension,
        size=workflow_file.size,
        path=workflow_file.path,
    )


def test_update_path():
    workflow_file = WorkflowFile(
        path="path", id="123", name="123", extension="jpg", size=42.3
    )
    workflow_file.update_path("new_path")
    workflow_file.path = "new_path"
