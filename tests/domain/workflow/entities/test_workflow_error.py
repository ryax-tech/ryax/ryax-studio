from unittest import mock

from ryax.studio.domain.workflow.entities.workflow_error import WorkflowError
from ryax.studio.domain.workflow.workflow_values import WorkflowErrorCode


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_workflow_error_id"))
def test_copy():
    workflow_error = WorkflowError(
        id="workflow_error_id", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
    )
    assert workflow_error.copy() == WorkflowError(
        id="new_workflow_error_id", code=workflow_error.code
    )
