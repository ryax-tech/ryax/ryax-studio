import pytest

from ryax.studio.domain.workflow.entities.workflow_result import WorkflowResult
from ryax.studio.domain.workflow.workflow_values import CreateWorkflowResultData


@pytest.mark.parametrize("is_an_output", [(True), (False)])
def test_create_from_data(is_an_output):
    workflow_result_create_data = CreateWorkflowResultData(
        key="foo",
        workflow_id="baz",
        workflow_action_io_id="fred",
    )
    result_constructor = WorkflowResult.create_from_data(
        workflow_result_create_data, is_an_output=is_an_output
    )
    result = WorkflowResult(
        id="ryax",
        workflow_id="baz",
        key="foo",
        workflow_action_input_id="fred" if not is_an_output else None,
        workflow_action_output_id="fred" if is_an_output else None,
    )

    assert (
        result_constructor.workflow_id == result.workflow_id
        and result_constructor.key == result.key
        and result_constructor.workflow_action_output_id
        == result.workflow_action_output_id
        and result_constructor.workflow_action_input_id
        == result.workflow_action_input_id
    )
    assert result_constructor.id
