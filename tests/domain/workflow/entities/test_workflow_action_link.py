from typing import Dict
from unittest import mock

from ryax.studio.domain.action.action_values import ActionKind
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_link import WorkflowActionLink


def test_has_action():
    workflow_action_1 = WorkflowAction(
        id="workflow_action_1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_2 = WorkflowAction(
        id="workflow_action_2",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_3 = WorkflowAction(
        id="workflow_action_3",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_link = WorkflowActionLink.create_from_workflow_actions(
        upstream_action=workflow_action_1, downstream_action=workflow_action_2
    )
    assert workflow_link.has_action(workflow_action_1.id) is True
    assert workflow_link.has_action(workflow_action_2.id) is True
    assert workflow_link.has_action(workflow_action_3.id) is False


def test_has_input_action():
    workflow_action_1 = WorkflowAction(
        id="workflow_action_1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_2 = WorkflowAction(
        id="workflow_action_2",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_3 = WorkflowAction(
        id="workflow_action_3",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_link = WorkflowActionLink.create_from_workflow_actions(
        upstream_action=workflow_action_1, downstream_action=workflow_action_2
    )
    assert workflow_link.has_input_action(workflow_action_1.id) is True
    assert workflow_link.has_input_action(workflow_action_2.id) is False
    assert workflow_link.has_input_action(workflow_action_3.id) is False


def test_has_output_action():
    workflow_action_1 = WorkflowAction(
        id="workflow_action_1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_2 = WorkflowAction(
        id="workflow_action_2",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_3 = WorkflowAction(
        id="workflow_action_3",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_link = WorkflowActionLink.create_from_workflow_actions(
        upstream_action=workflow_action_1, downstream_action=workflow_action_2
    )
    assert workflow_link.has_output_action(workflow_action_1.id) is False
    assert workflow_link.has_output_action(workflow_action_2.id) is True
    assert workflow_link.has_output_action(workflow_action_3.id) is False


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_link_id"))
def test_copy():
    new_workflow_action_1 = WorkflowAction(
        id="workflow_action_11",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    new_workflow_action_2 = WorkflowAction(
        id="workflow_action_12",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action_map: Dict[str, WorkflowAction] = {
        "workflow_action_1": new_workflow_action_1,
        "workflow_action_2": new_workflow_action_2,
    }
    workflow_link = WorkflowActionLink(
        id="link_1",
        upstream_action_id="workflow_action_1",
        downstream_action_id="workflow_action_2",
    )
    assert workflow_link.copy(workflow_action_map) == WorkflowActionLink(
        id="new_link_id",
        upstream_action_id=new_workflow_action_1.id,
        downstream_action_id=new_workflow_action_2.id,
    )
