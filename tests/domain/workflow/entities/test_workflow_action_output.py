from unittest import mock

import pytest

from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.workflow_values import (
    UpdateWorkflowActionDynamicOutput,
)


@pytest.mark.parametrize(
    "input_data,expected_data",
    [
        (
            UpdateWorkflowActionDynamicOutput(
                technical_name="workflow_action_dyanmic_output_technical_name",
                display_name="workflow_action_dyanmic_output_display_name",
                help="workflow_action_dyanmic_output_help",
                type=ActionIOType.STRING,
                enum_values=["option1", "option2"],
            ),
            WorkflowActionOutput(
                optional=False,
                id="id1",
                technical_name="workflow_action_dyanmic_output_technical_name",
                display_name="workflow_action_dyanmic_output_display_name",
                help="workflow_action_dyanmic_output_help",
                type=ActionIOType.STRING,
                enum_values=["option1", "option2"],
            ),
        ),
        (
            UpdateWorkflowActionDynamicOutput(
                technical_name="workflow_action_dyanmic_output_technical_name",
                display_name="workflow_action_dyanmic_output_display_name",
                help="workflow_action_dyanmic_output_display_name",
                type=ActionIOType.STRING,
            ),
            WorkflowActionOutput(
                optional=False,
                id="id1",
                technical_name="workflow_action_dyanmic_output_technical_name",
                display_name="workflow_action_dyanmic_output_display_name",
                help="workflow_action_dyanmic_output_display_name",
                type=ActionIOType.STRING,
            ),
        ),
    ],
)
def test_dynamic_output_update_data(
    input_data: UpdateWorkflowActionDynamicOutput,
    expected_data: WorkflowActionOutput,
) -> None:
    dynamic_output = WorkflowActionOutput(
        optional=False,
        id="id1",
        technical_name="tn",
        display_name="dn",
        help="h",
        type=ActionIOType.FLOAT,
    )
    dynamic_output.update(input_data)
    assert dynamic_output == expected_data


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_id"))
def test_copy() -> None:
    output = WorkflowActionOutput(
        optional=False,
        id="id",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.ENUM,
        enum_values=["1", "2"],
    )
    assert output.copy() == WorkflowActionOutput(
        optional=False,
        id="new_id",
        technical_name=output.technical_name,
        display_name=output.display_name,
        help=output.help,
        type=output.type,
        enum_values=output.enum_values,
    )
