from typing import Any, List
from unittest import mock

import pytest

from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionInput,
)
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.workflow_exceptions import (
    DirectoryInputsMustBeZipfileException,
    InvalidInputValueException,
    NotImplementedException,
    WorkflowFileAlreadyPresentException,
    WorkflowInputHasNoStaticFileException,
)


@pytest.mark.parametrize(
    "workflow_action_input,expected_result",
    [
        pytest.param(
            WorkflowActionInput(
                optional=False,
                id="id",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
                static_value="static_value",
            ),
            True,
            id="With defined static value",
        ),
        pytest.param(
            WorkflowActionInput(
                optional=False,
                id="id",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
                reference_value="reference_value",
            ),
            True,
            id="With defined reference value",
        ),
        pytest.param(
            WorkflowActionInput(
                optional=False,
                id="id",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
                file_value=WorkflowFile(
                    id="123", name="123", extension="jpg", path="a", size=42.3
                ),
            ),
            True,
            id="With defined static file",
        ),
        pytest.param(
            WorkflowActionInput(
                optional=False,
                id="id",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
            ),
            False,
            id="With no value defined",
        ),
    ],
)
def test_has_defined_value(
    workflow_action_input: WorkflowActionInput, expected_result: bool
):
    assert workflow_action_input.has_defined_value() == expected_result


@pytest.mark.parametrize(
    "workflow_action_input,expected_result",
    [
        pytest.param(
            WorkflowActionInput(
                optional=False,
                id="id",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
                static_value="static_value",
            ),
            True,
            id="With static value",
        ),
        pytest.param(
            WorkflowActionInput(
                optional=False,
                id="id",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
            ),
            False,
            id="Without static value",
        ),
    ],
)
def test_has_static_value(
    workflow_action_input: WorkflowActionInput, expected_result: bool
):
    assert workflow_action_input.has_static_value() == expected_result


@pytest.mark.parametrize(
    "workflow_action_input,expected_result",
    [
        pytest.param(
            WorkflowActionInput(
                optional=False,
                id="id",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
                reference_value="workflow_action.output",
            ),
            True,
            id="With reference value",
        ),
        pytest.param(
            WorkflowActionInput(
                optional=False,
                id="id",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
            ),
            False,
            id="Without reference value",
        ),
    ],
)
def test_has_reference_value(
    workflow_action_input: WorkflowActionInput, expected_result: bool
):
    assert workflow_action_input.has_reference_value() == expected_result


def test_set_reference_value():
    """Method set reference value"""
    workflow_action_output = WorkflowActionOutput(
        optional=False,
        id="workflow_action_static_output_id",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="workflow_action_input",
        static_value="initial_value",
        file_value=WorkflowFile(
            id="123", name="123", extension="jpg", path="a", size=42.3
        ),
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.FILE,
    )
    workflow_action_input.set_reference_value(workflow_action_output)
    assert workflow_action_input.static_value is None
    assert workflow_action_input.file_value is None
    assert workflow_action_input.reference_value == workflow_action_output


def test_set_file_value():
    """Method set file value"""
    workflow_file = WorkflowFile(
        id="123", name="123", extension="jpg", path="a", size=42.3
    )
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="workflow_action_input",
        static_value="initial_value",
        reference_value=WorkflowActionOutput(
            optional=False,
            id="workflow_action_static_output_id",
            technical_name="technical_name",
            display_name="display_name",
            help="help",
            type=ActionIOType.STRING,
        ),
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_input.set_file_value(workflow_file)
    assert workflow_action_input.static_value is None
    assert workflow_action_input.reference_value is None
    assert workflow_action_input.file_value == workflow_file


@pytest.mark.parametrize(
    "workflow_action_input_type, input_static_value, expected_static_value",
    [
        (ActionIOType.STRING, "string value", "string value"),
        (ActionIOType.STRING, 1, "1"),
        (ActionIOType.PASSWORD, "secretPassword", "secretPassword"),
        (ActionIOType.PASSWORD, 23, "23"),
        (
            ActionIOType.LONGSTRING,
            """long string for testing""",
            """long string for testing""",
        ),
        (ActionIOType.LONGSTRING, 45, "45"),
        (ActionIOType.INTEGER, 1, 1),
        (ActionIOType.FLOAT, 1.2, 1.2),
        (ActionIOType.ENUM, "option1", "option1"),
        (ActionIOType.ENUM, "other_option", None),
    ],
    ids=[
        "With type string",
        "With type string and number input",
        "With password type",
        "With password type and number input",
        "With longstring type",
        "With longstring type and number input",
        "With integer type",
        "With float type",
        "With enum type and allowed option",
        "With enum and not allowed option",
    ],
)
def test_set_static_value(
    workflow_action_input_type: ActionIOType,
    input_static_value: Any,
    expected_static_value: Any,
):
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="ebcc54f3-ed5d-4d18-968a-06602bcc80e2",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=workflow_action_input_type,
        enum_values=["option1", "option2"],
        static_value="initial_value",
        reference_value=WorkflowActionOutput(
            optional=False,
            id="123",
            technical_name="technical_name",
            display_name="display_name",
            help="help",
            type=workflow_action_input_type,
        ),
        file_value=WorkflowFile(
            id="13", name="13", extension="jpg", path="a", size=42.3
        ),
    )
    workflow_action_input.set_static_value(input_static_value)
    assert workflow_action_input.static_value == expected_static_value
    assert workflow_action_input.reference_value is None
    assert workflow_action_input.file_value is None


@pytest.mark.parametrize(
    "workflow_action_input_type, input_static_value",
    [
        (ActionIOType.INTEGER, "salut"),
        (ActionIOType.FLOAT, "coucou"),
    ],
    ids=[
        "With integer type",
        "With float type",
    ],
)
def test_set_static_value_with_wrong_type(
    workflow_action_input_type: ActionIOType,
    input_static_value: Any,
):
    """Set input string value with wrong type should fail"""
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="ebcc54f3-ed5d-4d18-968a-06602bcc80e2",
        type=workflow_action_input_type,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
    )
    with pytest.raises(InvalidInputValueException):
        workflow_action_input.set_static_value(input_static_value)


@pytest.mark.parametrize(
    "workflow_action_input_type, input_static_value",
    [
        (ActionIOType.FILE, "file"),
        (ActionIOType.DIRECTORY, "directory"),
    ],
    ids=[
        "With file type",
        "With directory type",
    ],
)
def test_set_static_value_with_not_handled_type(
    workflow_action_input_type: ActionIOType,
    input_static_value: Any,
):
    """Set input string value with wrong type should fail"""
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="ebcc54f3-ed5d-4d18-968a-06602bcc80e2",
        type=workflow_action_input_type,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
    )
    with pytest.raises(NotImplementedException):
        workflow_action_input.set_static_value(input_static_value)


@pytest.mark.parametrize(
    "workflow_action_output, workflow_action_outputs_deleted, expected_reference_value",
    [
        pytest.param(
            None,
            [
                WorkflowActionOutput(
                    optional=False,
                    id="workflow_action_output_id",
                    technical_name="technical_name",
                    display_name="display_name",
                    help="help",
                    type=ActionIOType.STRING,
                )
            ],
            None,
            id="Without reference value",
        ),
        pytest.param(
            WorkflowActionOutput(
                optional=False,
                id="workflow_action_output_id",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
            ),
            [
                WorkflowActionOutput(
                    optional=False,
                    id="workflow_action_output_id",
                    technical_name="technical_name",
                    display_name="display_name",
                    help="help",
                    type=ActionIOType.STRING,
                )
            ],
            None,
            id="Whith deleted output referenced",
        ),
        pytest.param(
            WorkflowActionOutput(
                optional=False,
                id="workflow_action_output_id",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
            ),
            [
                WorkflowActionOutput(
                    optional=False,
                    id="workflow_action_output_other_id",
                    technical_name="technical_name",
                    display_name="display_name",
                    help="help",
                    type=ActionIOType.STRING,
                )
            ],
            WorkflowActionOutput(
                optional=False,
                id="workflow_action_output_id",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
            ),
            id="Whithout deleted output referenced",
        ),
    ],
)
def test_clear_reference_value(
    workflow_action_output: WorkflowActionOutput,
    workflow_action_outputs_deleted: List[WorkflowActionOutput],
    expected_reference_value: WorkflowActionOutput,
):
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="ebcc54f3-ed5d-4d18-968a-06602bcc80e2",
        reference_value=workflow_action_output,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_input.clear_reference_value_for(workflow_action_outputs_deleted)
    assert workflow_action_input.reference_value == expected_reference_value


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_id"))
def test_copy():
    new_workflow_action_output = WorkflowActionOutput(
        optional=False,
        id="workflow_action_output_1",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    copy_object = {"old_workflow_action_output": new_workflow_action_output}
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="workflow_action_input_id",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        enum_values=["1", "2"],
        static_value="static_value",
        reference_value=WorkflowActionOutput(
            optional=False,
            id="old_workflow_action_output",
            technical_name="technical_name",
            display_name="display_name",
            help="help",
            type=ActionIOType.STRING,
        ),
        file_value=WorkflowFile(
            id="workflow_file_id",
            name="static_file_name",
            extension="jpg",
            path="a",
            size=42.3,
        ),
    )
    assert workflow_action_input.copy(copy_object) == WorkflowActionInput(
        optional=False,
        id="new_id",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        enum_values=["1", "2"],
        static_value="static_value",
        reference_value=new_workflow_action_output,
        file_value=WorkflowFile(
            id="new_id", name="static_file_name", extension="jpg", path="a", size=42.3
        ),
    )


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_workflow_action_input_id"))
def test_copy_without_reference_value():
    new_workflow_action_output = WorkflowActionOutput(
        optional=False,
        id="workflow_action_output_1",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    copy_object = {"old_workflow_action_output": new_workflow_action_output}
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="workflow_action_input_id",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        enum_values=["1", "2"],
        static_value="static_value",
    )
    assert workflow_action_input.copy(copy_object) == WorkflowActionInput(
        optional=False,
        id="new_workflow_action_input_id",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        enum_values=["1", "2"],
        static_value="static_value",
    )


@pytest.mark.parametrize(
    "file_value, expected",
    [
        (
            WorkflowFile(id="123", name="123", extension="jpg", path="a", size=42.3),
            True,
        ),
        (None, False),
    ],
)
def test_has_file_value(file_value: WorkflowFile, expected: bool):
    workflow_input = WorkflowActionInput(
        optional=False,
        id="7970844a-252c-4164-9ee4-b99e236e2647",
        file_value=file_value,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    assert workflow_input.has_file_value() == expected


@pytest.mark.parametrize("input_type", [(ActionIOType.FILE), (ActionIOType.DIRECTORY)])
def test_delete_input_file(input_type):
    workflow_file = WorkflowFile(
        id="7970844a-252c-4164-9ee4-b99e236e2647",
        name="123",
        extension="jpg",
        path="a",
        size=42.3,
    )
    workflow_action_input = WorkflowActionInput(
        id="ebcc54f3-ed5d-4d18-968a-06602bcc80e2",
        file_value=workflow_file,
        type=input_type,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        optional=False,
    )
    workflow_action_input.has_file_value = mock.MagicMock(return_value=True)
    workflow_action_input.delete_input_file()
    assert workflow_action_input.file_value is None


def test_delete_input_file_when_no_file_value():
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="ebcc54f3-ed5d-4d18-968a-06602bcc80e2",
        type=ActionIOType.FILE,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
    )
    workflow_action_input.has_file_value = mock.MagicMock(return_value=False)
    with pytest.raises(WorkflowInputHasNoStaticFileException):
        workflow_action_input.delete_input_file()


def test_delete_input_file_when_wrong_file_type():
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="ebcc54f3-ed5d-4d18-968a-06602bcc80e2",
        type=ActionIOType.STRING,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
    )
    workflow_action_input.has_file_value = mock.MagicMock(return_value=True)
    with pytest.raises(WorkflowInputHasNoStaticFileException):
        workflow_action_input.delete_input_file()


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_id"))
def test_add_input_file():
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="ebcc54f3-ed5d-4d18-968a-06602bcc80e2",
        type=ActionIOType.FILE,
        static_value="value",
        reference_value=WorkflowActionOutput(
            optional=False,
            id="123",
            technical_name="technical_name",
            display_name="display_name",
            help="help",
            type=ActionIOType.FILE,
        ),
        technical_name="technical_name",
        display_name="display_name",
        help="help",
    )
    workflow_action_input.add_input_file("name", "extension", "path", 1.0)
    assert workflow_action_input.static_value is None
    assert workflow_action_input.reference_value is None
    assert workflow_action_input.file_value == WorkflowFile(
        id="new_id",
        name="name",
        extension="extension",
        path="path",
        size=1.0,
    )


def test_add_input_file_when_wrong_file_type():
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="ebcc54f3-ed5d-4d18-968a-06602bcc80e2",
        type=ActionIOType.STRING,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
    )
    workflow_action_input.has_file_value = mock.MagicMock(return_value=True)
    with pytest.raises(WorkflowFileAlreadyPresentException):
        workflow_action_input.add_input_file("name", "extension", "path", 1.0)


def test_add_input_file_when_file_already_present():
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="ebcc54f3-ed5d-4d18-968a-06602bcc80e2",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_input.has_file_value = mock.MagicMock(return_value=False)
    with pytest.raises(WorkflowInputHasNoStaticFileException):
        workflow_action_input.add_input_file("name", "extension", "path", 1.0)


def test_add_input_file_when_directory_but_not_zipfile():
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="ebcc54f3-ed5d-4d18-968a-06602bcc80e2",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.DIRECTORY,
    )
    workflow_action_input.has_file_value = mock.MagicMock(return_value=False)
    with pytest.raises(DirectoryInputsMustBeZipfileException):
        workflow_action_input.add_input_file(
            "name", "extension_is_not_zip", "path", 1.0
        )


def test_reset_value():
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="ebcc54f3-ed5d-4d18-968a-06602bcc80e2",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        static_value="123",
        reference_value=None,
        file_value=None,
    )
    workflow_action_input.reset_value()
    assert (
        workflow_action_input.static_value is None
        and workflow_action_input.reference_value is None
        and workflow_action_input.file_value is None
    )
