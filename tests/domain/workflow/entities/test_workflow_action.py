from datetime import datetime
from typing import List
from unittest import mock

import pytest

from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_category import ActionCategory
from ryax.studio.domain.action.action_io import ActionIO
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_category import (
    WorkflowActionCategory,
)
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionInput,
)
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.entities.workflow_error import WorkflowError
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.workflow_exceptions import (
    InvalidUserDefinedEndpointException,
    WorkflowActionHasNoDynamicOutputsException,
    WorkflowActionInputNotFoundException,
    WorkflowActionOutputNotFoundException,
    UnknownAddonParameterError,
)
from ryax.studio.domain.workflow.workflow_values import (
    AddWorkflowActionDynamicOutput,
    UpdateWorkflowActionDynamicOutput,
    WorkflowActionDeployStatus,
    WorkflowActionValidityStatus,
    WorkflowErrorCode,
)
from ryax.studio.infrastructure.addons.addons_service import PoCAddonsService


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="uuid_value"))
def test_create_from_action():
    category_1 = ActionCategory(id="11", name="cat1")
    category_2 = ActionCategory(id="22", name="cat2")
    action = Action(
        id="action_id",
        project="project_id",
        name="action_name",
        description="action_description",
        build_date=datetime.fromtimestamp(0),
        version="action_version",
        kind=ActionKind.SOURCE,
        has_dynamic_outputs=True,
        categories=[category_1, category_2],
        lockfile=b"",
        technical_name="wfmtname",
    )
    custom_name = "custom_name"
    position_x = 0
    position_y = 23
    result = WorkflowAction.create_from_action(
        action, custom_name, position_x, position_y
    )
    assert result == WorkflowAction(
        id="uuid_value",
        name=action.name,
        description=action.description,
        version=action.version,
        kind=action.kind,
        has_dynamic_outputs=action.has_dynamic_outputs,
        custom_name=custom_name,
        position_x=position_x,
        position_y=position_y,
        action_id=action.id,
        deployment_status=WorkflowActionDeployStatus.NONE,
        technical_name="wfmtname",
        endpoint="uuid_value",
    )


def test_update_input_static_value():
    """Method should return workflow action input if exists"""
    workflow_action_input_id = "workflow_action_input_id"
    workflow_action_input_static_value = "static_value"
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id=workflow_action_input_id,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_input.set_static_value = mock.MagicMock()
    workflow_action = WorkflowAction(
        id="workflow_action",
        inputs=[workflow_action_input],
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.update_input_static_value(
        workflow_action_input_id, workflow_action_input_static_value
    )
    workflow_action_input.set_static_value.assert_called_with(
        workflow_action_input_static_value
    )


def test_update_input_static_value_when_not_exists():
    """Method should return workflow action input if exists"""
    workflow_action_input_id = "workflow_action_input_id"
    workflow_action_input_static_value = "static_value"
    workflow_action = WorkflowAction(
        id="workflow_action",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    with pytest.raises(WorkflowActionInputNotFoundException):
        workflow_action.update_input_static_value(
            workflow_action_input_id, workflow_action_input_static_value
        )


def test_update_input_reference_value():
    """Method should return workflow action input if exists"""
    workflow_action_input_id = "workflow_action_input_id"
    workflow_action_input_reference_value = WorkflowActionOutput(
        id=workflow_action_input_id,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        optional=False,
    )
    workflow_action_input = WorkflowActionInput(
        id=workflow_action_input_id,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        optional=False,
    )
    workflow_action_input.set_reference_value = mock.MagicMock()
    workflow_action = WorkflowAction(
        id="workflow_action",
        inputs=[workflow_action_input],
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.update_input_reference_value(
        workflow_action_input_id, workflow_action_input_reference_value
    )
    workflow_action_input.set_reference_value.assert_called_with(
        workflow_action_input_reference_value
    )


def test_update_input_reference_value_when_not_exists():
    """Method should return workflow action input if exists"""
    workflow_action_input_id = "workflow_action_input_id"
    workflow_action_input_reference_value = mock.MagicMock(WorkflowActionOutput)
    workflow_action = WorkflowAction(
        id="workflow_action",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    with pytest.raises(WorkflowActionInputNotFoundException):
        workflow_action.update_input_reference_value(
            workflow_action_input_id, workflow_action_input_reference_value
        )


def test_delete_input_file():
    workflow_action_input_id = "workflow_action_input_id"
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id=workflow_action_input_id,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action = WorkflowAction(
        id="workflow_action",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.get_input = mock.MagicMock(return_value=workflow_action_input)
    workflow_action_input.delete_input_file = mock.MagicMock()
    workflow_action.delete_input_file(workflow_action_input_id)
    workflow_action.get_input.assert_called_with(workflow_action_input_id)
    workflow_action_input.delete_input_file.assert_called_once()


def test_get_input_when_not_exists():
    """Method should raise error if workflow action inputs if exists"""
    workflow_action = WorkflowAction(
        id="workflow_action",
        inputs=[],
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    with pytest.raises(WorkflowActionInputNotFoundException):
        workflow_action.get_input("workflow_action_input_unknown")


def test_clear_inputs_reference_values():
    def generate_workflow_action_input(id):
        workflow_action_input = WorkflowActionInput(
            optional=False,
            id=id,
            technical_name="technical_name",
            display_name="display_name",
            help="help",
            type=ActionIOType.STRING,
        )
        workflow_action_input.clear_reference_value_for = mock.MagicMock()
        return workflow_action_input

    workflow_action_outputs_deleted = [
        WorkflowActionOutput(
            optional=False,
            id="workflow_action_output_1",
            technical_name="technical_name",
            display_name="display_name",
            help="help",
            type=ActionIOType.STRING,
        ),
        WorkflowActionOutput(
            optional=False,
            id="workflow_action_output_2",
            technical_name="technical_name",
            display_name="display_name",
            help="help",
            type=ActionIOType.STRING,
        ),
    ]
    workflow_action_input_1 = generate_workflow_action_input("workflow_action_input_1")
    workflow_action_input_2 = generate_workflow_action_input("workflow_action_input_1")
    workflow_action = WorkflowAction(
        id="workflow_action",
        inputs=[workflow_action_input_1, workflow_action_input_2],
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.clear_inputs_reference_values_for(workflow_action_outputs_deleted)
    for item in workflow_action.inputs:
        item.clear_reference_value_for.assert_called_with(
            workflow_action_outputs_deleted
        )


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="workflow_action_input_id"))
def test_add_input():
    action_io = ActionIO(
        optional=False,
        id="action_io_id",
        technical_name="action_io_technical_name",
        display_name="action_io_display_name",
        help="action_io_help",
        type=ActionIOType.STRING,
        enum_values=["Option1", "Option34"],
    )
    workflow_action = WorkflowAction(
        id="workflow_action_id",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.add_input(action_io, 0)
    assert workflow_action.inputs == [
        WorkflowActionInput(
            optional=False,
            id="workflow_action_input_id",
            technical_name=action_io.technical_name,
            display_name=action_io.display_name,
            help=action_io.help,
            type=action_io.type,
            enum_values=action_io.enum_values,
            position=0,
        )
    ]


def test_has_output():
    workflow_action_output_id = "workflow_action_output_id"
    workflow_action_output = WorkflowActionOutput(
        optional=False,
        id=workflow_action_output_id,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action = WorkflowAction(
        id="workflow_action",
        outputs=[workflow_action_output],
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    assert workflow_action.has_output(workflow_action_output_id) is True


@pytest.mark.parametrize(
    "" "io_to_search_for, input_id, output_id, dynamic_id",
    [
        ("io_id", True, False, False),
        ("io_id", False, True, False),
        ("io_id", False, False, True),
    ],
)
def test_has_io(io_to_search_for, input_id, output_id, dynamic_id):
    workflow_action_output = WorkflowActionOutput(
        optional=False,
        id=io_to_search_for if output_id else "wf_out",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id=io_to_search_for if input_id else "wf_in",
        technical_name="foo",
        display_name="bar",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_dynamic_output = WorkflowActionOutput(
        optional=False,
        id=io_to_search_for if dynamic_id else "dynamic",
        technical_name="test",
        display_name="name",
        help="help",
        type=ActionIOType.STRING,
        is_dynamic=True,
    )
    workflow_action = WorkflowAction(
        id="workflow_action",
        outputs=[workflow_action_output],
        inputs=[workflow_action_input],
        dynamic_outputs=[workflow_action_dynamic_output],
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    assert workflow_action.has_io(io_to_search_for) is True


def test_get_output():
    workflow_action_output_id = "workflow_action_output_id"
    workflow_action_output = WorkflowActionOutput(
        optional=False,
        id=workflow_action_output_id,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action = WorkflowAction(
        id="workflow_action",
        outputs=[workflow_action_output],
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    assert (
        workflow_action.get_output(workflow_action_output_id) == workflow_action_output
    )


def test_get_output_when_not_exists():
    workflow_action_output_id = "workflow_action_output_id"
    workflow_action = WorkflowAction(
        id="workflow_action",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    with pytest.raises(WorkflowActionOutputNotFoundException):
        workflow_action.get_output(workflow_action_output_id)


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="uuid_value"))
def test_add_output():
    action_io = ActionIO(
        id="action_io_id",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        enum_values=["Option1", "Option2"],
        optional=False,
    )
    workflow_action = WorkflowAction(
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.add_output(action_io, 0)
    assert workflow_action.outputs == [
        WorkflowActionOutput(
            optional=False,
            id="uuid_value",
            technical_name=action_io.technical_name,
            display_name=action_io.display_name,
            help=action_io.help,
            type=action_io.type,
            enum_values=action_io.enum_values,
        )
    ]


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="uuid_value"))
def test_add_category():
    category = ActionCategory(
        id="action_category_id",
        name="test",
    )
    workflow_action = WorkflowAction(
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.add_category(category)
    assert workflow_action.categories == [
        WorkflowActionCategory(
            id="uuid_value",
            name=category.name,
        )
    ]


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_workflow_action_id"))
def test_copy():
    """Copy workflow action should return workflow action"""
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id="workflow_action_input",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    new_workflow_action_input = WorkflowActionInput(
        optional=False,
        id="new_workflow_action_input",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )

    workflow_action_output = WorkflowActionOutput(
        optional=False,
        id="workflow_action_output",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    new_workflow_action_output = WorkflowActionOutput(
        optional=False,
        id="new_workflow_action_output",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )

    workflow_action_error = WorkflowError(
        id="workflow_action_error", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
    )
    new_workflow_action_error = WorkflowError(
        id="new_workflow_action_error", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
    )
    copy_object = {
        "workflow_action_error": new_workflow_action_error,
        "workflow_action_input": new_workflow_action_input,
        "workflow_action_output": new_workflow_action_output,
    }
    workflow_action = WorkflowAction(
        id="workflow_action_id",
        name="name",
        technical_name="technical_name",
        description="description",
        version="version",
        kind=ActionKind.SOURCE,
        has_dynamic_outputs=False,
        custom_name="custom_name_1",
        position_x=0,
        position_y=0,
        inputs=[workflow_action_input],
        outputs=[workflow_action_output],
        validity_status=WorkflowActionValidityStatus.INVALID,
        deployment_status=WorkflowActionDeployStatus.DEPLOYED,
        action_id="action1",
        errors=[workflow_action_error],
    )
    assert workflow_action.copy(copy_object) == WorkflowAction(
        id="new_workflow_action_id",
        name=workflow_action.name,
        technical_name=workflow_action.technical_name,
        description=workflow_action.description,
        version=workflow_action.version,
        kind=workflow_action.kind,
        has_dynamic_outputs=workflow_action.has_dynamic_outputs,
        custom_name=workflow_action.custom_name,
        position_y=workflow_action.position_y,
        position_x=workflow_action.position_x,
        inputs=[new_workflow_action_input],
        outputs=[new_workflow_action_output],
        action_id=workflow_action.action_id,
        validity_status=WorkflowActionValidityStatus.INVALID,
        deployment_status=WorkflowActionDeployStatus.NONE,
        errors=[new_workflow_action_error],
    )


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="workflow_error_id"))
@pytest.mark.parametrize(
    "workflow_action,input_code,expected_errors",
    [
        pytest.param(
            WorkflowAction(
                errors=[
                    WorkflowError(
                        id="wfe1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                has_dynamic_outputs=False,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
            [WorkflowError(id="wfe1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED)],
            id="When code exists",
        ),
        pytest.param(
            WorkflowAction(
                errors=[
                    WorkflowError(
                        id="wfe1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                has_dynamic_outputs=False,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowErrorCode.WORKFLOW_HAS_CYCLES,
            [
                WorkflowError(id="wfe1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED),
                WorkflowError(
                    id="workflow_error_id", code=WorkflowErrorCode.WORKFLOW_HAS_CYCLES
                ),
            ],
            id="When code not exists",
        ),
        pytest.param(
            WorkflowAction(
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
            [
                WorkflowError(
                    id="workflow_error_id",
                    code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
                )
            ],
            id="When no code",
        ),
    ],
)
def test_add_error(
    workflow_action: WorkflowAction,
    input_code: WorkflowErrorCode,
    expected_errors: List[WorkflowError],
):
    """Workflow.add_error should add an error to the action"""
    workflow_action.add_error(input_code)
    assert workflow_action.errors == expected_errors


@pytest.mark.parametrize(
    "workflow_action,input_code,expected_errors",
    [
        pytest.param(
            WorkflowAction(
                errors=[
                    WorkflowError(
                        id="wfe1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
            [],
            id="When code exists",
        ),
        pytest.param(
            WorkflowAction(
                errors=[
                    WorkflowError(
                        id="wfe1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowErrorCode.WORKFLOW_HAS_CYCLES,
            [WorkflowError(id="wfe1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED)],
            id="When code not exists",
        ),
        pytest.param(
            WorkflowAction(
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
            [],
            id="When no code",
        ),
    ],
)
def test_remove_error(
    workflow_action: WorkflowAction,
    input_code: WorkflowErrorCode,
    expected_errors: List[WorkflowError],
):
    """Workflow.add_error should add an error to the action"""
    workflow_action.remove_error(input_code)
    assert workflow_action.errors == expected_errors


@pytest.mark.parametrize(
    "workflow_action,workflow_action_links_number,expected_added_error,expected_removed_error",
    [
        pytest.param(
            WorkflowAction(
                id="1",
                kind=ActionKind.SOURCE,
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            0,
            [],
            [
                WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_UPPER_LIMIT_REACHED,
                WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_LOWER_LIMIT_REACHED,
            ],
            id="With not linked source",
        ),
        pytest.param(
            WorkflowAction(
                id="1",
                kind=ActionKind.SOURCE,
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            0,
            [],
            [WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_UPPER_LIMIT_REACHED],
            id="With linked source as input action",
        ),
        pytest.param(
            WorkflowAction(
                id="1",
                kind=ActionKind.PUBLISHER,
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            0,
            [WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_LOWER_LIMIT_REACHED],
            [WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_UPPER_LIMIT_REACHED],
            id="With not linked publisher",
        ),
        pytest.param(
            WorkflowAction(
                id="1",
                kind=ActionKind.PUBLISHER,
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            1,
            [],
            [WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_UPPER_LIMIT_REACHED],
            id="With linked publisher as input action",
        ),
        pytest.param(
            WorkflowAction(
                id="1",
                kind=ActionKind.PUBLISHER,
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            2,
            [WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_UPPER_LIMIT_REACHED],
            [WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_LOWER_LIMIT_REACHED],
            id="With linked published as input action twice",
        ),
        pytest.param(
            WorkflowAction(
                kind=ActionKind.PROCESSOR,
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            0,
            [WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_LOWER_LIMIT_REACHED],
            [WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_UPPER_LIMIT_REACHED],
            id="With not linked processor",
        ),
        pytest.param(
            WorkflowAction(
                id="1",
                kind=ActionKind.PROCESSOR,
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            1,
            [],
            [
                WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_UPPER_LIMIT_REACHED,
                WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_LOWER_LIMIT_REACHED,
            ],
            id="With linked processor as input action",
        ),
        pytest.param(
            WorkflowAction(
                id="1",
                kind=ActionKind.PROCESSOR,
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            2,
            [WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_UPPER_LIMIT_REACHED],
            [WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_LOWER_LIMIT_REACHED],
            id="With linked processor as input action twice",
        ),
    ],
)
def test_check_action_links_inputs(
    workflow_action: WorkflowAction,
    workflow_action_links_number: int,
    expected_added_error: List[WorkflowErrorCode],
    expected_removed_error: List[WorkflowErrorCode],
):
    workflow_action.add_error = mock.MagicMock()
    workflow_action.remove_error = mock.MagicMock()
    workflow_action.check_action_links_inputs(workflow_action_links_number)

    if len(expected_added_error):
        workflow_action.add_error.assert_has_calls(
            [mock.call(item) for item in expected_added_error]
        )
    else:
        workflow_action.add_error.assert_not_called()

    if len(expected_removed_error):
        workflow_action.remove_error.assert_has_calls(
            [mock.call(item) for item in expected_removed_error]
        )
    else:
        workflow_action.remove_error.assert_not_called()


def test_check_action_inputs():
    workflow_action_input_1 = WorkflowActionInput(
        optional=False,
        id="workflow_action_input_1",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_input_1.has_defined_value = mock.MagicMock(return_value=True)
    workflow_action_input_2 = WorkflowActionInput(
        optional=False,
        id="workflow_action_input_2",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_input_2.has_defined_value = mock.MagicMock(return_value=True)
    workflow_action = WorkflowAction(
        inputs=[workflow_action_input_1, workflow_action_input_2],
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.remove_error = mock.MagicMock()
    workflow_action.check_action_inputs()
    workflow_action.remove_error.assert_called_with(
        WorkflowErrorCode.WORKFLOW_ACTION_INPUTS_NOT_DEFINED
    )


def test_check_action_inputs_when_one_has_undefined_value():
    workflow_action_input_1 = WorkflowActionInput(
        optional=False,
        id="workflow_action_input_1",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_input_1.has_defined_value = mock.MagicMock(return_value=False)
    workflow_action_input_2 = WorkflowActionInput(
        optional=False,
        id="workflow_action_input_2",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_input_2.has_defined_value = mock.MagicMock(return_value=True)
    workflow_action = WorkflowAction(
        inputs=[workflow_action_input_1, workflow_action_input_2],
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.add_error = mock.MagicMock()
    workflow_action.check_action_inputs()
    workflow_action.add_error.assert_called_with(
        WorkflowErrorCode.WORKFLOW_ACTION_INPUTS_NOT_DEFINED
    )


@pytest.mark.parametrize(
    "workflow_action,expected_validity_status",
    [
        pytest.param(
            WorkflowAction(
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowActionValidityStatus.VALID,
            id="With no errors",
        ),
        pytest.param(
            WorkflowAction(
                errors=[
                    WorkflowError(
                        id="wfe1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowActionValidityStatus.INVALID,
            id="With errors",
        ),
    ],
)
def test_update_validity_status(
    workflow_action: WorkflowAction,
    expected_validity_status: WorkflowActionValidityStatus,
):
    workflow_action.update_validity_status()
    assert workflow_action.validity_status == expected_validity_status


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="uuid_value"))
@pytest.mark.parametrize(
    "input_data,expected_data",
    [
        (
            AddWorkflowActionDynamicOutput(
                optional=False,
                technical_name="workflow_action_dyanmic_output_technical_name",
                display_name="workflow_action_dyanmic_output_display_name",
                help="workflow_action_dyanmic_output_help",
                type=ActionIOType.STRING,
                enum_values=["option1", "option2"],
            ),
            WorkflowActionOutput(
                optional=False,
                id="uuid_value",
                technical_name="uuid_value",
                display_name="workflow_action_dyanmic_output_display_name",
                help="workflow_action_dyanmic_output_help",
                type=ActionIOType.STRING,
                enum_values=["option1", "option2"],
                is_dynamic=True,
            ),
        ),
        (
            AddWorkflowActionDynamicOutput(
                optional=False,
                technical_name="workflow_action_dyanmic_output_technical_name",
                display_name="workflow_action_dyanmic_output_display_name",
                help="workflow_action_dyanmic_output_display_name",
                type=ActionIOType.STRING,
            ),
            WorkflowActionOutput(
                optional=False,
                id="uuid_value",
                technical_name="uuid_value",
                display_name="workflow_action_dyanmic_output_display_name",
                help="workflow_action_dyanmic_output_display_name",
                type=ActionIOType.STRING,
                is_dynamic=True,
            ),
        ),
    ],
)
def test_add_dynamic_output(
    input_data: AddWorkflowActionDynamicOutput,
    expected_data: WorkflowActionOutput,
):
    workflow_action = WorkflowAction(
        id="workflow_action_id",
        has_dynamic_outputs=True,
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.add_dynamic_output(input_data, 0)
    assert workflow_action.get_outputs() == [expected_data]


def test_add_dynamic_output_when_not_has_dynamic_outputs():
    """Add dynamic output to a action that doesn't take dynamic outputs should return an error"""
    workflow_action = WorkflowAction(
        id="workflow_action_id",
        has_dynamic_outputs=False,
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    data = AddWorkflowActionDynamicOutput(
        optional=False,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    with pytest.raises(WorkflowActionHasNoDynamicOutputsException):
        workflow_action.add_dynamic_output(data, 0)


def test_update_dynamic_output():
    workflow_action_output_id = "workflow_action_output_id"
    data = UpdateWorkflowActionDynamicOutput(
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_output = WorkflowActionOutput(
        optional=False,
        id=workflow_action_output_id,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_output.update = mock.MagicMock()
    workflow_action = WorkflowAction(
        has_dynamic_outputs=True,
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.get_output = mock.MagicMock(return_value=workflow_action_output)
    workflow_action.update_dynamic_output(workflow_action_output_id, data)
    workflow_action.get_output.assert_called_with(workflow_action_output_id)
    workflow_action_output.update.assert_called_once_with(data)


def test_update_dynamic_output_when_not_has_dynamic_outputs():
    workflow_action_output_id = "workflow_action_output_id"
    data = UpdateWorkflowActionDynamicOutput(
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_output = WorkflowActionOutput(
        optional=False,
        id=workflow_action_output_id,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action_output.update = mock.MagicMock()
    workflow_action = WorkflowAction(
        has_dynamic_outputs=False,
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    with pytest.raises(WorkflowActionHasNoDynamicOutputsException):
        workflow_action.update_dynamic_output(workflow_action_output_id, data)


def test_delete_dynamic_output():
    workflow_action_output_id = "workflow_action_output_id"
    workflow_action_output = WorkflowActionOutput(
        optional=False,
        id="id",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        is_dynamic=True,
    )
    workflow_action = WorkflowAction(
        has_dynamic_outputs=True,
        outputs=[],
        dynamic_outputs=[workflow_action_output],
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.get_output = mock.MagicMock(return_value=workflow_action_output)
    workflow_action.delete_dynamic_output(workflow_action_output_id)
    assert workflow_action.get_outputs() == []
    workflow_action.get_output.assert_called_with(workflow_action_output_id)


def test_delete_dynamic_output_when_not_has_dynamic_outputs():
    workflow_action_dynamic_output_id = "workflow_action_output_id"
    workflow_action = WorkflowAction(
        id="workflow_action_id",
        has_dynamic_outputs=False,
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    with pytest.raises(WorkflowActionHasNoDynamicOutputsException):
        workflow_action.delete_dynamic_output(workflow_action_dynamic_output_id)


def test_add_input_file():
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_action_input_id"
    filename = "filename"
    extension = ".csv"
    path = "path"
    size = 1.0
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id=workflow_action_input_id,
        type=ActionIOType.FILE,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
    )
    workflow_action = WorkflowAction(
        id=workflow_action_id,
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.get_input = mock.MagicMock(return_value=workflow_action_input)
    workflow_action_input.add_input_file = mock.MagicMock()

    workflow_action.add_input_file(
        workflow_action_input_id, filename, extension, path, size
    )
    workflow_action.get_input.assert_called_once_with(workflow_action_input_id)
    workflow_action_input.add_input_file.assert_called_once_with(
        filename, extension, path, size
    )


def test_add_input_file_when_action_not_exists():
    workflow_action_input_id = "workflow_action_input_id"
    workflow_action = WorkflowAction(
        id="workflow_action",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    with pytest.raises(WorkflowActionInputNotFoundException):
        assert workflow_action.add_input_file(
            workflow_action_input_id, "name", "extension", "path", 1.0
        )


@pytest.mark.parametrize(
    "workflow_action_1,workflow_action_2,expected_workflow_action",
    [
        (
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="id1",
                        technical_name="test-input",
                        static_value="value",
                        type=ActionIOType.STRING,
                        display_name="display_name",
                        help="help",
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="id1",
                        technical_name="test-input",
                        type=ActionIOType.STRING,
                        display_name="display_name",
                        help="help",
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="id1",
                        technical_name="test-input",
                        static_value="value",
                        type=ActionIOType.STRING,
                        display_name="display_name",
                        help="help",
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
        ),
        (
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="id1",
                        technical_name="test-input",
                        static_value="value",
                        type=ActionIOType.STRING,
                        display_name="display_name",
                        help="help",
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="id1",
                        technical_name="test-input2",
                        type=ActionIOType.STRING,
                        display_name="display_name",
                        help="help",
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="id1",
                        technical_name="test-input2",
                        type=ActionIOType.STRING,
                        display_name="display_name",
                        help="help",
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
        ),
        (
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        technical_name="test-input",
                        id="id1",
                        static_value="value",
                        type=ActionIOType.STRING,
                        display_name="display_name",
                        help="help",
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="id1",
                        technical_name="test-input",
                        type=ActionIOType.FLOAT,
                        display_name="display_name",
                        help="help",
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="id1",
                        technical_name="test-input",
                        type=ActionIOType.FLOAT,
                        display_name="display_name",
                        help="help",
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
        ),
        (
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="id1",
                        technical_name="test-input",
                        reference_value=WorkflowActionOutput(
                            optional=False,
                            id="111",
                            technical_name="technical_name",
                            display_name="display_name",
                            help="help",
                            type=ActionIOType.STRING,
                        ),
                        display_name="display_name",
                        help="help",
                        type=ActionIOType.STRING,
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        technical_name="test-input",
                        id="id1",
                        display_name="display_name",
                        help="help",
                        type=ActionIOType.STRING,
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        technical_name="test-input",
                        reference_value=WorkflowActionOutput(
                            optional=False,
                            id="111",
                            technical_name="technical_name",
                            display_name="display_name",
                            help="help",
                            type=ActionIOType.STRING,
                        ),
                        id="id1",
                        display_name="display_name",
                        help="help",
                        type=ActionIOType.STRING,
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
        ),
        (
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        technical_name="test-input",
                        file_value=WorkflowFile(
                            id="123", name="123", extension="jpg", path="a", size=42.3
                        ),
                        id="id1",
                        display_name="display_name",
                        help="help",
                        type=ActionIOType.STRING,
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        technical_name="test-input",
                        id="id1",
                        display_name="display_name",
                        help="help",
                        type=ActionIOType.STRING,
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
            WorkflowAction(
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        technical_name="test-input",
                        file_value=WorkflowFile(
                            id="123", name="123", extension="jpg", path="a", size=42.3
                        ),
                        id="id1",
                        display_name="display_name",
                        help="help",
                        type=ActionIOType.STRING,
                    )
                ],
                id="wfm1",
                name="wfmname",
                technical_name="wfmtname",
                description="wfmdescr",
                version="wfmver",
                kind=ActionKind.PROCESSOR,
                position_x=2,
                position_y=2,
                action_id="action_id",
            ),
        ),
    ],
    ids=[
        "Input with correct static value",
        "Input with static value but different technical_name",
        "Input with static value but different type",
        "Input with correct reference value",
        "Input with correct file value",
    ],
)
def test_patch_workflow_action_ios_values(
    workflow_action_1, workflow_action_2, expected_workflow_action
):
    workflow_action_2.patch_workflow_action_ios_values(workflow_action_1)
    assert workflow_action_2 == expected_workflow_action


def test_get_static_files():
    workflow_input_1 = WorkflowActionInput(
        optional=False,
        id="input1",
        file_value=WorkflowFile(id="1", name="1", extension="jpg", path="a", size=42.3),
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_input_2 = WorkflowActionInput(
        optional=False,
        id="input2",
        file_value=WorkflowFile(id="2", name="2", extension="jpg", path="a", size=42.3),
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_input_3 = WorkflowActionInput(
        optional=False,
        id="input3",
        static_value="static_value",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_action = WorkflowAction(
        inputs=[workflow_input_1, workflow_input_2, workflow_input_3],
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    assert workflow_action.get_static_files() == [
        WorkflowFile(id="1", name="1", extension="jpg", path="a", size=42.3),
        WorkflowFile(id="2", name="2", extension="jpg", path="a", size=42.3),
    ]


@pytest.mark.parametrize(
    "invalid_string",
    [
        "twoslashes//twoslashes-is-bad",
        "/invalid-char@",
        "/invalid-char#",
        "/invalid-char[",
        "/invalid-char]",
        "/invalid-char:",
        "/invalid-char?",
    ],
)
def test_update_endpoint_when_invalid_string(invalid_string: str):
    workflow_action = WorkflowAction(
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    with pytest.raises(InvalidUserDefinedEndpointException):
        workflow_action.update_endpoint(invalid_string)


@pytest.mark.parametrize(
    "valid_string",
    ["/foo/bar", "/valid_endpoint-valid", "/valid123456789/valid/valid", None],
)
def test_update_endpoint(valid_string: str):
    workflow_action = WorkflowAction(
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_action.update_endpoint(valid_string)
    assert workflow_action.endpoint == valid_string


def test_reset_input_value():
    wfmod_input = WorkflowActionInput(
        optional=False,
        id="foo",
        technical_name="bar",
        display_name="dis",
        help="help",
        type=ActionIOType.STRING,
    )
    wfmod_input.reset_value = mock.MagicMock()
    workflow_action = WorkflowAction(
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
        inputs=[wfmod_input],
    )
    workflow_action.reset_input_value(wfmod_input.id)
    wfmod_input.reset_value.assert_called_once()


def test_reset_input_value_when_not_found():
    workflow_action = WorkflowAction(
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
        inputs=[],
    )
    with pytest.raises(WorkflowActionInputNotFoundException):
        workflow_action.reset_input_value("NONEXISTING_INPUT")


def test_add_addon():
    workflow_action = WorkflowAction(
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
        inputs=[],
    )
    addon_values = {"port": 1234, "endpoint_prefix": "/test"}
    addons = PoCAddonsService().load()
    workflow_action.add_addon("http_service", addon_values, addons)

    assert len(workflow_action.addons_inputs) == len(
        addons.get("http_service").parameters
    )
    assert workflow_action.get_io_from_technical_name("endpoint_prefix")


def test_add_addon_wrong_args():
    workflow_action = WorkflowAction(
        id="wfm1",
        name="wfmname",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
        inputs=[],
    )
    addon_values = {"wrong_args": 1234}
    addons = PoCAddonsService().load()
    with pytest.raises(UnknownAddonParameterError):
        workflow_action.add_addon("http_service", addon_values, addons)
