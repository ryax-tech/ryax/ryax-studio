from datetime import datetime
from unittest import mock

from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_category import ActionCategory
from ryax.studio.domain.action.action_events import ActionDeletedEvent
from ryax.studio.domain.action.action_values import ActionKind


def test_add_delete_event():
    action = Action(
        id="action_id",
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.fromtimestamp(0),
    )
    action.add_delete_event()
    assert action.events == [
        ActionDeletedEvent(action_id=action.id, version="action1name")
    ]


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_id"))
def test_add_new_category():
    action = Action(
        id="action1",
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.fromtimestamp(0),
    )
    action.add_new_category("test")
    assert action.categories == [ActionCategory(id="new_id", name="test")]
