from datetime import datetime

import pytest

from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_events import ActionReadyEvent
from ryax.studio.domain.action.action_factory import ActionFactory
from ryax.studio.domain.action.action_io import ActionIO
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.domain.logo.logo import Logo


@pytest.fixture(scope="function")
def app_container():
    container = ApplicationContainer()
    yield container


def test_from_action_ready_event(app_container: ApplicationContainer) -> None:
    """From create action data should return action"""
    action_ready_event_input = ActionReadyEvent.ActionIO(
        optional=False,
        id="input_id",
        technical_name="input_tech_name",
        display_name="input_display_name",
        help="input_help",
        type=ActionIOType.STRING,
        enum_values=[],
    )
    action_ready_event_output = ActionReadyEvent.ActionIO(
        optional=False,
        id="output_id",
        technical_name="output_tech_name",
        display_name="output_display_name",
        help="output_help",
        type=ActionIOType.STRING,
        enum_values=[],
    )
    action_ready_event = ActionReadyEvent(
        id="id",
        name="name",
        technical_name="action_technical_name",
        version="1.0",
        kind=ActionKind.SOURCE,
        logo=ActionReadyEvent.Logo(
            id="logoid",
            name="logoname",
            extension="png",
            content=b"content",
        ),
        description="desc",
        lockfile=b"",
        dynamic_outputs=True,
        inputs=[action_ready_event_input],
        outputs=[action_ready_event_output],
        owner_id="",
        build_date=datetime.now(),
        categories=[],
        project_id="project1",
        resources=None,
    )

    action_factory: ActionFactory = app_container.action_factory()
    assert action_factory.from_action_ready_event(action_ready_event) == Action(
        id=action_ready_event.id,
        name=action_ready_event.name,
        version=action_ready_event.version,
        resources=None,
        logo=Logo(
            action_id=action_ready_event.id,
            id="logoid",
            name="logoname",
            extension="png",
            content=b"content",
        ),
        lockfile=b"",
        kind=ActionKind(action_ready_event.kind),
        description=action_ready_event.description,
        has_dynamic_outputs=True,
        inputs=[
            ActionIO(
                optional=False,
                id=action_ready_event_input.id,
                technical_name=action_ready_event_input.technical_name,
                display_name=action_ready_event_input.display_name,
                help=action_ready_event_input.help,
                type=action_ready_event_input.type,
                enum_values=action_ready_event_input.enum_values,
            )
        ],
        outputs=[
            ActionIO(
                optional=False,
                id=action_ready_event_output.id,
                technical_name=action_ready_event_output.technical_name,
                display_name=action_ready_event_output.display_name,
                help=action_ready_event_output.help,
                type=action_ready_event_output.type,
                enum_values=action_ready_event_output.enum_values,
            )
        ],
        project=action_ready_event.project_id,
        technical_name=action_ready_event.technical_name,
        build_date=action_ready_event.build_date,
    )


def test_from_action_ready_event_logo_is_none(
    app_container: ApplicationContainer,
) -> None:
    """From create action data should return action"""
    action_ready_event_input = ActionReadyEvent.ActionIO(
        optional=False,
        id="input_id",
        technical_name="input_tech_name",
        display_name="input_display_name",
        help="input_help",
        type=ActionIOType.STRING,
        enum_values=[],
    )
    action_ready_event_output = ActionReadyEvent.ActionIO(
        optional=False,
        id="output_id",
        technical_name="output_tech_name",
        display_name="output_display_name",
        help="output_help",
        type=ActionIOType.STRING,
        enum_values=[],
    )
    action_ready_event = ActionReadyEvent(
        id="id",
        name="name",
        technical_name="action_technical_name",
        version="1.0",
        kind=ActionKind.SOURCE,
        logo=None,
        description="desc",
        lockfile=b"",
        dynamic_outputs=True,
        inputs=[action_ready_event_input],
        outputs=[action_ready_event_output],
        owner_id="",
        build_date=datetime.now(),
        categories=[],
        project_id="project1",
        resources=None,
    )

    action_factory: ActionFactory = app_container.action_factory()
    assert action_factory.from_action_ready_event(action_ready_event) == Action(
        id=action_ready_event.id,
        name=action_ready_event.name,
        version=action_ready_event.version,
        logo=None,
        kind=ActionKind(action_ready_event.kind),
        description=action_ready_event.description,
        lockfile=b"",
        has_dynamic_outputs=True,
        inputs=[
            ActionIO(
                optional=False,
                id=action_ready_event_input.id,
                technical_name=action_ready_event_input.technical_name,
                display_name=action_ready_event_input.display_name,
                help=action_ready_event_input.help,
                type=action_ready_event_input.type,
                enum_values=action_ready_event_input.enum_values,
            )
        ],
        outputs=[
            ActionIO(
                optional=False,
                id=action_ready_event_output.id,
                technical_name=action_ready_event_output.technical_name,
                display_name=action_ready_event_output.display_name,
                help=action_ready_event_output.help,
                type=action_ready_event_output.type,
                enum_values=action_ready_event_output.enum_values,
            )
        ],
        project=action_ready_event.project_id,
        technical_name=action_ready_event.technical_name,
        build_date=action_ready_event.build_date,
        resources=action_ready_event.resources,
    )
