import asyncio
import logging
import os
import time
from multiprocessing import Process
from unittest import mock

import aio_pika
import pytest
import requests
from aio_pika import connect_robust
from aiohttp import web
from sqlalchemy.orm import clear_mappers

from ryax.studio.app import on_cleanup, on_startup
from ryax.studio.application.authentication_service import AuthenticationService
from ryax.studio.application.project_authorization_service import (
    ProjectAuthorizationService,
)
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.auth.auth_token import AuthToken
from ryax.studio.infrastructure.api.setup import setup as api_setup
from ryax.studio.infrastructure.database.engine import DatabaseEngine
from ryax.studio.infrastructure.database.metadata import metadata
from ryax.studio.infrastructure.messaging.messages.repository_messages_pb2 import (
    ModuleReady,
)
from ryax.studio.infrastructure.messaging.setup import setup as messaging_setup

PROJECT_ID = "fake_project_id"


@pytest.fixture
async def runner(loop):
    db = DatabaseEngine(connection_url=os.environ["RYAX_DATASTORE"])
    db.connect()
    clear_mappers()
    metadata.drop_all(db.connection)
    db.disconnect()

    def start() -> None:
        container = ApplicationContainer()
        container.configuration.from_dict(
            {
                "ryax_datastore": os.environ["RYAX_DATASTORE"],
                "ryax_broker": os.environ["RYAX_BROKER"],
                "ryax_filestore": os.environ["RYAX_FILESTORE"],
                "ryax_filestore_access_key": os.environ["RYAX_FILESTORE_ACCESS_KEY"],
                "ryax_filestore_secret_key": os.environ["RYAX_FILESTORE_SECRET_KEY"],
                "ryax_filestore_bucket": os.environ["RYAX_FILESTORE_BUCKET"],
            }
        )
        # Disable project id check and auth
        project_authorization_service_mock = mock.MagicMock(ProjectAuthorizationService)
        project_authorization_service_mock.get_current_project = mock.AsyncMock(
            return_value=PROJECT_ID
        )
        container.project_authorization_service.override(
            project_authorization_service_mock
        )
        authentication_service_mock = mock.MagicMock(AuthenticationService)
        authentication_service_mock.check_access = mock.MagicMock(
            return_value=AuthToken(user_id="user")
        )
        container.authentication_service.override(authentication_service_mock)

        app: web.Application = web.Application()
        api_setup(app, container)
        consumer = container.messaging_consumer()
        messaging_setup(consumer, container)

        app["container"] = container

        logging.getLogger().info(f"CONFIG: {container.configuration()}")

        app.on_startup.append(on_startup)
        app.on_cleanup.append(on_cleanup)

        async def clean_the_database(app: web.Application) -> None:
            """Define hook when application stop"""
            container: ApplicationContainer = app["container"]
            clear_mappers()
            engine = container.database_engine()
            metadata.drop_all(engine._engine)
            engine.disconnect()

        app.on_cleanup.append(clean_the_database)

        web.run_app(app)

    runner_app = Process(target=start)
    runner_app.start()
    url = "http://127.0.0.1:8080"
    counter = 0
    while counter < 20:
        try:
            requests.head(url)
            break
        except requests.exceptions.ConnectionError:
            print(".", end="")
            time.sleep(0.5)
            counter += 1

    yield url
    runner_app.terminate()
    counter = 0
    while runner_app.is_alive() and counter < 20:
        time.sleep(1)
        counter += 1
    runner_app.kill()


async def test_get_documentation_from_API(runner: str) -> None:
    response = requests.get(runner + "/docs/swagger.json")
    assert response.status_code == 200
    result = response.json()
    assert result["swagger"] == "2.0"
    assert len(result["paths"]) > 4


async def test_health_check_from_API(runner: str) -> None:
    response = requests.get(runner + "/healthz")
    assert response.status_code == 200


async def inject_action(actions: list[dict], runner_api: str) -> object:
    connection = await connect_robust(os.environ["RYAX_BROKER"])
    channel = await connection.channel()
    exchange = await channel.declare_exchange(
        "domain_events", aio_pika.ExchangeType.TOPIC, durable=True
    )
    for action in actions:
        message_content = ModuleReady()
        message_content.module_id = action["id"]
        message_content.module_kind = action["kind"]
        message_content.module_version = action["version"]
        message_content.module_description = action.get("description", "")
        message_content.module_technical_name = action.get("technical_name", "")
        message_content.module_name = action.get("name", action["id"])
        for input in action["inputs"]:
            message_content.module_inputs.append(ModuleReady.ModuleIO(**input))
        for output in action["outputs"]:
            message_content.module_outputs.append(ModuleReady.ModuleIO(**output))
        message_content.module_project_id = PROJECT_ID
        message_content.module_dynamic_outputs = action.get(
            "has_dynamic_outputs", False
        )
        message = aio_pika.Message(
            type="ModuleReady", body=message_content.SerializeToString()
        )
        await exchange.publish(message, routing_key="Repository.ModuleReady")
    counter = 0
    created_actions = {}
    while counter < 20:
        try:
            response = requests.get(runner_api + "/modules")
            assert response.status_code == 200
            created_actions = response.json()
            if len(created_actions) == len(actions):
                return
            else:
                await asyncio.sleep(0.5)
        except (requests.exceptions.ConnectionError, AssertionError):
            print(".", end="")
            time.sleep(0.5)
            counter += 1

    raise Exception(
        f"Unable to create actions. Available actions are: {created_actions}"
    )


async def test_workflow_creation(runner: str) -> None:
    # Inject action in the store

    await inject_action(
        [
            # {
            #    "id": "2",
            #    "technical_name": "mod2",
            #    "version": "1",
            #    "kind": 2,
            #    "inputs": [{"id": "13", "technical_name": "test", "type": "STRING"}],
            #    "outputs": [{"id": "14", "technical_name": "test", "type": "INTEGER"}],
            # },
            {
                "id": "1",
                "technical_name": "mod1",
                "version": "1",
                "kind": 1,
                "inputs": [{"id": "13", "technical_name": "test", "type": "STRING"}],
                "outputs": [{"id": "14", "technical_name": "test", "type": "INTEGER"}],
                "has_dynamic_outputs": True,
            }
        ],
        runner,
    )

    response = requests.post(runner + "/workflows", data={"name": "test"})
    assert response.status_code == 201
    workflow_id = response.json()["workflow_id"]

    response = requests.get(runner + "/workflows")
    assert response.status_code == 200
    assert len(response.json()) == 1

    response = requests.get(runner + f"/workflows/{workflow_id}")
    assert response.status_code == 200
    assert response.json()["name"] == "test"

    # Add actions to the workflow
    response = requests.post(
        runner + f"/workflows/{workflow_id}/modules", data={"module_id": "1"}
    )
    assert response.status_code == 201
    action_id = response.json()["id"]

    # FIXME add more actions with links and references
    # response = requests.post(
    #    runner + f"/workflows/{workflow_id}/modules", data={"module_id": "2"}
    # )
    # assert response.status_code == 201

    # Add dynamic output
    response = requests.post(
        runner + f"/workflows/{workflow_id}/modules/{action_id}/outputs",
        data={
            "technical_name": "dyn_out1",
            "type": "integer",
            "display_name": "toto",
            "help": "tata",
            "enum_values": [],
        },
    )
    assert response.status_code == 201

    # deploy the workflow
    response = requests.post(runner + f"/workflows/{workflow_id}/deploy")
    assert response.status_code == 400
    error = response.json()
    assert error == {"error": "Workflow not valid"}
