import datetime
from unittest import mock

from aiohttp.test_utils import TestClient
from marshmallow import RAISE

from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_io import ActionIO
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.domain.workflow.workflow_values import WorkflowErrorCode
from ryax.studio.infrastructure.api.schemas.add_workflow_schema import (
    AddWorkflowResponseSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_error_schema import (
    WorkflowErrorSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_result_schema import (
    AddWorkflowResultResponseSchema,
    WorkflowResultSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_schema import WorkflowSchema
from ryax.studio.infrastructure.database.engine import Session


class TestWorkflowEdition:
    async def _create_simple_workflow(
        self,
        database_session: Session,
        api_client: TestClient,
    ):
        # Add some actions to be able to create a workflow later
        now = datetime.datetime.fromisoformat("2000-06-06T06:00")
        action_def1 = Action(
            id="mdf1",
            project="project_id",
            name="action human_name",
            technical_name="tmdf1",
            lockfile=b"",
            version="1.0",
            kind=ActionKind.SOURCE,
            description="descr",
            build_date=now,
            inputs=[
                ActionIO(
                    id="in1",
                    technical_name="in1",
                    display_name="in1",
                    help="in1",
                    type=ActionIOType.STRING,
                    optional=False,
                ),
                ActionIO(
                    id="in2",
                    technical_name="in2",
                    display_name="in2",
                    help="in2",
                    type=ActionIOType.FLOAT,
                    default_value="42.5",
                    optional=False,
                ),
            ],
            outputs=[
                ActionIO(
                    optional=False,
                    id="out1",
                    technical_name="out1",
                    display_name="out1",
                    help="out1",
                    type=ActionIOType.STRING,
                ),
            ],
        )
        database_session.add(action_def1)
        action_def2 = Action(
            id="mdf2",
            project="project_id",
            name="action human_name",
            technical_name="tmdf2",
            version="1.0",
            kind=ActionKind.PROCESSOR,
            description="descr",
            lockfile=b"",
            build_date=now,
            inputs=[
                ActionIO(
                    optional=False,
                    id="pin1",
                    technical_name="pin1",
                    display_name="pin1",
                    help="pin1",
                    type=ActionIOType.STRING,
                ),
                ActionIO(
                    optional=False,
                    id="pin2",
                    technical_name="pin2",
                    display_name="pin2",
                    help="pin2",
                    type=ActionIOType.FLOAT,
                    default_value="42.5",
                ),
            ],
            outputs=[
                ActionIO(
                    optional=False,
                    id="pout1",
                    technical_name="pout1",
                    display_name="pout1",
                    help="pout1",
                    type=ActionIOType.STRING,
                ),
            ],
        )
        database_session.add(action_def2)
        database_session.commit()

        # There is no workflow
        response = await api_client.get("/workflows")
        assert response.status == 200
        result = await response.json()
        assert len(result) == 0
        WorkflowSchema(many=True).load(result, unknown=RAISE, partial=False)

        # Let's create a first one
        response = await api_client.post("/workflows", json={"name": "new wf"})
        assert response.status == 201
        result = await response.json()
        AddWorkflowResponseSchema().load(result, unknown=RAISE, partial=False)
        assert len(result["workflow_id"]) > 0
        workflow_id = result["workflow_id"]

        # Add this stage we should have an error: there is no actions
        response = await api_client.get(
            f"/workflows/{workflow_id}/errors",
        )
        assert response.status == 200
        result = await response.json()
        WorkflowErrorSchema(many=True).load(result, unknown=RAISE, partial=False)
        assert len(result) == 1
        assert (
            result[0]["code"]
            == WorkflowErrorCode.WORKFLOW_SOURCES_LOWER_LIMIT_REACHED.value
        )

        # add a source
        response = await api_client.post(
            f"/v2/workflows/{workflow_id}/modules",
            data={"module_definition_id": "mdf1"},
        )
        assert response.status == 201
        result = await response.json()
        source_id = result["id"]
        if result["inputs"][0]["technical_name"] == "in1":
            source_in1_id = result["inputs"][0]["id"]
            source_in2_id = result["inputs"][1]["id"]
            assert result["inputs"][1]["static_value"] == 42.5
        else:
            source_in1_id = result["inputs"][1]["id"]
            source_in2_id = result["inputs"][0]["id"]
            assert result["inputs"][0]["static_value"] == 42.5

        # now, the error is: input not configured
        response = await api_client.get(
            f"/workflows/{workflow_id}/errors",
        )
        assert response.status == 200
        result = await response.json()
        WorkflowErrorSchema(many=True).load(result, unknown=RAISE, partial=False)
        assert len(result) == 1
        assert (
            result[0]["code"]
            == WorkflowErrorCode.WORKFLOW_ACTION_INPUTS_NOT_DEFINED.value
        )

        # let's edit inputs
        response = await api_client.put(
            f"/v2/workflows/{workflow_id}/modules/{source_id}",
            json={
                "custom_name": "action custom name",
                "inputs": [
                    {
                        "id": source_in1_id,
                        "static_value": "Héllo",
                    },
                    {
                        "id": source_in2_id,
                        "static_value": 53.9,
                    },
                ],
                "dynamic_outputs": [],
            },
        )
        assert response.status == 200

        # there is now no errors
        response = await api_client.get(
            f"/workflows/{workflow_id}/errors",
        )
        assert response.status == 200
        result = await response.json()
        WorkflowErrorSchema(many=True).load(result, unknown=RAISE, partial=False)
        assert len(result) == 0

        # add an action
        response = await api_client.post(
            f"/v2/workflows/{workflow_id}/modules",
            json={
                "module_definition_id": "mdf2",
                "parent_workflow_module_id": source_id,
            },
        )
        assert response.status == 201
        result = await response.json()
        action_id = result["id"]
        action_output_id = result["outputs"][0]["id"]
        assert result["module_id"] == "mdf2"

        # only 1 error: an input is missing in the action
        response = await api_client.get(
            f"/workflows/{workflow_id}/errors",
        )
        assert response.status == 200
        result = await response.json()
        assert len(result) == 1
        assert (
            result[0]["code"]
            == WorkflowErrorCode.WORKFLOW_ACTION_INPUTS_NOT_DEFINED.value
        )
        assert result[0]["workflow_module_id"] == action_id

        return workflow_id, action_id, action_output_id

    async def test_delete_middle_action(
        self,
        database_session: Session,
        project_authorization_service_mock: mock.MagicMock,
        authentication_service_mock: mock.MagicMock,
        api_client: TestClient,
    ) -> None:
        (
            workflow_id,
            action_middle_id,
            action_middle_output_id,
        ) = await self._create_simple_workflow(
            database_session=database_session, api_client=api_client
        )

        # add another action after the last one
        response = await api_client.post(
            f"/v2/workflows/{workflow_id}/modules",
            json={
                "module_definition_id": "mdf2",
                "parent_workflow_module_id": action_middle_id,
            },
        )
        assert response.status == 201
        result = await response.json()
        action_id = result["id"]
        if result["inputs"][0]["technical_name"] == "pin1":
            action_in1_id = result["inputs"][0]["id"]
            action_in2_id = result["inputs"][1]["id"]
            assert result["inputs"][1]["static_value"] == 42.5
        else:
            action_in1_id = result["inputs"][1]["id"]
            action_in2_id = result["inputs"][0]["id"]
            assert result["inputs"][0]["static_value"] == 42.5

        # configure the created action
        response = await api_client.put(
            f"/v2/workflows/{workflow_id}/modules/{action_id}",
            json={
                "custom_name": "action custom name",
                "inputs": [
                    {
                        "id": action_in1_id,
                        "reference_value": action_middle_output_id,
                    },
                    {
                        "id": action_in2_id,
                        "static_value": 190.190,
                    },
                ],
                "dynamic_outputs": [],
                "addons_inputs": [],
            },
        )
        assert response.status == 200

        # remove middle processor
        response = await api_client.delete(
            f"/workflows/{workflow_id}/modules/{action_middle_id}"
        )
        assert response.status == 200

        # Get the workflow to check that the action is gone
        response = await api_client.get(f"/v2/workflows/{workflow_id}")
        result = await response.json()
        assert len(result["modules"]) == 2

        # We should only have 1 error since the downstream action has no inputs defined
        response = await api_client.get(f"/workflows/{workflow_id}/errors")
        assert response.status == 200
        result = await response.json()
        WorkflowErrorSchema(many=True).load(result, unknown=RAISE, partial=False)
        assert len(result) == 1
        assert (
            result[0]["code"]
            == WorkflowErrorCode.WORKFLOW_ACTION_INPUTS_NOT_DEFINED.value
        )
        # Check that the ID of the downstream action is correct
        action_with_inputs_error_id = result[0]["workflow_module_id"]
        response = await api_client.get(f"/workflows/{workflow_id}")
        result = await response.json()
        downstream_action_matching_inputs_error_action = result["modules"][1]["id"]
        assert (
            action_with_inputs_error_id
            == downstream_action_matching_inputs_error_action
        )

    async def test_results(
        self,
        database_session: Session,
        project_authorization_service_mock: mock.MagicMock,
        authentication_service_mock: mock.MagicMock,
        api_client: TestClient,
    ) -> None:
        (
            workflow_id,
            action_middle_id,
            action_middle_output_id,
        ) = await self._create_simple_workflow(
            database_session=database_session, api_client=api_client
        )

        # Get the IO ID from the workflow
        response = await api_client.get(
            f"/v2/workflows/{workflow_id}",
        )
        assert response.status == 200
        workflow = await response.json()
        to_be_added_actions = []
        for m in workflow["modules"]:
            for i in m["inputs"]:
                to_be_added_actions.append(
                    {
                        "key": f"{m['name']}.{i['technical_name']}",
                        "workflow_module_io_id": i["id"],
                    }
                )
            for o in m["outputs"]:
                to_be_added_actions.append(
                    {
                        "key": f"{m['name']}.{o['technical_name']}",
                        "workflow_module_io_id": o["id"],
                    }
                )

        # There is no result yet
        response = await api_client.get(
            f"/v2/workflows/{workflow_id}/results",
        )
        assert response.status == 200
        result = await response.json()
        WorkflowResultSchema(many=True).load(result, unknown=RAISE, partial=False)
        assert len(result) == 0

        # add results with the same key
        response = await api_client.put(
            f"/v2/workflows/{workflow_id}/results",
            json={
                "workflow_results_to_add": [
                    {
                        "key": "a këÿ",
                        "workflow_module_io_id": action_middle_output_id,
                    },
                    {
                        "key": "a këÿ",
                        "workflow_module_io_id": action_middle_output_id,
                    },
                ]
            },
        )
        assert response.status == 400

        # add results with unknown IO ID
        response = await api_client.put(
            f"/v2/workflows/{workflow_id}/results",
            json={
                "workflow_results_to_add": [
                    {
                        "key": "a këÿ",
                        "workflow_module_io_id": action_middle_output_id,
                    },
                    {"key": "another këÿ", "workflow_module_io_id": "WRONG_ID"},
                ]
            },
        )
        assert response.status == 404

        # add some results
        response = await api_client.put(
            f"/v2/workflows/{workflow_id}/results",
            headers={"content-type": "application/json; charset=utf-8"},
            json={"workflow_results_to_add": to_be_added_actions},
        )
        assert response.status == 200
        result = await response.json()
        AddWorkflowResultResponseSchema(many=True).load(
            result, unknown=RAISE, partial=False
        )
        assert len(result) == len(to_be_added_actions)

        response = await api_client.get(
            f"/v2/workflows/{workflow_id}/results",
        )
        assert response.status == 200
        result = await response.json()

        WorkflowResultSchema(many=True).load(result, unknown=RAISE, partial=False)
        assert len(result) == len(to_be_added_actions)
