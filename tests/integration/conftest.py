import os
from unittest import mock

import pytest
import sqlalchemy
from aiohttp import test_utils, web
from sqlalchemy.orm import clear_mappers
from sqlalchemy.orm.session import _SessionClassMethods

from ryax.studio.application.authentication_service import AuthenticationService
from ryax.studio.application.project_authorization_service import (
    ProjectAuthorizationService,
)
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.auth.auth_token import AuthToken
from ryax.studio.infrastructure.api.setup import setup
from ryax.studio.infrastructure.database.engine import DatabaseEngine
from ryax.studio.infrastructure.database.metadata import metadata


@pytest.fixture()
async def app_container() -> ApplicationContainer:
    app_container = ApplicationContainer()
    yield app_container
    app_container.unwire()


@pytest.fixture()
def api_client(
    loop, app_container: ApplicationContainer, aiohttp_client
) -> test_utils.TestClient:
    app = web.Application()
    setup(app, app_container)
    return loop.run_until_complete(aiohttp_client(app))


@pytest.fixture()
def database_engine(app_container: ApplicationContainer) -> DatabaseEngine:
    database_url = os.environ["RYAX_DATASTORE"]
    engine = DatabaseEngine(database_url)
    engine.connect()
    app_container.database_engine.override(engine)
    yield engine
    clear_mappers()
    metadata.drop_all(engine.connection)
    engine.disconnect()


@pytest.fixture()
def database_session(database_engine: DatabaseEngine) -> _SessionClassMethods:
    session = database_engine.get_session()
    yield session
    sqlalchemy.orm.close_all_sessions()


@pytest.fixture()
def authentication_service_mock(app_container: ApplicationContainer):
    authentication_service_mock = mock.MagicMock(AuthenticationService)
    authentication_service_mock.check_access = mock.MagicMock(
        return_value=AuthToken(user_id="user")
    )
    app_container.authentication_service.override(authentication_service_mock)
    return authentication_service_mock


@pytest.fixture()
def project_authorization_service_mock(app_container: ApplicationContainer):
    project_authorization_service_mock = mock.MagicMock(ProjectAuthorizationService)
    project_authorization_service_mock.get_current_project = mock.AsyncMock(
        return_value="project_id"
    )
    app_container.project_authorization_service.override(
        project_authorization_service_mock
    )
    return project_authorization_service_mock
