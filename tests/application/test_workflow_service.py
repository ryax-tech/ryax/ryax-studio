from dataclasses import asdict
from datetime import datetime
from unittest import mock

import pytest

from ryax.studio.application.workflow_service import WorkflowService
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_repository import IActionRepository
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.domain.common.unit_of_work import IUnitOfWork
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_category import (
    WorkflowActionCategory,
)
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionInput,
)
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.entities.workflow_result import WorkflowResult
from ryax.studio.domain.workflow.views.workflow_error_view import WorkflowErrorView
from ryax.studio.domain.workflow.views.workflow_result_view import WorkflowResultView
from ryax.studio.domain.workflow.views.workflow_view import WorkflowExtendedView
from ryax.studio.domain.workflow.workflow_events import (
    WorkflowDeployEvent,
    WorkflowUndeployEvent,
)
from ryax.studio.domain.workflow.workflow_exceptions import (
    SchemaValidWorkflowInvalidException,
    WorkflowActionIONotFoundException,
    WorkflowNotDeletableException,
    WrongStatusFilterException,
)
from ryax.studio.domain.workflow.workflow_factory import WorkflowFactory
from ryax.studio.domain.workflow.workflow_repository import IWorkflowRepository
from ryax.studio.domain.workflow.workflow_values import (
    CopyWorkflowData,
    CreateWorkflowData,
    CreateWorkflowResultData,
    WorkflowDeploymentStatus,
    WorkflowErrorCode,
    WorkflowInformations,
    WorkflowValidityStatus,
)
from ryax.studio.infrastructure.filestore.filestore_service import FilestoreService
from ryax.studio.infrastructure.messaging.utils.publisher import MessagingPublisher


def _generate_workflow(
    id="workflow_1",
    description=None,
    validity_status=WorkflowValidityStatus.VALID,
    deployment_status=WorkflowDeploymentStatus.DEPLOYED,
    project="80f2c9fc-0377-4002-82f3-45bf55b96f7a",
) -> Workflow:
    return Workflow(
        id=id,
        name=id,
        description=description,
        validity_status=validity_status,
        deployment_status=deployment_status,
        project=project,
        owner="",
    )


def test_list_workflows(
    app_container: ApplicationContainer,
) -> None:
    """When fetching workflows endpoint should return list of workflows"""
    workflow_1 = Workflow(
        id="workflow_1_id",
        name="Workflow 1 name",
        description="Workflow 1 description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        project="1234",
        owner="",
        actions=[
            WorkflowAction(
                id="1",
                name="test1",
                kind=ActionKind.SOURCE,
                endpoint="endpoint1",
                technical_name="postgw",
                categories=[],
                action_id="1",
                description="",
                version="",
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="in0",
                        type=ActionIOType.FILE,
                        technical_name="file",
                        display_name="file0",
                        help="help",
                        file_value=WorkflowFile(
                            id="123",
                            name="toto",
                            extension="txt",
                            path="/empty",
                            size=1024 * 1024,
                        ),
                    ),
                    WorkflowActionInput(
                        optional=False,
                        id="in1",
                        type=ActionIOType.STRING,
                        technical_name="string",
                        display_name="string0",
                        help="help",
                        static_value="VALUE",
                    ),
                ],
            )
        ],
    )
    workflow_2 = Workflow(
        id="workflow_2_id",
        name="Workflow 2 name",
        description="Workflow 2 description",
        validity_status=WorkflowValidityStatus.INVALID,
        deployment_status=WorkflowDeploymentStatus.ERROR,
        project="project1",
        owner="",
        actions=[
            WorkflowAction(
                id="1",
                name="1",
                custom_name="custom",
                kind=ActionKind.SOURCE,
                endpoint="endpoint2",
                technical_name="123",
                action_id="1",
                description="",
                version="",
                categories=[WorkflowActionCategory(id="1", name="test")],
            ),
            WorkflowAction(
                id="2",
                name="2",
                custom_name="custom_test",
                kind=ActionKind.PROCESSOR,
                endpoint=None,
                technical_name="abc",
                action_id="2",
                description="",
                version="",
                categories=[
                    WorkflowActionCategory(id="1", name="test"),
                    WorkflowActionCategory(id="2", name="toto"),
                ],
            ),
        ],
    )

    workflow_repository_mock: IWorkflowRepository = app_container.workflow_repository()
    workflow_repository_mock.list_workflows = mock.MagicMock(
        return_value=[workflow_1, workflow_2]
    )
    workflow_service: WorkflowService = app_container.workflow_service()
    result = workflow_service.list_workflows("123")

    assert [asdict(wf) for wf in result] == [
        {
            "id": "workflow_1_id",
            "name": "Workflow 1 name",
            "description": "Workflow 1 description",
            "validity_status": WorkflowValidityStatus.VALID,
            "deployment_status": WorkflowDeploymentStatus.NONE,
            "has_form": True,
            "endpoint_prefix": "endpoint1",
            "categories": [],
            "parameters": [
                {"name": "file0", "value": "toto.txt (1.0MiB)"},
                {"name": "string0", "value": "VALUE"},
            ],
            "trigger": "test1",
        },
        {
            "id": "workflow_2_id",
            "name": "Workflow 2 name",
            "description": "Workflow 2 description",
            "validity_status": WorkflowValidityStatus.INVALID,
            "deployment_status": WorkflowDeploymentStatus.ERROR,
            "has_form": False,
            "endpoint_prefix": "endpoint2",
            "categories": ["test", "toto"],
            "parameters": [],
            "trigger": "custom",
        },
    ]


def test_list_workflows_with_incorrect_deployment_status_filter(
    app_container: ApplicationContainer,
) -> None:
    """List workflow with search should return searched workflows"""
    deployment_status_filter = "Dep"
    workflow_1 = _generate_workflow(id="workflow_1")
    workflow_2 = _generate_workflow(id="workflow_2")
    mock_project_id = "80f2c9fc-0377-4002-82f3-45bf55b96f7a"
    workflow_repository_mock: IWorkflowRepository = app_container.workflow_repository()
    workflow_repository_mock.list_workflows = mock.MagicMock(
        return_value=[workflow_1, workflow_2]
    )
    workflow_service: WorkflowService = app_container.workflow_service()
    with pytest.raises(WrongStatusFilterException):
        workflow_service.list_workflows(
            mock_project_id, deployment_status_filter_value=deployment_status_filter
        )
    workflow_repository_mock.list_workflows.assert_not_called()


def test_create_workflow(app_container: ApplicationContainer) -> None:
    """Create workflow should return workflow"""
    create_workflow_data = CreateWorkflowData(
        name="workflow name", description="workflow description"
    )
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="project_id",
    )
    workflow.check_all = mock.MagicMock()
    workflow_factory_mock: mock.MagicMock[
        WorkflowFactory
    ] = app_container.workflow_factory()
    mock_project_id = "80f2c9fc-0377-4002-82f3-45bf55b96f7a"
    workflow_factory_mock.from_create_workflow_data.return_value = workflow
    workflow_repository_mock: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_service: WorkflowService = app_container.workflow_service()
    assert (
        workflow_service.create_workflow(create_workflow_data, "user", mock_project_id)
        == workflow.id
    )
    workflow.check_all.assert_called_once()
    workflow_factory_mock.from_create_workflow_data.assert_called_once_with(
        create_workflow_data, "user", mock_project_id
    )
    workflow_repository_mock.add_workflow.assert_called_once_with(workflow)
    unit_of_work_mock.commit.assert_called_once()


async def test_copy_workflow(app_container: ApplicationContainer) -> None:
    """Copy workflow should return new workflow"""
    copy_workflow_data = CopyWorkflowData(
        from_workflow="from_workflow_id",
        name="workflow name",
        description="workflow description",
    )
    workflow = Workflow(
        id="workflow_original_id",
        name="Workflow_name",
        description="Workflow_description",
        owner="wfowner",
        project="project_id",
    )
    workflow_copy = Workflow(
        id="workflow_copy_id",
        name="Workflow_name",
        description="Workflow_description",
        owner="wfowner",
        project="project_id",
    )
    new_file_path = "new_file_path"
    workflow_file = WorkflowFile(
        id="file_name", name="file_name", path="file_path", extension="a", size=42.5
    )
    workflow.copy = mock.MagicMock()
    workflow.copy.return_value = workflow_copy
    workflow_copy.get_all_static_files = mock.MagicMock(return_value=[workflow_file])
    workflow_copy.check_all = mock.MagicMock()
    workflow_file.update_path = mock.MagicMock()

    workflow_repository_mock: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository_mock.get_workflow.return_value = workflow
    filestore_service: FilestoreService = app_container.filestore_service()
    filestore_service.generate_file_path = mock.MagicMock(return_value=new_file_path)
    filestore_service.copy_file = mock.AsyncMock()
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_service: WorkflowService = app_container.workflow_service()
    assert (
        await workflow_service.copy_workflow(copy_workflow_data, "user", "project_id")
        == workflow_copy.id
    )
    workflow_repository_mock.get_workflow.assert_called_once_with(
        copy_workflow_data.from_workflow
    )
    workflow.copy.assert_called_once_with(copy_workflow_data, "user")
    workflow_copy.get_all_static_files.assert_called_once()
    filestore_service.generate_file_path.assert_called_once_with(workflow_file.name)
    filestore_service.copy_file.assert_called_once_with(
        workflow_file.path, new_file_path
    )
    workflow_copy.check_all.assert_called_once()
    workflow_repository_mock.add_workflow.assert_called_once_with(workflow_copy)
    unit_of_work_mock.commit.assert_called_once()


def test_get_workflow_view_only(app_container: ApplicationContainer) -> None:
    """Get workflow should return workflow"""
    workflow_id = "workflow_id"
    workflow_view = _generate_workflow(id=workflow_id)
    workflow_repository_mock: IWorkflowRepository = app_container.workflow_repository()
    workflow_repository_mock.get_workflow_view = mock.MagicMock(
        return_value=workflow_view
    )
    workflow_service: WorkflowService = app_container.workflow_service()
    assert workflow_service.get_workflow(workflow_id) == workflow_view
    workflow_repository_mock.get_workflow_view.assert_called_with(workflow_id)


def test_get_workflow_extended(app_container: ApplicationContainer) -> None:
    """Get workflow should return workflow"""
    workflow_id = "workflow_id"
    workflow = Workflow(
        name="12",
        description="toto",
        project="1231",
        owner="",
        actions=[
            WorkflowAction(
                description="",
                name="123",
                action_id="123",
                technical_name="123",
                custom_name="",
                id="",
                position_x=1,
                position_y=1,
                kind=ActionKind.PROCESSOR,
                version="3",
                inputs=[
                    WorkflowActionInput(
                        optional=False,
                        id="id",
                        technical_name="toto",
                        type=ActionIOType.STRING,
                        display_name="display_name",
                        help="help",
                        static_value=None,
                        reference_value=WorkflowActionOutput(
                            optional=False,
                            id="id-ref",
                            technical_name="tata",
                            type=ActionIOType.INTEGER,
                            display_name="display_name",
                            help="help",
                        ),
                    )
                ],
                outputs=[
                    WorkflowActionOutput(
                        optional=False,
                        id="id",
                        technical_name="tata",
                        type=ActionIOType.INTEGER,
                        display_name="display_name",
                        help="help",
                    )
                ],
            )
        ],
    )
    workflow_repository_mock: IWorkflowRepository = app_container.workflow_repository()
    workflow_repository_mock.get_workflow = mock.MagicMock(return_value=workflow)
    action_repository_mock: IActionRepository = app_container.action_repository()
    action_1 = Action(
        id="action_1_id",
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="1.0",
        lockfile=b"",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        build_date=datetime.now(),
    )
    action_2 = Action(
        id="action_2_id",
        project="project_id",
        name="action2name",
        technical_name="action1name",
        version="1.2",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.now(),
    )
    action_repository_mock.get_action_versions = mock.MagicMock(
        return_value=[action_1, action_2]
    )
    workflow_service: WorkflowService = app_container.workflow_service()
    workflow_view = WorkflowExtendedView.from_workflow(workflow)
    workflow_view.actions[0].versions = [action_1, action_2]
    assert workflow_service.get_workflow_extended(workflow_id) == workflow_view
    workflow_repository_mock.get_workflow.assert_called_with(workflow_id)


def test_update_workflow(app_container: ApplicationContainer) -> None:
    """Update workflow should return updated workflow"""
    workflow_id = "workflow_id"
    workflow_informations = WorkflowInformations(
        name="Workflow name", description="Workflow description"
    )
    workflow = Workflow(
        id="workflow_id",
        name="Workflow_name",
        description="Workflow_description",
        owner="wfowner",
        project="project_id",
    )
    workflow.update_informations = mock.MagicMock()
    workflow_repository_mock: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository_mock.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_service: WorkflowService = app_container.workflow_service()
    workflow_service.update_workflow(workflow_id, workflow_informations, "user")
    workflow_repository_mock.get_workflow.assert_called_once_with(workflow_id)
    workflow.update_informations.assert_called_once_with(workflow_informations, "user")
    workflow_unit_mock.commit.assert_called()


async def test_delete_workflow(app_container: ApplicationContainer) -> None:
    """Delete workflow should return deleted workflow"""
    workflow_id = "workflow_id"
    workflow = Workflow(
        id="workflow_id",
        name="Workflow_name",
        description="Workflow_description",
        owner="wfowner",
        project="project_id",
    )
    all_static_files = [
        WorkflowFile(
            path="test/path", id="file_name", name="file_name", extension="a", size=42.5
        )
    ]
    workflow.is_deletable = mock.MagicMock(return_value=True)
    workflow.prepare_workflow_deleted_event = mock.MagicMock()
    workflow.get_all_static_files = mock.MagicMock(return_value=all_static_files)
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    filestore_service: FilestoreService = app_container.filestore_service()
    filestore_service.remove_file = mock.AsyncMock()
    workflow_service: WorkflowService = app_container.workflow_service()
    message_publisher: mock.AsyncMock[
        MessagingPublisher
    ] = app_container.messaging_publisher()
    message_publisher.publish = mock.AsyncMock()
    with mock.patch("asyncio.create_task", mock.MagicMock()):
        await workflow_service.delete_workflow(workflow_id, "authorization")
        workflow_repository.get_workflow.assert_called_once_with(workflow_id)
        workflow.is_deletable.assert_called_once()
        workflow_repository.delete_workflow.assert_called_once_with(workflow)
        workflow_unit_mock.commit.assert_called()
        filestore_service.remove_file.assert_called_once_with("test/path")
        message_publisher.publish.assert_called_once()
        workflow.prepare_workflow_deleted_event.assert_called_once()


async def test_delete_workflow_when_not_deletable(
    app_container: ApplicationContainer,
) -> None:
    """Delete workflow should raise error when not exists"""
    workflow_id = "workflow_id"
    workflow = Workflow(
        id="workflow_id",
        name="Workflow_name",
        description="Workflow_description",
        owner="wfowner",
        project="project_id",
    )
    workflow.is_deletable = mock.MagicMock(return_value=False)
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_service: WorkflowService = app_container.workflow_service()
    with pytest.raises(WorkflowNotDeletableException):
        await workflow_service.delete_workflow(workflow_id, "authorization")
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_deletable.assert_called_once()
    workflow_repository.delete_workflow.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_get_workflow_errors(app_container: ApplicationContainer) -> None:
    """Get workflow errors should return list of workflow errors"""
    workflow_id = "workflow_id"
    workflow_error_view_1 = WorkflowErrorView(
        id="id1", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
    )
    workflow_error_view_2 = WorkflowErrorView(
        id="id2", code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED
    )
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow_errors.return_value = [
        workflow_error_view_1,
        workflow_error_view_2,
    ]

    workflow_service: WorkflowService = app_container.workflow_service()
    assert workflow_service.get_workflow_errors(workflow_id) == [
        workflow_error_view_1,
        workflow_error_view_2,
    ]
    workflow_repository.check_workflow_exists.assert_called_with(workflow_id)
    workflow_repository.get_workflow_errors.assert_called_with(workflow_id)


async def test_import_workflow(app_container: ApplicationContainer) -> None:
    workflow_data = b"workflow_yaml_content"
    workflow_owner = "workflow_owner"
    project_id = "project_id"
    workflow = Workflow(
        id="workflow_id",
        project=project_id,
        name="wfname",
        description="wfdescr",
        owner="wfowner",
    )
    workflow.check_all = mock.MagicMock()

    action_1 = Action(
        id="action_1_id",
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.fromtimestamp(0),
    )
    action_2 = Action(
        id="action_2_id",
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.fromtimestamp(0),
    )

    workflow_files = [
        {"name": "video.mp4", "size": 12, "content": b"video_content"},
        {"name": "file.txt", "size": 1, "content": b"text_content"},
    ]
    workflow_file_metadata = {
        "video.mp4": {"size": 12, "path": "random_path"},
        "file.txt": {"size": 1, "path": "random_path_2"},
    }

    action_repository = app_container.action_repository()
    action_repository.get_action_by_name_version_project = mock.MagicMock(
        side_effect=[action_1, action_2]
    )
    workflow_repository_mock: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_import_service = app_container.packaging_import_service()
    workflow_import_service.load_workflow_package = mock.MagicMock()
    workflow_import_service.check_workflow_schema_validity = mock.MagicMock()
    workflow_import_service.get_actions_references = mock.MagicMock(
        return_value=[
            ("action_1_technical_name", "action_1_version"),
            ("action_2_technical_name", "action_2_version"),
        ]
    )
    workflow_import_service.get_workflow_io_files = mock.MagicMock(
        return_value=workflow_files
    )
    workflow_import_service.process = mock.MagicMock(return_value=workflow)

    filestore_service: FilestoreService = app_container.filestore_service()
    filestore_service.generate_file_path = mock.MagicMock(
        side_effect=["random_path", "random_path_2"]
    )
    filestore_service.upload_file = mock.AsyncMock()

    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow_unit_mock.commit = mock.MagicMock()

    workflow_service: WorkflowService = app_container.workflow_service()

    assert (
        await workflow_service.import_workflow(
            workflow_data, workflow_owner, project_id
        )
        == workflow.id
    )

    workflow_import_service.load_workflow_package.assert_called_with(workflow_data)
    workflow_import_service.check_workflow_schema_validity.assert_called()
    workflow_import_service.get_actions_references.assert_called()
    action_repository.get_action_by_name_version_project.assert_has_calls(
        [
            mock.call("action_1_technical_name", "action_1_version", "project_id"),
            mock.call("action_2_technical_name", "action_2_version", "project_id"),
        ]
    )
    workflow_import_service.get_workflow_io_files.assert_called_once()
    assert filestore_service.generate_file_path.call_count == 2
    workflow_import_service.process.assert_called_with(
        workflow_owner, [action_1, action_2], workflow_file_metadata, "project_id"
    )
    filestore_service.upload_file.assert_has_calls(
        [
            mock.call("random_path", b"video_content", 12),
            mock.call("random_path_2", b"text_content", 1),
        ]
    )
    workflow.check_all.assert_called()
    workflow_repository_mock.add_workflow.assert_called_once_with(workflow)
    workflow_unit_mock.commit.assert_called()


async def test_deploy_workflow(app_container: ApplicationContainer) -> None:
    """test workflow deploy"""
    workflow_id = "workflow_id"
    workflow = Workflow(
        id=workflow_id,
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="project_id",
    )
    workflow.events.append(
        WorkflowDeployEvent(
            workflow_id="workflow_id1",
            workflow_actions=[],
            workflow_project_id="workflow_project_id1",
            workflow_name="workflow_name1",
            results=[],
        )
    )
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow.deploy = mock.MagicMock()
    workflow.deploy.return_value = workflow
    message_publisher: mock.AsyncMock[
        MessagingPublisher
    ] = app_container.messaging_publisher()
    message_publisher.publish = mock.AsyncMock()
    workflow_service: WorkflowService = app_container.workflow_service()
    await workflow_service.deploy_workflow(workflow_id)
    workflow.deploy.assert_called_once()
    message_publisher.publish.assert_called_once_with(workflow.events)
    workflow_unit_mock.commit.assert_called_once()


def test_undeploy_workflow_success(app_container: ApplicationContainer) -> None:
    workflow_id = "workflow_id"
    workflow = Workflow(
        id=workflow_id,
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="project_id",
    )
    workflow.undeploy_success = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow_service: WorkflowService = app_container.workflow_service()
    workflow_service.undeploy_workflow_success(workflow_id)
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.undeploy_success.assert_called_once()
    workflow_unit_mock.commit.assert_called_once()


async def test_stop_deploy_workflow(app_container: ApplicationContainer) -> None:
    """test workflow stop deployment"""
    workflow_id = "workflow_id"
    workflow = Workflow(
        id=workflow_id,
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="project_id",
    )
    workflow.events.append(
        WorkflowUndeployEvent(
            workflow_id=workflow_id, workflow_project_id="project1", graceful=False
        )
    )
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow.undeploy = mock.MagicMock()
    workflow.undeploy.return_value = workflow
    message_publisher: mock.AsyncMock[
        MessagingPublisher
    ] = app_container.messaging_publisher()
    message_publisher.publish = mock.AsyncMock()
    workflow_service: WorkflowService = app_container.workflow_service()
    await workflow_service.stop_deploy_workflow(workflow_id, False)
    workflow.undeploy.assert_called_once()
    message_publisher.publish.assert_called_once_with(workflow.events)
    workflow_unit_mock.commit.assert_called_once()


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_workflow_id"))
async def test_export_workflow(app_container: ApplicationContainer) -> None:
    workflow_id = "workflow_id"
    workflow = Workflow(
        id=workflow_id,
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="project_id",
    )
    zip_file_path = "/123-workflow"
    file_data = b"file_content"
    workflow_input = WorkflowActionInput(
        optional=False,
        id="input_id",
        file_value=WorkflowFile(
            id="input_file_name",
            name="input_file_name",
            extension="mp4",
            path="/inputs/file_path",
            size=42.3,
        ),
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow_repository_mock: IWorkflowRepository = app_container.workflow_repository()
    workflow_repository_mock.get_workflow = mock.MagicMock(return_value=workflow)
    workflow_export_service_mock = app_container.packaging_export_service()
    workflow_export_service_mock.generate_workflow_zip_file = mock.MagicMock(
        return_value=zip_file_path
    )
    workflow_export_service_mock.export_workflow = mock.MagicMock()
    workflow.get_all_inputs = mock.MagicMock(return_value=[workflow_input])
    workflow_input.has_file_value = mock.MagicMock(return_value=True)
    filestore_service: FilestoreService = app_container.filestore_service()
    filestore_service.get_file = mock.AsyncMock(return_value=file_data)
    workflow_service: WorkflowService = app_container.workflow_service()
    assert await workflow_service.export_workflow(workflow_id) == zip_file_path
    workflow_repository_mock.get_workflow.assert_called_with(workflow_id)
    workflow_export_service_mock.generate_workflow_zip_file.assert_called_once()
    workflow.get_all_inputs.assert_called_once()
    filestore_service.get_file.assert_called_once_with(workflow_input.file_value.path)
    workflow_export_service_mock.export_workflow.assert_called_with(
        workflow, {workflow_input.file_value.path: file_data}
    )


def test_get_workflow_schema(app_container: ApplicationContainer) -> None:
    workflow_schema_mock = {"schema": "Test workflow schema"}
    workflow_import_service = app_container.packaging_import_service()
    workflow_import_service.get_schema = mock.PropertyMock(
        return_value=workflow_schema_mock
    )
    workflow_service: WorkflowService = app_container.workflow_service()
    assert workflow_service.get_workflow_schema() == workflow_schema_mock


def test_import_workflow_check_only_when_valid(
    app_container: ApplicationContainer,
) -> None:
    workflow_data = b"workflow_yaml_content"
    workflow_owner = "workflow_owner"
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="project_id",
    )
    workflow.check_all = mock.MagicMock()

    action_1 = Action(
        id="action_1_id",
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.fromtimestamp(0),
    )
    action_2 = Action(
        id="action_2_id",
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.fromtimestamp(0),
    )

    action_repository = app_container.action_repository()
    action_repository.get_action_by_name_version_project = mock.MagicMock(
        side_effect=[action_1, action_2]
    )
    workflow_repository_mock: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_import_service = app_container.packaging_import_service()
    workflow_import_service.load_workflow_schema = mock.MagicMock()
    workflow_import_service.check_workflow_schema_validity = mock.MagicMock()
    workflow_import_service.get_actions_references = mock.MagicMock(
        return_value=[
            ("action_1_technical_name", "action_1_version"),
            ("action_2_technical_name", "action_2_version"),
        ]
    )
    workflow_import_service.process = mock.MagicMock(return_value=workflow)

    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow_unit_mock.commit = mock.MagicMock()

    workflow_service: WorkflowService = app_container.workflow_service()

    workflow_service.validate_workflow_schema(
        workflow_data, workflow_owner, "project_id"
    )

    workflow_import_service.load_workflow_schema.assert_called_with(workflow_data)
    workflow_import_service.check_workflow_schema_validity.assert_called()
    workflow_import_service.get_actions_references.assert_called()
    action_repository.get_action_by_name_version_project.assert_has_calls(
        [
            mock.call("action_1_technical_name", "action_1_version", "project_id"),
            mock.call("action_2_technical_name", "action_2_version", "project_id"),
        ]
    )
    workflow_import_service.process.assert_called_with(
        workflow_owner, [action_1, action_2], {}, "project_id"
    )
    workflow.check_all.assert_called()
    workflow_repository_mock.add_workflow.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_import_workflow_check_only_when_invalid(
    app_container: ApplicationContainer,
) -> None:
    workflow_data = b"workflow_yaml_content"
    workflow_owner = "workflow_owner"
    workflow = Workflow(
        id="workflow_id",
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="project_id",
    )
    workflow.check_all = mock.MagicMock()

    action_1 = Action(
        id="action_1_id",
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.fromtimestamp(0),
    )
    action_2 = Action(
        id="action_2_id",
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.fromtimestamp(0),
    )

    action_repository = app_container.action_repository()
    action_repository.get_action_by_name_version_project = mock.MagicMock(
        side_effect=[action_1, action_2]
    )
    workflow_repository_mock: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_import_service = app_container.packaging_import_service()
    workflow_import_service.load_workflow_schema = mock.MagicMock()
    workflow_import_service.check_workflow_schema_validity = mock.MagicMock()
    workflow_import_service.get_actions_references = mock.MagicMock(
        return_value=[
            ("action_1_technical_name", "action_1_version"),
            ("action_2_technical_name", "action_2_version"),
        ]
    )
    workflow_import_service.process = mock.MagicMock(return_value=workflow)

    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow_unit_mock.commit = mock.MagicMock()

    workflow_service: WorkflowService = app_container.workflow_service()
    workflow.validity_status = WorkflowValidityStatus.INVALID
    with pytest.raises(SchemaValidWorkflowInvalidException):
        workflow_service.validate_workflow_schema(
            workflow_data, workflow_owner, "project_id"
        )
    workflow_import_service.load_workflow_schema.assert_called_with(workflow_data)
    workflow_import_service.check_workflow_schema_validity.assert_called()
    workflow_import_service.get_actions_references.assert_called()
    action_repository.get_action_by_name_version_project.assert_has_calls(
        [
            mock.call("action_1_technical_name", "action_1_version", "project_id"),
            mock.call("action_2_technical_name", "action_2_version", "project_id"),
        ]
    )
    workflow_import_service.process.assert_called_with(
        workflow_owner, [action_1, action_2], {}, "project_id"
    )
    workflow.check_all.assert_called()
    workflow_repository_mock.add_workflow.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_overwrite_workflow_results(
    app_container: ApplicationContainer,
) -> None:
    workflow = Workflow(
        id="foo", name="name", description="desc", owner="owner", project="project"
    )
    workflow.get_all_inputs = mock.MagicMock(
        return_value=[
            WorkflowActionInput(
                optional=False,
                id="input_id",
                technical_name="tech",
                display_name="dis",
                help="help",
                type=ActionIOType.STRING,
            )
        ]
    )
    workflow_result = CreateWorkflowResultData(
        key="key", workflow_id="foo", workflow_action_io_id="input_id"
    )
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow_unit_mock.commit = mock.MagicMock()

    workflow_service: WorkflowService = app_container.workflow_service()

    workflow_repository_mock: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository_mock.get_workflow.return_value = workflow
    with mock.patch("uuid.uuid4", mock.MagicMock(return_value="baz")):
        assert workflow_service.overwrite_workflow_results(
            workflow.id, [workflow_result]
        ) == [
            WorkflowResult(
                id="baz",
                workflow_id=workflow.id,
                workflow_action_input_id="input_id",
                workflow_action_output_id=None,
                key=workflow_result.key,
            )
        ]
        workflow_repository_mock.get_workflow.assert_called_once_with(workflow.id)
        workflow_unit_mock.commit.assert_called_once()
        assert workflow.results == [
            WorkflowResult(
                id="baz",
                workflow_id=workflow.id,
                workflow_action_input_id="input_id",
                workflow_action_output_id=None,
                key=workflow_result.key,
            )
        ]


def test_overwrite_workflow_results_when_workflow_io_not_found(
    app_container: ApplicationContainer,
) -> None:
    workflow_id = "id"
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow_unit_mock.commit = mock.MagicMock()

    workflow_result = CreateWorkflowResultData(
        key="key", workflow_id="foo", workflow_action_io_id="io_id_not_found"
    )
    workflow_service: WorkflowService = app_container.workflow_service()

    workflow_repository_mock: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow = Workflow(
        id="foo", name="name", description="desc", owner="owner", project="project"
    )
    workflow.get_all_inputs = mock.MagicMock(return_value=[])
    workflow.get_all_outputs = mock.MagicMock(return_value=[])
    workflow_repository_mock.get_workflow.return_value = workflow
    with pytest.raises(WorkflowActionIONotFoundException):
        workflow_service.overwrite_workflow_results(workflow_id, [workflow_result])
        workflow_repository_mock.get_workflow.assert_called_once_with(workflow_id)
        workflow.get_all_inputs.assert_called_once()
        workflow.get_all_outputs.assert_called_once()


def test_list_workflow_results(
    app_container: ApplicationContainer,
) -> None:
    workflow_service: WorkflowService = app_container.workflow_service()

    workflow_repository_mock: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_results = [
        WorkflowResult(
            id="foo",
            workflow_id="foo",
            key="to_be_deleted",
            workflow_action_input_id="wfmioid",
            workflow_action_output_id=None,
        )
    ]
    workflow = Workflow(
        id="foo",
        name="name",
        description="desc",
        owner="owner",
        project="project",
        actions=[
            WorkflowAction(
                id="Action",
                name="Action name",
                outputs=[
                    mock.MagicMock(
                        WorkflowActionInput,
                        display_name="FooBar",
                        id="wfmioid",
                        type=ActionIOType.STRING,
                    )
                ],
                inputs=[],
                version="1.0",
                workflow_id="test",
                endpoint=None,
                action_id="mod_id",
                technical_name="",
                description="",
                kind=ActionKind.PROCESSOR,
            )
        ],
        results=workflow_results,
    )
    workflow_repository_mock.get_workflow.return_value = workflow

    assert workflow_service.list_workflow_results(workflow_id="foo") == [
        WorkflowResultView(
            key="to_be_deleted",
            workflow_action_io_id="wfmioid",
            description='Value from input "FooBar (string)" of action "Action name (1)"',
            type=ActionIOType.STRING,
        )
    ]


def test_overwrite_workflow_endpoint(
    app_container: ApplicationContainer,
):
    workflow_service: WorkflowService = app_container.workflow_service()

    workflow_repository_mock: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_id = "foo"
    new_path = "/path"
    workflow = Workflow(
        id=workflow_id,
        name="name",
        description="desc",
        owner="owner",
        project="project",
        actions=[
            WorkflowAction(
                description="",
                name="123",
                action_id="123",
                technical_name="123",
                custom_name="",
                id="",
                position_x=1,
                position_y=1,
                kind=ActionKind.SOURCE,
                version="3",
            )
        ],
    )
    workflow_repository_mock.get_workflow.return_value = workflow
    assert (
        workflow_service.overwrite_workflow_endpoint(workflow_id, new_path) == new_path
    )
    workflow_repository_mock.get_workflow.assert_called_once_with(workflow_id)
    workflow_repository_mock.check_if_endpoint_exists.assert_called_once_with(
        new_path, project_id="project"
    )
