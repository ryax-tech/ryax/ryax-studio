from unittest import mock

import pytest

from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action_factory import ActionFactory
from ryax.studio.domain.action.action_repository import IActionRepository
from ryax.studio.domain.common.unit_of_work import IUnitOfWork
from ryax.studio.domain.project_variables.project_variable_repository import (
    IUserObjectRepository,
)
from ryax.studio.domain.services import ISecurityService
from ryax.studio.domain.services.filestore_service import IFilestoreService
from ryax.studio.domain.workflow.services.workflow_export_service import (
    IWorkflowExportService,
)
from ryax.studio.domain.workflow.services.workflow_import_service import (
    IWorkflowImportService,
)
from ryax.studio.domain.workflow.workflow_factory import WorkflowFactory
from ryax.studio.domain.workflow.workflow_repository import IWorkflowRepository


@pytest.fixture(scope="function")
def app_container():
    app_container = ApplicationContainer()
    security_service = mock.MagicMock(ISecurityService)
    app_container.jwt_service.override(security_service)
    workflow_factory = mock.MagicMock(WorkflowFactory)
    app_container.workflow_factory.override(workflow_factory)

    module_factory = mock.MagicMock(ActionFactory)
    app_container.action_factory.override(module_factory)
    module_repository = mock.MagicMock(IActionRepository)
    app_container.action_repository.override(module_repository)
    workflow_repository = mock.MagicMock(IWorkflowRepository)
    app_container.workflow_repository.override(workflow_repository)
    project_variable_repository = mock.MagicMock(IUserObjectRepository)
    app_container.project_variable_repository.override(project_variable_repository)

    unit_of_work: IUnitOfWork = mock.MagicMock(IUnitOfWork)
    unit_of_work.workflows = workflow_repository
    unit_of_work.actions = module_repository
    unit_of_work.project_variables = project_variable_repository
    app_container.unit_of_work.override(unit_of_work)

    workflow_export_service = mock.MagicMock(IWorkflowExportService)
    app_container.packaging_export_service.override(workflow_export_service)
    workflow_import_service: IWorkflowImportService = mock.MagicMock(
        IWorkflowImportService
    )
    app_container.packaging_import_service.override(workflow_import_service)

    filestore_service_mock = mock.MagicMock(IFilestoreService)
    app_container.filestore_service.override(filestore_service_mock)
    return app_container
