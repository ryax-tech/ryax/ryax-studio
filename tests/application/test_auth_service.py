from unittest import mock

from ryax.studio.application.authentication_service import AuthenticationService
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.auth.auth_token import AuthToken
from ryax.studio.domain.services import ISecurityService


def test_check_access(app_container: ApplicationContainer):
    auth_service: AuthenticationService = app_container.authentication_service()
    security_service_mock: mock.MagicMock[
        ISecurityService
    ] = app_container.jwt_service()
    security_service_mock.get_auth_token.return_value = AuthToken(user_id="user")
    result = auth_service.check_access("token")
    assert result == AuthToken(user_id="user")


def test_check_access_with_empty_token(app_container: ApplicationContainer):
    auth_service: AuthenticationService = app_container.authentication_service()
    security_service_mock: mock.MagicMock[
        ISecurityService
    ] = app_container.jwt_service()
    security_service_mock.get_auth_token.return_value = AuthToken(user_id="user")
    result = auth_service.check_access("")
    assert not result


def test_check_access_fail(app_container: ApplicationContainer):
    auth_service: AuthenticationService = app_container.authentication_service()
    security_service_mock: mock.MagicMock[
        ISecurityService
    ] = app_container.jwt_service()
    security_service_mock.get_auth_token.return_value = None
    result = auth_service.check_access("token")
    assert not result
