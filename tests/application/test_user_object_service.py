from unittest import mock

from ryax.studio.application.project_variable_service import ProjectVariableService
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.project_variables.project_variable_entities import (
    ProjectVariable,
    ProjectVariableTransactionData,
)
from ryax.studio.domain.project_variables.project_variable_repository import (
    IUserObjectRepository,
)


def test_list_project_variables(
    app_container: ApplicationContainer,
) -> None:
    project_variables = [
        ProjectVariable(
            id="id1",
            name="name1",
            value=123,
            type=ActionIOType.INTEGER,
            project_id="project_id",
        ),
        ProjectVariable(
            id="id2",
            name="name2",
            value="456",
            type=ActionIOType.STRING,
            project_id="project_id",
        ),
    ]

    project_variable_repository_mock: IUserObjectRepository = (
        app_container.project_variable_repository()
    )
    project_variable_repository_mock.list = mock.MagicMock(
        return_value=project_variables
    )
    project_variable_service: ProjectVariableService = (
        app_container.project_variable_service()
    )
    result = project_variable_service.list("project_id", None)
    assert result == project_variables


def test_list_project_variables_with_type_filter(
    app_container: ApplicationContainer,
) -> None:
    project_variables = [
        ProjectVariable(
            id="id1",
            name="name1",
            value=123,
            type=ActionIOType.INTEGER,
            project_id="project_id",
        ),
        ProjectVariable(
            id="id2",
            name="name2",
            value="456",
            type=ActionIOType.STRING,
            project_id="project_id",
        ),
    ]

    project_variable_repository_mock: IUserObjectRepository = (
        app_container.project_variable_repository()
    )
    project_variable_repository_mock.list = mock.MagicMock(
        return_value=project_variables[0]
    )
    project_variable_service: ProjectVariableService = (
        app_container.project_variable_service()
    )
    result = project_variable_service.list("project_id", "Integer")
    project_variable_repository_mock.list.assert_called_once_with(
        "project_id", "Integer"
    )
    assert result == project_variables[0]


def test_get_project_variable(
    app_container: ApplicationContainer,
) -> None:
    project_variable = ProjectVariable(
        id="project_variable_id",
        name="name1",
        value=123,
        type=ActionIOType.INTEGER,
        project_id="project_id",
    )
    project_variable_repository_mock: IUserObjectRepository = (
        app_container.project_variable_repository()
    )
    project_variable_repository_mock.get = mock.MagicMock(return_value=project_variable)
    project_variable_service: ProjectVariableService = (
        app_container.project_variable_service()
    )
    result = project_variable_service.get(project_variable.id, "project_id")
    assert result == project_variable


@mock.patch(
    "ryax.studio.domain.project_variables.project_variable_entities.get_random_id",
    return_value="new_project_variable_id",
)
def test_add_project_variable(_, app_container: ApplicationContainer) -> None:
    new_id = "var-new_project_variable_id"
    project_variable_data = ProjectVariableTransactionData(
        name="name",
        value="val",
        type="string",
        project_id="project_id",
        description="desc",
    )
    project_variable_repository_mock: IUserObjectRepository = (
        app_container.project_variable_repository()
    )
    project_variable_repository_mock.add = mock.MagicMock(return_value=new_id)
    project_variable_service: ProjectVariableService = (
        app_container.project_variable_service()
    )
    result = project_variable_service.add(project_variable_data)
    assert result == new_id


async def test_delete_project_variable(
    app_container: ApplicationContainer,
) -> None:
    project_variable = ProjectVariable(
        id="id1",
        name="name1",
        value=123,
        type=ActionIOType.INTEGER,
        project_id="project_id",
    )
    project_variable_repository_mock: IUserObjectRepository = (
        app_container.project_variable_repository()
    )
    project_variable_repository_mock.get = mock.MagicMock(return_value=project_variable)
    project_variable_repository_mock.delete = mock.MagicMock()
    project_variable_repository_mock.get_linked_workflows = mock.MagicMock(
        return_value=[]
    )
    project_variable_service: ProjectVariableService = (
        app_container.project_variable_service()
    )
    await project_variable_service.delete(project_variable.id, "project_id")
    project_variable_repository_mock.get.assert_called_once_with(
        project_variable.id, project_variable.project_id
    )
    project_variable_repository_mock.delete.assert_called_once_with(project_variable)


async def test_update_project_variable(app_container: ApplicationContainer) -> None:
    project_variable = ProjectVariable(
        id="id1",
        name="name1",
        value=123,
        type=ActionIOType.INTEGER,
        project_id="project_id",
    )
    project_variable_data = ProjectVariableTransactionData(
        name="name",
        value="val",
        type="integer",
        project_id="project_id",
        description="desc",
    )
    project_variable_repository_mock: IUserObjectRepository = (
        app_container.project_variable_repository()
    )
    project_variable_repository_mock.get = mock.MagicMock(return_value=project_variable)
    project_variable.update = mock.MagicMock()
    project_variable_service: ProjectVariableService = (
        app_container.project_variable_service()
    )
    await project_variable_service.update(
        project_variable.id, project_variable_data, project_variable.project_id
    )
    project_variable_repository_mock.get.assert_called_once_with(
        project_variable.id, project_variable.project_id
    )
    project_variable.update.assert_called_once_with(project_variable_data)
