import io
from datetime import datetime
from unittest import mock

import pytest

from ryax.studio.application.workflow_action_service import WorkflowActionService
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_io import ActionIO
from ryax.studio.domain.action.action_repository import IActionRepository
from ryax.studio.domain.action.action_values import (
    ActionIOType,
    ActionKind,
    DynamicOutputOrigin,
)
from ryax.studio.domain.common.unit_of_work import IUnitOfWork
from ryax.studio.domain.services.filestore_service import IFilestoreService
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionInput,
)
from ryax.studio.domain.workflow.entities.workflow_action_link import WorkflowActionLink
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.views.workflow_action_input_view import (
    WorkflowActionInputView,
)
from ryax.studio.domain.workflow.views.workflow_action_output_view import (
    WorkflowActionOutputView,
)
from ryax.studio.domain.workflow.workflow_exceptions import (
    ActionNotFoundException,
    WorkflowActionNotFoundException,
    WorkflowNotFoundException,
    WorkflowNotUpdatableException,
)
from ryax.studio.domain.workflow.workflow_repository import IWorkflowRepository
from ryax.studio.domain.workflow.workflow_values import (
    AddWorkflowAction,
    AddWorkflowActionDynamicOutput,
    AddWorkflowActionLink,
    AddWorkflowActionWithLink,
    UpdateAllWorkflowActionIOData,
    UpdateWorkflowAction,
    UpdateWorkflowActionDynamicOutput,
    UpdateWorkflowActionInputValue,
    UpdateWorkflowActionLinks,
)


@pytest.fixture
def new_workflow_action():
    new_workflow_action = WorkflowAction(
        id="1234",
        name="new wf action",
        technical_name="new wf action",
        description="new wf action",
        version="1",
        kind=ActionKind.PROCESSOR,
        position_x=0,
        position_y=0,
        action_id="something",
        inputs=[
            WorkflowActionInput(
                optional=False,
                id="wfmin1",
                type=ActionIOType.FILE,
                technical_name="technical_name",
                display_name="display_name",
                help="help",
            )
        ],
        outputs=[
            WorkflowActionOutput(
                optional=False,
                id="wfmout1",
                type=ActionIOType.FILE,
                technical_name="technical_name",
                display_name="display_name",
                help="help",
            )
        ],
        workflow_id="Workflow 1",
    )
    return new_workflow_action


@pytest.fixture
def workflow(new_workflow_action):
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow.add_action = mock.MagicMock(return_value=new_workflow_action)
    workflow.is_updatable = mock.MagicMock(return_value=True)
    workflow.check_all = mock.MagicMock()
    return workflow


@pytest.fixture
def workflow_repository(app_container, workflow):
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    return workflow_repository


@pytest.fixture
def action() -> Action:
    action = Action(
        id="CHANGEME",
        name="action 1",
        description="action description",
        kind=ActionKind.PROCESSOR,
        project="project_id",
        technical_name="action1name",
        version="action1name",
        lockfile=b"",
        build_date=datetime.fromtimestamp(0),
        inputs=[
            ActionIO(
                id="id",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
                optional=False,
            )
        ],
        outputs=[
            ActionIO(
                optional=False,
                id="id",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
            )
        ],
    )
    return action


@pytest.fixture
def action_repository(app_container, action):
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository.get_action.return_value = action
    action_repository.get_action_versions = mock.MagicMock(
        return_value=mock.MagicMock()
    )
    return action_repository


@pytest.fixture
def unit_of_work(app_container):
    unit_of_work: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    return unit_of_work


@pytest.fixture
def workflow_action_service(app_container):
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    return workflow_action_service


@pytest.fixture
def addons(app_container):
    return app_container.addons_service().load()


def test_add_workflow_action(
    workflow_repository,
    action_repository,
    unit_of_work,
    workflow_action_service,
    workflow,
    action,
    addons,
):
    """Adding action to a workflow should work"""
    workflow_id = "workflow_id"
    data = AddWorkflowAction(
        action_id="action_id",
        custom_name="action custom name",
        position_x=0,
        position_y=0,
    )
    action.id = data.action_id
    workflow_action_service.add_workflow_action(workflow_id, data)

    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    action_repository.get_action.assert_called_once_with(data.action_id)
    workflow.add_action.assert_called_once_with(action, data, addons)
    workflow.is_updatable.assert_called_once()
    workflow.check_all.assert_called_once()
    unit_of_work.commit.assert_called()


@pytest.mark.parametrize(
    "data",
    [
        AddWorkflowActionWithLink(
            action_definition_id="action_id",
            parent_workflow_action_id="parent_id",
        ),
        AddWorkflowActionWithLink(
            action_definition_id="action_id",
            parent_workflow_action_id="parent_id",
            replace_workflow_action_id="replace_id",
        ),
        AddWorkflowActionWithLink(
            action_definition_id="action_id",
        ),
    ],
)
def test_add_workflow_action_with_link(
    workflow_repository,
    action_repository,
    unit_of_work,
    workflow_action_service,
    workflow,
    action,
    data,
    new_workflow_action,
    addons,
):
    """Adding action to a workflow should work"""
    workflow_id = "workflow_id"
    add_workflow_action = AddWorkflowAction(
        action_id=data.action_definition_id,
        custom_name=action.name,
        position_x=None,
        position_y=None,
    )
    replaced_action = Action(
        id="old action id",
        name="action 2",
        description="action description",
        lockfile=b"",
        kind=ActionKind.PROCESSOR,
        project="project_id",
        technical_name="action2name",
        version="action2name",
        build_date=datetime.fromtimestamp(0),
        inputs=[
            ActionIO(
                optional=False,
                id="idin1",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
            )
        ],
        outputs=[
            ActionIO(
                optional=False,
                id="idout1",
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING,
            )
        ],
    )
    action.id = data.action_definition_id

    if data.replace_workflow_action_id is None:
        workflow.add_action_link = mock.MagicMock()
    else:
        workflow.get_action = mock.MagicMock(return_value=replaced_action)
        workflow.transfer_actions_references = mock.MagicMock()
        workflow.transfer_workflow_action_links = mock.MagicMock()
        workflow.delete_action = mock.MagicMock()

    new_workflow_action = workflow_action_service.add_workflow_action_with_link(
        workflow_id, data
    )

    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_called_once()
    action_repository.get_action.assert_called_once_with(data.action_definition_id)
    workflow.add_action.assert_called_once_with(action, add_workflow_action, addons)
    workflow.check_all.assert_called_once()
    unit_of_work.commit.assert_called()

    if data.replace_workflow_action_id is None:
        if data.parent_workflow_action_id is None:
            workflow.add_action_link.assert_not_called()
        else:
            workflow.add_action_link.assert_called_once_with(
                upstream_action_id=data.parent_workflow_action_id,
                downstream_action_id=new_workflow_action.id,
            )
    else:
        workflow.get_action.assert_called_once_with(data.replace_workflow_action_id)
        workflow.transfer_actions_references.assert_called_once()
        workflow.transfer_workflow_action_links.assert_called_once()
        workflow.delete_action.assert_called_once_with(replaced_action.id)

    action_repository.get_action_versions.assert_called_once_with(
        new_workflow_action.action_id
    )
    assert len(new_workflow_action.inputs) == 1
    assert new_workflow_action.inputs[0].id == "wfmin1"
    assert len(new_workflow_action.outputs) == 1
    assert new_workflow_action.outputs[0].id == "wfmout1"


def test_add_workflow_action_with_link_when_action_not_exists(
    workflow_repository,
    action_repository,
    unit_of_work,
    workflow_action_service,
    workflow,
):
    """If the action does not exists, should raise an error"""
    workflow_id = "workflow_id"
    data = AddWorkflowActionWithLink(
        action_definition_id="action_id",
        parent_workflow_action_id="parent_id",
    )
    action_repository.get_action.return_value = None

    with pytest.raises(ActionNotFoundException):
        workflow_action_service.add_workflow_action_with_link(workflow_id, data)
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_called_once()
    action_repository.get_action.assert_called_once_with(data.action_definition_id)
    workflow.add_action.assert_not_called()
    unit_of_work.commit.assert_not_called()


def test_add_workflow_action_with_link_when_workflow_not_exists(
    workflow_repository, action_repository, unit_of_work, workflow_action_service
):
    """If the workflow does not exists, should raise an error"""
    workflow_id = "workflow_id"
    data = AddWorkflowActionWithLink(
        action_definition_id="action_id",
        parent_workflow_action_id="parent_id",
    )
    workflow_repository.get_workflow.return_value = None

    with pytest.raises(WorkflowNotFoundException):
        workflow_action_service.add_workflow_action_with_link(workflow_id, data)
    action_repository.get_action.assert_not_called()
    unit_of_work.commit.assert_not_called()


def test_add_workflow_action_with_link_when_workflow_action_not_exists(
    workflow_repository,
    action_repository,
    unit_of_work,
    workflow_action_service,
    workflow,
    action,
    addons,
):
    """If the replaced workflow action does not exists, should raise an error"""
    workflow_id = "workflow_id"
    data = AddWorkflowActionWithLink(
        action_definition_id="action_id",
        parent_workflow_action_id="parent_id",
        replace_workflow_action_id="replace_id",
    )
    add_workflow_action = AddWorkflowAction(
        action_id=data.action_definition_id,
        custom_name=action.name,
        position_x=None,
        position_y=None,
    )
    action.id = data.action_definition_id

    workflow.get_action = mock.MagicMock(side_effect=WorkflowActionNotFoundException())
    workflow.transfer_actions_references = mock.MagicMock()
    workflow.transfer_workflow_action_links = mock.MagicMock()
    workflow.delete_action = mock.MagicMock()

    with pytest.raises(WorkflowActionNotFoundException):
        workflow_action_service.add_workflow_action_with_link(workflow_id, data)

    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_called_once()
    action_repository.get_action.assert_called_once_with(data.action_definition_id)
    workflow.add_action.assert_called_once_with(action, add_workflow_action, addons)

    workflow.get_action.assert_called_once_with(data.replace_workflow_action_id)
    workflow.transfer_actions_references.assert_not_called()
    workflow.transfer_workflow_action_links.assert_not_called()
    workflow.delete_action.assert_not_called()

    workflow.check_all.assert_not_called()
    unit_of_work.commit.assert_not_called()


def test_add_workflow_action_when_action_not_exists(
    app_container: ApplicationContainer,
):
    """Adding action to a workflow should work"""
    workflow_id = "workflow_id"
    data = AddWorkflowAction(action_id="action_id")
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow.is_updatable = mock.MagicMock(return_value=True)
    workflow.add_action = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository.get_action.return_value = None
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(ActionNotFoundException):
        workflow_action_service.add_workflow_action(workflow_id, data)
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_called_once()
    action_repository.get_action.assert_called_once_with(data.action_id)
    workflow.add_action.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_add_workflow_action_when_workflow_not_exists(
    app_container: ApplicationContainer,
):
    """Adding action to a workflow should raise error when workflow not exists"""
    workflow_id = "workflow_id"
    data = AddWorkflowAction(action_id="action_id")
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow.is_updatable = mock.MagicMock()
    workflow.add_action = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = None
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotFoundException):
        workflow_action_service.add_workflow_action(workflow_id, data)
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_not_called()
    workflow.add_action.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_add_workflow_action_when_workflow_not_updatable(
    app_container: ApplicationContainer,
):
    """Adding action to a workflow should raise error when workflow not updatable"""
    workflow_id = "workflow_id"
    data = AddWorkflowAction(action_id="action_id")
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow.is_updatable = mock.MagicMock(return_value=False)
    workflow.add_action = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotUpdatableException):
        workflow_action_service.add_workflow_action(workflow_id, data)
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_called_once()
    workflow.add_action.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


async def test_delete_workflow_action(app_container: ApplicationContainer):
    """Removing a action from a workflow"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    static_files = [
        WorkflowFile(
            id="file1", path="path/test", name="file_name", extension="a", size=42.5
        )
    ]
    workflow.is_updatable = mock.MagicMock(return_value=True)
    workflow.delete_action = mock.MagicMock()
    workflow.check_all = mock.MagicMock()
    workflow.get_action_static_files = mock.MagicMock(return_value=static_files)
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    filestore_service: mock.MagicMock[
        IFilestoreService
    ] = app_container.filestore_service()
    filestore_service.remove_file = mock.AsyncMock()
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    await workflow_action_service.delete_workflow_action(
        workflow_id, workflow_action_id
    )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_called_once()
    workflow.get_action_static_files.assert_called_once_with(workflow_action_id)
    filestore_service.remove_file.assert_called_once_with("path/test")
    workflow.delete_action.assert_called_with(workflow_action_id)
    workflow.check_all.assert_called()
    workflow_unit_mock.commit.assert_called()


async def test_delete_workflow_action_when_workflow_not_exists(
    app_container: ApplicationContainer,
):
    """Removing a action to a workflow should raise an error if workflow not exists"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow.is_updatable = mock.MagicMock()
    workflow.delete_action = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = None
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotFoundException):
        await workflow_action_service.delete_workflow_action(
            workflow_id, workflow_action_id
        )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_not_called()
    workflow.delete_action.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


async def test_delete_workflow_action_when_workflow_not_updatable(
    app_container: ApplicationContainer,
):
    """Removing a action to a workflow should raise an error if workflow not updatable"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow.is_updatable = mock.MagicMock(return_value=False)
    workflow.delete_action = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotUpdatableException):
        await workflow_action_service.delete_workflow_action(
            workflow_id, workflow_action_id
        )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_called_once()
    workflow.delete_action.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_update_workflow_action(app_container: ApplicationContainer):
    """Updating a action in a workflow"""
    workflow_id = "workflow_id"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow_action_id = "workflow_action_id"
    data = UpdateWorkflowAction(
        custom_name="action custom name", position_x=0, position_y=1
    )
    # Mocks
    workflow.update_action = mock.MagicMock()
    workflow.is_updatable = mock.MagicMock(return_value=True)
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    workflow_action_service.update_workflow_action(
        workflow_id, workflow_action_id, data
    )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.update_action.assert_called_once()
    workflow.update_action.assert_called_once_with(workflow_action_id, data)
    workflow_unit_mock.commit.assert_called()


def test_update_workflow_action_when_workflow_not_found(
    app_container: ApplicationContainer,
):
    """Updating a action in a workflow should return error when workflow is not found"""
    workflow_id = "workflow_id"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow_action_id = "workflow_action_id"
    data = UpdateWorkflowAction(
        custom_name="action custom name", position_x=0, position_y=1
    )
    # Mocks
    workflow.is_updatable = mock.MagicMock()
    workflow.update_action = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = None
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotFoundException):
        workflow_action_service.update_workflow_action(
            workflow_id, workflow_action_id, data
        )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_not_called()
    workflow.update_action.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_update_workflow_action_when_workflow_not_updatable(
    app_container: ApplicationContainer,
):
    """Updating a action in a workflow should return error when workflow is not updatable"""
    workflow_id = "workflow_id"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow_action_id = "workflow_action_id"
    data = UpdateWorkflowAction(
        custom_name="action custom name", position_x=0, position_y=1
    )
    # Mocks
    workflow.update_action = mock.MagicMock()
    workflow.is_updatable = mock.MagicMock(return_value=False)
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotUpdatableException):
        workflow_action_service.update_workflow_action(
            workflow_id, workflow_action_id, data
        )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_called_once()
    workflow.update_action.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_change_workflow_action_version(app_container: ApplicationContainer, addons):
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    action_id = "action_id"
    workflow = Workflow(
        id=workflow_id,
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    action = Action(
        id=action_id,
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.fromtimestamp(0),
    )
    workflow_action = WorkflowAction(
        id=workflow_action_id,
        custom_name="custom name",
        position_x=5,
        position_y=5,
        action_id="old_action",
        name="wfmname",
        technical_name="wfmname",
        description="wfmname",
        version="wfmname",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=False,
        endpoint="toto",
    )
    new_workflow_action = WorkflowAction(
        id="new_id",
        name="wfmname",
        technical_name="wfmname",
        description="wfmname",
        version="wfmname",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=False,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository.get_action.return_value = action
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow.get_action = mock.MagicMock(return_value=workflow_action)
    workflow.delete_action = mock.MagicMock()
    workflow.add_action = mock.MagicMock(return_value=new_workflow_action)
    workflow.check_all = mock.MagicMock()
    new_workflow_action.patch_workflow_action_ios_values = mock.MagicMock()
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    assert workflow_action_service.change_workflow_action_version(
        workflow_id, workflow_action_id, action_id
    ) == ("new_id", [])
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    action_repository.get_action.assert_called_once_with(action_id)
    workflow.get_action.assert_called_once_with(workflow_action_id)
    workflow.delete_action.assert_called_once_with(workflow_action_id)
    workflow.add_action.assert_called_once_with(
        action,
        AddWorkflowAction(
            action_id=action.id,
            custom_name=workflow_action.custom_name,
            position_x=workflow_action.position_x,
            position_y=workflow_action.position_y,
        ),
        addons,
    )
    new_workflow_action.patch_workflow_action_ios_values.assert_called_once_with(
        workflow_action
    )
    workflow.check_all.assert_called_once()
    workflow_unit_mock.commit.assert_called()
    assert new_workflow_action.endpoint == workflow_action.endpoint


def test_change_workflow_action_version_when_workflow_not_found(
    app_container: ApplicationContainer,
):
    workflow_id = "workflow_id"
    workflow = Workflow(
        id=workflow_id,
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow = mock.MagicMock(
        side_effect=WorkflowNotFoundException
    )
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow.get_action = mock.MagicMock()
    workflow.delete_action = mock.MagicMock()
    workflow.add_action = mock.MagicMock()
    workflow.check_all = mock.MagicMock()
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )

    with pytest.raises(WorkflowNotFoundException):
        workflow_action_service.change_workflow_action_version(
            workflow_id, "workflow_action_id", "action_id"
        )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    action_repository.get_action.assert_not_called()
    workflow.get_action.assert_not_called()
    workflow.delete_action.assert_not_called()
    workflow.add_action.assert_not_called()
    workflow.check_all.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_change_workflow_action_version_when_workflow_not_updatable(
    app_container: ApplicationContainer,
):
    workflow_id = "workflow_id"
    workflow = Workflow(
        id=workflow_id,
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow.is_updatable = mock.MagicMock(return_value=False)
    workflow.get_action = mock.MagicMock()
    workflow.delete_action = mock.MagicMock()
    workflow.add_action = mock.MagicMock()
    workflow.check_all = mock.MagicMock()
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotUpdatableException):
        workflow_action_service.change_workflow_action_version(
            workflow_id, "workflow_action_id", "action_id"
        )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    action_repository.get_action.assert_not_called()
    workflow.get_action.assert_not_called()
    workflow.delete_action.assert_not_called()
    workflow.add_action.assert_not_called()
    workflow.check_all.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_change_workflow_action_version_when_action_not_found(
    app_container: ApplicationContainer,
):
    workflow_id = "workflow_id"
    workflow = Workflow(
        id=workflow_id,
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    action_id = "action_id"
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    action_repository: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository.get_action = mock.MagicMock(side_effect=ActionNotFoundException)
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow.is_updatable = mock.MagicMock(return_value=True)
    workflow.get_action = mock.MagicMock()
    workflow.delete_action = mock.MagicMock()
    workflow.add_action = mock.MagicMock()
    workflow.check_all = mock.MagicMock()
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(ActionNotFoundException):
        workflow_action_service.change_workflow_action_version(
            workflow_id, "workflow_action_id", action_id
        )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    action_repository.get_action.assert_called_once_with(action_id)
    workflow.get_action.assert_not_called()
    workflow.delete_action.assert_not_called()
    workflow.add_action.assert_not_called()
    workflow.check_all.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_add_workflow_action_link(app_container: ApplicationContainer):
    """Add a action link from to workflow should work"""
    workflow_id = "workflow_id"
    data = AddWorkflowActionLink(
        upstream_action_id="input_mod_id", downstream_action_id="output_mod_id"
    )
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow.add_action_link = mock.MagicMock()
    workflow.is_updatable = mock.MagicMock(return_value=True)
    workflow.check_all = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    workflow_action_service.add_workflow_action_link(workflow_id=workflow_id, data=data)
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_called_once()
    workflow.add_action_link.assert_called_once_with(
        data.upstream_action_id, data.downstream_action_id
    )
    workflow.check_all.assert_called()
    workflow_unit_mock.commit.assert_called()


def test_add_workflow_action_link_when_workflow_not_exists(
    app_container: ApplicationContainer,
):
    """Adding a action link to a workflow should raise an error if workflow not exists"""
    workflow_id = "workflow_id"
    data = AddWorkflowActionLink(
        upstream_action_id="input_mod_id", downstream_action_id="output_mod_id"
    )
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow.is_updatable = mock.MagicMock()
    workflow.add_action_link = mock.MagicMock()
    workflow_repository.get_workflow.return_value = None
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotFoundException):
        workflow_action_service.add_workflow_action_link(workflow_id, data)
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_not_called()
    workflow.add_action_link.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_add_workflow_action_link_when_workflow_not_updatable(
    app_container: ApplicationContainer,
):
    """Adding a action link to a workflow should raise an error if workflow not updatable"""
    workflow_id = "workflow_id"
    data = AddWorkflowActionLink(
        upstream_action_id="input_mod_id", downstream_action_id="output_mod_id"
    )
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow.is_updatable = mock.MagicMock(return_value=False)
    workflow.add_action_link = mock.MagicMock()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotUpdatableException):
        workflow_action_service.add_workflow_action_link(workflow_id, data)
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_called_once()
    workflow.add_action_link.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_delete_workflow_action_link(app_container: ApplicationContainer):
    """Delete a action link from workflow should work"""
    workflow_id = "workflow_id"
    workflow_action_link_id = "workflow_action_link_id"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow.delete_action_link = mock.MagicMock()
    workflow.check_all = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    workflow_action_service.delete_workflow_action_link(
        workflow_id, workflow_action_link_id
    )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.delete_action_link.assert_called_once_with(workflow_action_link_id)
    workflow.check_all.assert_called()
    workflow_unit_mock.commit.assert_called()


def test_delete_workflow_action_link_when_workflow_not_exists(
    app_container: ApplicationContainer,
):
    """Delete a action link in a workflow should raise an error if workflow not exists"""
    workflow_id = "workflow_id"
    workflow_action_link_id = "workflow_action_link_id"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow.is_updatable = mock.MagicMock()
    workflow.delete_action_link = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = None
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotFoundException):
        workflow_action_service.delete_workflow_action_link(
            workflow_id, workflow_action_link_id
        )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_not_called()
    workflow.delete_action_link.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_delete_workflow_action_link_when_workflow_not_updatable(
    app_container: ApplicationContainer,
):
    """Delete a action link in a workflow should raise an error if workflow not updatable"""
    workflow_id = "workflow_id"
    workflow_action_link_id = "workflow_action_link_id"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow.is_updatable = mock.MagicMock(return_value=False)
    workflow.delete_action_link = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotUpdatableException):
        workflow_action_service.delete_workflow_action_link(
            workflow_id, workflow_action_link_id
        )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_called_once()
    workflow.delete_action_link.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_update_workflow_action_input(
    app_container: ApplicationContainer,
):
    """Update workflow action input value with a static value"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_action_input_id"
    workflow = Workflow(
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
    )
    workflow.update_action_input = mock.MagicMock()
    workflow.check_all = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    data = UpdateWorkflowActionInputValue()
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    workflow_action_service.update_workflow_action_input(
        workflow_id, workflow_action_id, workflow_action_input_id, data
    )
    workflow_repository.get_workflow.assert_called_with(workflow_id)
    workflow.update_action_input.assert_called_with(
        workflow_action_id, workflow_action_input_id, data
    )
    workflow.check_all.assert_called_once()
    workflow_unit_mock.commit.assert_called()


def test_update_workflow_action_input_when_workflow_not_exists(
    app_container: ApplicationContainer,
):
    """Update input value in a workflow when workflow doesn't exist should fail"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_input_id = "workflow_input_id"
    data = UpdateWorkflowActionInputValue()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = None
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotFoundException):
        workflow_action_service.update_workflow_action_input(
            workflow_id, workflow_action_id, workflow_input_id, data
        )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow_unit_mock.commit.assert_not_called()


async def test_delete_workflow_action_input_file(app_container: ApplicationContainer):
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_input_id = "workflow_input_id"
    file_path = "file_path"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow.delete_input_file = mock.MagicMock()
    workflow.check_all = mock.MagicMock()
    workflow.is_updatable = mock.MagicMock(return_value=True)
    workflow.get_action_input_file_path = mock.MagicMock(return_value=file_path)
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    filestore_service: mock.MagicMock[
        IFilestoreService
    ] = app_container.filestore_service()
    filestore_service.remove_file = mock.AsyncMock()
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    workflow.get_input_file = mock.MagicMock()
    workflow_repository.delete_static_file = mock.MagicMock()
    await workflow_action_service.delete_workflow_action_input_file(
        workflow_id, workflow_action_id, workflow_input_id
    )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_called_once()
    workflow.check_all.assert_called()
    workflow.get_action_input_file_path.assert_called_once_with(
        workflow_action_id, workflow_input_id
    )
    filestore_service.remove_file.assert_called_once_with(file_path)
    workflow_unit_mock.commit.assert_called()


async def test_delete_workflow_action_input_file_when_workflow_not_updatable(
    app_container: ApplicationContainer,
):
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_input_id = "workflow_input_id"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    workflow.delete_input_file = mock.MagicMock()
    workflow.check_all = mock.MagicMock()
    workflow.get_action_input_file_path = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    filestore_service: mock.AsyncMock[
        IFilestoreService
    ] = app_container.filestore_service()
    filestore_service.remove_file = mock.MagicMock()
    workflow_repository.get_workflow.return_value = workflow
    workflow.is_updatable = mock.MagicMock(return_value=False)
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotUpdatableException):
        await workflow_action_service.delete_workflow_action_input_file(
            workflow_id, workflow_action_id, workflow_input_id
        )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.is_updatable.assert_called_once()
    filestore_service.remove_file.assert_not_called()
    workflow.delete_input_file.assert_not_called()
    workflow.check_all.assert_not_called()
    workflow.get_action_input_file_path.assert_not_called()
    workflow_unit_mock.assert_not_called()


def test_add_workflow_action_dynamic_output(app_container: ApplicationContainer):
    """Add dynamic output to workflow"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    data = AddWorkflowActionDynamicOutput(
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow.add_action_dynamic_output = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    workflow_action_service.add_workflow_action_dynamic_output(
        workflow_id=workflow_id, workflow_action_id=workflow_action_id, data=data
    )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.add_action_dynamic_output.assert_called_once_with(workflow_action_id, data)
    workflow_unit_mock.commit.assert_called()


def test_add_workflow_action_dynamic_output_when_workflow_not_exists(
    app_container: ApplicationContainer,
):
    """Add dynamic output to workflow should return an error if workflow doesn't exist"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    data = AddWorkflowActionDynamicOutput(
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow.add_action_dynamic_output = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = None
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotFoundException):
        workflow_action_service.add_workflow_action_dynamic_output(
            workflow_id, workflow_action_id, data
        )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.add_action_dynamic_output.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_update_workflow_dynamic_output(app_container: ApplicationContainer):
    """Update dynamic output in a workflow"""
    workflow_id = "workflow_id"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_id = "workflow_dynamic_output_id"
    data = UpdateWorkflowActionDynamicOutput(
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )

    workflow.update_action_dynamic_output = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    workflow_action_service.update_workflow_action_dynamic_output(
        workflow_id, workflow_action_id, workflow_dynamic_output_id, data
    )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.update_action_dynamic_output.assert_called_once_with(
        workflow_action_id, workflow_dynamic_output_id, data
    )
    workflow_unit_mock.commit.assert_called()


def test_update_workflow_dynamic_output_when_workflow_not_exists(
    app_container: ApplicationContainer,
):
    """Update dynamic output to workflow should return an error if workflow doesn't exist"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_id = "workflow_dynamic_output_id"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    data = UpdateWorkflowActionDynamicOutput(
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
    )
    workflow.update_action_dynamic_output = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = None
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotFoundException):
        workflow_action_service.update_workflow_action_dynamic_output(
            workflow_id, workflow_action_id, workflow_dynamic_output_id, data
        )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.update_action_dynamic_output.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_delete_workflow_dynamic_output(app_container: ApplicationContainer):
    """Delete a dynamic output from workflow"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_id = "workflow_dynamic_output_id"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow.delete_action_dynamic_output = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    workflow_action_service.delete_workflow_action_dynamic_output(
        workflow_id, workflow_action_id, workflow_dynamic_output_id
    )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.delete_action_dynamic_output.assert_called_once_with(
        workflow_action_id, workflow_dynamic_output_id
    )
    workflow_unit_mock.commit.assert_called()


def test_delete_workflow_dynamic_output_when_workflow_not_exists(
    app_container: ApplicationContainer,
):
    """Delete dynamic output in workflow should return an error if workflow doesn't exist"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_id = "workflow_dynamic_output_id"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow.delete_action_dynamic_output = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = None
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotFoundException):
        workflow_action_service.delete_workflow_action_dynamic_output(
            workflow_id, workflow_action_id, workflow_dynamic_output_id
        )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.delete_action_dynamic_output.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


def test_list_workflow_action_inputs(app_container: ApplicationContainer):
    """List workflow inputs should return list workflow inputs view"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_id"
    input_1 = WorkflowActionInputView(
        id="input_1",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        enum_values=[],
        static_value=None,
        reference_value=None,
        reference_output=None,
        workflow_file=None,
        project_variable_value=None,
        optional=False,
    )
    input_2 = WorkflowActionInputView(
        id="input_2",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        enum_values=[],
        static_value=None,
        reference_value=None,
        reference_output=None,
        workflow_file=None,
        project_variable_value=None,
        optional=False,
    )
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.list_workflow_action_inputs_view.return_value = [
        input_1,
        input_2,
    ]
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    assert workflow_action_service.list_workflow_action_inputs(
        workflow_id, workflow_action_id
    ) == [input_1, input_2]
    workflow_repository.check_workflow_exists.assert_called_with(workflow_id)
    workflow_repository.check_workflow_action_exists.assert_called_with(
        workflow_action_id
    )
    workflow_repository.list_workflow_action_inputs_view.assert_called_once_with(
        workflow_action_id
    )


def test_list_workflow_action_inputs_when_workflow_not_found(
    app_container: ApplicationContainer,
):
    """List workflow inputs should return error when workflow doesn't exist"""
    workflow_id = "workflow_id"
    workflow_repository_mock: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository_mock.check_workflow_exists.side_effect = (
        WorkflowNotFoundException
    )
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )

    with pytest.raises(WorkflowNotFoundException):
        workflow_action_service.list_workflow_action_inputs(
            workflow_id, "workflow_action_id"
        )
    workflow_repository_mock.check_workflow_exists.assert_called_with(workflow_id)
    workflow_repository_mock.list_workflow_action_inputs_view.assert_not_called()


def test_list_workflow_action_inputs_when_workflow_action_not_found(
    app_container: ApplicationContainer,
):
    """List workflow inputs should return error when workflow action doesn't exist"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.check_workflow_action_exists.side_effect = (
        WorkflowActionNotFoundException
    )
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )

    with pytest.raises(WorkflowActionNotFoundException):
        workflow_action_service.list_workflow_action_inputs(
            workflow_id, workflow_action_id
        )
    workflow_repository.check_workflow_exists.assert_called_with(workflow_id)
    workflow_repository.check_workflow_action_exists.assert_called_with(
        workflow_action_id
    )
    workflow_repository.list_workflow_action_inputs_view.assert_not_called()


def test_list_workflow_action_outputs(app_container: ApplicationContainer):
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    output_1 = WorkflowActionOutputView(
        id="output_1",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        optional=False,
    )
    output_2 = WorkflowActionOutputView(
        id="output_2",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        optional=False,
    )
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.list_workflow_action_outputs_view.return_value = [
        output_1,
        output_2,
    ]
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )

    assert workflow_action_service.list_workflow_action_outputs(
        workflow_id, workflow_action_id
    ) == [output_1, output_2]
    workflow_repository.check_workflow_exists.assert_called_with(workflow_id)
    workflow_repository.check_workflow_action_exists.assert_called_with(
        workflow_action_id
    )
    workflow_repository.list_workflow_action_outputs_view.assert_called_with(
        workflow_action_id
    )


def test_list_workflow_action_outputs_when_workflow_not_found(
    app_container: ApplicationContainer,
):
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.check_workflow_exists.side_effect = WorkflowNotFoundException
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )

    with pytest.raises(WorkflowNotFoundException):
        workflow_action_service.list_workflow_action_outputs(
            workflow_id, workflow_action_id
        )
    workflow_repository.check_workflow_exists.assert_called_with(workflow_id)
    workflow_repository.list_workflow_action_outputs_view.assert_not_called()


def test_list_workflow_action_outputs_when_workflow_action_not_found(
    app_container: ApplicationContainer,
):
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.check_workflow_action_exists.side_effect = (
        WorkflowActionNotFoundException
    )
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )

    with pytest.raises(WorkflowActionNotFoundException):
        workflow_action_service.list_workflow_action_outputs(
            workflow_id, workflow_action_id
        )
    workflow_repository.check_workflow_exists.assert_called_with(workflow_id)
    workflow_repository.check_workflow_action_exists.assert_called_with(
        workflow_action_id
    )
    workflow_repository.list_workflow_action_outputs_view.assert_not_called()


def test_search_workflow_action_outputs(app_container: ApplicationContainer):
    workflow_id = "workflow_id"
    with_type = "string"
    accessible_from = "workflow_action_id"
    previous_actions_ids = ["prev_action_1", "prev_action_2"]
    output_1 = WorkflowActionOutputView(
        id="output_1",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        optional=False,
    )
    output_2 = WorkflowActionOutputView(
        id="output_2",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        optional=False,
    )
    workflow_repository: IWorkflowRepository = app_container.workflow_repository()
    workflow_repository.check_workflow_exists = mock.MagicMock()
    workflow_repository.list_previous_workflow_actions_view = mock.MagicMock(
        return_value=previous_actions_ids
    )
    workflow_repository.search_workflow_action_outputs_view = mock.MagicMock(
        return_value=[output_1, output_2]
    )
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    assert workflow_action_service.search_workflow_action_outputs(
        workflow_id, with_type=with_type, accessible_from=accessible_from
    ) == [output_1, output_2]
    workflow_repository.check_workflow_exists.assert_called_with(workflow_id)
    workflow_repository.list_previous_workflow_actions_view.assert_called_with(
        accessible_from
    )
    workflow_repository.search_workflow_action_outputs_view.assert_called_with(
        workflow_id,
        with_type=ActionIOType.STRING,
        in_workflow_actions=previous_actions_ids,
    )


def test_search_workflow_action_outputs_without_accessible_from_param(
    app_container: ApplicationContainer,
):
    workflow_id = "workflow_id"
    with_type = "string"
    previous_actions_ids = ["prev_action_1", "prev_action_2"]
    output_1 = WorkflowActionOutputView(
        id="output_1",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        optional=False,
    )
    output_2 = WorkflowActionOutputView(
        id="output_2",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        optional=False,
    )
    workflow_repository: IWorkflowRepository = app_container.workflow_repository()
    workflow_repository.check_workflow_exists = mock.MagicMock()
    workflow_repository.list_previous_workflow_actions_view = mock.MagicMock(
        return_value=previous_actions_ids
    )
    workflow_repository.search_workflow_action_outputs_view = mock.MagicMock(
        return_value=[output_1, output_2]
    )
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    assert workflow_action_service.search_workflow_action_outputs(
        workflow_id, with_type=with_type
    ) == [output_1, output_2]
    workflow_repository.check_workflow_exists.assert_called_with(workflow_id)
    workflow_repository.list_previous_workflow_actions_view.assert_not_called()
    workflow_repository.search_workflow_action_outputs_view.assert_called_with(
        workflow_id, with_type=ActionIOType.STRING, in_workflow_actions=None
    )


def test_search_workflow_action_outputs_when_workflow_not_exists(
    app_container: ApplicationContainer,
):
    workflow_id = "workflow_id"
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.check_workflow_exists.side_effect = WorkflowNotFoundException
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotFoundException):
        workflow_action_service.search_workflow_action_outputs(workflow_id)
    workflow_repository.check_workflow_exists.assert_called_with(workflow_id)
    workflow_repository.search_workflow_action_outputs_view.assert_not_called()


async def test_upload_static_file_input(app_container: ApplicationContainer):
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_input_id = "workflow_action_input_id"
    filename = "file"
    extension = "csv"
    file_content = bytearray([1, 2, 3, 4])
    file_io = io.BytesIO(file_content)
    file_size = len(file_content)
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id=workflow_input_id,
        type=ActionIOType.FILE,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
    )
    workflow = Workflow(
        id=workflow_id,
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[
            WorkflowAction(
                id=workflow_action_id,
                inputs=[workflow_action_input],
                name="wfmname",
                technical_name="wfmname",
                description="wfmname",
                version="wfmname",
                kind=ActionKind.PROCESSOR,
                has_dynamic_outputs=False,
                position_x=2,
                position_y=2,
                action_id="action_id",
            )
        ],
    )
    workflow.add_input_file = mock.MagicMock()
    workflow.check_all = mock.MagicMock()
    workflow.add_input_file = mock.MagicMock()
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow = mock.MagicMock(return_value=workflow)

    filestore_service_mock: IFilestoreService = app_container.filestore_service()
    filestore_service_mock.write = mock.AsyncMock()
    filestore_service_mock.generate_file_path = mock.MagicMock(return_value="mock_path")
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    await workflow_action_service.upload_static_file_input(
        workflow_id,
        workflow_action_id,
        workflow_input_id,
        filename,
        extension,
        file_io,
        file_size,
    )
    workflow_repository.get_workflow.assert_called_with(workflow_id)
    filestore_service_mock.generate_file_path.assert_called_once_with(
        f"{filename}.{extension}"
    )
    workflow.add_input_file.assert_called_once_with(
        workflow_action_id,
        workflow_input_id,
        filename,
        extension,
        "mock_path",
        file_size,
    )
    filestore_service_mock.write.assert_called_once_with("mock_path", file_io)
    workflow.check_all.assert_called()
    workflow_unit_mock.commit.assert_called()


async def test_upload_static_file_input_when_no_extension(
    app_container: ApplicationContainer,
):
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_input_id = "workflow_action_input_id"
    filename = "file"
    extension = ""
    file_content = bytearray([1, 2, 3, 4])
    file_io = io.BytesIO(file_content)
    file_size = len(file_content)
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id=workflow_input_id,
        type=ActionIOType.FILE,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
    )
    workflow = Workflow(
        id=workflow_id,
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[
            WorkflowAction(
                id=workflow_action_id,
                inputs=[workflow_action_input],
                name="wfmname",
                technical_name="wfmname",
                description="wfmname",
                version="wfmname",
                kind=ActionKind.PROCESSOR,
                has_dynamic_outputs=False,
                position_x=2,
                position_y=2,
                action_id="action_id",
            )
        ],
    )
    workflow.add_input_file = mock.MagicMock()
    workflow.check_all = mock.MagicMock()
    workflow.add_input_file = mock.MagicMock()
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow = mock.MagicMock(return_value=workflow)

    filestore_service_mock: IFilestoreService = app_container.filestore_service()
    filestore_service_mock.write = mock.AsyncMock()
    filestore_service_mock.generate_file_path = mock.MagicMock(return_value="mock_path")
    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    await workflow_action_service.upload_static_file_input(
        workflow_id,
        workflow_action_id,
        workflow_input_id,
        filename,
        extension,
        file_io,
        file_size,
    )
    workflow_repository.get_workflow.assert_called_with(workflow_id)
    filestore_service_mock.generate_file_path.assert_called_once_with(filename)
    workflow.add_input_file.assert_called_once_with(
        workflow_action_id,
        workflow_input_id,
        filename,
        extension,
        "mock_path",
        file_size,
    )
    filestore_service_mock.write.assert_called_once_with("mock_path", file_io)
    workflow.check_all.assert_called()
    workflow_unit_mock.commit.assert_called()


async def test_upload_static_file_input_when_workflow_not_exist(
    app_container: ApplicationContainer,
):
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_input_id = "workflow_action_input_id"
    workflow_action_input = WorkflowActionInput(
        optional=False,
        id=workflow_input_id,
        type=ActionIOType.FILE,
        technical_name="technical_name",
        display_name="display_name",
        help="help",
    )
    workflow = Workflow(
        id=workflow_id,
        name="wfname",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[
            WorkflowAction(
                id=workflow_action_id,
                inputs=[workflow_action_input],
                name="wfmname",
                technical_name="wfmname",
                description="wfmname",
                version="wfmname",
                kind=ActionKind.PROCESSOR,
                has_dynamic_outputs=False,
                position_x=2,
                position_y=2,
                action_id="action_id",
            )
        ],
    )
    filename = "file"
    extension = "csv"
    file_io = io.BytesIO(bytearray([1, 2, 3, 4]))

    workflow.add_input_file = mock.MagicMock()
    workflow.check_all = mock.MagicMock()
    workflow.add_input_file = mock.MagicMock()

    workflow_repository_mock: IWorkflowRepository = app_container.workflow_repository()
    workflow_repository_mock.get_workflow = mock.MagicMock()
    workflow_repository_mock.get_workflow.side_effect = WorkflowNotFoundException

    filestore_service_mock: IFilestoreService = app_container.filestore_service()
    filestore_service_mock.upload_file = mock.AsyncMock()
    filestore_service_mock.generate_file_path = mock.MagicMock(return_value="mock_path")

    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )
    with pytest.raises(WorkflowNotFoundException):
        assert await workflow_action_service.upload_static_file_input(
            workflow_id,
            workflow_action_id,
            workflow_input_id,
            filename,
            extension,
            file_io,
            12,
        )
    workflow_repository_mock.get_workflow.assert_called_once_with(workflow_id)
    filestore_service_mock.generate_file_path.assert_not_called()
    workflow.add_input_file.assert_not_called()
    filestore_service_mock.upload_file.assert_not_called()
    workflow.check_all.assert_not_called()
    workflow_unit_mock.commit.assert_not_called()


@pytest.mark.parametrize(
    "data, current_action_links",
    [
        (UpdateWorkflowActionLinks(action_id="action_id", next_actions_ids=[]), []),
        (
            UpdateWorkflowActionLinks(action_id="action_id", next_actions_ids=[]),
            [
                WorkflowActionLink(
                    upstream_action_id="", downstream_action_id="", id="action link"
                )
            ],
        ),
        (
            UpdateWorkflowActionLinks(
                action_id="action_id", next_actions_ids=["next_id"]
            ),
            [],
        ),
        (
            UpdateWorkflowActionLinks(
                action_id="action_id", next_actions_ids=["next_id 1", "next_id 2"]
            ),
            [
                WorkflowActionLink(
                    upstream_action_id="", downstream_action_id="", id="link 1"
                ),
                WorkflowActionLink(
                    upstream_action_id="", downstream_action_id="", id="link 2"
                ),
            ],
        ),
    ],
    ids=["empty", "empty with current links", "no current links", "several links"],
)
def test_update_workflow_action_links(
    unit_of_work,
    workflow_action_service,
    workflow,
    workflow_repository,
    data,
    current_action_links,
):
    workflow.get_links_from_workflow_action = mock.MagicMock(
        return_value=current_action_links
    )
    workflow.delete_action_link = mock.MagicMock()
    workflow.add_action_link = mock.MagicMock()

    workflow_action_service.update_workflow_action_links("workflow_id", data)
    if len(current_action_links) == 0:
        workflow.delete_action_link.assert_not_called()
    else:
        workflow.delete_action_link.assert_has_calls(
            [mock.call(link.id) for link in current_action_links], any_order=True
        )
    if len(data.next_actions_ids) == 0:
        workflow.add_action_link.assert_not_called()
    else:
        workflow.add_action_link.assert_has_calls(
            [mock.call(data.action_id, id) for id in data.next_actions_ids],
            any_order=True,
        )
    workflow.check_all.assert_called_once()
    unit_of_work.commit.assert_called_once()


def test_update_workflow_action_links_when_workflow_not_exists(
    unit_of_work,
    workflow_action_service,
    workflow,
    workflow_repository,
):
    data = UpdateWorkflowActionLinks(action_id="aaa")
    workflow_repository.get_workflow.return_value = None

    workflow.get_links_from_workflow_action = mock.MagicMock()
    workflow.delete_action_link = mock.MagicMock()
    workflow.add_action_link = mock.MagicMock()

    with pytest.raises(WorkflowNotFoundException):
        workflow_action_service.update_workflow_action_links("workflow_id", data)

    workflow.delete_action_link.assert_not_called()
    workflow.add_action_link.assert_not_called()
    workflow.check_all.assert_not_called()
    unit_of_work.commit.assert_not_called()


def test_update_all_workflow_action_io(app_container: ApplicationContainer):
    """Updating a action in a workflow"""
    workflow_id = "workflow_id"
    workflow = Workflow(
        name="Workflow 1",
        description="Wf description",
        owner="wfowner",
        project="wfproject",
    )
    workflow_action_id = "workflow_action_id"
    data = UpdateAllWorkflowActionIOData(
        custom_name="foo",
        inputs=[
            UpdateAllWorkflowActionIOData.InputValue(id="testid", static_value="1")
        ],
        dynamic_outputs=[
            UpdateAllWorkflowActionIOData.DynamicOutputDefinition(
                optional=False,
                id="foo",
                technical_name="bar",
                display_name="baz",
                help="fred",
                type=ActionIOType.STRING,
                origin=DynamicOutputOrigin.PATH,
            )
        ],
        addons_inputs=[
            UpdateAllWorkflowActionIOData.InputValue(
                id="addon_input_1", static_value="2"
            )
        ],
    )
    workflow.update_action_input = mock.MagicMock()
    workflow.update_action_dynamic_output = mock.MagicMock()
    workflow.update_action = mock.MagicMock()
    workflow.check_all = mock.MagicMock()
    workflow_repository: mock.MagicMock[
        IWorkflowRepository
    ] = app_container.workflow_repository()
    workflow_repository.get_workflow.return_value = workflow
    workflow_unit_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()

    workflow_action_service: WorkflowActionService = (
        app_container.workflow_action_service()
    )

    workflow_action_service.update_all_workflow_action_io(
        workflow_id, workflow_action_id, data
    )
    workflow_repository.get_workflow.assert_called_once_with(workflow_id)
    workflow.update_action_input.assert_called_with(
        workflow_action_id="workflow_action_id",
        workflow_action_input_id="addon_input_1",
        data=UpdateWorkflowActionInputValue(static_value="2", reference_value=None),
    )
    workflow.update_action_dynamic_output.assert_called_once_with(
        workflow_action_id="workflow_action_id",
        workflow_dynamic_output_id="foo",
        data=UpdateWorkflowActionDynamicOutput(
            technical_name="bar",
            display_name="baz",
            help="fred",
            type=ActionIOType.STRING,
            enum_values=None,
            origin=DynamicOutputOrigin.PATH,
            optional=False,
        ),
    )

    workflow.update_action.assert_called_once_with(
        workflow_action_id, UpdateWorkflowAction(custom_name=data.custom_name)
    )
    workflow_unit_mock.commit.assert_called_once()
    workflow.check_all.assert_called_once()
