import datetime
from unittest import mock

import pytest

from ryax.studio.application.action_service import ActionService
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_category import ActionCategory
from ryax.studio.domain.action.action_events import ActionReadyEvent
from ryax.studio.domain.action.action_exceptions import (
    ActionNotDeletableException,
    ActionNotFoundException,
)
from ryax.studio.domain.action.action_factory import ActionFactory
from ryax.studio.domain.action.action_repository import IActionRepository
from ryax.studio.domain.action.action_values import ActionKind
from ryax.studio.domain.action.views.action import ActionView
from ryax.studio.domain.common.event_publisher import IEventPublisher
from ryax.studio.domain.common.unit_of_work import IUnitOfWork
from ryax.studio.domain.logo.logo import Logo
from ryax.studio.domain.workflow.entities.workflow import Workflow


def test_get_logo(app_container: ApplicationContainer):
    """Get logo should return logo"""
    action_id = "action_id"
    logo_id = "logo_id"
    logo = Logo(
        id=logo_id,
        action_id=action_id,
        name="logoname",
        extension="png",
        content=b"content",
    )
    action = Action(
        id=action_id,
        project="project_id",
        name="action1name",
        lockfile=b"",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        build_date=datetime.datetime.fromtimestamp(0),
        logo=logo,
    )
    action.get_logo = mock.MagicMock(return_value=logo)
    action_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()

    action_repository_mock.get_action.return_value = action
    action_service: ActionService = app_container.action_service()
    assert action_service.get_logo(action_id) == logo
    action_repository_mock.get_action.assert_called_once_with(action_id)


def test_get_logo_not_found_ret_default_logo(app_container: ApplicationContainer):
    """Get logo should return error when not found"""
    action_id = "action_id"
    logo_id = "logo_id"
    logo = Logo(
        id=logo_id,
        action_id=action_id,
        name="logoname",
        extension="png",
        content=b"content",
    )
    action = Action(
        id=action_id,
        project="project_id",
        name="action1name",
        technical_name="action1name",
        lockfile=b"",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        build_date=datetime.datetime.fromtimestamp(0),
    )
    action.get_logo = mock.MagicMock(return_value=None)

    action_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()

    action_repository_mock.get_action.return_value = action
    action_service: ActionService = app_container.action_service()
    action_service.assets_storage_service.get_default_logo = mock.MagicMock(
        return_value=logo
    )
    assert action_service.get_logo(action_id) == logo
    action_service.assets_storage_service.get_default_logo.assert_called_once()


def test_list_actions(app_container: ApplicationContainer):
    """List actions should return actions"""
    action_1 = ActionView(
        id="action_1",
        name="n",
        version="v",
        kind=ActionKind.PROCESSOR,
        description=None,
        lockfile=b"",
    )
    action_2 = ActionView(
        id="action_2",
        name="n",
        version="v",
        kind=ActionKind.PROCESSOR,
        description=None,
        lockfile=b"",
    )
    mock_project_id = "88090135-0a30-45ab-b4e3-69739a583bad"
    action_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository_mock.get_actions_views.return_value = [action_1, action_2]
    action_service: ActionService = app_container.action_service()
    assert action_service.get_actions(mock_project_id) == [action_1, action_2]
    action_repository_mock.get_actions_views.assert_called_with(
        mock_project_id, "", None
    )


def test_list_actions_with_search(app_container: ApplicationContainer):
    """List actions with search parameter should return matching actions"""
    action_1 = ActionView(
        id="action_1",
        name="n",
        version="v",
        kind=ActionKind.PROCESSOR,
        description=None,
        lockfile=b"",
    )
    action_2 = ActionView(
        id="action_2",
        name="n",
        version="v",
        kind=ActionKind.PROCESSOR,
        description=None,
        lockfile=b"",
    )
    mock_project_id = "88090135-0a30-45ab-b4e3-69739a583bad"
    action_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository_mock.get_actions_views.return_value = [action_1, action_2]
    action_service: ActionService = app_container.action_service()
    search = "test"
    assert action_service.get_actions(
        mock_project_id, search=search, category=None
    ) == [
        action_1,
        action_2,
    ]
    action_repository_mock.get_actions_views.assert_called_with(
        mock_project_id, search, None
    )


def test_list_actions_with_category_filter(app_container: ApplicationContainer):
    """List actions with search parameter should return matching actions"""
    action_1 = ActionView(
        id="action_1",
        name="n",
        version="v",
        kind=ActionKind.PROCESSOR,
        description=None,
        lockfile=b"",
    )
    action_2 = ActionView(
        id="action_2",
        name="n",
        version="v",
        kind=ActionKind.PROCESSOR,
        description=None,
        lockfile=b"",
    )
    mock_project_id = "88090135-0a30-45ab-b4e3-69739a583bad"
    action_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository_mock.get_actions_views.return_value = [action_1, action_2]
    action_service: ActionService = app_container.action_service()
    category = "category_1"
    assert action_service.get_actions(mock_project_id, category=category) == [
        action_1,
        action_2,
    ]
    action_repository_mock.get_actions_views.assert_called_with(
        mock_project_id, "", category
    )


def test_list_action_versions(app_container: ApplicationContainer):
    action_1_id = "action_1_id"
    action_1 = Action(
        id=action_1_id,
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.datetime.fromtimestamp(0),
    )
    action_2 = Action(
        id="action_2_id",
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.datetime.fromtimestamp(0),
    )
    action_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository_mock.get_action_versions.return_value = [action_1, action_2]
    action_service: ActionService = app_container.action_service()
    assert action_service.list_action_versions(action_1_id) == [action_1, action_2]
    action_repository_mock.get_action_versions.assert_called_once_with(action_1_id)


def test_list_action_versions_when_action_not_found(
    app_container: ApplicationContainer,
):
    action_1_id = "action_1_id"
    action_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository_mock.get_action_versions.side_effect = ActionNotFoundException
    action_service: ActionService = app_container.action_service()
    with pytest.raises(ActionNotFoundException):
        action_service.list_action_versions(action_1_id)
    action_repository_mock.get_action_versions.assert_called_once_with(action_1_id)


def test_get_action(app_container: ApplicationContainer):
    """Get action should return action"""
    action_id = "action_id"
    action = Action(
        id=action_id,
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.datetime.fromtimestamp(0),
    )
    action_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository_mock.get_action.return_value = action
    action_service: ActionService = app_container.action_service()
    assert action_service.get_action(action_id) == action
    action_repository_mock.get_action.assert_called_once_with(action_id)


def test_get_action_not_found(app_container: ApplicationContainer):
    """Get action should return action"""
    action_id = "action_id"
    action_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository_mock.get_action.side_effect = ActionNotFoundException
    action_service: ActionService = app_container.action_service()
    with pytest.raises(ActionNotFoundException):
        action_service.get_action(action_id)
    action_repository_mock.get_action.assert_called_once_with(action_id)


async def test_add_action(app_container: ApplicationContainer):
    """Create action should return action"""
    action = Action(
        id="action_id",
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.datetime.fromtimestamp(0),
    )
    action_ready_event = ActionReadyEvent(
        id="action_id",
        name="action_name",
        technical_name="action_technical_name",
        version="action_version",
        kind=ActionKind.PROCESSOR,
        description="action_description",
        logo=None,
        dynamic_outputs=False,
        inputs=[],
        outputs=[],
        owner_id="action_owner_id",
        build_date=datetime.datetime.now(),
        lockfile=b"",
        categories=[],
        project_id="project1",
        resources=None,
    )
    action_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    action_factory: mock.MagicMock[ActionFactory] = app_container.action_factory()
    action_factory.from_action_ready_event.return_value = action
    action_service: ActionService = app_container.action_service()
    action_service.add_action(action_ready_event)
    action_factory.from_action_ready_event.assert_called_with(action_ready_event)
    action_repository_mock.add_action.assert_called_with(action)
    unit_of_work_mock.commit.assert_called_once()


async def test_delete_action(app_container: ApplicationContainer):
    """Delete action should works"""
    action_id = "action_id"
    action = Action(
        id=action_id,
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.datetime.fromtimestamp(0),
    )
    action_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository_mock.get_action.return_value = action
    workflow_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.workflow_repository()
    workflow_repository_mock.list_workflows_where_action_used.return_value = []
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    event_publisher_mock: mock.MagicMock[
        IEventPublisher
    ] = app_container.messaging_publisher()
    event_publisher_mock.publish = mock.AsyncMock()
    action.add_delete_event = mock.MagicMock()
    action_service: ActionService = app_container.action_service()
    assert await action_service.delete_action(action_id) is None
    action_repository_mock.get_action.assert_called_once_with(action_id)
    workflow_repository_mock.list_workflows_where_action_used.assert_called_once_with(
        action_id
    )
    action_repository_mock.delete_action.assert_called_once_with(action)
    action.add_delete_event.assert_called_once()
    event_publisher_mock.publish.assert_called_once_with(action.events)
    unit_of_work_mock.commit.assert_called_once()


async def test_delete_action_not_exists(app_container: ApplicationContainer):
    """Delete action should error if it doesn't exist"""
    action_id = "action_id"
    action = Action(
        id=action_id,
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.datetime.fromtimestamp(0),
    )
    action.add_delete_event = mock.MagicMock()
    action_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository_mock.get_action.side_effect = ActionNotFoundException
    workflow_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.workflow_repository()
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    event_publisher_mock: mock.MagicMock[
        IEventPublisher
    ] = app_container.messaging_publisher()
    event_publisher_mock.publish = mock.AsyncMock()
    action_service: ActionService = app_container.action_service()
    with pytest.raises(ActionNotFoundException):
        await action_service.delete_action(action_id)
    action_repository_mock.get_action.assert_called_once_with(action_id)
    workflow_repository_mock.list_workflows_where_action_used.assert_not_called()
    action.add_delete_event.assert_not_called()
    action_repository_mock.delete_action.assert_not_called()
    event_publisher_mock.publish.assert_not_called()
    unit_of_work_mock.commit.assert_not_called()


async def test_delete_action_not_deletable(app_container: ApplicationContainer):
    """Delete action should error if it doesn't exist"""
    action_id = "action_id"
    action = Action(
        id=action_id,
        project="project_id",
        name="action1name",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        lockfile=b"",
        description="action1name",
        build_date=datetime.datetime.fromtimestamp(0),
    )
    action.add_delete_event = mock.MagicMock()
    action_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository_mock.get_action.return_value = action
    workflow_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.workflow_repository()
    workflow_repository_mock.list_workflows_where_action_used.return_value = [
        Workflow(
            id="123",
            name="workflow name",
            description="wfdescr",
            owner="wfowner",
            project="wfproject",
        )
    ]
    unit_of_work_mock: mock.MagicMock[IUnitOfWork] = app_container.unit_of_work()
    event_publisher_mock: mock.MagicMock[
        IEventPublisher
    ] = app_container.messaging_publisher()
    event_publisher_mock.publish = mock.AsyncMock()
    action_service: ActionService = app_container.action_service()
    with pytest.raises(ActionNotDeletableException):
        await action_service.delete_action(action_id)
    action_repository_mock.get_action.assert_called_once_with(action_id)
    workflow_repository_mock.list_workflows_where_action_used.assert_called_once_with(
        action_id
    )
    action.add_delete_event.assert_not_called()
    action_repository_mock.delete_action.assert_not_called()
    event_publisher_mock.publish.assert_not_called()
    unit_of_work_mock.commit.assert_not_called()


def test_categories(app_container: ApplicationContainer):
    category_1 = ActionCategory(id="category_1", name="test_1")
    category_2 = ActionCategory(id="category_2", name="test_2")
    action_repository_mock: mock.MagicMock[
        IActionRepository
    ] = app_container.action_repository()
    action_repository_mock.list_categories.return_value = [category_1, category_2]
    action_service: ActionService = app_container.action_service()
    assert action_service.list_categories() == [category_1, category_2]
    action_repository_mock.list_categories.assert_called_once()
