from unittest.mock import AsyncMock, MagicMock

from aio_pika import Channel, Exchange, ExchangeType
from aio_pika.abc import AbstractIncomingMessage
from google.protobuf import json_format

from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action_events import ActionDeletedEvent
from ryax.studio.infrastructure.messaging.utils.engine import MessagingEngine
from ryax.studio.infrastructure.messaging.utils.publisher import MessagingPublisher


def test_serialize_action_deleted_event(app_container: ApplicationContainer):
    engine = app_container.messaging_engine()
    exchange = MagicMock(Exchange)
    exchange.publish = AsyncMock()
    channel = MagicMock(Channel)
    channel.declare_exchange = AsyncMock(return_value="")
    engine.get_channel = AsyncMock(return_value=channel)
    event = ActionDeletedEvent(action_id="action ID", version="action_version")
    event_publisher: MessagingPublisher = app_container.messaging_publisher()
    event_publisher.handle_event(event)
    assert json_format.MessageToDict(
        event_publisher.handle_event(event),
        preserving_proto_field_name=True,
    ) == {"action_id": event.action_id, "version": event.version}


async def test_publish(app_container: ApplicationContainer, engine: MessagingEngine):
    # create a consumer
    channel = await engine.get_channel()
    await channel.set_qos(prefetch_count=1)
    exchange = await channel.declare_exchange(
        "domain_events", ExchangeType.TOPIC, durable=True
    )
    queue = await channel.declare_queue("studio_events", durable=True)
    await queue.bind(exchange, "Studio.ModuleDeleted")

    async def handle_message(message: AbstractIncomingMessage) -> None:
        """Method to handle received message"""
        async with message.process():
            assert message.type == "ModuleDeleted"

    await queue.consume(handle_message)

    # Publish
    event = ActionDeletedEvent(action_id="action ID", version="v1")
    event_publisher: MessagingPublisher = app_container.messaging_publisher()
    await event_publisher.publish([event])
