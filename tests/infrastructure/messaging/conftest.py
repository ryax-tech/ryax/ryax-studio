import pytest

from ryax.studio.container import ApplicationContainer


@pytest.fixture(scope="function")
def app_container():
    app_container = ApplicationContainer()
    yield app_container
    app_container.unwire()


@pytest.fixture(scope="function")
async def engine(loop, app_container):
    app_container.configuration.ryax_broker.from_env("RYAX_BROKER")
    engine = app_container.messaging_engine()
    await engine.connect()
    yield engine
    await engine.disconnect()
