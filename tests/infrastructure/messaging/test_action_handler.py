import datetime
from unittest import mock

import pytest
from aio_pika import IncomingMessage
from google.protobuf.struct_pb2 import Struct

from ryax.studio.application.action_service import ActionService
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action_events import ActionReadyEvent
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.infrastructure.messaging.handlers import action_handler
from ryax.studio.infrastructure.messaging.messages.action_builder_messages_pb2 import (
    ActionReady,
)


@pytest.fixture(scope="function")
def action_service_mock(app_container: ApplicationContainer):
    action_service_mock = mock.MagicMock(ActionService)
    app_container.action_service.override(action_service_mock)
    return action_service_mock


# TODO : Need to generate more data
async def test_on_action_ready_event(
    action_service_mock: ActionService, app_container: ApplicationContainer
):
    app_container.wire(modules=[action_handler])

    action_ready_input = ActionReady.ActionIO()
    action_ready_input.id = "input_id"
    action_ready_input.technical_name = "input_technical_name"
    action_ready_input.display_name = "input_display_name"
    action_ready_input.help = "input_help"
    action_ready_input.type = ActionReady.ActionIO.Type.INTEGER
    action_ready_input.enum_values.extend(["option1", "option2"])
    action_ready_input.default_value = "4"

    action_ready_output = ActionReady.ActionIO()
    action_ready_output.id = "output_id"
    action_ready_output.technical_name = "output_technical_name"
    action_ready_output.display_name = "output_display_name"
    action_ready_output.help = "output_help"
    action_ready_output.type = ActionReady.ActionIO.Type.STRING
    action_ready_output.enum_values.extend(["option1", "option2"])

    action_ready_logo = ActionReady.Logo()
    action_ready_logo.action_logo_id = "logo_id"
    action_ready_logo.action_logo_name = "logo_name"
    action_ready_logo.action_logo_extension = "png"
    action_ready_logo.action_logo_content = b"123"

    action_ready = ActionReady()
    action_ready.action_id = "action_id"
    action_ready.action_name = "action_name"
    action_ready.action_technical_name = "action_technical_name"
    action_ready.action_version = "action_version"
    action_ready.action_kind = ActionReady.Kind.SOURCE
    action_ready.action_description = "action_description"
    action_ready.action_logo.CopyFrom(action_ready_logo)
    action_ready.action_dynamic_outputs = True
    action_ready.action_owner_id = "action_owner"
    action_ready.action_inputs.extend([action_ready_input])
    action_ready.action_outputs.extend([action_ready_output])
    build_date = datetime.datetime.utcnow()
    action_ready.action_build_date.FromDatetime(build_date)
    action_ready.action_categories.extend(["new_category_name"])
    action_ready.action_project_id = "project_id"
    action_ready.action_addons.get_or_create("addon_key")
    value_struct = Struct()
    addons_values = {"addon_spec": "spec1_value"}
    value_struct.update(addons_values)
    action_ready.action_addons["addon_key"].CopyFrom(value_struct)
    action_ready_resources = action_ready.ActionResources(
        cpu=1.0,
        time=1,
    )
    action_ready.action_resources.CopyFrom(action_ready_resources)

    message = mock.MagicMock(IncomingMessage)
    message.body = action_ready.SerializeToString()
    await action_handler.on_action_ready(message)
    action_service_mock.add_action.assert_called_with(
        ActionReadyEvent(
            id=action_ready.action_id,
            name=action_ready.action_name,
            technical_name=action_ready.action_technical_name,
            version=action_ready.action_version,
            kind=ActionKind.SOURCE,
            description=action_ready.action_description,
            lockfile=action_ready.lockfile,
            logo=ActionReadyEvent.Logo(
                id=action_ready.action_logo.action_logo_id,
                name=action_ready.action_logo.action_logo_name,
                extension=action_ready.action_logo.action_logo_extension,
                content=action_ready.action_logo.action_logo_content,
            ),
            dynamic_outputs=action_ready.action_dynamic_outputs,
            owner_id=action_ready.action_owner_id,
            build_date=build_date,
            inputs=[
                ActionReadyEvent.ActionIO(
                    optional=False,
                    id=action_ready_input.id,
                    technical_name=action_ready_input.technical_name,
                    display_name=action_ready_input.display_name,
                    help=action_ready_input.help,
                    type=ActionIOType.INTEGER,
                    enum_values=["option1", "option2"],
                    default_value=action_ready_input.default_value,
                )
            ],
            outputs=[
                ActionReadyEvent.ActionIO(
                    optional=False,
                    id=action_ready_output.id,
                    technical_name=action_ready_output.technical_name,
                    display_name=action_ready_output.display_name,
                    help=action_ready_output.help,
                    type=ActionIOType.STRING,
                    enum_values=["option1", "option2"],
                )
            ],
            categories=action_ready.action_categories,
            project_id=action_ready.action_project_id,
            addons={"addon_key": addons_values},
            resources=ActionReadyEvent.ActionResources(
                cpu=action_ready.action_resources.cpu,
                time=action_ready.action_resources.time,
                memory=None,
                gpu=None,
            ),
        )
    )


async def test_on_action_ready_event_no_logo_no_resources(
    action_service_mock: mock.MagicMock,
    app_container: ApplicationContainer,
):
    app_container.wire(modules=[action_handler])

    action_ready_input = ActionReady.ActionIO()
    action_ready_input.id = "input_id"
    action_ready_input.technical_name = "input_technical_name"
    action_ready_input.display_name = "input_display_name"
    action_ready_input.help = "input_help"
    action_ready_input.type = ActionReady.ActionIO.Type.INTEGER
    action_ready_input.enum_values.extend(["option1", "option2"])
    action_ready_input.help = "4"

    action_ready_output = ActionReady.ActionIO()
    action_ready_output.id = "output_id"
    action_ready_output.technical_name = "output_technical_name"
    action_ready_output.display_name = "output_display_name"
    action_ready_output.help = "output_help"
    action_ready_output.type = ActionReady.ActionIO.Type.STRING
    action_ready_output.enum_values.extend(["option1", "option2"])

    action_ready = ActionReady()
    action_ready.action_id = "action_id"
    action_ready.action_name = "action_name"
    action_ready.action_technical_name = "action_technical_name"
    action_ready.action_version = "action_version"
    action_ready.action_kind = ActionReady.Kind.SOURCE
    action_ready.action_description = "action_description"
    action_ready.action_dynamic_outputs = True
    action_ready.action_owner_id = "action_owner"
    action_ready.action_inputs.extend([action_ready_input])
    action_ready.action_outputs.extend([action_ready_output])

    action_ready.action_project_id = "project_id"
    build_date = datetime.datetime.utcnow()
    action_ready.action_build_date.FromDatetime(build_date)
    action_ready.action_addons.update({})

    assert not action_ready.HasField("action_resources")

    message = mock.MagicMock(IncomingMessage)
    message.body = action_ready.SerializeToString()
    await action_handler.on_action_ready(message)
    action_service_mock.add_action.assert_called_with(
        ActionReadyEvent(
            id=action_ready.action_id,
            name=action_ready.action_name,
            technical_name=action_ready.action_technical_name,
            version=action_ready.action_version,
            kind=ActionKind.SOURCE,
            description=action_ready.action_description,
            lockfile=action_ready.lockfile,
            dynamic_outputs=action_ready.action_dynamic_outputs,
            owner_id=action_ready.action_owner_id,
            build_date=build_date,
            inputs=[
                ActionReadyEvent.ActionIO(
                    optional=False,
                    id=action_ready_input.id,
                    technical_name=action_ready_input.technical_name,
                    display_name=action_ready_input.display_name,
                    help=action_ready_input.help,
                    type=ActionIOType.INTEGER,
                    enum_values=["option1", "option2"],
                    default_value=action_ready_input.default_value,
                )
            ],
            outputs=[
                ActionReadyEvent.ActionIO(
                    optional=False,
                    id=action_ready_output.id,
                    technical_name=action_ready_output.technical_name,
                    display_name=action_ready_output.display_name,
                    help=action_ready_output.help,
                    type=ActionIOType.STRING,
                    enum_values=["option1", "option2"],
                )
            ],
            project_id=action_ready.action_project_id,
            logo=None,
            categories=[],
            addons={},
            resources=None,
        )
    )
