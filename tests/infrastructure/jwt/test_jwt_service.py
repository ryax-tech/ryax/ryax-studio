from unittest import mock

import jwt
import pytest

from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.auth.auth_token import AuthToken
from ryax.studio.infrastructure.jwt import JwtService


@pytest.fixture()
def app_container():
    container = ApplicationContainer()
    return container


def test_get_auth_token(app_container: ApplicationContainer):
    token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyLCJ1c2VyX2lkIjoidXNlcl8xIn0.gp4JbqCocCuzLen3tZyYk-9_kxYNH7bjUwP_VJ3A9JI"
    jwt_service: JwtService = app_container.jwt_service()
    jwt_service.secret_key = "secret_key"
    result = jwt_service.get_auth_token(token)
    assert result == AuthToken(user_id="user_1")


@mock.patch("jwt.decode", mock.MagicMock(side_effect=jwt.DecodeError))
def test_get_auth_with_fail_decoding_token(app_container: ApplicationContainer):
    jwt_service: JwtService = app_container.jwt_service()
    assert not jwt_service.get_auth_token("token")


@mock.patch("jwt.decode", mock.MagicMock(side_effect=jwt.ExpiredSignatureError))
def test_get_auth_with_expired_token(app_container: ApplicationContainer):
    jwt_service: JwtService = app_container.jwt_service()
    assert not jwt_service.get_auth_token("token")
