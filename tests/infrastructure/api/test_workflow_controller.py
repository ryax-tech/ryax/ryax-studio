import os
from tempfile import NamedTemporaryFile
from unittest import mock

import pytest
from aiohttp import MultipartWriter
from aiohttp.test_utils import TestClient
from marshmallow import RAISE

from ryax.studio.application.workflow_service import WorkflowService
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action_category import ActionCategory
from ryax.studio.domain.action.action_exceptions import ActionNotFoundException
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_category import (
    WorkflowActionCategory,
)
from ryax.studio.domain.workflow.entities.workflow_action_link import WorkflowActionLink
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.entities.workflow_result import (
    WorkflowResult,
    WorkflowResultType,
)
from ryax.studio.domain.workflow.views.workflow_action_input_view import (
    WorkflowActionInputView,
)
from ryax.studio.domain.workflow.views.workflow_action_output_view import (
    WorkflowActionOutputView,
)
from ryax.studio.domain.workflow.views.workflow_action_version_view import (
    WorkflowActionVersion,
)
from ryax.studio.domain.workflow.views.workflow_action_view import (
    WorkflowActionExtendedView,
)
from ryax.studio.domain.workflow.views.workflow_error_view import WorkflowErrorView
from ryax.studio.domain.workflow.views.workflow_result_view import WorkflowResultView
from ryax.studio.domain.workflow.views.workflow_view import WorkflowExtendedView
from ryax.studio.domain.workflow.workflow_exceptions import (
    EndpointAlreadyExistsException,
    FilestoreEntryNotFoundException,
    InvalidUserDefinedEndpointException,
    SchemaValidWorkflowInvalidException,
    WorkflowActionIONotFoundException,
    WorkflowBadZipFile,
    WorkflowHasNoTriggerException,
    WorkflowInvalidSchemaException,
    WorkflowNotDeployable,
    WorkflowNotFoundException,
    WorkflowNotValidException,
    WorkflowParsingYamlException,
    WorkflowSchemaFileTooLargeException,
    WorkflowYamlNotFound,
    WrongStatusFilterException,
)
from ryax.studio.domain.workflow.workflow_values import (
    CopyWorkflowData,
    CreateWorkflowData,
    CreateWorkflowResultData,
    WorkflowActionDeployStatus,
    WorkflowActionValidityStatus,
    WorkflowDeploymentStatus,
    WorkflowErrorCode,
    WorkflowInformations,
    WorkflowValidityStatus,
)
from ryax.studio.infrastructure.api.schemas.workflow_error_schema import (
    WorkflowErrorSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_result_schema import (
    AddWorkflowResultResponseSchema,
    WorkflowResultConfigurationSchema,
    WorkflowResultSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_schema import (
    WorkflowDetailsSchema,
    WorkflowEndpointSchema,
    WorkflowExtendedSchema,
    WorkflowSchema,
)


async def test_get_detailed_workflow(
    workflow_service_mock: WorkflowService, api_client: TestClient
):
    """Details workflow check"""
    workflow_1 = WorkflowExtendedView(
        id="orkflow_1_id",
        name="Workflow 1 name",
        project="",
        description="Workflow 1 description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        actions=[
            WorkflowActionExtendedView(
                id="module1",
                name="Module 1",
                version="1.0",
                description="test",
                kind=ActionKind.PROCESSOR,
                action_id="1",
                addons_inputs=[],
                inputs=[
                    WorkflowActionInputView(
                        id="1234",
                        technical_name="io1",
                        display_name="my io 1",
                        help="do this",
                        type=ActionIOType.ENUM,
                        enum_values=["1", "2"],
                        static_value="2",
                        reference_value=None,
                        reference_output=None,
                        workflow_file=None,
                        project_variable_value=None,
                        optional=False,
                    ),
                    WorkflowActionInputView(
                        id="12345",
                        technical_name="io1",
                        display_name="my io 1",
                        help="do this",
                        type=ActionIOType.FILE,
                        enum_values=[],
                        static_value=None,
                        reference_value=None,
                        reference_output=None,
                        workflow_file=WorkflowFile(
                            size=35088,
                            name="Screenshot from 2022-06-28 18-42-06",
                            path="iodata/baf8eb22-eadb-41b5-bd9b-24dd284cb097/Screenshot from 2022-06-28 18-42-06.png",
                            extension="png",
                            id="86bac05e-198c-487c-93f0-841c2f5b05a8",
                        ),
                        project_variable_value=None,
                        optional=False,
                    ),
                ],
                outputs=[
                    WorkflowActionOutputView(
                        id="12345",
                        technical_name="io1",
                        display_name="my io 1",
                        help="do this",
                        type=ActionIOType.STRING,
                        enum_values=[],
                        optional=False,
                    )
                ],
                dynamic_outputs=[],
                technical_name="action-1",
                categories=[WorkflowActionCategory(id="123", name="cat1")],
                versions=[WorkflowActionVersion(id="234", version="1.0")],
                has_dynamic_outputs=True,
                workflow_id="123",
                custom_name="",
                validity_status=WorkflowActionValidityStatus.VALID,
                deployment_status=WorkflowActionDeployStatus.ERROR,
                endpoint="",
                position_y=None,
                position_x=None,
                resources=None,
                objectives=None,
                constraints=None,
            )
        ],
    )
    workflow_service_mock.get_workflow_extended = mock.MagicMock(
        return_value=workflow_1
    )
    response = await api_client.get("/v2/workflows/fakeworkflowid")
    assert response.status == 200
    response_json = await response.json()
    dump_object = WorkflowExtendedSchema().dump(workflow_1)
    assert response_json == dump_object
    assert response_json["modules"][0]["inputs"][0]["type"] == "enum"
    assert response_json["modules"][0]["inputs"][1]["type"] == "file"
    assert (
        response_json["modules"][0]["inputs"][1]["static_value"]
        == "Screenshot from 2022-06-28 18-42-06.png"
    )
    assert response_json["modules"][0]["outputs"][0]["type"] == "string"


async def test_list_workflows_with_search(
    workflow_service_mock: WorkflowService, api_client: TestClient
):
    """When fetching workflows endpoint should return list of searched workflows"""
    search = "test"
    workflows = [
        {
            "id": "workflow_1_id",
            "name": "Workflow 1 name",
            "description": "Workflow 1 description",
            "validity_status": WorkflowValidityStatus.VALID,
            "deployment_status": WorkflowDeploymentStatus.NONE,
            "has_form": True,
            "endpoint_prefix": "endpoint1",
            "categories": [],
            "parameters": [],
            "trigger": "test1",
        },
        {
            "id": "workflow_2_id",
            "name": "Workflow 2 name",
            "description": "Workflow 2 description",
            "validity_status": WorkflowValidityStatus.INVALID,
            "deployment_status": WorkflowDeploymentStatus.ERROR,
            "has_form": False,
            "endpoint_prefix": "endpoint2",
            "categories": ["test", "toto"],
            "parameters": [],
            "trigger": "custom",
        },
    ]
    workflow_service_mock.list_workflows = mock.MagicMock(return_value=workflows)
    response = await api_client.get(f"/workflows?search={search}")
    assert response.status == 200
    result = await response.json()
    WorkflowSchema(many=True).load(result, unknown=RAISE, partial=False)
    assert result == [
        {
            "categories": [],
            "deployment_status": "None",
            "description": "Workflow 1 description",
            "endpoint_prefix": "endpoint1",
            "has_form": True,
            "id": "workflow_1_id",
            "name": "Workflow 1 name",
            "parameters": [],
            "status": "valid",
            "trigger": "test1",
        },
        {
            "categories": ["test", "toto"],
            "deployment_status": "Error",
            "description": "Workflow 2 description",
            "endpoint_prefix": "endpoint2",
            "has_form": False,
            "id": "workflow_2_id",
            "name": "Workflow 2 name",
            "parameters": [],
            "status": "invalid",
            "trigger": "custom",
        },
    ]
    workflow_service_mock.list_workflows.assert_called_with(
        "project_id", search=search, deployment_status_filter_value=None
    )


async def test_list_workflows_with_deployment_status_filter(
    workflow_service_mock: WorkflowService, api_client: TestClient
):
    """When fetching workflows endpoint should return list of deployed workflows"""
    deployment_status_filter = "Deployed"
    workflows = [
        {
            "id": "workflow_1_id",
            "name": "Workflow 1 name",
            "description": "Workflow 1 description",
            "validity_status": WorkflowValidityStatus.VALID,
            "deployment_status": WorkflowDeploymentStatus.NONE,
            "has_form": True,
            "endpoint_prefix": "endpoint1",
            "categories": [],
            "parameters": [],
            "trigger": "test1",
        },
        {
            "id": "workflow_2_id",
            "name": "Workflow 2 name",
            "description": "Workflow 2 description",
            "validity_status": WorkflowValidityStatus.INVALID,
            "deployment_status": WorkflowDeploymentStatus.ERROR,
            "has_form": False,
            "endpoint_prefix": "endpoint2",
            "categories": ["test", "toto"],
            "parameters": [],
            "trigger": "custom",
        },
    ]
    workflow_service_mock.list_workflows = mock.MagicMock(return_value=workflows)
    response = await api_client.get(
        f"/workflows?deployment_status={deployment_status_filter}"
    )
    assert response.status == 200
    result = await response.json()
    WorkflowSchema(many=True).load(result, unknown=RAISE, partial=False)
    assert result == [
        {
            "categories": [],
            "deployment_status": "None",
            "description": "Workflow 1 description",
            "endpoint_prefix": "endpoint1",
            "has_form": True,
            "id": "workflow_1_id",
            "name": "Workflow 1 name",
            "parameters": [],
            "status": "valid",
            "trigger": "test1",
        },
        {
            "categories": ["test", "toto"],
            "deployment_status": "Error",
            "description": "Workflow 2 description",
            "endpoint_prefix": "endpoint2",
            "has_form": False,
            "id": "workflow_2_id",
            "name": "Workflow 2 name",
            "parameters": [],
            "status": "invalid",
            "trigger": "custom",
        },
    ]

    workflow_service_mock.list_workflows.assert_called_with(
        "project_id",
        search=None,
        deployment_status_filter_value=deployment_status_filter,
    )


async def test_list_workflows_with_incorrect_deployment_status_filter(
    workflow_service_mock, api_client: TestClient
):
    deployment_status_filter = "Dep"
    workflow_service_mock.list_workflows.side_effect = WrongStatusFilterException()
    response = await api_client.get(
        f"/workflows?deployment_status={deployment_status_filter}"
    )
    assert response.status == 400
    assert await response.json() == {"error": "The status filter is not correct"}
    workflow_service_mock.list_workflows.assert_called_once_with(
        "project_id",
        search=None,
        deployment_status_filter_value=deployment_status_filter,
    )


async def test_list_workflows_with_search_and_deployment_status_filter(
    workflow_service_mock: WorkflowService, api_client: TestClient
):
    """When fetching workflows endpoint should return list of searched workflows"""
    search = "test"
    deployment_status_filter = "Deployed"
    workflow_1 = {
        "id": "workflow_1_id",
        "name": "Workflow 1 name",
        "description": "Workflow 1 description",
        "validity_status": WorkflowValidityStatus.VALID,
        "deployment_status": WorkflowDeploymentStatus.NONE,
        "has_form": True,
        "endpoint_prefix": "endpoint1",
        "categories": [],
        "parameters": [],
        "trigger": "test1",
    }
    workflow_service_mock.list_workflows = mock.MagicMock(return_value=[workflow_1])
    response = await api_client.get(
        f"/workflows?search={search}&deployment_status={deployment_status_filter}"
    )
    assert response.status == 200
    assert await response.json() == [
        {
            "categories": [],
            "deployment_status": "None",
            "description": "Workflow 1 description",
            "endpoint_prefix": "endpoint1",
            "has_form": True,
            "id": "workflow_1_id",
            "name": "Workflow 1 name",
            "parameters": [],
            "status": "valid",
            "trigger": "test1",
        }
    ]
    workflow_service_mock.list_workflows.assert_called_with(
        "project_id",
        search=search,
        deployment_status_filter_value=deployment_status_filter,
    )


async def test_create_workflow(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Create workflow through api should work and return data when data is valid"""
    workflow_data = {"name": "Workflow name", "description": "Workflow description"}
    workflow_id = "workflow_id"
    workflow_service_mock.create_workflow.return_value = workflow_id
    response = await api_client.post("/workflows", data=workflow_data)
    assert response.status == 201
    assert await response.json() == {"workflow_id": workflow_id}
    workflow_service_mock.create_workflow.assert_called_once_with(
        CreateWorkflowData(
            name=workflow_data["name"], description=workflow_data["description"]
        ),
        "user",
        "project_id",
    )


async def test_create_workflow_with_from_workflow(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Create workflow through api should work and return data when data is valid"""
    workflow_data = {
        "name": "Workflow name",
        "description": "Workflow description",
        "from_workflow": "workflow_id_2",
    }
    workflow_id = "workflow_id"
    workflow_service_mock.copy_workflow.return_value = workflow_id
    response = await api_client.post("/workflows", data=workflow_data)
    assert response.status == 201
    assert await response.json() == {"workflow_id": workflow_id}
    workflow_service_mock.copy_workflow.assert_called_once_with(
        CopyWorkflowData(
            from_workflow=workflow_data["from_workflow"],
            name=workflow_data["name"],
            description=workflow_data["description"],
        ),
        "user",
        "project_id",
    )


async def test_create_workflow_with_from_workflow_when_not_exists(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Create workflow through api should work and return data when data is valid"""
    workflow_data = {
        "name": "Workflow name",
        "description": "Workflow description",
        "from_workflow": "workflow_id_2",
    }
    workflow_service_mock.copy_workflow.side_effect = WorkflowNotFoundException()
    response = await api_client.post("/workflows", data=workflow_data)
    assert response.status == 400
    assert await response.json() == {"error": "Workflow not found"}
    workflow_service_mock.copy_workflow.assert_called_once_with(
        CopyWorkflowData(
            from_workflow=workflow_data["from_workflow"],
            name=workflow_data["name"],
            description=workflow_data["description"],
        ),
        "user",
        "project_id",
    )


async def test_create_workflow_with_invalid_data(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Create workflow through api should return error when data is not valid"""
    workflow_data = {}
    response = await api_client.post("/workflows", data=workflow_data)
    assert response.status == 422
    workflow_service_mock.create_workflow.assert_not_called()


async def test_get_workflow(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Get workflow through api should return data"""
    workflow_id = "workflow_id"
    workflow = Workflow(
        id="workflow_id",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        project="project1",
        owner="",
        actions=[
            WorkflowAction(
                id="workflow_module_id",
                name="Workflow Module",
                technical_name="Workflow Module technical name",
                version="1.0",
                description="Workflow action description",
                kind=ActionKind.SOURCE,
                validity_status=WorkflowActionValidityStatus.VALID,
                deployment_status=WorkflowActionDeployStatus.NONE,
                custom_name="Workflow action custom name",
                position_x=0,
                position_y=0,
                has_dynamic_outputs=True,
                action_id="module_id",
                categories=[
                    WorkflowActionCategory(
                        id="390a8106-0156-4127-8030-3638e3358513", name="category1"
                    ),
                    ActionCategory(
                        id="925c4742-1207-40d6-a1c0-b84e1d1caeb7", name="category2"
                    ),
                ],
                workflow_id="workflow_id",
                endpoint="",
            )
        ],
        actions_links=[
            WorkflowActionLink(
                id="workflow_module_link_id",
                upstream_action_id="input_module_id",
                downstream_action_id="output_module_id",
            )
        ],
    )

    workflow_service_mock.get_workflow.return_value = workflow
    response = await api_client.get(f"/workflows/{workflow_id}")
    assert response.status == 200
    result = await response.json()
    WorkflowDetailsSchema().load(result, unknown=RAISE, partial=False)
    assert result == {
        "id": "workflow_id",
        "name": "Workflow name",
        "description": "Workflow description",
        "status": "valid",
        "deployment_status": "None",
        "deployment_error": None,
        "has_form": False,
        "modules": [
            {
                "id": "workflow_module_id",
                "name": "Workflow Module",
                "technical_name": "Workflow Module technical name",
                "version": "1.0",
                "description": "Workflow action description",
                "kind": "Source",
                "status": "valid",
                "deployment_status": "None",
                "custom_name": "Workflow action custom name",
                "position_x": 0,
                "position_y": 0,
                "has_dynamic_outputs": True,
                "module_id": "module_id",
                "categories": [
                    {"id": "390a8106-0156-4127-8030-3638e3358513", "name": "category1"},
                    {"id": "925c4742-1207-40d6-a1c0-b84e1d1caeb7", "name": "category2"},
                ],
                "endpoint": "",
                "resources": None,
            }
        ],
        "modules_links": [
            {
                "id": "workflow_module_link_id",
                "input_module_id": "input_module_id",
                "output_module_id": "output_module_id",
            }
        ],
    }
    workflow_service_mock.get_workflow.assert_called_once_with(workflow_id)


async def test_get_workflow_with_form(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Get workflow through api should return data. has_form should be true when postgw is included in the workflow"""
    workflow_id = "workflow_id"
    workflow = Workflow(
        id="workflow_id",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        project="project1",
        owner="",
        actions=[
            WorkflowAction(
                id="postgw_id",
                name="postgw_name",
                technical_name="postgw",
                version="1.0",
                description="Workflow action description",
                kind=ActionKind.SOURCE,
                validity_status=WorkflowActionValidityStatus.VALID,
                deployment_status=WorkflowActionDeployStatus.NONE,
                custom_name="Workflow action custom name",
                position_x=0,
                position_y=0,
                has_dynamic_outputs=True,
                action_id="module_id",
                categories=[
                    WorkflowActionCategory(
                        id="390a8106-0156-4127-8030-3638e3358513", name="category1"
                    ),
                    WorkflowActionCategory(
                        id="925c4742-1207-40d6-a1c0-b84e1d1caeb7", name="category2"
                    ),
                ],
                workflow_id="workflow_id",
                endpoint="",
                resources=None,
            )
        ],
        actions_links=[
            WorkflowActionLink(
                id="workflow_module_link_id",
                upstream_action_id="input_module_id",
                downstream_action_id="output_module_id",
            )
        ],
    )

    workflow_service_mock.get_workflow.return_value = workflow
    response = await api_client.get(f"/workflows/{workflow_id}")
    assert response.status == 200
    result = await response.json()
    WorkflowDetailsSchema().load(result, unknown=RAISE, partial=False)
    assert result == {
        "id": "workflow_id",
        "name": "Workflow name",
        "description": "Workflow description",
        "status": "valid",
        "deployment_status": "None",
        "deployment_error": None,
        "has_form": True,
        "modules": [
            {
                "id": "postgw_id",
                "name": "postgw_name",
                "technical_name": "postgw",
                "version": "1.0",
                "description": "Workflow action description",
                "kind": "Source",
                "status": "valid",
                "deployment_status": "None",
                "custom_name": "Workflow action custom name",
                "position_x": 0,
                "position_y": 0,
                "has_dynamic_outputs": True,
                "module_id": "module_id",
                "categories": [
                    {"id": "390a8106-0156-4127-8030-3638e3358513", "name": "category1"},
                    {"id": "925c4742-1207-40d6-a1c0-b84e1d1caeb7", "name": "category2"},
                ],
                "endpoint": "",
                "resources": None,
            }
        ],
        "modules_links": [
            {
                "id": "workflow_module_link_id",
                "input_module_id": "input_module_id",
                "output_module_id": "output_module_id",
            }
        ],
    }
    workflow_service_mock.get_workflow.assert_called_once_with(workflow_id)


async def test_get_workflow_when_not_exists(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Get workflow through api should return not found error when not exists"""
    workflow_id = "workflow_id"
    workflow_service_mock.get_workflow.side_effect = WorkflowNotFoundException()
    response = await api_client.get(f"/workflows/{workflow_id}")
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_service_mock.get_workflow.assert_called_once_with(workflow_id)


async def test_update_workflow(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Update workflow through api should work and return workflow"""
    workflow_id = "workflow_id"
    workflow_data = {
        "name": "Workflow updated name",
        "description": "Workflow updated description",
    }
    workflow = Workflow(
        id="workflow_id",
        name="workflow name",
        description="workflow description",
        owner="wfowner",
        project="wfproject",
    )
    workflow_service_mock.update_workflow.return_value = workflow
    response = await api_client.put(f"/workflows/{workflow_id}", data=workflow_data)
    assert response.status == 200
    assert await response.json() is None
    workflow_service_mock.update_workflow.assert_called_once_with(
        workflow_id,
        WorkflowInformations(
            name=workflow_data["name"], description=workflow_data["description"]
        ),
        "user",
    )


async def test_update_workflow_when_not_exists(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Update workflow through api should return error when not exists"""
    workflow_id = "workflow_id"
    workflow_data = {
        "name": "Workflow updated name",
        "description": "Workflow updated description",
    }
    workflow_service_mock.update_workflow.side_effect = WorkflowNotFoundException()
    response = await api_client.put(f"/workflows/{workflow_id}", data=workflow_data)
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_service_mock.update_workflow.assert_called_once_with(
        workflow_id,
        WorkflowInformations(
            name=workflow_data["name"], description=workflow_data["description"]
        ),
        "user",
    )


async def test_delete_workflow(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Delete workflow through api should work and return nothing when not exists"""
    workflow_id = "workflow_id"
    workflow = Workflow(
        id="workflow_id",
        name="workflow name",
        description="workflow description",
        owner="wfowner",
        project="wfproject",
    )
    workflow_service_mock.delete_workflow.return_value = workflow
    response = await api_client.delete(f"/workflows/{workflow_id}")
    assert response.status == 200
    assert await response.json() is None
    workflow_service_mock.delete_workflow.assert_called_once_with(workflow_id, "")


async def test_delete_workflow_when_not_exists(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Delete workflow through api should return error when not exists"""
    workflow_id = "workflow_id"
    workflow_service_mock.delete_workflow.side_effect = WorkflowNotFoundException
    response = await api_client.delete(f"/workflows/{workflow_id}")
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_service_mock.delete_workflow.assert_called_once_with(workflow_id, "")


async def test_get_workflow_errors(
    workflow_service_mock: WorkflowService, api_client: TestClient
):
    """Fetching workflow errors endpoint should return list of workflow errors"""
    workflow_id = "workflow_id"
    workflow_error = WorkflowErrorView(
        id="workflow_error_id_1",
        code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
        workflow_id="workflow_id",
    )
    workflow_action_error = WorkflowErrorView(
        id="workflow_error_id_1",
        code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
        workflow_action_id="workflow_id",
    )
    workflow_service_mock.get_workflow_errors.return_value = [
        workflow_error,
        workflow_action_error,
    ]
    response = await api_client.get(f"/workflows/{workflow_id}/errors")
    assert response.status == 200
    result = await response.json()
    WorkflowErrorSchema(many=True).load(result, unknown=RAISE, partial=False)
    assert result == [
        {
            "id": workflow_error.id,
            "code": workflow_error.code.value,
            "workflow_module_id": workflow_error.workflow_action_id,
        },
        {
            "id": workflow_action_error.id,
            "code": workflow_action_error.code.value,
            "workflow_module_id": workflow_action_error.workflow_action_id,
        },
    ]


async def test_get_workflow_errors_when_workflow_not_found(
    workflow_service_mock: WorkflowService, api_client: TestClient
):
    """Fetching workflow errors endpoint should return error when workflow not found"""
    workflow_id = "workflow_id"
    workflow_service_mock.get_workflow_errors.side_effect = WorkflowNotFoundException
    response = await api_client.get(f"/workflows/{workflow_id}/errors")
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}


async def test_deploy_workflow(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Deploy workflow through api should work and return nothing when exists"""
    workflow_id = "workflow_id"
    workflow_service_mock.deploy_workflow.return_value = None
    response = await api_client.post(f"/workflows/{workflow_id}/deploy")
    assert response.status == 200
    assert await response.json() is None
    workflow_service_mock.deploy_workflow.assert_called_once_with(workflow_id)


async def test_deploy_workflow_when_not_exists(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Deploy workflow through api should return error when not exists"""
    workflow_id = "workflow_id"
    workflow_service_mock.deploy_workflow.side_effect = WorkflowNotFoundException
    response = await api_client.post(f"/workflows/{workflow_id}/deploy")
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_service_mock.deploy_workflow.assert_called_once_with(workflow_id)


async def test_deploy_workflow_when_invalid(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Deploy workflow through api should return error when invalid"""
    workflow_id = "workflow_id"
    workflow_service_mock.deploy_workflow.side_effect = WorkflowNotValidException
    response = await api_client.post(f"/workflows/{workflow_id}/deploy")
    assert response.status == 400
    assert await response.json() == {"error": "Workflow not valid"}
    workflow_service_mock.deploy_workflow.assert_called_once_with(workflow_id)


async def test_deploy_workflow_when_not_deployable(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Deploy workflow through api should return error when not deployable"""
    workflow_id = "workflow_id"
    workflow_service_mock.deploy_workflow.side_effect = WorkflowNotDeployable
    response = await api_client.post(f"/workflows/{workflow_id}/deploy")
    assert response.status == 400
    assert await response.json() == {
        "error": "Workflow can't be deployed at this status"
    }
    workflow_service_mock.deploy_workflow.assert_called_once_with(workflow_id)


async def test_stop_deploy_workflow(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Stop workflow deployment through api should work and return nothing when exists"""
    workflow_id = "workflow_id"
    workflow_service_mock.stop_deploy_workflow.return_value = None
    response = await api_client.post(f"/workflows/{workflow_id}/stop")
    assert response.status == 200
    assert await response.json() is None
    workflow_service_mock.stop_deploy_workflow.assert_called_once_with(
        workflow_id, False
    )


async def test_stop_deploy_workflow_when_not_exists(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Stop workflow deployment through api should return error when not exists"""
    workflow_id = "workflow_id"
    workflow_service_mock.stop_deploy_workflow.side_effect = WorkflowNotFoundException
    response = await api_client.post(f"/workflows/{workflow_id}/stop")
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_service_mock.stop_deploy_workflow.assert_called_once_with(
        workflow_id, False
    )


async def test_stop_deploy_workflow_graceful(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    """Stop workflow deployment through api should work and return nothing when exists"""
    workflow_id = "workflow_id"
    workflow_service_mock.stop_deploy_workflow.return_value = None
    response = await api_client.post(f"/workflows/{workflow_id}/stop?graceful=true")
    assert response.status == 200
    assert await response.json() is None
    workflow_service_mock.stop_deploy_workflow.assert_called_once_with(
        workflow_id, True
    )


async def test_import_workflow(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_id = "workflow_id"
    workflow_service_mock.import_workflow.return_value = workflow_id
    file = os.urandom(8193)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.packaging")
    response = await api_client.post("/workflows/import", data=mp_writer)
    assert response.status == 201
    assert await response.json() == {"workflow_id": workflow_id}
    workflow_service_mock.import_workflow.assert_called_once_with(
        file, "user", "project_id"
    )


async def test_import_workflow_when_yaml_load_exception(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.import_workflow.side_effect = WorkflowParsingYamlException
    file = os.urandom(8193)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.packaging")
    response = await api_client.post("/workflows/import", data=mp_writer)
    assert response.status == 400
    assert await response.json() == {
        "error": "Error while parsing workflow packaging data"
    }
    workflow_service_mock.import_workflow.assert_called_once_with(
        file, "user", "project_id"
    )


async def test_import_workflow_when_invalid_schema(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.import_workflow.side_effect = WorkflowInvalidSchemaException
    file = os.urandom(8193)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.packaging")
    response = await api_client.post("/workflows/import", data=mp_writer)
    assert response.status == 400
    assert await response.json() == {"error": "The workflow schema is not valid"}
    workflow_service_mock.import_workflow.assert_called_once_with(
        file, "user", "project_id"
    )


async def test_import_workflow_when_action_not_found(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.import_workflow.side_effect = ActionNotFoundException
    file = os.urandom(8193)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.packaging")
    response = await api_client.post("/workflows/import", data=mp_writer)
    assert response.status == 404
    assert await response.json() == {"error": "Action not found"}
    workflow_service_mock.import_workflow.assert_called_once_with(
        file, "user", "project_id"
    )


async def test_import_workflow_when_yaml_file_not_found(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.import_workflow.side_effect = WorkflowYamlNotFound
    file = os.urandom(8193)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.packaging")
    response = await api_client.post("/workflows/import", data=mp_writer)
    assert response.status == 400
    assert await response.json() == {"error": "Workflow yaml file not found"}
    workflow_service_mock.import_workflow.assert_called_once_with(
        file, "user", "project_id"
    )


async def test_import_workflow_when_bad_zipfile(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.import_workflow.side_effect = WorkflowYamlNotFound
    with MultipartWriter() as mp_writer:
        mp_writer_2 = MultipartWriter()
        file_1 = mp_writer_2
        part = mp_writer.append(file_1)
        part.set_content_disposition("attachment", filename="test.packaging")
    response = await api_client.post("/workflows/import", data=mp_writer)
    assert response.status == 400
    assert await response.json() == {"error": "Workflow importing bad file"}
    workflow_service_mock.import_workflow.assert_not_called()


async def test_export_workflow(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_id = "workflow_id"
    file = NamedTemporaryFile()
    file.write(b"file_content")
    file.seek(0)
    workflow_service_mock.export_workflow.return_value = file.name
    response = await api_client.get(f"/workflows/{workflow_id}/export")
    assert response.status == 200
    assert await response.read() == file.read()
    assert response.content_type == "application/zip"
    workflow_service_mock.export_workflow.assert_called_once_with(workflow_id)


async def test_export_workflow_when_not_found(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_id = "workflow_id"
    workflow_service_mock.export_workflow.side_effect = WorkflowNotFoundException

    response = await api_client.get(f"/workflows/{workflow_id}/export")
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_service_mock.export_workflow.assert_called_once_with(workflow_id)


async def test_export_workflow_when_file_not_found(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_id = "workflow_id"
    workflow_service_mock.export_workflow.side_effect = FilestoreEntryNotFoundException

    response = await api_client.get(f"/workflows/{workflow_id}/export")
    assert response.status == 404
    assert await response.json() == {"error": "Static file is not found"}
    workflow_service_mock.export_workflow.assert_called_once_with(workflow_id)


async def test_get_workflow_schema(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_schema = {"Mock workflow schema": "Mock workflow schema value"}
    workflow_service_mock.get_workflow_schema.return_value = workflow_schema
    response = await api_client.get("/workflows/schema/get")
    assert response.status == 200
    assert await response.json() == workflow_schema
    workflow_service_mock.get_workflow_schema.assert_called_once()


async def test_get_workflow_schema_when_not_valid_attribute(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    error_message = "Schema field is invalid"
    workflow_service_mock.get_workflow_schema.side_effect = AttributeError(
        error_message
    )
    response = await api_client.get("/workflows/schema/get")
    assert response.status == 404
    assert await response.json() == {"error": error_message}
    workflow_service_mock.get_workflow_schema.assert_called_once()


async def test_validate_workflow_schema(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.validate_workflow_schema.return_value = True
    file = os.urandom(4096)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.packaging")
    response = await api_client.post("/workflows/schema/validate", data=mp_writer)
    assert response.status == 200
    assert await response.json() == {
        "status": "Valid",
        "message": "Workflow packaging is valid",
    }
    workflow_service_mock.validate_workflow_schema.assert_called_once_with(
        file, "user", "project_id"
    )


async def test_validate_workflow_schema_when_schema_valid_workflow_invalid(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.validate_workflow_schema.side_effect = (
        SchemaValidWorkflowInvalidException
    )
    file = os.urandom(64)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.packaging")
    response = await api_client.post("/workflows/schema/validate", data=mp_writer)
    assert response.status == 200
    assert await response.json() == {
        "status": "Valid",
        "message": SchemaValidWorkflowInvalidException.message,
    }
    workflow_service_mock.validate_workflow_schema.assert_called_once_with(
        file, "user", "project_id"
    )


async def test_validate_workflow_schema_when_file_too_large(
    workflow_service_mock: mock.MagicMock,
    api_client: TestClient,
    app_container: ApplicationContainer,
):
    app_container.configuration.from_dict({"max_upload_size": 20})
    file = os.urandom(30)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.packaging")
    response = await api_client.post("/workflows/schema/validate", data=mp_writer)
    assert response.status == 400
    assert await response.json() == {
        "error": WorkflowSchemaFileTooLargeException.message
    }


async def test_validate_workflow_schema_when_yaml_parsing_exception(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.validate_workflow_schema.side_effect = (
        WorkflowParsingYamlException
    )
    file = os.urandom(64)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.packaging")
    response = await api_client.post("/workflows/schema/validate", data=mp_writer)
    assert response.status == 200
    assert await response.json() == {
        "status": "Invalid",
        "message": WorkflowParsingYamlException.message,
    }
    workflow_service_mock.validate_workflow_schema.assert_called_once_with(
        file, "user", "project_id"
    )


async def test_validate_workflow_schema_when_invalid_schema(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.validate_workflow_schema.side_effect = (
        WorkflowInvalidSchemaException
    )
    file = os.urandom(64)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.packaging")
    response = await api_client.post("/workflows/schema/validate", data=mp_writer)
    assert response.status == 200
    assert await response.json() == {
        "status": "Invalid",
        "message": WorkflowInvalidSchemaException.message,
    }
    workflow_service_mock.validate_workflow_schema.assert_called_once_with(
        file, "user", "project_id"
    )


async def test_validate_workflow_schema_when_action_not_found(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.validate_workflow_schema.side_effect = ActionNotFoundException
    file = os.urandom(64)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.packaging")
    response = await api_client.post("/workflows/schema/validate", data=mp_writer)
    assert response.status == 200
    assert await response.json() == {
        "status": "Invalid",
        "message": ActionNotFoundException.message,
    }
    workflow_service_mock.validate_workflow_schema.assert_called_once_with(
        file, "user", "project_id"
    )


async def test_validate_workflow_schema_when_bad_zipfile(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    with MultipartWriter() as mp_writer:
        mp_writer_2 = MultipartWriter()
        file_1 = mp_writer_2
        part = mp_writer.append(file_1)
        part.set_content_disposition("attachment", filename="test.packaging")
    response = await api_client.post("/workflows/schema/validate", data=mp_writer)
    assert response.status == 400
    assert await response.json() == {"error": WorkflowBadZipFile.message}


async def test_get_workflow_results(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.list_workflow_results.return_value = [
        WorkflowResultView(
            key="key",
            workflow_action_io_id="foo",
            description="description",
            type=ActionIOType.STRING,
        )
    ]
    workflow_id = "wf_id"
    response = await api_client.get(f"/v2/workflows/{workflow_id}/results")
    assert response.status == 200
    result = await response.json()
    WorkflowResultSchema(many=True).load(result, unknown=RAISE, partial=False)
    assert result == [
        {
            "workflow_module_io_id": "foo",
            "key": "key",
            "type": "string",
            "description": "description",
        }
    ]

    workflow_service_mock.list_workflow_results.assert_called_once_with(workflow_id)


async def test_get_workflow_results_with_global_type(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.list_workflow_results.return_value = [
        WorkflowResultView(
            key="key",
            workflow_action_io_id="foo",
            description="description",
            type=ActionIOType.STRING,
        )
    ]
    workflow_id = "wf_id"
    response = await api_client.get(
        f"/v2/workflows/{workflow_id}/run-results",
    )
    assert response.status == 200
    result = await response.json()
    WorkflowResultConfigurationSchema().load(result, unknown=RAISE, partial=False)
    assert result == {
        "type": WorkflowResultType.JSON.value,
        "workflow_results": [
            {
                "workflow_module_io_id": "foo",
                "key": "key",
                "type": "string",
                "description": "description",
            }
        ],
    }

    workflow_service_mock.list_workflow_results.assert_called_once_with(workflow_id)


async def test_add_workflow_results(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    add_workflow_results_data = {
        "workflow_results_to_add": [
            {
                "key": "foo",
                "workflow_module_io_id": "qux",
            },
            {
                "key": "foo2",
                "workflow_module_io_id": "qux2",
            },
        ]
    }

    workflow_service_mock.overwrite_workflow_results = mock.MagicMock(
        return_value=[
            WorkflowResult(
                id="id1",
                key="foo",
                workflow_id="bar",
                workflow_action_input_id="qux",
                workflow_action_output_id=None,
            ),
            WorkflowResult(
                id="id2",
                key="foo",
                workflow_id="bar",
                workflow_action_output_id="qux2",
                workflow_action_input_id=None,
            ),
        ]
    )
    workflow_id = "foo"
    response = await api_client.put(
        f"/v2/workflows/{workflow_id}/results", json=add_workflow_results_data
    )
    assert response.status == 200
    workflow_service_mock.overwrite_workflow_results.assert_called_once_with(
        workflow_id,
        [
            CreateWorkflowResultData(
                key=d["key"],
                workflow_action_io_id=d["workflow_module_io_id"],
                workflow_id="foo",
            )
            for d in add_workflow_results_data["workflow_results_to_add"]
        ],
    )
    result = await response.json()
    AddWorkflowResultResponseSchema(many=True).load(
        result, unknown=RAISE, partial=False
    )
    assert result == [{"id": "id1"}, {"id": "id2"}]


async def test_get_workflow_results_when_workflow_not_found(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.list_workflow_results.side_effect = WorkflowNotFoundException
    workflow_id = "foo"
    response = await api_client.get(f"/v2/workflows/{workflow_id}/results")
    assert response.status == 400
    assert await response.json() == {"error": WorkflowNotFoundException.message}


async def test_add_workflow_results_when_workflow_not_found(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    add_workflow_results_data = {
        "workflow_results_to_add": [
            {
                "key": "foo",
                "workflow_module_io_id": "qux",
            },
            {
                "key": "foo2",
                "workflow_module_io_id": "qux2",
            },
        ]
    }
    workflow_service_mock.overwrite_workflow_results = mock.MagicMock(
        side_effect=WorkflowNotFoundException
    )
    workflow_id = "foo"
    response = await api_client.put(
        f"/v2/workflows/{workflow_id}/results", json=add_workflow_results_data
    )
    assert response.status == 404
    assert await response.json() == {"error": WorkflowNotFoundException.message}
    workflow_service_mock.overwrite_workflow_results.assert_called_once_with(
        workflow_id,
        [
            CreateWorkflowResultData(
                key=d["key"],
                workflow_action_io_id=d["workflow_module_io_id"],
                workflow_id="foo",
            )
            for d in add_workflow_results_data["workflow_results_to_add"]
        ],
    )


async def test_add_workflow_results_when_workflow_action_io_not_found(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    add_workflow_results_data = {
        "workflow_results_to_add": [
            {
                "key": "foo",
                "workflow_module_io_id": "qux",
            },
            {
                "key": "foo2",
                "workflow_module_io_id": "qux2",
            },
        ]
    }
    workflow_service_mock.overwrite_workflow_results = mock.MagicMock(
        side_effect=WorkflowActionIONotFoundException
    )
    workflow_id = "foo"
    response = await api_client.put(
        f"/v2/workflows/{workflow_id}/results", json=add_workflow_results_data
    )
    assert response.status == 404
    assert await response.json() == {"error": WorkflowActionIONotFoundException.message}
    workflow_service_mock.overwrite_workflow_results.assert_called_once_with(
        workflow_id,
        [
            CreateWorkflowResultData(
                key=d["key"],
                workflow_action_io_id=d["workflow_module_io_id"],
                workflow_id="foo",
            )
            for d in add_workflow_results_data["workflow_results_to_add"]
        ],
    )


async def test_get_workflow_endpoint(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.get_workflow_endpoint = mock.MagicMock(
        return_value="/foo/bar"
    )
    response = await api_client.get(
        "/v2/workflows/workflow_id/endpoint",
    )
    assert response.status == 200
    result = await response.json()
    WorkflowEndpointSchema().load(result, unknown=RAISE, partial=False)
    assert result == {"path": "/foo/bar"}
    workflow_service_mock.get_workflow_endpoint.assert_called_once_with("workflow_id")


async def test_get_workflow_endpoint_when_workflow_has_no_trigger(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.get_workflow_endpoint = mock.MagicMock(return_value=None)
    response = await api_client.get(
        "/v2/workflows/workflow_id/endpoint",
    )
    assert response.status == 200
    workflow_service_mock.get_workflow_endpoint.assert_called_once_with("workflow_id")


async def test_get_workflow_endpoint_when_workflow_not_found(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    workflow_service_mock.get_workflow_endpoint = mock.MagicMock(
        side_effect=WorkflowNotFoundException
    )
    response = await api_client.get(
        "/v2/workflows/workflow_id/endpoint",
    )
    assert response.status == 404
    workflow_service_mock.get_workflow_endpoint.assert_called_once_with("workflow_id")


async def test_overwrite_workflow_endpoint(
    workflow_service_mock: mock.MagicMock, api_client: TestClient
):
    new_trigger_path = "/my/test/path"
    workflow_service_mock.overwrite_workflow_endpoint = mock.MagicMock(
        return_value=new_trigger_path
    )
    response = await api_client.put(
        "/v2/workflows/workflow_id/endpoint", data={"path": new_trigger_path}
    )
    assert response.status == 200
    assert await response.json() == {"path": "/my/test/path"}
    workflow_service_mock.overwrite_workflow_endpoint.assert_called_once_with(
        "workflow_id", new_trigger_path
    )


@pytest.mark.parametrize(
    "error",
    [
        (WorkflowNotFoundException),
        (WorkflowHasNoTriggerException),
        (InvalidUserDefinedEndpointException),
        (EndpointAlreadyExistsException),
    ],
)
async def test_overwrite_workflow_endpoint_when_errors(
    workflow_service_mock: mock.MagicMock, api_client: TestClient, error
):
    new_trigger_path = "/my/test/path"
    workflow_service_mock.overwrite_workflow_endpoint = mock.MagicMock(
        side_effect=error
    )
    response = await api_client.put(
        "/v2/workflows/workflow_id/endpoint", data={"path": new_trigger_path}
    )
    assert response.status == 400
    workflow_service_mock.overwrite_workflow_endpoint.assert_called_once_with(
        "workflow_id", new_trigger_path
    )
