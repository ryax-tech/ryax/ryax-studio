from unittest import mock

import pytest
from aiohttp import web

from ryax.studio.application.action_service import ActionService
from ryax.studio.application.authentication_service import AuthenticationService
from ryax.studio.application.project_authorization_service import (
    ProjectAuthorizationService,
)
from ryax.studio.application.project_variable_service import ProjectVariableService
from ryax.studio.application.util_service import UtilService
from ryax.studio.application.workflow_action_service import WorkflowActionService
from ryax.studio.application.workflow_service import WorkflowService
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.auth.auth_token import AuthToken
from ryax.studio.infrastructure.api.setup import setup as setup_api


@pytest.fixture()
def app_container():
    app_container = ApplicationContainer()
    yield app_container
    app_container.unwire()


@pytest.fixture()
async def api_client(app_container: ApplicationContainer, aiohttp_client):
    """Instanciate client to test api part"""
    app = web.Application()
    setup_api(app, app_container)
    return await aiohttp_client(app)


@pytest.fixture()
def authentication_service_mock(app_container: ApplicationContainer):
    authentication_service_mock = mock.MagicMock(AuthenticationService)
    authentication_service_mock.check_access = mock.MagicMock(
        return_value=AuthToken(user_id="user")
    )
    app_container.authentication_service.override(authentication_service_mock)
    return authentication_service_mock


@pytest.fixture()
def project_authorization_service_mock(app_container: ApplicationContainer):
    project_authorization_service_mock = mock.MagicMock(ProjectAuthorizationService)
    project_authorization_service_mock.get_current_project = mock.AsyncMock(
        return_value="project_id"
    )
    app_container.project_authorization_service.override(
        project_authorization_service_mock
    )
    return project_authorization_service_mock


@pytest.fixture()
def util_service_mock(app_container: ApplicationContainer):
    util_service_mock = mock.MagicMock(UtilService)
    app_container.util_service.override(util_service_mock)

    return util_service_mock


@pytest.fixture()
def action_service_mock(
    app_container: ApplicationContainer,
    authentication_service_mock,
    project_authorization_service_mock,
):
    action_service_mock = mock.MagicMock(ActionService)
    app_container.action_service.override(action_service_mock)
    return action_service_mock


@pytest.fixture(scope="function")
def workflow_action_service_mock(
    app_container: ApplicationContainer,
    authentication_service_mock,
    project_authorization_service_mock,
):
    app_container.configuration.max_upload_size.from_value(1_000_000)
    workflow_action_service_mock = mock.MagicMock(WorkflowActionService)
    app_container.workflow_action_service.override(workflow_action_service_mock)
    auth_service_mock = mock.MagicMock(AuthenticationService)
    auth_service_mock.check_access.return_value = AuthToken(user_id="user")
    app_container.authentication_service.override(auth_service_mock)
    return workflow_action_service_mock


@pytest.fixture()
def workflow_service_mock(
    app_container: ApplicationContainer,
    authentication_service_mock,
    project_authorization_service_mock,
):
    app_container.configuration.max_upload_size.from_value(1_000_000)
    workflow_service_mock = mock.MagicMock(WorkflowService)
    app_container.workflow_service.override(workflow_service_mock)
    return workflow_service_mock


@pytest.fixture()
def project_variable_service_mock(
    app_container: ApplicationContainer,
    authentication_service_mock,
    project_authorization_service_mock,
):
    project_variable_service_mock = mock.MagicMock(ProjectVariableService)
    app_container.project_variable_service.override(project_variable_service_mock)
    return project_variable_service_mock
