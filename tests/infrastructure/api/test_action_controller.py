from datetime import datetime
from unittest import mock

from aiohttp.test_utils import TestClient
from marshmallow import RAISE

from ryax.studio.application.action_service import ActionService
from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_exceptions import (
    ActionLogoNotFoundException,
    ActionNotDeletableException,
    ActionNotFoundException,
)
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.domain.action.views.action import (
    ActionCategoryView,
    ActionExtendedView,
    ActionIOView,
    ActionVersionView,
)
from ryax.studio.domain.logo.logo import Logo
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.infrastructure.api.schemas.action_category_schema import (
    ModuleCategorySchema,
)
from ryax.studio.infrastructure.api.schemas.module_schema import (
    ModuleDetailsSchema,
    ModuleSchema,
)
from ryax.studio.infrastructure.api.schemas.module_versions_schema import (
    ModuleVersionsSchema,
)


async def test_get_logo(action_service_mock: mock.MagicMock, api_client: TestClient):
    """Get logo through api should return data when exists"""
    logo_id = "logo_id"
    action_id = "action_id_for_logo_1"

    logo = Logo(
        id=logo_id,
        name="Logo name",
        action_id=action_id,
        extension="png",
        content=b"abc",
    )

    action_service_mock.get_logo.return_value = logo
    response = await api_client.get(f"/modules/{action_id}/logo")
    assert response.status == 200
    assert await response.content.read() == logo.content
    assert response.content_type == f"image/{logo.extension}"
    action_service_mock.get_logo.assert_called_once_with(action_id)


async def test_get_logo_when_not_exists(
    action_service_mock: mock.MagicMock, api_client: TestClient
):
    """Get logo through api should return not found error when not exists"""
    action_id = "action_1_id"
    action_service_mock.get_logo.side_effect = ActionLogoNotFoundException
    response = await api_client.get(f"modules/{action_id}/logo")
    assert response.status == 404
    assert await response.json() == {"error": "Action logo not found"}
    action_service_mock.get_logo.assert_called_once_with(action_id)


async def test_list_actions(action_service_mock: ActionService, api_client: TestClient):
    """When fetching actions endpoint should return list of actions"""
    action_1 = Action(
        id="action_1_id",
        name="action 1 name",
        description="Mock action 1 description",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        project="b262b130-caf0-43b6-b495-4cc21201321b",
        technical_name="action1name",
        build_date=datetime.fromtimestamp(0),
    )
    action_2 = Action(
        id="action_2_id",
        name="action 2 name",
        description="Mock action 2 description",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.SOURCE,
        project="b262b130-caf0-43b6-b495-4cc21201321b",
        technical_name="action1name",
        build_date=datetime.fromtimestamp(0),
    )
    action_service_mock.get_actions.return_value = [action_1, action_2]
    response = await api_client.get("/modules")
    assert response.status == 200
    response = await response.json()
    ModuleSchema(many=True).load(response, unknown=RAISE, partial=False)
    assert response == [
        {
            "id": action_1.id,
            "name": action_1.name,
            "description": action_1.description,
            "version": action_1.version,
            "kind": action_1.kind.value,
        },
        {
            "id": action_2.id,
            "name": action_2.name,
            "description": action_2.description,
            "version": action_2.version,
            "kind": action_2.kind.value,
        },
    ]


async def test_list_actions_with_search(
    action_service_mock: ActionService, api_client: TestClient
):
    """When fetching actions the endpoint should return a list of searched actions"""
    action_1 = Action(
        id="action_1_id",
        name="action 1 name",
        description="Mock action 1 description",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        project="project_id",
        technical_name="action1name",
        build_date=datetime.fromtimestamp(0),
    )
    action_2 = Action(
        id="action_2_id",
        name="action 2 name",
        description="Mock action 2 description",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.SOURCE,
        project="project_id",
        technical_name="action1name",
        build_date=datetime.fromtimestamp(0),
    )
    action_service_mock.get_actions = mock.MagicMock(return_value=[action_1, action_2])
    search = "test-action-search"
    response = await api_client.get(f"/modules?search={search}")
    assert response.status == 200
    action_service_mock.get_actions.assert_called_with(
        "project_id", search=search, category=None
    )
    result = await response.json()
    ModuleSchema(many=True).load(result, unknown=RAISE, partial=False)
    assert result == [
        {
            "id": action_1.id,
            "name": action_1.name,
            "description": action_1.description,
            "version": action_1.version,
            "kind": action_1.kind.value,
        },
        {
            "id": action_2.id,
            "name": action_2.name,
            "description": action_2.description,
            "version": action_2.version,
            "kind": action_2.kind.value,
        },
    ]


async def test_list_actions_with_category_filter(
    action_service_mock: ActionService, api_client: TestClient
):
    """When fetching actions the endpoint should return a list of actions that filtered by category"""
    action_1 = Action(
        id="action_1_id",
        name="action 1 name",
        description="Mock action 1 description",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        categories=["category_1"],
        project="project_id",
        technical_name="action1name",
        build_date=datetime.fromtimestamp(0),
    )
    action_service_mock.get_actions = mock.MagicMock(return_value=[action_1])
    category = "category_1"
    response = await api_client.get(f"/modules?category={category}")
    assert response.status == 200
    action_service_mock.get_actions.assert_called_once_with(
        "project_id", search="", category=category
    )
    result = await response.json()
    ModuleSchema(many=True).load(result, unknown=RAISE, partial=False)
    assert result == [
        {
            "id": action_1.id,
            "name": action_1.name,
            "description": action_1.description,
            "version": action_1.version,
            "kind": action_1.kind.value,
        },
    ]


async def test_list_actions_versions(
    action_service_mock: mock.MagicMock, api_client: TestClient
):
    """List actions versions sould return at least 1 (id, version) tuple when exists"""
    action_1_id = "action_1_id"
    action_1 = Action(
        id=action_1_id,
        name=action_1_id,
        technical_name="same_technical_name",
        description="Mock action 1 description",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        project="b262b130-caf0-43b6-b495-4cc21201321b",
        build_date=datetime.fromtimestamp(0),
    )
    action_2 = Action(
        id="action_2_id",
        name="action_2_id",
        technical_name="same_technical_name",
        description="Mock action 2 description",
        lockfile=b"",
        version="2.0",
        kind=ActionKind.PROCESSOR,
        project="b262b130-caf0-43b6-b495-4cc21201321b",
        build_date=datetime.fromtimestamp(0),
    )
    action_service_mock.list_action_versions.return_value = [action_1, action_2]
    response = await api_client.get(f"/modules/{action_1_id}/list_versions")
    assert response.status == 200
    action_service_mock.list_action_versions.assert_called_once_with(action_1_id)
    result = await response.json()
    ModuleVersionsSchema(many=True).load(result, unknown=RAISE, partial=False)
    assert result == [
        {"id": action_1.id, "version": action_1.version},
        {"id": action_2.id, "version": action_2.version},
    ]


async def test_list_actions_versions_when_action_not_exists(
    action_service_mock: mock.MagicMock, api_client: TestClient
):
    action_id = "action_id"
    action_service_mock.list_action_versions.side_effect = ActionNotFoundException()
    response = await api_client.get(f"/modules/{action_id}/list_versions")
    assert response.status == 404
    assert await response.json() == {"error": "Action not found"}
    action_service_mock.list_action_versions.assert_called_once_with(action_id)


async def test_get_action_extended_view(
    action_service_mock: mock.MagicMock, api_client: TestClient
):
    """Get action through api should return data when exists"""
    action_id = "action_id"
    action = ActionExtendedView(
        id="action_1_id",
        name="action name",
        technical_name="action technical name",
        description="action description",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PUBLISHER,
        inputs=[
            ActionIOView(
                id="action_input_id",
                technical_name="action_input_technical_name",
                display_name="action_input_display_name",
                help="action_input_help",
                type=ActionIOType.STRING,
                enum_values=["option1", "option2"],
                default_value="option1",
                optional=False,
            )
        ],
        outputs=[
            ActionIOView(
                id="action_output_id",
                technical_name="action_output_technical_name",
                display_name="action_output_display_name",
                help="action_output_help",
                type=ActionIOType.LONGSTRING,
                enum_values=["option1", "option2"],
                optional=False,
            )
        ],
        categories=[
            ActionCategoryView(
                id="08a2b1e7-3b27-49bc-9968-8a61a6abe660", name="category1"
            ),
            ActionCategoryView(
                id="410611c1-f7d9-4b85-a680-b19cef044875", name="category2"
            ),
        ],
        addons={"toto": {"addon1": "tata"}},
        versions=[ActionVersionView(id="action_1_id", version="1.0")],
    )
    action_service_mock.get_action_extended_view.return_value = action
    response = await api_client.get(f"/modules/{action_id}")
    assert response.status == 200
    result = await response.json()
    ModuleDetailsSchema().load(result, unknown=RAISE, partial=False)
    assert result == {
        "id": action.id,
        "name": action.name,
        "technical_name": action.technical_name,
        "description": action.description,
        "version": action.version,
        "kind": action.kind.value,
        "lockfile": action.lockfile.decode("utf-8"),
        "inputs": [
            {
                "id": item.id,
                "technical_name": item.technical_name,
                "display_name": item.display_name,
                "help": item.help,
                "type": item.type.value,
                "enum_values": item.enum_values,
                "default_value": item.default_value,
                "optional": item.optional,
            }
            for item in action.inputs
        ],
        "outputs": [
            {
                "id": item.id,
                "technical_name": item.technical_name,
                "display_name": item.display_name,
                "help": item.help,
                "type": item.type.value,
                "enum_values": item.enum_values,
                "default_value": item.default_value,
                "optional": item.optional,
            }
            for item in action.outputs
        ],
        "categories": [
            {"id": item.id, "name": item.name} for item in action.categories
        ],
        "versions": [{"id": action.id, "version": action.version}],
        "has_dynamic_outputs": False,
        "addons": action.addons,
        "resources": None,
    }
    action_service_mock.get_action_extended_view.assert_called_once_with(action_id)


async def test_get_action_extended_view_when_not_exists(
    action_service_mock: mock.MagicMock, api_client: TestClient
):
    """Get action through api should return not found error when not exists"""
    action_id = "action_id"
    action_service_mock.get_action_extended_view.side_effect = ActionNotFoundException()
    response = await api_client.get(f"/modules/{action_id}")
    assert response.status == 404
    assert await response.json() == {"error": "Action not found"}
    action_service_mock.get_action_extended_view.assert_called_once_with(action_id)


async def test_delete_action(
    action_service_mock: mock.MagicMock, api_client: TestClient
):
    """Delete action through api should work"""
    action_id = "action_id"
    action_service_mock.delete_action.return_value = None
    response = await api_client.delete(f"/modules/{action_id}")
    assert response.status == 200
    assert await response.json() is None
    action_service_mock.delete_action.assert_called_once_with(action_id)


async def test_delete_action_when_action_not_found(
    action_service_mock: mock.MagicMock, api_client: TestClient
):
    """Delete action through api should an error if action doesn't exist"""
    action_id = "action_id"
    action_service_mock.delete_action.side_effect = ActionNotFoundException
    response = await api_client.delete(f"/modules/{action_id}")
    assert response.status == 404
    assert await response.json() == {"error": "Action not found"}
    action_service_mock.delete_action.assert_called_once_with(action_id)


async def test_delete_action_when_action_is_not_deletable(
    action_service_mock: mock.MagicMock, api_client: TestClient
):
    """Delete action through api should an error if action doesn't exist"""
    action_id = "action_id"
    action_service_mock.delete_action.side_effect = ActionNotDeletableException(
        workflows=[
            Workflow(
                id="123",
                name="workflow",
                description="wfdescr",
                owner="wfowner",
                project="wfproject",
            )
        ]
    )
    response = await api_client.delete(f"/modules/{action_id}")
    assert response.status == 400
    assert await response.json() == {
        "error": "Action not deletable",
        "workflows": [{"id": "123", "name": "workflow"}],
        "code": 1,
    }
    action_service_mock.delete_action.assert_called_once_with(action_id)


async def test_list_categories(
    action_service_mock: ActionService, api_client: TestClient
):
    category_1 = Action(
        id="cat_1_id",
        name="category 1",
        project="project_id",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.fromtimestamp(0),
    )
    category_2 = Action(
        id="cat_2_id",
        name="category 2",
        project="project_id",
        technical_name="action1name",
        version="action1name",
        kind=ActionKind.PROCESSOR,
        description="action1name",
        lockfile=b"",
        build_date=datetime.fromtimestamp(0),
    )
    action_service_mock.list_categories.return_value = [category_1, category_2]
    response = await api_client.get("/modules/list_categories")
    assert response.status == 200
    result = await response.json()
    ModuleCategorySchema(many=True).load(result, unknown=RAISE, partial=False)
    assert result == [
        {
            "id": category_1.id,
            "name": category_1.name,
        },
        {
            "id": category_2.id,
            "name": category_2.name,
        },
    ]
