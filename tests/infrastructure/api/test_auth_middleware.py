from unittest import mock

import pytest

from ryax.studio.application.authentication_service import AuthenticationService
from ryax.studio.container import ApplicationContainer


@pytest.fixture()
def auth_service_mock(app_container: ApplicationContainer):
    auth_service_mock = mock.MagicMock(AuthenticationService)
    app_container.authentication_service.override(auth_service_mock)
    return auth_service_mock


# async def test_check_token(auth_service_mock: AuthService, api_client: TestClient):
#     response = await api_client.get("/modules")
#     assert response.status == 200
