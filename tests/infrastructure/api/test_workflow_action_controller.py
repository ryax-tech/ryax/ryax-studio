import os
import tempfile
from typing import Optional
from unittest import mock

import pytest
from aiohttp import MultipartWriter
from aiohttp.test_utils import TestClient
from marshmallow import RAISE

from ryax.studio.application.workflow_action_service import WorkflowActionService
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action_exceptions import ActionNotFoundException
from ryax.studio.domain.action.action_values import ActionIOType, DynamicOutputOrigin
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.views.workflow_action_input_view import (
    WorkflowActionInputView,
)
from ryax.studio.domain.workflow.views.workflow_action_output_view import (
    WorkflowActionOutputView,
)
from ryax.studio.domain.workflow.workflow_exceptions import (
    InvalidInputValueException,
    NotImplementedException,
    WorkflowActionDynamicOutputNotFoundException,
    WorkflowActionHasNoDynamicOutputsException,
    WorkflowActionInputNotFoundException,
    WorkflowActionLinkNotFoundException,
    WorkflowActionNotFoundException,
    WorkflowActionOutputNotFoundException,
    WorkflowInputHasNoStaticFileException,
    WorkflowNotFoundException,
    WorkflowNotUpdatableException,
)
from ryax.studio.domain.workflow.workflow_values import (
    AddWorkflowAction,
    AddWorkflowActionDynamicOutput,
    AddWorkflowActionLink,
    AddWorkflowActionWithLink,
    UpdateAllWorkflowActionIOData,
    UpdateWorkflowAction,
    UpdateWorkflowActionDynamicOutput,
    UpdateWorkflowActionInputValue,
)
from ryax.studio.infrastructure.api.controllers import workflow_action_controller
from ryax.studio.infrastructure.api.schemas.workflow_action_input_schema import (
    WorkflowModuleInputSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_action_output_schema import (
    WorkflowModuleOutputSchema,
)


@pytest.mark.parametrize(
    "request_data",
    [
        (
            {
                "module_id": "module_id",
                "custom_name": "new_name",
                "position_x": 0,
                "position_y": 1,
            }
        ),
        ({"module_id": "module_id", "position_x": 0, "position_y": 1}),
    ],
    ids=["Add with all data", "Add without custom_name"],
)
async def test_add_workflow_action(
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
    request_data: dict,
) -> None:
    """Add action to workflow through api should work"""
    workflow_id = "workflow_id"
    workflow_action_service_mock.add_workflow_action.return_value = None
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules", data=request_data
    )
    assert response.status == 201
    assert await response.json() is not None
    workflow_action_service_mock.add_workflow_action.assert_called_once_with(
        workflow_id=workflow_id,
        data=AddWorkflowAction(
            action_id=request_data["module_id"],
            custom_name=request_data.get("custom_name"),
            position_x=request_data.get("position_x"),
            position_y=request_data.get("position_y"),
        ),
    )


@pytest.mark.parametrize(
    "request_data",
    [
        (
            {
                "module_definition_id": "module_id",
                "parent_workflow_action_id": "new_name",
            }
        ),
        (
            {
                "module_definition_id": "module_id",
                "replace_workflow_action_id": "replace_id",
            }
        ),
        (
            {
                "module_definition_id": "module_id",
            }
        ),
    ],
    ids=["Add without replace", "Add and replace", "Add as first action"],
)
async def test_add_workflow_action_with_link(
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
    request_data: dict,
):
    """Add action to workflow through api should work"""
    workflow_id = "workflowid"
    workflow_action_service_mock.add_workflow_action_with_link.return_value = None
    response = await api_client.post(
        f"/v2/workflows/{workflow_id}/modules", data=request_data
    )
    assert response.status == 201
    assert await response.json() == {}
    workflow_action_service_mock.add_workflow_action_with_link.assert_called_once_with(
        workflow_id=workflow_id,
        data=AddWorkflowActionWithLink(
            request_data["module_definition_id"],
            request_data.get("parent_workflow_module_id"),
            request_data.get("replace_workflow_module_id"),
        ),
    )


@pytest.mark.parametrize(
    "request_data",
    [
        ({"custom_name": "new_name", "position_x": 0, "position_y": 1}),
    ],
    ids=["Add without action_id"],
)
async def test_add_workflow_action_with_invalid_data(
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
    request_data: dict,
) -> None:
    """Add action to workflow through api should work"""
    workflow_id = "workflow_id"
    workflow_action_service_mock.add_workflow_action.return_value = None
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules", data=request_data
    )
    assert response.status == 422
    workflow_action_service_mock.add_workflow_action.assert_not_called()


@pytest.mark.parametrize(
    "request_data",
    [
        ({}),
        ({"parent_workflow_action_id": "parent_id"}),
        ({"replace_workflow_action_id": "module_id"}),
        (
            {
                "replace_workflow_action_id": "module_id",
                "parent_workflow_action_id": "parent_id",
            }
        ),
        ({"something": "irrelevant"}),
    ],
    ids=[
        "empty request",
        "only parent",
        "only replace",
        "both replace and parent",
        "wrong attribute",
    ],
)
async def test_add_workflow_action_with_link_with_invalid_data(
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
    request_data: dict,
) -> None:
    """Add action to workflow through api should work"""
    workflow_id = "workflow_id"
    workflow_action_service_mock.add_workflow_action_with_link.return_value = None
    response = await api_client.post(
        f"/v2/workflows/{workflow_id}/modules", data=request_data
    )
    assert response.status == 422
    workflow_action_service_mock.add_workflow_action_with_link.assert_not_called()


@pytest.mark.parametrize(
    "exception, response_status, response_message",
    [
        (
            WorkflowNotFoundException(),
            404,
            "Workflow not found",
        ),
        (
            ActionNotFoundException(),
            404,
            "Action not found",
        ),
        (
            WorkflowNotUpdatableException(),
            400,
            "Workflow can not be modified at this status",
        ),
    ],
    ids=["workflow not found", "action not found", "workflow not updatable"],
)
async def test_add_workflow_action_when_exception_raised(
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
    exception,
    response_status,
    response_message,
) -> None:
    """Add action to workflow through api should return error when workflow doesn't exist"""
    workflow_id = "workflow_id"
    data = {
        "module_id": "action_id",
        "custom_name": "Action custom name",
        "position_x": 1,
        "position_y": 1,
    }
    workflow_action_service_mock.add_workflow_action.side_effect = exception
    response = await api_client.post(f"/workflows/{workflow_id}/modules", data=data)
    assert response.status == response_status
    assert await response.json() == {"error": response_message}
    workflow_action_service_mock.add_workflow_action.assert_called_once_with(
        workflow_id=workflow_id,
        data=AddWorkflowAction(
            data["module_id"],
            data["custom_name"],
            data["position_x"],
            data["position_y"],
        ),
    )


@pytest.mark.parametrize(
    "exception, response_status, response_message",
    [
        (
            WorkflowNotFoundException(),
            404,
            "Workflow not found",
        ),
        (
            ActionNotFoundException(),
            404,
            "Action not found",
        ),
        (
            WorkflowActionNotFoundException(),
            404,
            "Action not found in the workflow",
        ),
    ],
    ids=["workflow not found", "action not found", "workflow action not found"],
)
async def test_add_workflow_action_with_link_when_exeption_raised(
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
    exception,
    response_status,
    response_message,
):
    """Add action to workflow through api should return error when workflow doesn't exist"""
    workflow_id = "workflow_id"
    data = {
        "module_definition_id": "action_id",
        "parent_workflow_module_id": "parent_id",
    }
    workflow_action_service_mock.add_workflow_action_with_link.side_effect = exception
    response = await api_client.post(f"/v2/workflows/{workflow_id}/modules", data=data)
    assert response.status == response_status
    assert await response.json() == {"error": response_message}
    workflow_action_service_mock.add_workflow_action_with_link.assert_called_once_with(
        workflow_id=workflow_id,
        data=AddWorkflowActionWithLink(
            action_definition_id=data["module_definition_id"],
            parent_workflow_action_id=data["parent_workflow_module_id"],
        ),
    )


async def test_delete_workflow_action_from_workflow(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Delete action from workflow through api should return the workflow action"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}"
    )
    assert response.status == 200
    assert await response.json() is None
    workflow_action_service_mock.delete_workflow_action.assert_called_once_with(
        workflow_id=workflow_id, workflow_action_id=workflow_action_id
    )


async def test_delete_workflow_action_when_workflow_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Delete action from workflow through api should return error when workflow doesn't exist"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_service_mock.delete_workflow_action.side_effect = (
        WorkflowNotFoundException()
    )
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}"
    )
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_action_service_mock.delete_workflow_action.assert_called_once_with(
        workflow_id=workflow_id, workflow_action_id=workflow_action_id
    )


async def test_delete_workflow_action_when_workflow_not_updatable(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Delete action from workflow through api should return error when workflow not updatable"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_service_mock.delete_workflow_action.side_effect = (
        WorkflowNotUpdatableException()
    )
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}"
    )
    assert response.status == 400
    assert await response.json() == {
        "error": "Workflow can not be modified at this status"
    }
    workflow_action_service_mock.delete_workflow_action.assert_called_once_with(
        workflow_id=workflow_id, workflow_action_id=workflow_action_id
    )


async def test_delete_workflow_action_when_workflow_action_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Delete action from workflow through api should return error when action doesn't exist"""
    workflow_id = "workflow_id"
    workflow_action_id = "action_id"
    workflow_action_service_mock.delete_workflow_action.side_effect = (
        WorkflowActionNotFoundException()
    )
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}"
    )
    assert response.status == 404
    assert await response.json() == {"error": "Action not found in the workflow"}
    workflow_action_service_mock.delete_workflow_action.assert_called_once_with(
        workflow_id=workflow_id, workflow_action_id=workflow_action_id
    )


async def test_add_workflow_action_link(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Add workflow action link through api should return the workflow action"""
    workflow_id = "workflow_id"
    data = {
        "input_module_id": "d5ca5593-6ba8-4c2f-bf7b-a5cf40baac22",
        "output_module_id": "4bb03cc3-4bd7-4e6c-8615-db7eaa079ce2",
    }
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules-links", data=data
    )
    assert response.status == 200
    assert await response.json() is None
    workflow_action_service_mock.add_workflow_action_link.assert_called_once_with(
        workflow_id=workflow_id,
        data=AddWorkflowActionLink(
            upstream_action_id=data["input_module_id"],
            downstream_action_id=data["output_module_id"],
        ),
    )


async def test_add_workflow_action_link_when_workflow_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Add workflow action link through api should return error when workflow not exists"""
    workflow_id = "workflow_id"
    data = {
        "input_module_id": "d5ca5593-6ba8-4c2f-bf7b-a5cf40baac22",
        "output_module_id": "4bb03cc3-4bd7-4e6c-8615-db7eaa079ce2",
    }
    workflow_action_service_mock.add_workflow_action_link.side_effect = (
        WorkflowNotFoundException()
    )
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules-links", data=data
    )
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_action_service_mock.add_workflow_action_link.assert_called_once_with(
        workflow_id=workflow_id,
        data=AddWorkflowActionLink(
            upstream_action_id=data["input_module_id"],
            downstream_action_id=data["output_module_id"],
        ),
    )


async def test_add_workflow_action_link_when_workflow_action_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Delete action from workflow through api should return error when action doesn't exist"""
    workflow_id = "workflow_id"
    data = {
        "input_module_id": "d5ca5593-6ba8-4c2f-bf7b-a5cf40baac22",
        "output_module_id": "4bb03cc3-4bd7-4e6c-8615-db7eaa079ce2",
    }
    workflow_action_service_mock.add_workflow_action_link.side_effect = (
        WorkflowActionNotFoundException()
    )
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules-links", data=data
    )
    assert response.status == 404
    assert await response.json() == {"error": "Action not found in the workflow"}
    workflow_action_service_mock.add_workflow_action_link.assert_called_once_with(
        workflow_id=workflow_id,
        data=AddWorkflowActionLink(
            upstream_action_id=data["input_module_id"],
            downstream_action_id=data["output_module_id"],
        ),
    )


async def test_add_workflow_action_link_when_workflow_not_updatable(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Add workflow action link through api should return error when workflow not updatable"""
    workflow_id = "workflow_id"
    data = {
        "input_module_id": "d5ca5593-6ba8-4c2f-bf7b-a5cf40baac22",
        "output_module_id": "4bb03cc3-4bd7-4e6c-8615-db7eaa079ce2",
    }
    workflow_action_service_mock.add_workflow_action_link.side_effect = (
        WorkflowNotUpdatableException()
    )
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules-links", data=data
    )
    assert response.status == 400
    assert await response.json() == {
        "error": "Workflow can not be modified at this status"
    }
    workflow_action_service_mock.add_workflow_action_link.assert_called_once_with(
        workflow_id=workflow_id,
        data=AddWorkflowActionLink(
            upstream_action_id=data["input_module_id"],
            downstream_action_id=data["output_module_id"],
        ),
    )


async def test_update_action_in_workflow(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Update action from workflow through api should return the workflow action"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_data = {
        "custom_name": "Custom name for the workflow action",
        "position_x": 0,
        "position_y": 1,
    }
    workflow_action_service_mock.update_workflow_action.return_value = None
    response = await api_client.put(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}",
        data=workflow_action_data,
    )
    assert response.status == 200
    assert await response.json() is None
    workflow_action_service_mock.update_workflow_action.assert_called_once_with(
        workflow_id=workflow_id,
        workflow_action_id=workflow_action_id,
        workflow_action_data=UpdateWorkflowAction(**workflow_action_data),
    )


async def test_update_action_in_workflow_when_workflow_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Update action in workflow through api should return error when workflow doesn't exist"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_data = {
        "custom_name": "Custom name for the workflow action",
        "position_x": 0,
        "position_y": 1,
    }
    workflow_action_service_mock.update_workflow_action.side_effect = (
        WorkflowNotFoundException()
    )
    response = await api_client.put(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}",
        data=workflow_action_data,
    )
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_action_service_mock.update_workflow_action.assert_called_once_with(
        workflow_id=workflow_id,
        workflow_action_id=workflow_action_id,
        workflow_action_data=UpdateWorkflowAction(**workflow_action_data),
    )


async def test_update_action_in_workflow_when_workflow_action_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Delete action in workflow through api should return error when action doesn't exist"""
    workflow_id = "workflow_id"
    workflow_action_id = "action_id"
    workflow_action_data = {
        "custom_name": "Custom name for the workflow action",
        "position_x": 0,
        "position_y": 1,
    }
    workflow_action_service_mock.update_workflow_action.side_effect = (
        WorkflowActionNotFoundException()
    )
    response = await api_client.put(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}",
        data=workflow_action_data,
    )
    assert response.status == 404
    assert await response.json() == {"error": "Action not found in the workflow"}
    workflow_action_service_mock.update_workflow_action.assert_called_once_with(
        workflow_id=workflow_id,
        workflow_action_id=workflow_action_id,
        workflow_action_data=UpdateWorkflowAction(**workflow_action_data),
    )


async def test_update_action_in_workflow_when_workflow_not_updatable(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Update action in workflow through api should return error when workflow doesn't exist"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_data = {
        "custom_name": "Custom name for the workflow action",
        "position_x": 0,
        "position_y": 1,
    }
    workflow_action_service_mock.update_workflow_action.side_effect = (
        WorkflowNotUpdatableException()
    )
    response = await api_client.put(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}",
        data=workflow_action_data,
    )
    assert response.status == 400
    assert await response.json() == {
        "error": "Workflow can not be modified at this status"
    }
    workflow_action_service_mock.update_workflow_action.assert_called_once_with(
        workflow_id=workflow_id,
        workflow_action_id=workflow_action_id,
        workflow_action_data=UpdateWorkflowAction(**workflow_action_data),
    )


async def test_delete_workflow_action_link_from_workflow(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Delete action from workflow through api should return the workflow action"""
    workflow_id = "workflow_id"
    workflow_action_link_id = "workflow_action_link_id"
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules-links/{workflow_action_link_id}"
    )
    assert response.status == 200
    assert await response.json() is None
    workflow_action_service_mock.delete_workflow_action_link.assert_called_once_with(
        workflow_id=workflow_id, workflow_action_link_id=workflow_action_link_id
    )


async def test_delete_workflow_action_link_when_workflow_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Delete action link from workflow through api should return error when workflow doesn't exist"""
    workflow_id = "workflow_id"
    workflow_action_link_id = "workflow_action_link_id"
    workflow_action_service_mock.delete_workflow_action_link.side_effect = (
        WorkflowNotFoundException()
    )
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules-links/{workflow_action_link_id}"
    )
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_action_service_mock.delete_workflow_action_link.assert_called_once_with(
        workflow_id=workflow_id, workflow_action_link_id=workflow_action_link_id
    )


async def test_delete_workflow_action_link_when_workflow_not_updatable(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Delete action link from workflow through api should return error when workflow is not updatable"""
    workflow_id = "workflow_id"
    workflow_action_link_id = "workflow_action_link_id"
    workflow_action_service_mock.delete_workflow_action_link.side_effect = (
        WorkflowNotUpdatableException()
    )
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules-links/{workflow_action_link_id}"
    )
    assert response.status == 400
    assert await response.json() == {
        "error": "Workflow can not be modified at this status"
    }
    workflow_action_service_mock.delete_workflow_action_link.assert_called_once_with(
        workflow_id=workflow_id, workflow_action_link_id=workflow_action_link_id
    )


async def test_delete_workflow_action_when_link_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Delete action link from workflow through api should return error when the link doesn't exist"""
    workflow_id = "workflow_id"
    workflow_action_link_id = "workflow_action_link_id"
    workflow_action_service_mock.delete_workflow_action_link.side_effect = (
        WorkflowActionLinkNotFoundException()
    )
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules-links/{workflow_action_link_id}"
    )
    assert response.status == 404
    assert await response.json() == {"error": "Action link not found in the workflow"}
    workflow_action_service_mock.delete_workflow_action_link.assert_called_once_with(
        workflow_id=workflow_id, workflow_action_link_id=workflow_action_link_id
    )


@pytest.mark.parametrize(
    "request_data,response_status,update_workflow_action_input_value",
    [
        (
            {"static_value": "static_value"},
            200,
            UpdateWorkflowActionInputValue(static_value="static_value"),
        ),
        (
            {"reference_value": "reference_value"},
            200,
            UpdateWorkflowActionInputValue(reference_value="reference_value"),
        ),
    ],
    ids=["Update with static value", "Update with reference value"],
)
async def test_update_workflow_action_input(
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
    request_data: dict,
    response_status: int,
    update_workflow_action_input_value: Optional[UpdateWorkflowActionInputValue],
) -> None:
    """Update input value through api"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_input_id"
    workflow_action_service_mock.update_workflow_action_input.return_value = None
    response = await api_client.put(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs/{workflow_action_input_id}/value",
        data=request_data,
    )
    assert response.status == response_status
    assert await response.json() is None
    if update_workflow_action_input_value:
        workflow_action_service_mock.update_workflow_action_input.assert_called_once_with(
            workflow_id,
            workflow_action_id,
            workflow_action_input_id,
            update_workflow_action_input_value,
        )
    else:
        workflow_action_service_mock.update_workflow_action_input.assert_not_called()


async def test_delete_workflow_input_static_file(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_input_id = "workflow_input_id"
    workflow_action_service_mock.delete_workflow_action_input_file.return_value = None
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs/{workflow_input_id}/file",
    )
    assert await response.json() is None
    workflow_action_service_mock.delete_workflow_action_input_file.assert_called_once_with(
        workflow_id, workflow_action_id, workflow_input_id
    )


async def test_delete_workflow_input_static_file_when_action_not_found(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_input_id = "workflow_input_id"
    workflow_action_service_mock.delete_workflow_action_input_file.side_effect = (
        WorkflowActionNotFoundException()
    )
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs/{workflow_input_id}/file",
    )
    assert response.status == 404
    assert await response.json() == {"error": "Action not found in the workflow"}
    workflow_action_service_mock.delete_workflow_action_input_file.assert_called_once_with(
        workflow_id, workflow_action_id, workflow_input_id
    )


async def test_delete_workflow_input_static_file_when_workflow_not_found(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_input_id = "workflow_input_id"
    workflow_action_service_mock.delete_workflow_action_input_file.side_effect = (
        WorkflowNotFoundException()
    )
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs/{workflow_input_id}/file",
    )
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_action_service_mock.delete_workflow_action_input_file.assert_called_once_with(
        workflow_id, workflow_action_id, workflow_input_id
    )


async def test_delete_workflow_input_static_file_when_input_not_found(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_input_id = "workflow_input_id"
    workflow_action_service_mock.delete_workflow_action_input_file.side_effect = (
        WorkflowActionInputNotFoundException()
    )
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs/{workflow_input_id}/file",
    )
    assert response.status == 404
    assert await response.json() == {"error": "Input not found in the workflow"}
    workflow_action_service_mock.delete_workflow_action_input_file.assert_called_once_with(
        workflow_id, workflow_action_id, workflow_input_id
    )


async def test_delete_workflow_input_static_file_when_input_file_not_found(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_input_id = "workflow_input_id"
    workflow_action_service_mock.delete_workflow_action_input_file.side_effect = (
        WorkflowInputHasNoStaticFileException()
    )
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs/{workflow_input_id}/file",
    )
    assert response.status == 404
    assert await response.json() == {"error": "Input has no static file"}
    workflow_action_service_mock.delete_workflow_action_input_file.assert_called_once_with(
        workflow_id, workflow_action_id, workflow_input_id
    )


async def test_delete_workflow_input_static_file_when_workflow_not_updatable(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_input_id = "workflow_input_id"
    workflow_action_service_mock.delete_workflow_action_input_file.side_effect = (
        WorkflowNotUpdatableException()
    )
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs/{workflow_input_id}/file",
    )
    assert response.status == 400
    assert await response.json() == {
        "error": "Workflow can not be modified at this status"
    }
    workflow_action_service_mock.delete_workflow_action_input_file.assert_called_once_with(
        workflow_id, workflow_action_id, workflow_input_id
    )


@pytest.mark.parametrize(
    "error,response_status,response_error",
    [
        (WorkflowNotFoundException(), 404, "Workflow not found"),
        (WorkflowActionNotFoundException(), 404, "Action not found in the workflow"),
        (
            WorkflowActionInputNotFoundException(),
            404,
            "Input not found in the workflow",
        ),
        (InvalidInputValueException(), 400, "Input value is invalid"),
        (NotImplementedException(), 400, "Feature not implemented"),
    ],
    ids=[
        "Workflow not found",
        "Workflow action not found",
        "Workflow action input not found",
        "Workflow action input value invalid",
        "Workflow action input not handled",
    ],
)
async def test_update_workflow_action_input_value_with_domain_error(
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
    error: Exception,
    response_status: int,
    response_error: str,
) -> None:
    """Update input value through api should return error when workflow doesn't exist"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_input_id"
    workflow_input_data = {
        "static_value": "value",
    }
    workflow_action_service_mock.update_workflow_action_input.side_effect = error
    response = await api_client.put(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs/{workflow_action_input_id}/value",
        data=workflow_input_data,
    )
    assert response.status == response_status
    assert await response.json() == {"error": response_error}
    workflow_action_service_mock.update_workflow_action_input.assert_called_once_with(
        workflow_id,
        workflow_action_id,
        workflow_action_input_id,
        UpdateWorkflowActionInputValue(**workflow_input_data),
    )


async def test_add_dynamic_output_to_workflow_action(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Add a dynamic output to workflow action should work"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_data = {
        "technical_name": "technical name",
        "display_name": "display name",
        "help": "help message",
        "type": "string",
        "enum_values": [],
    }
    workflow_action_service_mock.add_workflow_action_dynamic_output.return_value = None
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs",
        data=workflow_dynamic_output_data,
    )
    assert response.status == 201
    assert await response.json() is None
    workflow_action_service_mock.add_workflow_action_dynamic_output.assert_called_once_with(
        workflow_id,
        workflow_action_id,
        AddWorkflowActionDynamicOutput(**workflow_dynamic_output_data),
    )


async def test_add_dynamic_output_to_workflow_module_when_workflow_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Add a dynamic output to workflow action should return error when workflow doesn't exists"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_data = {
        "technical_name": "technical_name",
        "display_name": "display_name",
        "help": "help",
        "type": ActionIOType.STRING.value,
    }
    workflow_action_service_mock.add_workflow_action_dynamic_output.side_effect = (
        WorkflowNotFoundException
    )
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs",
        data=workflow_dynamic_output_data,
    )
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_action_service_mock.add_workflow_action_dynamic_output.assert_called_once_with(
        workflow_id,
        workflow_action_id,
        AddWorkflowActionDynamicOutput(**workflow_dynamic_output_data),
    )


async def test_add_dynamic_output_to_workflow_module_when_workflow_module_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Add a dynamic output to workflow action should return error when workflow action doesn't exists"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_data = {
        "technical_name": "technical_name",
        "display_name": "display_name",
        "help": "help",
        "type": ActionIOType.STRING.value,
    }
    workflow_action_service_mock.add_workflow_action_dynamic_output.side_effect = (
        WorkflowActionNotFoundException
    )
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs",
        data=workflow_dynamic_output_data,
    )
    assert response.status == 404
    assert await response.json() == {"error": "Action not found in the workflow"}
    workflow_action_service_mock.add_workflow_action_dynamic_output.assert_called_once_with(
        workflow_id,
        workflow_action_id,
        AddWorkflowActionDynamicOutput(**workflow_dynamic_output_data),
    )


async def test_add_dynamic_output_to_workflow_module_when_workflow_module_take_no_dynamic_output(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Add dynamic output to workflow action should return error when workflow action doesn't take dynamic outputs"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_data = {
        "technical_name": "technical_name",
        "display_name": "display_name",
        "help": "help",
        "type": ActionIOType.STRING.value,
    }
    workflow_action_service_mock.add_workflow_action_dynamic_output.side_effect = (
        WorkflowActionHasNoDynamicOutputsException
    )
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs",
        data=workflow_dynamic_output_data,
    )
    assert response.status == 404
    assert await response.json() == {
        "error": "The selected action can't take dynamic outputs"
    }
    workflow_action_service_mock.add_workflow_action_dynamic_output.assert_called_once_with(
        workflow_id,
        workflow_action_id,
        AddWorkflowActionDynamicOutput(**workflow_dynamic_output_data),
    )


async def test_update_workflow_action_dynamic_output(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Update a dynamic output to workflow action should work"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_id = "workflow_dynamic_output_id"
    workflow_dynamic_output_data = {
        "technical_name": "technical_name",
    }
    workflow_action_service_mock.update_workflow_action_dynamic_output.return_value = (
        None
    )
    response = await api_client.put(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs/{workflow_dynamic_output_id}",
        data=workflow_dynamic_output_data,
    )
    assert response.status == 200
    assert await response.json() is None
    workflow_action_service_mock.update_workflow_action_dynamic_output.assert_called_once_with(
        workflow_id,
        workflow_action_id,
        workflow_dynamic_output_id,
        UpdateWorkflowActionDynamicOutput(**workflow_dynamic_output_data),
    )


async def test_update_workflow_action_dynamic_output_when_workflow_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Update a dynamic output in workflow action should return error when workflow doesn't exists"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_id = "workflow_dynamic_output_id"
    workflow_dynamic_output_data = {
        "technical_name": "technical_name",
    }
    workflow_action_service_mock.update_workflow_action_dynamic_output.side_effect = (
        WorkflowNotFoundException
    )
    response = await api_client.put(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs/{workflow_dynamic_output_id}",
        data=workflow_dynamic_output_data,
    )
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_action_service_mock.update_workflow_action_dynamic_output.assert_called_once_with(
        workflow_id,
        workflow_action_id,
        workflow_dynamic_output_id,
        UpdateWorkflowActionDynamicOutput(**workflow_dynamic_output_data),
    )


async def test_update_workflow_action_dynamic_output_when_workflow_action_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Update a dynamic output in workflow action should return error when workflow action doesn't exists"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_id = "workflow_dynamic_output_id"
    workflow_dynamic_output_data = {
        "technical_name": "technical_name",
    }
    workflow_action_service_mock.update_workflow_action_dynamic_output.side_effect = (
        WorkflowActionNotFoundException
    )
    response = await api_client.put(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs/{workflow_dynamic_output_id}",
        data=workflow_dynamic_output_data,
    )
    assert response.status == 404
    assert await response.json() == {"error": "Action not found in the workflow"}
    workflow_action_service_mock.update_workflow_action_dynamic_output.assert_called_once_with(
        workflow_id,
        workflow_action_id,
        workflow_dynamic_output_id,
        UpdateWorkflowActionDynamicOutput(**workflow_dynamic_output_data),
    )


async def test_update_workflow_action_dynamic_output_when_dynamic_output_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Update a dynamic output in workflow action should return error when dynamic output doesn't exists"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_id = "workflow_dynamic_output_id"
    workflow_dynamic_output_data = {
        "technical_name": "technical_name",
    }
    workflow_action_service_mock.update_workflow_action_dynamic_output.side_effect = (
        WorkflowActionDynamicOutputNotFoundException
    )
    response = await api_client.put(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs/{workflow_dynamic_output_id}",
        data=workflow_dynamic_output_data,
    )
    assert response.status == 404
    assert await response.json() == {
        "error": "Dynamic output not found in the workflow"
    }
    workflow_action_service_mock.update_workflow_action_dynamic_output.assert_called_once_with(
        workflow_id,
        workflow_action_id,
        workflow_dynamic_output_id,
        UpdateWorkflowActionDynamicOutput(**workflow_dynamic_output_data),
    )


async def test_update_dynamic_output_in_workflow_action_when_workflow_action_take_no_dynamic_output(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Update dynamic output of workflow action should return error when workflow action doesn't take dynamic outputs"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_id = "workflow_dynamic_output_id"
    workflow_dynamic_output_data = {
        "technical_name": "technical_name",
    }
    workflow_action_service_mock.update_workflow_action_dynamic_output.side_effect = (
        WorkflowActionHasNoDynamicOutputsException
    )
    response = await api_client.put(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs/{workflow_dynamic_output_id}",
        data=workflow_dynamic_output_data,
    )
    assert response.status == 404
    assert await response.json() == {
        "error": "The selected action can't take dynamic outputs"
    }
    workflow_action_service_mock.update_workflow_action_dynamic_output.assert_called_once_with(
        workflow_id,
        workflow_action_id,
        workflow_dynamic_output_id,
        UpdateWorkflowActionDynamicOutput(**workflow_dynamic_output_data),
    )


async def test_delete_workflow_action_dynamic_output(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Delete a dynamic output in workflow action should work"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_id = "workflow_dynamic_output_id"
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs/{workflow_dynamic_output_id}"
    )
    assert response.status == 200
    assert await response.json() is None
    workflow_action_service_mock.delete_workflow_action_dynamic_output.assert_called_once_with(
        workflow_id, workflow_action_id, workflow_dynamic_output_id
    )


async def test_delete_workflow_action_dynamic_output_when_workflow_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Delete a dynamic output in workflow action should return error when workflow doesn't exists"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_id = "workflow_dynamic_output_id"
    workflow_action_service_mock.delete_workflow_action_dynamic_output.side_effect = (
        WorkflowNotFoundException
    )
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs/{workflow_dynamic_output_id}"
    )
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_action_service_mock.delete_workflow_action_dynamic_output.assert_called_once_with(
        workflow_id, workflow_action_id, workflow_dynamic_output_id
    )


async def test_delete_workflow_action_dynamic_output_when_workflow_action_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Delete a dynamic output in workflow action should return error when workflow action doesn't exists"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_id = "workflow_dynamic_output_id"
    workflow_action_service_mock.delete_workflow_action_dynamic_output.side_effect = (
        WorkflowActionNotFoundException
    )
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs/{workflow_dynamic_output_id}"
    )
    assert response.status == 404
    assert await response.json() == {"error": "Action not found in the workflow"}
    workflow_action_service_mock.delete_workflow_action_dynamic_output.assert_called_once_with(
        workflow_id, workflow_action_id, workflow_dynamic_output_id
    )


async def test_delete_workflow_action_dynamic_output_when_dynamic_output_not_exists(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Delete a dynamic output in workflow action should return error when dynamic output doesn't exists"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_id = "workflow_dynamic_output_id"
    workflow_action_service_mock.delete_workflow_action_dynamic_output.side_effect = (
        WorkflowActionDynamicOutputNotFoundException
    )
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs/{workflow_dynamic_output_id}"
    )
    assert response.status == 404
    assert await response.json() == {
        "error": "Dynamic output not found in the workflow"
    }
    workflow_action_service_mock.delete_workflow_action_dynamic_output.assert_called_once_with(
        workflow_id, workflow_action_id, workflow_dynamic_output_id
    )


async def test_delete_workflow_action_dynamic_output_when_workflow_action_take_no_dynamic_outputs(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Delete a dynamic output in workflow action should return error when dynamic output doesn't exists"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_dynamic_output_id = "workflow_dynamic_output_id"
    workflow_action_service_mock.delete_workflow_action_dynamic_output.side_effect = (
        WorkflowActionHasNoDynamicOutputsException
    )
    response = await api_client.delete(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs/{workflow_dynamic_output_id}"
    )
    assert response.status == 404
    assert await response.json() == {
        "error": "The selected action can't take dynamic outputs"
    }
    workflow_action_service_mock.delete_workflow_action_dynamic_output.assert_called_once_with(
        workflow_id, workflow_action_id, workflow_dynamic_output_id
    )


async def test_list_workflow_action_inputs(
    workflow_action_service_mock: WorkflowActionService, api_client: TestClient
) -> None:
    """When fetching workflow action inputs endpoint should return list of inputs"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    input_1 = WorkflowActionInputView(
        id="action_input_id_1",
        technical_name="technical name",
        display_name="display name",
        help="help",
        type=ActionIOType.STRING,
        enum_values=["option1", "option2"],
        static_value="static_value",
        workflow_file=WorkflowFile(
            id="35794a5e-a0fd-43bc-b499-85914b90ba97",
            name="data-file",
            extension="txt",
            size=12.12,
            path="input/file",
        ),
        reference_output=None,
        reference_value=None,
        project_variable_value=None,
        optional=False,
    )
    input_2 = WorkflowActionInputView(
        id="action_input_id_2",
        technical_name="technical name 2",
        display_name="display name 2",
        help="help 2",
        type=ActionIOType.STRING,
        enum_values=["option1", "option2"],
        reference_value="reference_value",
        reference_output=WorkflowActionOutputView(
            id="reference_value",
            technical_name="output_technical_name",
            display_name="Output display name",
            help="Output help",
            type=ActionIOType.STRING,
            enum_values=["option1", "option2"],
            optional=False,
        ),
        static_value=None,
        workflow_file=None,
        project_variable_value=None,
        optional=False,
    )
    workflow_action_service_mock.list_workflow_action_inputs.return_value = [
        input_1,
        input_2,
    ]
    response = await api_client.get(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs"
    )
    assert response.status == 200
    result = await response.json()
    WorkflowModuleInputSchema(many=True).load(result, unknown=RAISE, partial=False)
    assert result == [
        {
            "id": "action_input_id_1",
            "technical_name": "technical name",
            "display_name": "display name",
            "help": "help",
            "type": "string",
            "enum_values": ["option1", "option2"],
            "static_value": "static_value",
            "reference_value": None,
            "reference_output": None,
            "workflow_file": {
                "id": "35794a5e-a0fd-43bc-b499-85914b90ba97",
                "name": "data-file",
                "extension": "txt",
                "size": 12.12,
                "path": "input/file",
            },
        },
        {
            "id": "action_input_id_2",
            "technical_name": "technical name 2",
            "display_name": "display name 2",
            "help": "help 2",
            "type": "string",
            "enum_values": ["option1", "option2"],
            "static_value": None,
            "reference_value": "reference_value",
            "reference_output": {
                "id": "reference_value",
                "technical_name": "output_technical_name",
                "display_name": "Output display name",
                "help": "Output help",
                "type": "string",
                "enum_values": ["option1", "option2"],
            },
            "workflow_file": None,
        },
    ]
    workflow_action_service_mock.list_workflow_action_inputs.assert_called_with(
        workflow_id, workflow_action_id
    )


async def test_list_workflow_action_inputs_when_workflow_not_found(
    workflow_action_service_mock: WorkflowActionService, api_client: TestClient
) -> None:
    """When fetching workflow action inputs endpoint should return list of inputs"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_service_mock.list_workflow_action_inputs.side_effect = (
        WorkflowNotFoundException
    )
    response = await api_client.get(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs"
    )
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_action_service_mock.list_workflow_action_inputs.assert_called_once()


async def test_list_workflow_action_inputs_when_workflow_action_not_found(
    workflow_action_service_mock: WorkflowActionService, api_client: TestClient
) -> None:
    """When fetching workflow action inputs endpoint should return list of inputs"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_service_mock.list_workflow_action_inputs.side_effect = (
        WorkflowActionNotFoundException
    )
    response = await api_client.get(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs"
    )
    assert response.status == 404
    assert await response.json() == {"error": "Action not found in the workflow"}
    workflow_action_service_mock.list_workflow_action_inputs.assert_called_once()


async def test_list_workflow_action_outputs(
    workflow_action_service_mock: WorkflowActionService, api_client: TestClient
) -> None:
    """When fetching workflow action outputs endpoint should return list of outputs"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    output_1 = WorkflowActionOutputView(
        id="output_1",
        technical_name="output_1_technical_name",
        display_name="Output 1 display name",
        help="Output 1 help",
        type=ActionIOType.STRING,
        enum_values=["option1", "option2"],
        optional=False,
    )
    output_2 = WorkflowActionOutputView(
        id="output_2",
        technical_name="output_2_technical_name",
        display_name="Output 2 display name",
        help="Output 2 help",
        type=ActionIOType.STRING,
        enum_values=["option1", "option2"],
        optional=False,
    )
    workflow_action_service_mock.list_workflow_action_outputs.return_value = [
        output_1,
        output_2,
    ]
    response = await api_client.get(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs"
    )
    assert response.status == 200
    result = await response.json()
    WorkflowModuleOutputSchema(many=True).load(result, unknown=RAISE, partial=False)
    assert result == [
        {
            "id": "output_1",
            "technical_name": "output_1_technical_name",
            "display_name": "Output 1 display name",
            "help": "Output 1 help",
            "type": "string",
            "enum_values": ["option1", "option2"],
        },
        {
            "id": "output_2",
            "technical_name": "output_2_technical_name",
            "display_name": "Output 2 display name",
            "help": "Output 2 help",
            "type": "string",
            "enum_values": ["option1", "option2"],
        },
    ]


async def test_list_workflow_action_outputs_when_workflow_not_found(
    workflow_action_service_mock: WorkflowActionService, api_client: TestClient
) -> None:
    """When fetching workflow action outputs endpoint should return list of outputs"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_service_mock.list_workflow_action_outputs.side_effect = (
        WorkflowNotFoundException
    )
    response = await api_client.get(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs"
    )
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}


async def test_list_workflow_action_outputs_when_workflow_action_not_found(
    workflow_action_service_mock: WorkflowActionService, api_client: TestClient
) -> None:
    """When fetching workflow action outputs endpoint should return list of outputs"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_service_mock.list_workflow_action_outputs.side_effect = (
        WorkflowActionNotFoundException
    )
    response = await api_client.get(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/outputs"
    )
    assert response.status == 404
    assert await response.json() == {"error": "Action not found in the workflow"}


async def test_search_workflow_action_outputs(
    workflow_action_service_mock: WorkflowActionService, api_client: TestClient
) -> None:
    workflow_id = "workflow_id"
    output_1 = WorkflowActionOutputView(
        id="output_1",
        technical_name="output_technical_name_1",
        display_name="Output display name 1",
        help="Output help 1",
        type=ActionIOType.STRING,
        enum_values=["option1", "option2"],
        optional=False,
    )
    output_2 = WorkflowActionOutputView(
        id="output_2",
        technical_name="output_technical_name_2",
        display_name="Output display name 2",
        help="Output help 2",
        type=ActionIOType.STRING,
        enum_values=["option1", "option2"],
        optional=False,
    )
    workflow_action_service_mock.search_workflow_action_outputs.return_value = [
        output_1,
        output_2,
    ]
    response = await api_client.get(f"/workflows/{workflow_id}/modules-outputs")
    assert response.status == 200
    result = await response.json()
    WorkflowModuleOutputSchema(many=True).load(result, unknown=RAISE, partial=False)
    assert result == [
        {
            "id": "output_1",
            "technical_name": "output_technical_name_1",
            "display_name": "Output display name 1",
            "help": "Output help 1",
            "type": "string",
            "enum_values": ["option1", "option2"],
        },
        {
            "id": "output_2",
            "technical_name": "output_technical_name_2",
            "display_name": "Output display name 2",
            "help": "Output help 2",
            "type": "string",
            "enum_values": ["option1", "option2"],
        },
    ]


async def test_search_workflow_action_outputs_with_type_param(
    workflow_action_service_mock: WorkflowActionService, api_client: TestClient
) -> None:
    workflow_id = "workflow_id"
    with_type_filter = "string"
    workflow_action_service_mock.search_workflow_action_outputs = mock.MagicMock(
        return_value=[]
    )
    await api_client.get(
        f"/workflows/{workflow_id}/modules-outputs",
        params={"with_type": with_type_filter},
    )
    workflow_action_service_mock.search_workflow_action_outputs.assert_called_with(
        workflow_id, with_type=with_type_filter, accessible_from=None
    )


async def test_search_workflow_action_outputs_with_accessible_from_param(
    workflow_action_service_mock: WorkflowActionService, api_client: TestClient
) -> None:
    workflow_id = "workflow_id"
    accessible_from_param = "workflow_action_id"
    workflow_action_service_mock.search_workflow_action_outputs = mock.MagicMock(
        return_value=[]
    )
    await api_client.get(
        f"/workflows/{workflow_id}/modules-outputs",
        params={"accessible_from": accessible_from_param},
    )
    workflow_action_service_mock.search_workflow_action_outputs.assert_called_with(
        workflow_id, accessible_from=accessible_from_param, with_type=None
    )


async def test_search_workflow_action_outputs_when_workflow_not_found(
    workflow_action_service_mock: WorkflowActionService, api_client: TestClient
) -> None:
    workflow_id = "workflow_id"
    workflow_action_service_mock.search_workflow_action_outputs.side_effect = (
        WorkflowNotFoundException
    )
    response = await api_client.get(f"/workflows/{workflow_id}/modules-outputs")
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}


@mock.patch.object(workflow_action_controller.tempfile, "TemporaryFile")
async def test_update_workflow_action_input_with_static_file(
    tmp_file_mock: tempfile.TemporaryFile,
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
) -> None:
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_action_input_id"
    workflow_action_service_mock.upload_static_file_input.return_value = workflow_id
    file = os.urandom(8193)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.csv")
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs/{workflow_action_input_id}/file",
        data=mp_writer,
    )
    assert response.status == 201
    workflow_action_service_mock.upload_static_file_input.assert_called_once_with(
        workflow_id,
        workflow_action_id,
        workflow_action_input_id,
        "test",
        "csv",
        tmp_file_mock().__enter__(),
        8193,
    )


@mock.patch.object(workflow_action_controller.tempfile, "TemporaryFile")
@pytest.mark.parametrize(
    "filename, expected_file_stem, expected_file_suffix",
    [
        ("foo.txt", "foo", "txt"),
        ("foo", "foo", ""),
        ("foo.", "foo.", ""),
        ("foo.......", "foo.......", ""),
        ("foo...txt", "foo..", "txt"),
        ("foo.tar.gz", "foo.tar", "gz"),
        ("./bar/foo/qux/baz.tar.gz", ".%2Fbar%2Ffoo%2Fqux%2Fbaz.tar", "gz"),
    ],
)
async def test_update_workflow_action_input_with_static_file_when_unusual_filenames(
    tmp_file_mock: tempfile.TemporaryFile,
    filename: str,
    expected_file_stem: str,
    expected_file_suffix: str,
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
) -> None:
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_action_input_id"
    workflow_action_service_mock.upload_static_file_input.return_value = workflow_id
    file = os.urandom(4)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename=filename)
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs/{workflow_action_input_id}/file",
        data=mp_writer,
    )
    assert response.status == 201
    workflow_action_service_mock.upload_static_file_input.assert_called_once_with(
        workflow_id,
        workflow_action_id,
        workflow_action_input_id,
        expected_file_stem,
        expected_file_suffix,
        tmp_file_mock().__enter__(),
        4,
    )


@mock.patch.object(workflow_action_controller.tempfile, "TemporaryFile")
async def test_update_workflow_action_input_with_static_file_workflow_not_found(
    tmp_file_mock: tempfile.TemporaryFile,
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
) -> None:
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_action_input_id"
    workflow_action_service_mock.upload_static_file_input.side_effect = (
        WorkflowNotFoundException
    )
    file = os.urandom(8193)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.csv")
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs/{workflow_action_input_id}/file",
        data=mp_writer,
    )
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_action_service_mock.upload_static_file_input.assert_called_once_with(
        workflow_id,
        workflow_action_id,
        workflow_action_input_id,
        "test",
        "csv",
        tmp_file_mock().__enter__(),
        8193,
    )


async def test_update_workflow_action_input_with_static_file_when_file_too_large(
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
    app_container: ApplicationContainer,
) -> None:
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_action_input_id"
    workflow_action_service_mock.upload_static_file_input.return_value = workflow_id
    app_container.configuration.from_dict({"max_upload_size": 20})

    file = os.urandom(21)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.csv")
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs/{workflow_action_input_id}/file",
        data=mp_writer,
    )
    assert response.status == 400
    workflow_action_service_mock.upload_static_file_input.assert_not_called()


@mock.patch.object(workflow_action_controller.tempfile, "TemporaryFile")
async def test_update_workflow_action_input_with_static_file_workflow_action_not_found(
    tmp_file_mock: tempfile.TemporaryFile,
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
) -> None:
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_action_input_id"
    workflow_action_service_mock.upload_static_file_input.side_effect = (
        WorkflowActionNotFoundException
    )
    file = os.urandom(64)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.csv")

    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs/{workflow_action_input_id}/file",
        data=mp_writer,
    )
    assert response.status == 404
    assert await response.json() == {"error": "Action not found in the workflow"}
    workflow_action_service_mock.upload_static_file_input.assert_called_once_with(
        workflow_id,
        workflow_action_id,
        workflow_action_input_id,
        "test",
        "csv",
        tmp_file_mock().__enter__(),
        64,
    )


@mock.patch.object(workflow_action_controller.tempfile, "TemporaryFile")
async def test_update_workflow_action_input_with_static_file_workflow_action_input_not_found(
    tmp_file_mock: tempfile.TemporaryFile,
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
) -> None:
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_action_input_id"
    workflow_action_service_mock.upload_static_file_input.side_effect = (
        WorkflowActionInputNotFoundException
    )
    file = os.urandom(64)
    with MultipartWriter() as mp_writer:
        part = mp_writer.append(file)
        part.set_content_disposition("attachment", filename="test.csv")

    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs/{workflow_action_input_id}/file",
        data=mp_writer,
    )
    assert response.status == 404
    assert await response.json() == {"error": "Input not found in the workflow"}
    workflow_action_service_mock.upload_static_file_input.assert_called_once_with(
        workflow_id,
        workflow_action_id,
        workflow_action_input_id,
        "test",
        "csv",
        tmp_file_mock().__enter__(),
        64,
    )


async def test_update_workflow_action_input_with_static_file_when_multiple_file(
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
) -> None:
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    workflow_action_input_id = "workflow_action_input_id"
    workflow_action_service_mock.upload_static_file_input.return_value = workflow_id
    with MultipartWriter() as mp_writer:
        mp_writer_2 = MultipartWriter()
        file_1 = mp_writer_2
        part = mp_writer.append(file_1)
        part.set_content_disposition("attachment", filename="test.packaging")
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/inputs/{workflow_action_input_id}/file",
        data=mp_writer,
    )
    assert response.status == 400
    assert await response.json() == {"error": "Input value is invalid"}
    workflow_action_service_mock.upload_static_file_input.assert_not_called()


async def test_change_action_version(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Update workflow action version through api should work"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    action_id = "action_id"
    workflow_action_service_mock.change_workflow_action_version.return_value = (
        "new_id",
        [],
    )
    data = {"module_id": action_id}
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/change_version",
        data=data,
    )
    assert response.status == 200
    assert await response.json() == {
        "workflow_module_id": "new_id",
        "removed_results": [],
    }
    workflow_action_service_mock.change_workflow_action_version.assert_called_once_with(
        workflow_id, workflow_action_id, action_id
    )


async def test_change_action_version_when_workflow_not_found(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Update workflow action version through api should return 404 when workflow not found"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    action_id = "action_id"
    workflow_action_service_mock.change_workflow_action_version.side_effect = (
        WorkflowNotFoundException
    )
    data = {"module_id": action_id}
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/change_version",
        data=data,
    )
    assert response.status == 404
    assert await response.json() == {"error": "Workflow not found"}
    workflow_action_service_mock.change_workflow_action_version.assert_called_once_with(
        workflow_id, workflow_action_id, action_id
    )


async def test_change_action_version_when_workflow_not_updatable(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Update workflow action version through api should return 404 when workflow not found"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    action_id = "action_id"
    workflow_action_service_mock.change_workflow_action_version.side_effect = (
        WorkflowNotUpdatableException
    )
    data = {"module_id": action_id}
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/change_version",
        data=data,
    )
    assert response.status == 400
    assert await response.json() == {
        "error": "Workflow can not be modified at this status"
    }
    workflow_action_service_mock.change_workflow_action_version.assert_called_once_with(
        workflow_id, workflow_action_id, action_id
    )


async def test_change_action_version_when_workflow_action_not_found(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Update workflow action version through api should return 404 when workflow action not found"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    action_id = "action_id"
    workflow_action_service_mock.change_workflow_action_version.side_effect = (
        WorkflowActionNotFoundException
    )
    data = {"module_id": action_id}
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/change_version",
        data=data,
    )
    assert response.status == 404
    assert await response.json() == {"error": "Action not found in the workflow"}
    workflow_action_service_mock.change_workflow_action_version.assert_called_once_with(
        workflow_id, workflow_action_id, action_id
    )


async def test_change_action_version_when_action_not_found(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient
) -> None:
    """Update workflow action version through api should return 404 when action not found"""
    workflow_id = "workflow_id"
    workflow_action_id = "workflow_action_id"
    action_id = "action_id"
    workflow_action_service_mock.change_workflow_action_version.side_effect = (
        ActionNotFoundException
    )
    data = {"module_id": action_id}
    response = await api_client.post(
        f"/workflows/{workflow_id}/modules/{workflow_action_id}/change_version",
        data=data,
    )
    assert response.status == 404
    assert await response.json() == {"error": "Action not found"}
    workflow_action_service_mock.change_workflow_action_version.assert_called_once_with(
        workflow_id, workflow_action_id, action_id
    )


@pytest.mark.parametrize(
    "data",
    [
        ({"links": []}),
        (
            {
                "links": [
                    {
                        "module_id": "actionid",
                        "next_modules_ids": [],
                    }
                ]
            }
        ),
        (
            {
                "links": [
                    {
                        "module_id": "actionid",
                        "next_modules_ids": [
                            "actionid",
                            "actionid",
                            "actionid",
                        ],
                    },
                    {
                        "module_id": "actionid",
                        "next_modules_ids": ["actionid"],
                    },
                ]
            }
        ),
    ],
    ids=["empty links list", "empty list for a action", "several actions"],
)
async def test_update_workflow_actions_links(
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
    data: dict,
):
    """Update links of several actions at once"""
    workflow_id = "workflowid"
    response = await api_client.put(f"/v2/workflows/{workflow_id}/links", json=data)
    assert response.status == 200
    assert await response.json() is None


@pytest.mark.parametrize(
    "data",
    [({}), ({"links": [{}]}), ({"links": []}), ({"what am i": "doing here"})],
    ids=[
        "empty request",
        "",
        "empty element",
        "something else entirely",
    ],
)
async def test_update_workflow_actions_links_invalid_data(
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
    data: dict,
) -> None:
    """Add action to workflow through api should work"""
    workflow_id = "workflow_id"
    workflow_action_service_mock.update_workflow_action_links.return_value = None
    response = await api_client.put(f"/v2/workflows/{workflow_id}/links", data=data)
    assert response.status == 422
    workflow_action_service_mock.add_workflow_action_with_link.assert_not_called()


@pytest.mark.parametrize(
    "exception, response_status, response_message",
    [
        (
            WorkflowNotFoundException(),
            404,
            "Workflow not found",
        ),
        (
            WorkflowActionLinkNotFoundException(),
            404,
            "Action link not found in the workflow",
        ),
        (
            WorkflowActionNotFoundException(),
            404,
            "Action not found in the workflow",
        ),
    ],
    ids=["workflow not found", "action not found", "workflow action not found"],
)
async def test_update_workflow_actions_links_with_exception(
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
    exception,
    response_status,
    response_message,
):
    """Add action to workflow through api should return error when workflow doesn't exist"""
    workflow_id = "workflow_id"
    data = {"links": [{"module_id": "", "next_modules_ids": [""]}]}
    workflow_action_service_mock.update_workflow_action_links.side_effect = exception
    response = await api_client.put(f"/v2/workflows/{workflow_id}/links", json=data)
    assert response.status == response_status
    assert await response.json() == {"error": response_message}


@pytest.mark.parametrize(
    "error, message",
    [
        (WorkflowNotFoundException, "Workflow not found"),
        (WorkflowActionNotFoundException, "Action not found in the workflow"),
        (WorkflowActionOutputNotFoundException, "Output not found in the workflow"),
        (InvalidInputValueException, "Input value is invalid"),
        (
            WorkflowActionHasNoDynamicOutputsException,
            "The selected action can't take dynamic outputs",
        ),
    ],
)
async def test_update_all_workflow_action_io_when_errors(
    workflow_action_service_mock: mock.MagicMock, api_client: TestClient, error, message
):
    """Update Workflow action IO"""
    workflow_id = "workflowid"
    workflow_action_id = "workflowaction"
    data = {"custom_name": "foo", "inputs": [], "dynamic_outputs": []}
    workflow_action_service_mock.update_all_workflow_action_io.side_effect = error
    response = await api_client.put(
        f"/v2/workflows/{workflow_id}/modules/{workflow_action_id}", json=data
    )
    assert response.status == 400
    assert await response.json() == {"error": message}


async def test_update_all_workflow_action_io(
    workflow_action_service_mock: mock.MagicMock,
    api_client: TestClient,
):
    """Update Workflow action IO"""
    workflow_id = "workflowid"
    workflow_action_id = "workflowaction"
    data = {
        "custom_name": "foo",
        "inputs": [{"id": "idtest", "static_value": "foo"}],
        "dynamic_outputs": [
            {
                "id": "idtest2",
                "technical_name": "techname",
                "display_name": "name",
                "help": "help",
                "type": "string",
                "origin": "path",
            }
        ],
        "addons_inputs": [{"id": "idtest3", "static_value": "foo"}],
    }
    response = await api_client.put(
        f"/v2/workflows/{workflow_id}/modules/{workflow_action_id}", json=data
    )
    assert response.status == 200
    assert await response.json() is None
    data["dynamic_outputs"][0]["origin"] = DynamicOutputOrigin.PATH
    data["dynamic_outputs"][0]["type"] = ActionIOType.STRING
    data["dynamic_outputs"][0]["optional"] = False
    call_data = UpdateAllWorkflowActionIOData(
        custom_name=data["custom_name"],
        inputs=[
            UpdateAllWorkflowActionIOData.InputValue(**input_data)
            for input_data in data["inputs"]
        ],
        dynamic_outputs=[
            UpdateAllWorkflowActionIOData.DynamicOutputDefinition(**output_data)
            for output_data in data["dynamic_outputs"]
        ],
        addons_inputs=[
            UpdateAllWorkflowActionIOData.InputValue(**addons_inputs_data)
            for addons_inputs_data in data["addons_inputs"]
        ],
    )
    workflow_action_service_mock.update_all_workflow_action_io.assert_called_once_with(
        workflow_id=workflow_id, workflow_action_id=workflow_action_id, data=call_data
    )
