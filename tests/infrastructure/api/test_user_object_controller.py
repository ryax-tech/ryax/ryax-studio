import io
import tempfile
from unittest import mock

import aiohttp
import pytest
from aiohttp.test_utils import TestClient
from marshmallow import RAISE

from ryax.studio.application.project_variable_service import ProjectVariableService
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.project_variables.project_variable_entities import (
    ProjectVariable,
    ProjectVariableTransactionData,
)
from ryax.studio.domain.project_variables.project_variable_exceptions import (
    ProjectVariableNotFoundException,
)
from ryax.studio.infrastructure.api.controllers import project_variable_controller
from ryax.studio.infrastructure.api.schemas.project_variable_schema import (
    ProjetcVariablesSchema,
)


async def test_list_project_variables(
    project_variable_service_mock: ProjectVariableService, api_client: TestClient
):
    project_variables = [
        ProjectVariable(
            id="id1",
            name="name1",
            value=123,
            type=ActionIOType.INTEGER,
            project_id="project_id",
        ),
        ProjectVariable(
            id="id2",
            name="name2",
            value="456",
            type=ActionIOType.STRING,
            project_id="project_id",
        ),
    ]
    project_variable_service_mock.list = mock.MagicMock(return_value=project_variables)
    response = await api_client.get("/v2/user-objects")
    assert response.status == 200
    result = await response.json()
    ProjetcVariablesSchema(many=True).load(result, unknown=RAISE, partial=False)
    assert result == [
        {
            "description": user_obj.description,
            "id": user_obj.id,
            "name": user_obj.name,
            "type": user_obj.type.value,
            "value": user_obj.value,
        }
        for user_obj in project_variables
    ]
    project_variable_service_mock.list.assert_called_once_with(
        "project_id", type_search=None
    )


async def test_list_project_variables_with_search(
    project_variable_service_mock: ProjectVariableService, api_client: TestClient
):
    project_variables = [
        ProjectVariable(
            id="id1",
            name="name1",
            value=123,
            type=ActionIOType.INTEGER,
            project_id="project_id",
        ),
        ProjectVariable(
            id="id2",
            name="name2",
            value="456",
            type=ActionIOType.STRING,
            project_id="project_id",
        ),
    ]
    project_variable_service_mock.list = mock.MagicMock(return_value=project_variables)
    search = "Integer"
    response = await api_client.get(f"/v2/user-objects?type={search}")
    assert response.status == 200
    result = await response.json()
    ProjetcVariablesSchema(many=True).load(result, unknown=RAISE, partial=False)
    assert result == [
        {
            "description": user_obj.description,
            "id": user_obj.id,
            "name": user_obj.name,
            "type": user_obj.type.value,
            "value": user_obj.value,
        }
        for user_obj in project_variables
    ]
    project_variable_service_mock.list.assert_called_once_with(
        "project_id", type_search=search
    )


async def test_get_project_variable(
    project_variable_service_mock: ProjectVariableService, api_client: TestClient
):
    project_variable = ProjectVariable(
        id="id1",
        name="name1",
        value=123,
        type=ActionIOType.INTEGER,
        project_id="project_id",
    )
    project_variable_service_mock.get = mock.MagicMock(return_value=project_variable)
    response = await api_client.get(f"/v2/user-objects/{project_variable.id}")
    assert response.status == 200
    result = await response.json()
    ProjetcVariablesSchema().load(result, unknown=RAISE, partial=False)
    assert result == {
        "description": project_variable.description,
        "id": project_variable.id,
        "name": project_variable.name,
        "type": project_variable.type.value,
        "value": project_variable.value,
    }

    project_variable_service_mock.get.assert_called_once_with(
        project_variable.id, "project_id"
    )


async def test_get_project_variable_when_not_found(
    project_variable_service_mock: ProjectVariableService, api_client: TestClient
):
    project_variable_service_mock.get = mock.MagicMock(
        side_effect=ProjectVariableNotFoundException
    )
    id_to_use = "foo"
    response = await api_client.get(f"/v2/user-objects/{id_to_use}")
    assert response.status == 404
    project_variable_service_mock.get.assert_called_once_with(id_to_use, "project_id")


@pytest.mark.parametrize(
    "request_data",
    [
        (
            {
                "name": "userobj",
                "value": "value",
                "type": "string",
            }
        ),
        (
            {
                "name": "userobj",
                "value": "value",
                "type": "string",
                "description": "desc",
            }
        ),
    ],
    ids=["Add with no description", "Add with all data"],
)
async def test_add_project_variable(
    project_variable_service_mock: ProjectVariableService,
    api_client: TestClient,
    request_data: dict,
):
    new_id = "foo"
    project_variable_service_mock.add = mock.MagicMock(return_value=new_id)
    response = await api_client.post("/v2/user-objects", data=request_data)
    assert response.status == 201
    assert await response.json() == {"id": new_id}
    project_variable_service_mock.add.assert_called_once_with(
        ProjectVariableTransactionData(
            name=request_data["name"],
            value=request_data["value"],
            type=request_data["type"],
            project_id="project_id",
            description=request_data.get("description"),
        )
    )


async def test_delete_project_variable(
    project_variable_service_mock: ProjectVariableService, api_client: TestClient
):
    object_id = "foo"
    project_variable_service_mock.delete = mock.AsyncMock()
    response = await api_client.delete(f"/v2/user-objects/{object_id}")
    assert response.status == 200
    assert not await response.json()
    project_variable_service_mock.delete.assert_awaited_once_with(
        object_id, "project_id"
    )


async def test_delete_project_variable_when_not_found(
    project_variable_service_mock: ProjectVariableService, api_client: TestClient
):
    object_id = "foo"
    project_variable_service_mock.delete = mock.AsyncMock(
        side_effect=ProjectVariableNotFoundException
    )
    response = await api_client.delete(f"/v2/user-objects/{object_id}")
    assert response.status == 404
    project_variable_service_mock.delete.assert_awaited_once_with(
        object_id, "project_id"
    )


@pytest.mark.parametrize(
    "request_data",
    [
        (
            {
                "name": "userobj",
                "value": "value",
                "type": "string",
            }
        ),
    ],
    ids=["Update with no description"],
)
async def test_update_project_variable(
    project_variable_service_mock: ProjectVariableService,
    api_client: TestClient,
    request_data: dict,
):
    new_id = "foo"
    project_variable_service_mock.update = mock.AsyncMock()
    response = await api_client.put(f"/v2/user-objects/{new_id}", data=request_data)
    assert response.status == 200
    assert not await response.json()
    project_variable_service_mock.update.assert_called_once_with(
        new_id,
        ProjectVariableTransactionData(
            name=request_data["name"],
            value=request_data["value"],
            type=request_data["type"],
            project_id="project_id",
            description=request_data.get("description"),
        ),
        "project_id",
    )


@pytest.mark.parametrize(
    "request_data",
    [
        (
            {
                "name": "userobj",
                "value": "value",
                "type": "string",
            }
        ),
    ],
    ids=["Update with no description"],
)
async def test_update_project_variable_when_not_found(
    project_variable_service_mock: ProjectVariableService,
    api_client: TestClient,
    request_data: dict,
):
    new_id = "foo"
    project_variable_service_mock.update = mock.AsyncMock(
        side_effect=ProjectVariableNotFoundException
    )
    response = await api_client.put(f"/v2/user-objects/{new_id}", data=request_data)
    assert response.status == 404
    project_variable_service_mock.update.assert_called_once_with(
        new_id,
        ProjectVariableTransactionData(
            name=request_data["name"],
            value=request_data["value"],
            type=request_data["type"],
            project_id="project_id",
            description=request_data.get("description"),
        ),
        "project_id",
    )


@mock.patch.object(project_variable_controller.tempfile, "TemporaryFile")
async def test_add_project_variable_with_file(
    tmp_file_mock: tempfile.TemporaryFile,
    app_container: ApplicationContainer,
    project_variable_service_mock: ProjectVariableService,
    api_client: TestClient,
):
    content = io.BytesIO(b"<h1>Hello World!</h1>")
    with aiohttp.MultipartWriter("form-data") as mpwriter:
        part = mpwriter.append("test")
        part.set_content_disposition("form-data", name="name")
        part = mpwriter.append("file")
        part.set_content_disposition("form-data", name="type")
        part = mpwriter.append("description blabla")
        part.set_content_disposition("form-data", name="description")
        part = mpwriter.append(content, {"CONTENT-TYPE": "text/html"})
        part.set_content_disposition("form-data", name="file", filename="index.html")

    app_container.configuration.from_dict({"max_upload_size": 2048})
    new_id = "foo"
    project_variable_service_mock.add_with_file = mock.AsyncMock(return_value=new_id)
    response = await api_client.post("/v2/user-objects/file", data=mpwriter)
    assert response.status == 201
    assert await response.json() == {"id": new_id}
    project_variable_service_mock.add_with_file.assert_called_once_with(
        "index",
        "html",
        tmp_file_mock().__enter__(),
        ProjectVariableTransactionData(
            name="test",
            type="file",
            project_id="project_id",
            description="description blabla",
            value=None,
            file_path=None,
        ),
    )
