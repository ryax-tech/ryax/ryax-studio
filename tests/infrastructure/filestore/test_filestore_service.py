import io
import logging
import os
import uuid
from unittest import mock

import minio
import pytest
from minio import Minio

from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.workflow.workflow_exceptions import (
    FilestoreEntryNotFoundException,
)
from ryax.studio.infrastructure.filestore.engine import FileStoreEngine
from ryax.studio.infrastructure.filestore.filestore_service import FilestoreService


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_uuid"))
def test_generate_file_path(app_container: ApplicationContainer):
    app_container.filestore_engine.override(
        mock.AsyncMock(FileStoreEngine, connection="MOCK", bucket="/test")
    )
    filestore_service: FilestoreService = app_container.filestore_service()
    assert (
        filestore_service.generate_file_path("file_name") == "iodata/new_uuid/file_name"
    )


async def test_upload_file(
    minio_client: Minio, filestore_bucket: str, app_container: ApplicationContainer
):
    file_id = uuid.uuid4()
    path = f"folder/{file_id}"
    file_size = 8193
    file = os.urandom(file_size)
    filestore_engine: FileStoreEngine = mock.MagicMock(FileStoreEngine)
    filestore_engine.connection = minio_client
    filestore_engine.bucket = filestore_bucket
    app_container.filestore_engine.override(filestore_engine)
    filestore_service: FilestoreService = app_container.filestore_service()
    await filestore_service.upload_file(path, file, file_size)
    check_file = minio_client.get_object(filestore_bucket, path)
    check_file_content = check_file.read()
    assert check_file_content == file
    assert len(check_file_content) == file_size


async def test_remove_file(
    minio_client: Minio, filestore_bucket: str, app_container: ApplicationContainer
):
    file_size = 8193
    file = io.BytesIO(os.urandom(file_size))
    file_id = uuid.uuid4()
    path = f"folder/{file_id}"
    filestore_engine: FileStoreEngine = mock.MagicMock(FileStoreEngine)
    filestore_engine.connection = minio_client
    filestore_engine.bucket = filestore_bucket
    app_container.filestore_engine.override(filestore_engine)
    minio_client.put_object(filestore_bucket, path, file, file_size)
    filestore_service: FilestoreService = app_container.filestore_service()
    await filestore_service.remove_file(path)
    with pytest.raises(minio.error.S3Error):
        minio_client.get_object(filestore_bucket, path)


async def test_remove_file_when_not_exists(
    caplog,
    minio_client: Minio,
    filestore_bucket: str,
    app_container: ApplicationContainer,
):
    logging.basicConfig(level=logging.WARNING)
    filestore_engine: FileStoreEngine = mock.MagicMock(FileStoreEngine)
    filestore_engine.connection = minio_client
    filestore_engine.bucket = filestore_bucket
    app_container.filestore_engine.override(filestore_engine)
    filestore_service: FilestoreService = app_container.filestore_service()
    await filestore_service.remove_file("random_file_path")
    assert "WARNING" in caplog.text
    assert "Error while deleting file at path" in caplog.text


async def test_get_file(
    minio_client: Minio, filestore_bucket: str, app_container: ApplicationContainer
):
    file_size = 8193
    file = io.BytesIO(os.urandom(file_size))
    file_id = str(uuid.uuid4())
    path = f"folder/{file_id}"
    filestore_engine: FileStoreEngine = mock.MagicMock(FileStoreEngine)
    filestore_engine.connection = minio_client
    filestore_engine.bucket = filestore_bucket
    app_container.filestore_engine.override(filestore_engine)
    minio_client.put_object(filestore_bucket, path, file, file_size)
    filestore_service: FilestoreService = app_container.filestore_service()
    file.seek(0)
    assert await filestore_service.get_file(path) == file.read()


async def test_get_file_when_not_found(
    minio_client: Minio, filestore_bucket: str, app_container: ApplicationContainer
):
    filestore_engine: FileStoreEngine = mock.MagicMock(FileStoreEngine)
    filestore_engine.connection = minio_client
    filestore_engine.bucket = filestore_bucket
    app_container.filestore_engine.override(filestore_engine)
    filestore_service: FilestoreService = app_container.filestore_service()
    with pytest.raises(FilestoreEntryNotFoundException):
        assert await filestore_service.get_file("nonexistent_file/path")


async def test_copy_file(
    minio_client: Minio, filestore_bucket: str, app_container: ApplicationContainer
):
    file_size = 8193
    file = io.BytesIO(os.urandom(file_size))
    file_id = uuid.uuid4()
    file_id_2 = uuid.uuid4()
    path = f"folder/{file_id}"
    new_path = f"folder2/{file_id_2}"
    filestore_engine: FileStoreEngine = mock.MagicMock(FileStoreEngine)
    filestore_engine.connection = minio_client
    filestore_engine.bucket = filestore_bucket
    app_container.filestore_engine.override(filestore_engine)
    minio_client.put_object(filestore_bucket, path, file, file_size)
    filestore_service: FilestoreService = app_container.filestore_service()
    await filestore_service.copy_file(path, new_path)
    check_file = minio_client.get_object(filestore_bucket, new_path)
    check_file_content = check_file.read()
    file.seek(0)
    assert check_file_content == file.read()
    assert len(check_file_content) == file_size
