import os

import pytest
from minio import Minio

from ryax.studio.container import ApplicationContainer


@pytest.fixture()
def app_container() -> ApplicationContainer:
    container = ApplicationContainer()
    return container


@pytest.fixture(scope="function")
def minio_client():
    filestore_url = os.environ["RYAX_FILESTORE"]
    filestore_access = os.environ["RYAX_FILESTORE_ACCESS_KEY"]
    filestore_secret = os.environ["RYAX_FILESTORE_SECRET_KEY"]
    filestore_bucket = os.environ["RYAX_FILESTORE_BUCKET"]
    minio_client: Minio = Minio(
        filestore_url,
        access_key=filestore_access,
        secret_key=filestore_secret,
        secure=False,
    )
    if not minio_client.bucket_exists(filestore_bucket):
        minio_client.make_bucket(filestore_bucket)
    yield minio_client
    for item in minio_client.list_objects(filestore_bucket, recursive=True):
        minio_client.remove_object(filestore_bucket, item.object_name)
    minio_client.remove_bucket(filestore_bucket)


@pytest.fixture()
def filestore_bucket() -> str:
    filestore_bucket = os.environ["RYAX_FILESTORE_BUCKET"]
    return filestore_bucket
