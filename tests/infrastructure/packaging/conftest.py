import pytest

from ryax.studio.container import ApplicationContainer


@pytest.fixture()
def app_container():
    container = ApplicationContainer()
    container.configuration.tmp_directory_path.from_env("RYAX_TMP_DIR")
    return container
