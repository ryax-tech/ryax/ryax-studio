import zipfile
from datetime import datetime
from unittest import mock

import pytest

from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_io import ActionIO
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionInput,
)
from ryax.studio.domain.workflow.entities.workflow_action_link import WorkflowActionLink
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.workflow_exceptions import (
    WorkflowBadZipFile,
    WorkflowInvalidSchemaException,
    WorkflowParsingYamlException,
    WorkflowYamlNotFound,
)
from ryax.studio.domain.workflow.workflow_values import WorkflowActionDeployStatus
from ryax.studio.infrastructure.packaging.packaging_import import PackagingImportService


@mock.patch("zipfile.ZipFile")
@mock.patch("builtins.open")
def test_load_workflow_package(
    open_mock, zipfile_mock: zipfile.ZipFile, app_container: ApplicationContainer
):
    workflow_import_service: PackagingImportService = (
        app_container.packaging_import_service()
    )
    data = b"""
    kind: Workflows
    spec:
     human_name: Testing workflow
    """
    archive_read = mock.MagicMock()
    archive_read.read.return_value = data
    zipfile_archive = mock.MagicMock()
    zipfile_archive.namelist.return_value = ["workflow.yaml"]
    zipfile_archive.open.return_value = archive_read
    zipfile_mock.return_value = zipfile_archive

    workflow_import_service.load_workflow_package(data)
    assert workflow_import_service.yaml_data == {
        "kind": "Workflows",
        "spec": {"human_name": "Testing workflow"},
    }
    open_mock.assert_called_once()
    zipfile_archive.open.assert_called_once()
    archive_read.read.assert_called_once()


@mock.patch("zipfile.ZipFile")
@mock.patch("builtins.open")
def test_load_workflow_package_when_invalid(
    open_mock, zipfile_mock: zipfile.ZipFile, app_container: ApplicationContainer
):
    workflow_import_service: PackagingImportService = (
        app_container.packaging_import_service()
    )
    data = bytes(3)
    archive_read = mock.MagicMock()
    archive_read.read.return_value = data
    zipfile_archive = mock.MagicMock()
    zipfile_archive.namelist.return_value = ["workflow.yaml"]
    zipfile_archive.open.return_value = archive_read
    zipfile_mock.return_value = zipfile_archive

    with pytest.raises(WorkflowParsingYamlException):
        workflow_import_service.load_workflow_package(data)


@mock.patch("zipfile.ZipFile")
@mock.patch("builtins.open")
def test_load_workflow_package_when_bad_zip(
    open_mock, zipfile_mock, app_container: ApplicationContainer
):
    workflow_import_service: PackagingImportService = (
        app_container.packaging_import_service()
    )
    data = bytes(3)
    zipfile_mock.side_effect = zipfile.BadZipfile
    with pytest.raises(WorkflowBadZipFile):
        workflow_import_service.load_workflow_package(data)


@mock.patch("zipfile.ZipFile")
@mock.patch("builtins.open")
def test_load_workflow_package_when_yaml_not_found(
    open_mock, zipfile_mock, app_container: ApplicationContainer
):
    workflow_import_service: PackagingImportService = (
        app_container.packaging_import_service()
    )
    data = bytes(3)
    zipfile_mock.namelist.return_value = []
    with pytest.raises(WorkflowYamlNotFound):
        workflow_import_service.load_workflow_package(data)


@pytest.mark.parametrize(
    "description,expected",
    [
        ("Ingest data from the pk5 sensor and push it to an influxdb server.", True),
        (None, True),
    ],
)
def test_check_workflow_schema_validity(
    description, expected, app_container: ApplicationContainer
):
    workflow_import_service: PackagingImportService = (
        app_container.packaging_import_service()
    )
    workflow_import_service.yaml_data = {
        "apiVersion": "ryax.tech/v1",
        "kind": "Workflows",
        "spec": {
            "human_name": "Demo Air Quality",
            "description": description,
            "functions": [
                {
                    "id": "mqttgw",
                    "from": "mqttgw",
                    "position": {"x": 0, "y": 0},
                    "version": "1.0",
                    "inputs_values": {
                        "mqtt_server": "mosquitto.default",
                        "mqtt_topic": "sensorpk5_no2",
                    },
                    "streams_to": ["deserialize"],
                },
                {
                    "id": "deserialize",
                    "from": "deserialize-sensor-pk5",
                    "position": {"x": 1, "y": 1},
                    "version": "1.0",
                    "inputs_values": {"raw": "=mqttgw.value"},
                    "streams_to": ["floattoinfluxdb"],
                },
                {
                    "id": "floattoinfluxdb",
                    "from": "floattoinfluxdb",
                    "position": {"x": 2, "y": 2},
                    "version": "1.0",
                    "inputs_values": {
                        "host": "influxdb-cutable.default",
                        "port": "8086",
                        "user": "root",
                        "password": "root",
                        "dbname": "example",
                        "measurement_name": "sensorpk5_NO2",
                        "time": "=deserialize.time",
                        "value": "=deserialize.value",
                    },
                    "streams_to": [],
                },
            ],
        },
    }
    workflow_import_service.check_workflow_schema_validity()


def test_check_workflow_schema_validity_when_invalid(
    app_container: ApplicationContainer,
):
    workflow_import_service: PackagingImportService = (
        app_container.packaging_import_service()
    )
    workflow_import_service.yaml_data = {
        "name": "workflow",
    }
    with pytest.raises(WorkflowInvalidSchemaException):
        workflow_import_service.check_workflow_schema_validity()


def test_get_actions_references(app_container: ApplicationContainer):
    workflow_import_service: PackagingImportService = (
        app_container.packaging_import_service()
    )
    workflow_import_service.yaml_data = {
        "spec": {
            "functions": [
                {"from": "action1", "version": "1.0"},
                {"from": "action2", "version": "2.0"},
            ]
        },
    }
    assert workflow_import_service.get_actions_references() == [
        ("action1", "1.0"),
        ("action2", "2.0"),
    ]


@mock.patch("zipfile.ZipFile")
def test_get_workflow_io_files(
    zipfile_mock: zipfile.ZipFile, app_container: ApplicationContainer
):
    workflow_import_service: PackagingImportService = (
        app_container.packaging_import_service()
    )

    archive_read = mock.MagicMock()
    archive_read.read.side_effect = [b"content_1", b"content_2"]
    zipfile_mock.open.return_value = archive_read

    zipfile_mock.namelist.return_value = ["file1", "file2"]
    info_1 = zipfile.ZipInfo()
    info_1.file_size = 10
    info_2 = zipfile.ZipInfo()
    info_2.file_size = 20
    zipfile_mock.getinfo.side_effect = [info_1, info_2]

    workflow_import_service.zip_file = zipfile_mock
    assert workflow_import_service.get_workflow_io_files() == [
        {"content": b"content_1", "name": "file1", "size": 10},
        {"content": b"content_2", "name": "file2", "size": 20},
    ]


def test_workflow_schema_property_value(app_container: ApplicationContainer):
    workflow_import_service = app_container.packaging_import_service()
    assert workflow_import_service.schema == workflow_import_service._schema


@mock.patch("uuid.uuid4", mock.MagicMock(return_value="new_guid"))
def test_process(
    app_container: ApplicationContainer,
):
    workflow_yaml_data = {
        "apiVersion": "ryax.tech/v1",
        "kind": "Workflows",
        "spec": {
            "human_name": "Workflow name",
            "description": "Workflow description",
            "functions": [
                {
                    "id": "wf_action_1",
                    "from": "action_1",
                    "version": "1.0",
                    "position": {"x": 0, "y": 0},
                    "inputs_values": {"action_1_in_1": "Form intro"},
                    "outputs": [
                        {
                            "name": "wf_action_1_dyn_output",
                            "human_name": "Wf action 1 Dynamic",
                            "help": "Wf action 1 Dynamic help",
                            "type": "integer",
                            "enum_values": [],
                        }
                    ],
                    "streams_to": ["wf_action_2"],
                },
                {
                    "id": "wf_action_2",
                    "from": "action_2",
                    "version": "1.0",
                    "position": {"x": 1, "y": 0},
                    "inputs_values": {
                        "action_2_in_1": "=wf_action_1.wf_action_1_dyn_output"
                    },
                    "outputs": [],
                    "streams_to": ["wf_action_3"],
                },
                {
                    "id": "wf_action_3",
                    "from": "action_3",
                    "version": "1.0",
                    "position": {"x": 2, "y": 0},
                    "inputs_values": {
                        "action_3_in_1": "static_file_name.mp4",
                        "action_3_in_2": "nonexistent_file.file",
                    },
                    "outputs": [],
                    "streams_to": [],
                },
            ],
        },
    }
    actions = [
        Action(
            id="action_1",
            technical_name="action_1",
            name="action 1",
            description="action 1 Description",
            version="1.0",
            lockfile=b"",
            kind=ActionKind.SOURCE,
            inputs=[
                ActionIO(
                    id="action_1_in_1",
                    technical_name="action_1_in_1",
                    display_name="action 1 In 1",
                    help="action 1 In 1 Help",
                    type=ActionIOType.STRING,
                    optional=False,
                ),
            ],
            outputs=[],
            has_dynamic_outputs=True,
            project="project_id",
            build_date=datetime.fromtimestamp(0),
        ),
        Action(
            id="action_2",
            technical_name="action_2",
            name="action 2",
            description="action 2 Description",
            lockfile=b"",
            version="1.0",
            kind=ActionKind.PROCESSOR,
            inputs=[
                ActionIO(
                    id="action_2_in_1",
                    technical_name="action_2_in_1",
                    display_name="action 2 In 1",
                    help="action 2 In 1 Help",
                    type=ActionIOType.INTEGER,
                    optional=False,
                ),
            ],
            outputs=[
                ActionIO(
                    id="action_2_out_1",
                    technical_name="action_2_out_1",
                    display_name="action 2 Out 1",
                    help="action 2 Out 1 Help",
                    type=ActionIOType.STRING,
                    optional=False,
                ),
            ],
            has_dynamic_outputs=False,
            project="project_id",
            build_date=datetime.fromtimestamp(0),
        ),
        Action(
            id="action_3",
            technical_name="action_3",
            name="action 3",
            description="action 3 Description",
            lockfile=b"",
            version="1.0",
            kind=ActionKind.PUBLISHER,
            inputs=[
                ActionIO(
                    optional=False,
                    id="action_3_in_1",
                    technical_name="action_3_in_1",
                    display_name="action 3 In 1",
                    help="action 3 In 1 Help",
                    type=ActionIOType.FILE,
                ),
                ActionIO(
                    optional=False,
                    id="action_3_in_2",
                    technical_name="action_3_in_2",
                    display_name="action 3 In 2",
                    help="action 3 In 2 Help",
                    type=ActionIOType.FILE,
                ),
            ],
            outputs=[],
            has_dynamic_outputs=False,
            project="project_id",
            build_date=datetime.fromtimestamp(0),
        ),
    ]
    workflow_file_metadata = {
        "static_file_name.mp4": {
            "path": "minio_static_path",
            "size": 10,
        }
    }
    yaml_import_service = app_container.packaging_import_service()
    yaml_import_service.yaml_data = workflow_yaml_data
    result = yaml_import_service.process(
        "owner", actions, workflow_file_metadata, "project"
    )

    expected_workflow_action_1_input_1 = WorkflowActionInput(
        optional=False,
        id="new_guid",
        technical_name="action_1_in_1",
        display_name="action 1 In 1",
        help="action 1 In 1 Help",
        type=ActionIOType.STRING,
        static_value="Form intro",
    )
    expected_workflow_action_1_output_1 = WorkflowActionOutput(
        optional=False,
        id="new_guid",
        technical_name="wf_action_1_dyn_output",
        display_name="Wf action 1 Dynamic",
        help="Wf action 1 Dynamic help",
        type=ActionIOType.INTEGER,
        is_dynamic=True,
    )
    expected_workflow_action_1 = WorkflowAction(
        id="new_guid",
        name="action 1",
        technical_name="action_1",
        description="action 1 Description",
        version="1.0",
        position_x=0,
        position_y=0,
        kind=ActionKind.SOURCE,
        inputs=[expected_workflow_action_1_input_1],
        dynamic_outputs=[expected_workflow_action_1_output_1],
        has_dynamic_outputs=True,
        deployment_status=WorkflowActionDeployStatus.NONE,
        action_id="action_1",
    )

    expected_workflow_action_2_input_1 = WorkflowActionInput(
        optional=False,
        id="new_guid",
        technical_name="action_2_in_1",
        display_name="action 2 In 1",
        help="action 2 In 1 Help",
        type=ActionIOType.INTEGER,
        reference_value=expected_workflow_action_1_output_1,
    )
    expected_workflow_action_2_output_1 = WorkflowActionOutput(
        optional=False,
        id="new_guid",
        technical_name="action_2_out_1",
        display_name="action 2 Out 1",
        help="action 2 Out 1 Help",
        type=ActionIOType.STRING,
    )
    expected_workflow_action_2 = WorkflowAction(
        id="new_guid",
        name="action 2",
        technical_name="action_2",
        description="action 2 Description",
        version="1.0",
        position_x=1,
        position_y=0,
        kind=ActionKind.PROCESSOR,
        inputs=[expected_workflow_action_2_input_1],
        outputs=[expected_workflow_action_2_output_1],
        has_dynamic_outputs=False,
        deployment_status=WorkflowActionDeployStatus.NONE,
        action_id="action_2",
    )

    expected_workflow_action_3_input = WorkflowActionInput(
        optional=False,
        id="new_guid",
        technical_name="action_3_in_1",
        display_name="action 3 In 1",
        help="action 3 In 1 Help",
        type=ActionIOType.FILE,
        file_value=WorkflowFile(
            id="new_guid",
            name="static_file_name",
            extension="mp4",
            size=10,
            path="minio_static_path",
        ),
        position=0,
    )
    expected_workflow_action_3_input_2 = WorkflowActionInput(
        optional=False,
        id="new_guid",
        technical_name="action_3_in_2",
        display_name="action 3 In 2",
        help="action 3 In 2 Help",
        type=ActionIOType.FILE,
        position=1,
    )
    expected_workflow_action_3 = WorkflowAction(
        id="new_guid",
        name="action 3",
        technical_name="action_3",
        description="action 3 Description",
        version="1.0",
        position_x=2,
        position_y=0,
        kind=ActionKind.PUBLISHER,
        inputs=[expected_workflow_action_3_input, expected_workflow_action_3_input_2],
        outputs=[],
        has_dynamic_outputs=False,
        deployment_status=WorkflowActionDeployStatus.NONE,
        action_id="action_3",
    )

    expected_workflow = Workflow(
        id="new_guid",
        name="Workflow name",
        description="Workflow description",
        owner="owner",
        project="project",
        actions=[
            expected_workflow_action_1,
            expected_workflow_action_2,
            expected_workflow_action_3,
        ],
        actions_links=[
            WorkflowActionLink.create_from_workflow_actions(
                upstream_action=expected_workflow_action_1,
                downstream_action=expected_workflow_action_2,
            ),
            WorkflowActionLink.create_from_workflow_actions(
                upstream_action=expected_workflow_action_2,
                downstream_action=expected_workflow_action_3,
            ),
        ],
    )

    assert result == expected_workflow
