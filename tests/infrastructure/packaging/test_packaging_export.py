from unittest import mock

import pytest
import yaml

from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionInput,
)
from ryax.studio.domain.workflow.entities.workflow_action_link import WorkflowActionLink
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.workflow_values import WorkflowValidityStatus
from ryax.studio.infrastructure.packaging.packaging_export import PackagingExportService


@mock.patch("zipfile.ZipFile")
@mock.patch("tempfile.NamedTemporaryFile")
def test_add_file_to_workflow_zip(
    tmp_file_mock, zipfile_mock, app_container: ApplicationContainer
):
    file_data = b"file_content"
    file_name = "file_name"
    type(tmp_file_mock.return_value).name = mock.PropertyMock(return_value="tmp_path")
    workflow_export_service: PackagingExportService = (
        app_container.packaging_export_service()
    )
    workflow_export_service.workflow_zip_file = zipfile_mock
    workflow_export_service.add_file_to_workflow_zip(file_data, file_name)
    zipfile_mock.write.assert_called_once_with("tmp_path", arcname=file_name)


@pytest.mark.parametrize(
    "workflow,expected_yaml",
    [
        (
            Workflow(
                name="Workflow 1",
                description="Workflow description",
                owner="wfowner",
                project="wfproject",
            ),
            yaml.safe_dump(
                {
                    "apiVersion": "ryax.tech/v1",
                    "kind": "Workflows",
                    "spec": {
                        "human_name": "Workflow 1",
                        "description": "Workflow description",
                        "functions": [],
                    },
                }
            ),
        ),
        (
            Workflow(
                name="Workflow 1",
                description="Workflow description",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397c",
                        name="action1",
                        action_id="action_id",
                        technical_name="technical_action1",
                        description="action1 description",
                        version="1.0",
                        kind=ActionKind.SOURCE,
                        position_x=0,
                        position_y=0,
                        inputs=[
                            WorkflowActionInput(
                                optional=False,
                                id="7df4e921-ba92-4cf5-9336-c8d9b4ea397d",
                                technical_name="technicalgwinput1",
                                display_name="displaynamegwinput1",
                                help="a test gateway input",
                                type=ActionIOType.INTEGER,
                                static_value="1",
                            ),
                        ],
                    ),
                    WorkflowAction(
                        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397e",
                        name="action2",
                        action_id="action_id",
                        technical_name="technical_action2",
                        description="action2 description",
                        version="1.0",
                        kind=ActionKind.PROCESSOR,
                        position_x=1,
                        position_y=1,
                        inputs=[
                            WorkflowActionInput(
                                optional=False,
                                id="7df4e921-ba92-4cf5-9336-c8d9b4ea397df",
                                technical_name="action_technical_input",
                                display_name="displaynameactioninput1",
                                help="a test action input",
                                type=ActionIOType.INTEGER,
                                static_value="2",
                            ),
                        ],
                    ),
                ],
                actions_links=[
                    WorkflowActionLink(
                        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397g",
                        upstream_action_id="7df4e921-ba92-4cf5-9336-c8d9b4ea397c",
                        downstream_action_id="7df4e921-ba92-4cf5-9336-c8d9b4ea397e",
                    )
                ],
            ),
            yaml.safe_dump(
                {
                    "apiVersion": "ryax.tech/v1",
                    "kind": "Workflows",
                    "spec": {
                        "human_name": "Workflow 1",
                        "description": "Workflow description",
                        "functions": [
                            {
                                "id": "7df4e921-ba92-4cf5-9336-c8d9b4ea397c",
                                "from": "technical_action1",
                                "position": {
                                    "x": 0,
                                    "y": 0,
                                },
                                "version": "1.0",
                                "inputs_values": {"technicalgwinput1": "1"},
                                "streams_to": ["7df4e921-ba92-4cf5-9336-c8d9b4ea397e"],
                            },
                            {
                                "id": "7df4e921-ba92-4cf5-9336-c8d9b4ea397e",
                                "from": "technical_action2",
                                "position": {
                                    "x": 1,
                                    "y": 1,
                                },
                                "version": "1.0",
                                "inputs_values": {"action_technical_input": "2"},
                                "streams_to": [],
                            },
                        ],
                    },
                }
            ),
        ),
        (
            Workflow(
                name="Workflow 1",
                description="Description",
                owner="wfowner",
                project="wfproject",
                actions=[
                    WorkflowAction(
                        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397c",
                        name="action1",
                        action_id="action_id",
                        technical_name="technical_action1",
                        description="action1 description",
                        version="1.0",
                        kind=ActionKind.SOURCE,
                        position_x=0,
                        position_y=0,
                        inputs=[],
                        outputs=[
                            WorkflowActionOutput(
                                optional=False,
                                id="7df4e921-ba92-4cf5-9336-c8d9b4ea397z",
                                technical_name="Technical_gw_output",
                                display_name="Display gw output",
                                type=ActionIOType.INTEGER,
                                help="help",
                            )
                        ],
                    ),
                    WorkflowAction(
                        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397e",
                        name="action2",
                        action_id="action_id",
                        technical_name="technical_action2",
                        description="action2 description",
                        version="1.0",
                        kind=ActionKind.PROCESSOR,
                        position_x=1,
                        position_y=1,
                        inputs=[
                            WorkflowActionInput(
                                optional=False,
                                id="7df4e921-ba92-4cf5-9336-c8d9b4ea397df",
                                technical_name="action_technical_input",
                                display_name="displaynameactioninput1",
                                help="a test action input",
                                type=ActionIOType.INTEGER,
                                reference_value=WorkflowActionOutput(
                                    optional=False,
                                    id="7df4e921-ba92-4cf5-9336-c8d9b4ea397z",
                                    technical_name="Technical_gw_output",
                                    display_name="Display gw output",
                                    type=ActionIOType.INTEGER,
                                    help="help",
                                ),
                            ),
                        ],
                    ),
                ],
                actions_links=[
                    WorkflowActionLink(
                        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397g",
                        upstream_action_id="7df4e921-ba92-4cf5-9336-c8d9b4ea397c",
                        downstream_action_id="7df4e921-ba92-4cf5-9336-c8d9b4ea397e",
                    )
                ],
            ),
            yaml.safe_dump(
                {
                    "apiVersion": "ryax.tech/v1",
                    "kind": "Workflows",
                    "spec": {
                        "human_name": "Workflow 1",
                        "description": "Description",
                        "functions": [
                            {
                                "id": "7df4e921-ba92-4cf5-9336-c8d9b4ea397c",
                                "from": "technical_action1",
                                "position": {
                                    "x": 0,
                                    "y": 0,
                                },
                                "version": "1.0",
                                "inputs_values": {},
                                "streams_to": ["7df4e921-ba92-4cf5-9336-c8d9b4ea397e"],
                            },
                            {
                                "id": "7df4e921-ba92-4cf5-9336-c8d9b4ea397e",
                                "from": "technical_action2",
                                "position": {
                                    "x": 1,
                                    "y": 1,
                                },
                                "version": "1.0",
                                "inputs_values": {
                                    "action_technical_input": "=7df4e921-ba92-4cf5-9336-c8d9b4ea397c.Technical_gw_output"
                                },
                                "streams_to": [],
                            },
                        ],
                    },
                }
            ),
        ),
    ],
    ids=[
        "export empty workflow",
        "export full workflow",
        "export workflow with reference input",
    ],
)
def test_export_workflow(
    workflow: Workflow, expected_yaml, app_container: ApplicationContainer
):
    all_inputs_files = {}
    workflow_export_service: PackagingExportService = (
        app_container.packaging_export_service()
    )
    workflow_export_service.add_file_to_workflow_zip = mock.MagicMock()
    workflow_export_service.workflow_zip_file = mock.MagicMock()
    workflow_export_service.export_workflow(workflow, all_inputs_files)
    workflow_export_service.add_file_to_workflow_zip.assert_called_once_with(
        expected_yaml.encode(), "workflow.yaml"
    )
    workflow_export_service.workflow_zip_file.close.assert_called_once()


@mock.patch(
    "ryax.studio.infrastructure.packaging.packaging_export.uuid4",
    mock.MagicMock(return_value="random_uuid"),
)
def test_export_workflow_with_file_inputs(app_container: ApplicationContainer):
    action1 = WorkflowAction(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397e",
        name="action2",
        action_id="action_id",
        technical_name="technical_action2",
        description="action2 description",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        position_x=1,
        position_y=1,
        inputs=[
            WorkflowActionInput(
                optional=False,
                id="7df4e921-ba92-4cf5-9336-c8d9b4ea397df",
                technical_name="action_technical_input",
                display_name="displaynameactioninput1",
                help="a test action input",
                type=ActionIOType.INTEGER,
                file_value=WorkflowFile(
                    id="7df4e921-ba92-4cf5-9336-c8d9b4ea397cg",
                    name="workflow_file",
                    extension="txt",
                    size=1.2,
                    path="path/file",
                ),
            ),
        ],
    )
    workflow = Workflow(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        name="Workflow 1",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        validity_status=WorkflowValidityStatus.INVALID,
        actions=[action1],
    )
    all_inputs_files = {"path/file": b"file_content"}
    expected_yaml = yaml.safe_dump(
        {
            "apiVersion": "ryax.tech/v1",
            "kind": "Workflows",
            "spec": {
                "human_name": workflow.name,
                "description": workflow.description,
                "functions": [
                    {
                        "id": action1.id,
                        "from": action1.technical_name,
                        "position": {
                            "x": action1.position_x,
                            "y": action1.position_y,
                        },
                        "version": action1.version,
                        "inputs_values": {
                            "action_technical_input": "random_uuid-workflow_file.txt"
                        },
                        "streams_to": [],
                    },
                ],
            },
        }
    )
    workflow_export_service: PackagingExportService = (
        app_container.packaging_export_service()
    )
    workflow_export_service.add_file_to_workflow_zip = mock.MagicMock()
    workflow_export_service.workflow_zip_file = mock.MagicMock()
    workflow_export_service.export_workflow(workflow, all_inputs_files)
    workflow_export_service.add_file_to_workflow_zip.assert_has_calls(
        [
            mock.call(b"file_content", "random_uuid-workflow_file.txt"),
            mock.call(expected_yaml.encode(), "workflow.yaml"),
        ]
    )
    workflow_export_service.workflow_zip_file.close.assert_called_once()


@mock.patch(
    "ryax.studio.infrastructure.packaging.packaging_export.uuid4",
    mock.MagicMock(return_value="random_uuid"),
)
@mock.patch("zipfile.ZipFile", mock.MagicMock())
def test_generate_workflow_zip_file(app_container: ApplicationContainer):
    workflow_export_service: PackagingExportService = (
        app_container.packaging_export_service()
    )
    assert (
        workflow_export_service.generate_workflow_zip_file()
        == f"{workflow_export_service.tmp_directory_path}/random_uuid-workflow.zip"
    )
