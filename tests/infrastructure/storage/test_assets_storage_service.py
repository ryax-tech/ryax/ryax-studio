import os
from unittest import mock

from ryax.studio.domain.logo.logo import Logo
from ryax.studio.infrastructure.storage.assets_storage_service import (
    AssetsStorageService,
)


@mock.patch("os.path.join", mock.MagicMock(return_value="default_logo_path"))
@mock.patch("builtins.open", mock.mock_open(read_data=b"default_logo_content"))
def test_get_default_logo():
    assets_path = "assets_path"
    storage_service = AssetsStorageService(assets_path=assets_path)
    assert storage_service.get_default_logo() == Logo(
        name="default_logo",
        content=b"default_logo_content",
        id="default_logo",
        extension="png",
        action_id="NONE",
    )
    os.path.join.assert_called_with(assets_path, "default_logo.png")
    open.assert_called_with("default_logo_path", "rb")
