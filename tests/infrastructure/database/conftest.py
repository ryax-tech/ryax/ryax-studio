import json
import os
import pickle
from datetime import datetime
from typing import Any, List

import pytest
from sqlalchemy.engine import Engine, create_engine
from sqlalchemy.orm import clear_mappers, sessionmaker
from sqlalchemy_utils import ScalarListType

from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.domain.logo.logo import Logo
from ryax.studio.domain.workflow.workflow_values import (
    WorkflowDeploymentStatus,
    WorkflowValidityStatus,
)
from ryax.studio.infrastructure.database.engine import Session
from ryax.studio.infrastructure.database.mapper import start_mapping
from ryax.studio.infrastructure.database.metadata import metadata


@pytest.fixture()
def app_container(database_session: Session):
    container = ApplicationContainer()
    return container


@pytest.fixture(scope="function")
def database_engine():
    database_url = os.environ["RYAX_DATASTORE"]
    engine: Engine = create_engine(database_url)
    metadata.create_all(engine)
    start_mapping()
    yield engine
    clear_mappers()
    metadata.drop_all(engine)
    engine.dispose()


@pytest.fixture(scope="function")
def database_session_factory(database_engine: Engine):
    return sessionmaker(bind=database_engine)


@pytest.fixture(scope="function")
def database_session(database_session_factory: Session):
    session = database_session_factory()
    yield session
    session.close()


@pytest.fixture(scope="function")
def generate_logo(database_session: Session):
    def _generate(
        id: None, name: None, action_id: None, extension: None, content: None
    ):
        logo_raw = {
            "id": id,
            "name": name,
            "action_id": action_id,
            "extension": extension,
            "content": content,
        }
        database_session.execute(
            "INSERT INTO logo (id, name, action_id, extension, content)"
            "VALUES (:id, :name, :action_id, :extension,:content)",
            logo_raw,
        )
        return logo_raw

    return _generate


@pytest.fixture(scope="function")
def generate_action(database_session: Session):
    def _generate(
        id: str = None,
        name: str = None,
        technical_name: str = None,
        version: str = None,
        kind: ActionKind = None,
        logo: str = None,
        description: str = None,
        lockfile: bytes = None,
        has_dynamic_outputs: bool = None,
        build_date: datetime = None,
        project: str = None,
        addons: dict = {},
    ):
        action_raw = {
            "id": id,
            "name": name,
            "technical_name": technical_name,
            "version": version,
            "kind": kind.value,
            "logo": logo,
            "description": description,
            "lockfile": lockfile,
            "has_dynamic_outputs": has_dynamic_outputs,
            "build_date": build_date,
            "project": project,
            "addons": json.dumps(addons),
        }
        database_session.execute(
            "INSERT INTO action (id, name, technical_name, version, kind,logo_id, description, lockfile, has_dynamic_outputs, build_date, project, addons)"
            "VALUES (:id, :name, :technical_name, :version, :kind,:logo, :description, :lockfile, :has_dynamic_outputs, :build_date, :project, :addons)",
            action_raw,
        )
        return action_raw

    return _generate


@pytest.fixture(scope="function")
def generate_action_input(database_session: Session):
    def _generate(
        id: str = None,
        display_name: str = None,
        technical_name: str = None,
        help: str = None,
        type: ActionIOType = None,
        enum_values: List[str] = [],
        action_id: str = None,
        optional: bool = False,
    ):
        action_input_raw = {
            "id": id,
            "display_name": display_name,
            "technical_name": technical_name,
            "help": help,
            "type": type.value,
            "enum_values": enum_values,
            "input_of_action_id": action_id,
            "optional": optional,
        }
        database_session.execute(
            "INSERT INTO action_io (id, display_name, technical_name, help, type, enum_values, input_of_action_id, optional)"
            "VALUES (:id, :display_name, :technical_name, :help, :type, :enum_values, :input_of_action_id, :optional)",
            action_input_raw,
        )
        return action_input_raw

    return _generate


@pytest.fixture(scope="function")
def generate_action_output(database_session: Session):
    def _generate(
        id: str = None,
        display_name: str = None,
        technical_name: str = None,
        help: str = None,
        type: ActionIOType = None,
        enum_values: List[str] = [],
        action_id: str = None,
        optional: bool = False,
    ):
        action_output_raw = {
            "id": id,
            "display_name": display_name,
            "technical_name": technical_name,
            "help": help,
            "type": type.value,
            "enum_values": enum_values,
            "output_of_action_id": action_id,
            "optional": optional,
        }
        database_session.execute(
            "INSERT INTO action_io (id, display_name, technical_name, help, type, enum_values, output_of_action_id, optional)"
            "VALUES (:id, :display_name, :technical_name, :help, :type, :enum_values, :output_of_action_id, :optional)",
            action_output_raw,
        )
        return action_output_raw

    return _generate


@pytest.fixture(scope="function")
def generate_workflow_result(database_session: Session):
    def _generate(
        id: str = None,
        workflow_id: str = None,
        key: str = None,
        workflow_action_input_id=None,
        workflow_action_output_id=None,
    ):
        workflow_result_raw = {
            "id": id,
            "workflow_id": workflow_id,
            "key": key,
            "workflow_action_input_id": workflow_action_input_id,
            "workflow_action_output_id": workflow_action_output_id,
        }
        database_session.execute(
            "INSERT INTO workflow_result (id, workflow_id, key, workflow_action_input_id, workflow_action_output_id) "
            "VALUES (:id, :workflow_id, :key, :workflow_action_input_id, :workflow_action_output_id)",
            workflow_result_raw,
        )
        return workflow_result_raw

    return _generate


@pytest.fixture(scope="function")
def generate_workflow(database_session: Session):
    def _generate(
        id: str = None,
        name: str = None,
        description: str = None,
        validity_status: WorkflowValidityStatus = None,
        deployment_status: WorkflowDeploymentStatus = None,
        owner: str = None,
        project: str = None,
    ):
        workflow_raw = {
            "id": id,
            "name": name,
            "description": description,
            "validity_status": validity_status.value,
            "deployment_status": deployment_status.value,
            "owner": owner,
            "project": project,
        }
        database_session.execute(
            "INSERT INTO workflow (id, name, description, validity_status, deployment_status, owner, project) "
            "VALUES (:id, :name, :description,:validity_status, :deployment_status, :owner, :project)",
            workflow_raw,
        )
        return workflow_raw

    return _generate


@pytest.fixture(scope="function")
def generate_workflow_action(database_session: Session):
    def _generate(
        id: str = None,
        name: str = None,
        technical_name: str = None,
        version: str = None,
        kind: str = None,
        description: str = None,
        has_dynamic_outputs: bool = None,
        custom_name: str = None,
        position_x: int = None,
        position_y: int = None,
        action_id: str = None,
        workflow_id: str = None,
        validity_status: str = None,
        deployment_status: str = None,
        endpoint: str = None,
        addons: dict = None,
    ):
        if not addons:
            addons = {}
        workflow_action_raw = {
            "id": id,
            "name": name,
            "technical_name": technical_name,
            "version": version,
            "kind": kind,
            "description": description,
            "has_dynamic_outputs": has_dynamic_outputs,
            "custom_name": custom_name,
            "position_x": position_x,
            "position_y": position_y,
            "action_id": action_id,
            "workflow_id": workflow_id,
            "validity_status": validity_status,
            "deployment_status": deployment_status,
            "endpoint": endpoint,
            "addons": json.dumps(addons),
        }
        database_session.execute(
            "INSERT INTO workflow_action (id, name, technical_name, version, kind, description, has_dynamic_outputs, custom_name, position_x, position_y, action_id, workflow_id, validity_status, deployment_status, endpoint, addons)"
            "VALUES (:id, :name, :technical_name, :version, :kind, :description, :has_dynamic_outputs, :custom_name, :position_x, :position_y, :action_id, :workflow_id, :validity_status, :deployment_status, :endpoint, :addons)",
            workflow_action_raw,
        )
        return workflow_action_raw

    return _generate


@pytest.fixture(scope="function")
def generate_workflow_action_link(database_session: Session):
    def _generate(
        id: str = None,
        upstream_action_id: str = None,
        downstream_action_id: str = None,
        workflow_id: str = None,
    ):
        workflow_action_link_raw = {
            "id": id,
            "upstream_action_id": upstream_action_id,
            "downstream_action_id": downstream_action_id,
            "workflow_id": workflow_id,
        }
        database_session.execute(
            "INSERT INTO workflow_link (id, upstream_action_id, downstream_action_id, workflow_id)"
            "VALUES (:id, :upstream_action_id, :downstream_action_id, :workflow_id)",
            workflow_action_link_raw,
        )
        return workflow_action_link_raw

    return _generate


@pytest.fixture(scope="function")
def generate_workflow_action_error(database_session: Session):
    def _generate(
        id: str = None,
        message: str = None,
        workflow_action_id: str = None,
    ):
        workflow_action_error = {
            "id": id,
            "message": message,
            "workflow_action_id": workflow_action_id,
        }
        database_session.execute(
            "INSERT INTO workflow_error (id, message, workflow_action_id)"
            "VALUES (:id, :message, :workflow_action_id)",
            workflow_action_error,
        )
        return workflow_action_error

    return _generate


@pytest.fixture(scope="function")
def generate_workflow_action_input(database_session: Session) -> object:
    def _generate(
        id: str = None,
        display_name: str = None,
        technical_name: str = None,
        help: str = None,
        type: str = None,
        enum_values=None,
        static_value: str = None,
        workflow_action_output_id: str = None,
        workflow_action_id: str = None,
        workflow_file_id: str = None,
        position: int = 0,
        optional: bool = False,
    ):
        if enum_values is None:
            enum_values = []
        workflow_action_input_raw = {
            "id": id,
            "technical_name": technical_name,
            "display_name": display_name,
            "help": help,
            "type": type,
            "enum_values": ScalarListType().process_bind_param(enum_values, None),
            "static_value": static_value,
            "workflow_action_output_id": workflow_action_output_id,
            "workflow_action_id": workflow_action_id,
            "workflow_file_id": workflow_file_id,
            "position": position,
            "optional": optional,
        }
        database_session.execute(
            "INSERT INTO workflow_action_input (id, display_name, technical_name, help, type, enum_values, static_value, workflow_action_output_id, workflow_action_id, workflow_file_id, position, optional)"
            "VALUES (:id, :display_name, :technical_name, :help, :type, :enum_values, :static_value, :workflow_action_output_id, :workflow_action_id, :workflow_file_id, :position, :optional)",
            workflow_action_input_raw,
        )
        workflow_action_input_raw["enum_values"] = enum_values
        return workflow_action_input_raw

    return _generate


@pytest.fixture(scope="function")
def generate_workflow_action_output(database_session: Session) -> object:
    def _generate(
        id: str = None,
        display_name: str = None,
        technical_name: str = None,
        help: str = None,
        type: str = None,
        enum_values=None,
        workflow_action_id: str = None,
        is_dynamic: bool = False,
        position: int = 0,
        optinal: bool = False,
    ):
        if enum_values is None:
            enum_values = []
        workflow_action_output_raw = {
            "id": id,
            "technical_name": technical_name,
            "display_name": display_name,
            "help": help,
            "type": type,
            "enum_values": ScalarListType().process_bind_param(enum_values, None),
            "workflow_action_id": workflow_action_id,
            "is_dynamic": is_dynamic,
            "position": position,
            "optional": optinal,
        }
        database_session.execute(
            "INSERT INTO workflow_action_output (id, technical_name, display_name, help, type, enum_values, workflow_action_id, is_dynamic, position, optional)"
            "VALUES (:id, :technical_name, :display_name, :help, :type, :enum_values, :workflow_action_id, :is_dynamic, :position, :optional)",
            workflow_action_output_raw,
        )
        workflow_action_output_raw["enum_values"] = enum_values
        return workflow_action_output_raw

    return _generate


@pytest.fixture(scope="function")
def generate_workflow_error(database_session: Session):
    def _generate(
        id: str = None,
        code: int = None,
        workflow_id: str = None,
        workflow_action_id: str = None,
    ):
        workflow_error_raw = {
            "id": id,
            "code": code,
            "workflow_id": workflow_id,
            "workflow_action_id": workflow_action_id,
        }
        database_session.execute(
            "INSERT INTO workflow_error (id, code, workflow_id, workflow_action_id)"
            "VALUES (:id, :code, :workflow_id, :workflow_action_id)",
            workflow_error_raw,
        )
        return workflow_error_raw

    return _generate


@pytest.fixture(scope="function")
def generate_workflow_file(database_session: Session):
    def _generate(
        id: str = None,
        name: str = None,
        extension: str = None,
        path: str = None,
        size: float = None,
    ):
        workflow_file_raw = {
            "id": id,
            "name": name,
            "extension": extension,
            "path": path,
            "size": size,
        }
        database_session.execute(
            "INSERT INTO workflow_file (id, name, extension, path, size)"
            "VALUES (:id, :name, :extension, :path, :size)",
            workflow_file_raw,
        )
        return workflow_file_raw

    return _generate


@pytest.fixture(scope="function")
def generate_action_category(database_session: Session):
    def _generate(id: str = None, name: str = None):
        action_category_raw = {"id": id, "name": name}
        database_session.execute(
            "INSERT INTO action_category (id, name)" "VALUES (:id, :name)",
            action_category_raw,
        )
        return action_category_raw

    return _generate


@pytest.fixture(scope="function")
def generate_workflow_action_category(database_session: Session):
    def _generate(id: str = None, name: str = None, workflow_action_id: str = None):
        workflow_action_category_raw = {
            "id": id,
            "name": name,
            "workflow_action_id": workflow_action_id,
        }
        database_session.execute(
            "INSERT INTO workflow_action_category (id, name, workflow_action_id)"
            "VALUES (:id, :name, :workflow_action_id)",
            workflow_action_category_raw,
        )
        return workflow_action_category_raw

    return _generate


@pytest.fixture(scope="function")
def generate_action_category_association(database_session: Session):
    def _generate(action_id: str, action_category_id: str):
        association = {
            "action_id": action_id,
            "action_category_id": action_category_id,
        }
        database_session.execute(
            "INSERT INTO action_categories_association (action_category_id, action_id )"
            "VALUES (:action_category_id, :action_id )",
            association,
        )
        return association

    return _generate


@pytest.fixture(scope="function")
def generate_workflow_action_category_association(database_session: Session):
    def _generate(workflow_action_id: str, workflow_action_category_id: str):
        association = {
            "workflow_action_id": workflow_action_id,
            "workflow_action_category_id": workflow_action_category_id,
        }
        database_session.execute(
            "INSERT INTO workflow_action_categories_association (workflow_action_id, workflow_action_category_id )"
            "VALUES (:workflow_action_id, :workflow_action_category_id )",
            association,
        )
        return association

    return _generate


@pytest.fixture(scope="function")
def generate_project_variable_raw(database_session: Session):
    def _generate(
        id: str,
        name: str,
        value: Any,
        type: ActionIOType,
        project_id: str,
        description: str,
    ):
        project_variable_raw = {
            "id": id,
            "name": name,
            "value": pickle.dumps(value),
            "type": type.value,
            "project_id": project_id,
            "description": description,
        }
        database_session.execute(
            "INSERT INTO project_variable (id, name, value, type, project_id, description)"
            "VALUES (:id, :name, :value, :type, :project_id, :description)",
            project_variable_raw,
        )
        return project_variable_raw

    return _generate


# Old fixtures with static data
@pytest.fixture(scope="function")
def action_input_raw(action_raw: dict, database_session: Session):
    """Add a workflow data in database"""
    action_input_raw = {
        "id": "608f5b39-f383-40f7-8c1b-cac79e7324f0",
        "display_name": "action Input",
        "technical_name": "action-input",
        "help": "action input help",
        "type": "string",
        "enum_values": "enum1,enum2",
        "input_of_action_id": action_raw["id"],
    }
    database_session.execute(
        "INSERT INTO action_io (id, display_name, technical_name, help, type, enum_values, input_of_action_id)"
        "VALUES (:id, :display_name, :technical_name, :help, :type, :enum_values, :input_of_action_id)",
        action_input_raw,
    )
    return action_input_raw


@pytest.fixture(scope="function")
def action_output_raw(action_raw: dict, database_session: Session):
    action_output_raw = {
        "id": "608f5b39-f383-40f7-8c1b-cac79e7324d5",
        "display_name": "action Output",
        "technical_name": "action-output",
        "help": "action output help",
        "type": "string",
        "enum_values": "enum1,enum2",
        "output_of_action_id": action_raw["id"],
    }
    database_session.execute(
        "INSERT INTO action_io (id, display_name, technical_name, help, type, enum_values, output_of_action_id)"
        "VALUES (:id, :display_name, :technical_name, :help, :type, :enum_values, :output_of_action_id)",
        action_output_raw,
    )
    return action_output_raw


@pytest.fixture(scope="function")
def action_raw_2(database_session: Session):
    """Add a workflow data in database"""
    logo = Logo(
        id="608f5b39-f383-40f7-8c1b-cac79e7324f1",
        name="logoname",
        action_id="action_id1",
        extension="png",
        content=b"content",
    )
    action_raw = {
        "id": "10e1235c-7904-4ae6-b06b-3d4a4df7b017",
        "name": "action 2",
        "version": "2.0",
        "logo": logo,
        "kind": "Processor",
        "description": "action description",
        "has_dynamic_outputs": False,
    }
    database_session.execute(
        "INSERT INTO action (id, name, version, kind, logo_id, description, has_dynamic_outputs)"
        "VALUES (:id, :name, :version, :kind,:logo.id, :description, :has_dynamic_outputs)",
        action_raw,
    )
    return action_raw


@pytest.fixture(scope="function")
def logo_raw(database_session: Session):
    logo_raw = {
        "id": "608f5b39-f383-40f7-8c1b-cac79e7324f2",
        "name": "logo_raw_1",
        "action_id": "10e1235c-7904-4ae6-b06b-3d4a4df7b017",
        "extension": "png",
        "content": b"123",
    }
    database_session.execute(
        "INSERT INTO logo (id, name, action_id, extension, content)"
        "VALUES (:id, :name, :action_id, :extension, :content)",
        logo_raw,
    )
    return logo_raw


@pytest.fixture(scope="function")
def workflow_raw(database_session: Session):
    workflow_raw = {
        "id": "608f5b39-f383-40f7-8c1b-cac79e7324f1",
        "name": "Workflow 1",
        "description": "Workflow description",
        "validity_status": "valid",
        "deployment_status": "None",
        "owner": "user",
        "project": "d5dd57ae-1953-4eef-b658-72465a1a03b6",
    }
    database_session.execute(
        "INSERT INTO workflow (id, name, description, validity_status, deployment_status, owner, project) "
        "VALUES (:id, :name, :description,:validity_status, :deployment_status, :owner, :project)",
        workflow_raw,
    )
    return workflow_raw


@pytest.fixture(scope="function")
def workflow_error_1_raw(database_session: Session, workflow_raw: dict):
    """Add a workflow data in database"""
    workflow_error_1_raw = {
        "id": "608f5b60-f383-40f7-8c1b-cac79e7355f1",
        "message": "Workflow error message",
        "workflow_id": workflow_raw["id"],
        "workflow_action_id": None,
    }
    database_session.execute(
        "INSERT INTO workflow_error (id, message, workflow_id, workflow_action_id) "
        "VALUES (:id, :message, :workflow_id, :workflow_action_id)",
        workflow_error_1_raw,
    )
    return workflow_error_1_raw


@pytest.fixture(scope="function")
def workflow_action_raw(
    action_raw: dict, workflow_raw: dict, database_session: Session
):
    """Add a workflow action data in database"""
    workflow_action_raw = {
        "id": "608f5b39-f383-40f7-8c1b-cac79e7324f2",
        "custom_name": "Workflow action 1",
        "position_x": 0,
        "position_y": 2,
        "action_id": action_raw["id"],
        "workflow_id": workflow_raw["id"],
    }
    database_session.execute(
        "INSERT INTO workflow_action (id, custom_name, position_x, position_y, action_id, workflow_id)"
        "VALUES (:id, :custom_name, :position_x, :position_y, :action_id, :workflow_id)",
        workflow_action_raw,
    )
    return workflow_action_raw


@pytest.fixture(scope="function")
def workflow_action_error_raw(
    database_session: Session, workflow_raw: dict, workflow_action_raw: dict
):
    """Add a workflow data in database"""
    workflow_action_error_raw = {
        "id": "608f5b39-f383-40f7-8c1b-cac79e7355f1",
        "message": "Workflow error message",
        "workflow_id": workflow_raw["id"],
        "workflow_action_id": workflow_action_raw["id"],
    }
    database_session.execute(
        "INSERT INTO workflow_error (id, message, workflow_id, workflow_action_id) "
        "VALUES (:id, :message, :workflow_id, :workflow_action_id)",
        workflow_action_error_raw,
    )
    return workflow_action_error_raw


@pytest.fixture(scope="function")
def workflow_action_raw_2(
    action_raw_2: dict, workflow_raw: dict, database_session: Session
):
    """Add a workflow action data in database"""
    workflow_action_raw = {
        "id": "4d3ec00d-eb9e-41c2-ae2b-3ef520fcc307",
        "custom_name": "Workflow action 2",
        "position_x": 1,
        "position_y": 1,
        "action_id": action_raw_2["id"],
        "workflow_id": workflow_raw["id"],
    }
    database_session.execute(
        "INSERT INTO workflow_action (id, custom_name, position_x, position_y, action_id, workflow_id)"
        "VALUES (:id, :custom_name, :position_x, :position_y, :action_id, :workflow_id)",
        workflow_action_raw,
    )
    return workflow_action_raw


@pytest.fixture(scope="function")
def workflow_action_link_raw(
    workflow_raw: dict,
    workflow_action_raw: dict,
    workflow_action_raw_2: dict,
    database_session: Session,
):
    """Add a workflow action link data in database"""
    workflow_action_link_raw = {
        "id": "608f5b39-f383-40f7-8c1b-cac79e7444f4",
        "input_action_id": workflow_action_raw["id"],
        "output_action_id": workflow_action_raw_2["id"],
        "workflow_id": workflow_raw["id"],
    }
    database_session.execute(
        "INSERT INTO workflow_link (id, input_action_id, output_action_id, workflow_id)"
        "VALUES (:id, :input_action_id, :output_action_id, :workflow_id)",
        workflow_action_link_raw,
    )
    return workflow_action_link_raw


@pytest.fixture(scope="function")
def workflow_action_input_raw(
    action_input_raw: dict,
    workflow_action_raw: dict,
    workflow_action_static_output_raw: dict,
    workflow_action_dynamic_output_raw: dict,
    database_session: Session,
):
    workflow_action_input_raw = {
        "id": "608f5b39-f383-40f7-8c1b-cac79e7324f2",
        "static_value": "workflow_action_input_value",
        "static_reference_value": workflow_action_static_output_raw["id"],
        "dynamic_reference_value": workflow_action_dynamic_output_raw["id"],
        "action_io_id": action_input_raw["id"],
        "workflow_action_id": workflow_action_raw["id"],
    }
    database_session.execute(
        "INSERT INTO workflow_action_input (id, static_value, static_reference_value, dynamic_reference_value, action_io_id, workflow_action_id)"
        "VALUES (:id, :static_value, :static_reference_value, :dynamic_reference_value, :action_io_id, :workflow_action_id)",
        workflow_action_input_raw,
    )
    return workflow_action_input_raw
