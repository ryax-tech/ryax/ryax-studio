from unittest import mock

from sqlalchemy.orm import Session

from ryax.studio.container import ApplicationContainer
from ryax.studio.infrastructure.database.engine import DatabaseEngine
from ryax.studio.infrastructure.database.unit_of_work import DatabaseUnitOfWork


def test_open(app_container: ApplicationContainer):
    """When starting workflow unit should work"""
    database_engine_mock = mock.MagicMock(DatabaseEngine)
    database_session_mock = mock.MagicMock(Session)
    database_engine_mock.get_session.return_value = database_session_mock
    app_container.database_engine.override(database_engine_mock)
    workflow_unit: DatabaseUnitOfWork = app_container.unit_of_work()
    workflow_unit.__enter__()
    assert workflow_unit.session == database_session_mock
    assert workflow_unit.workflows.session == database_session_mock
    assert workflow_unit.actions.session == database_session_mock


def test_close(app_container: ApplicationContainer):
    """When closing workflow unit should work"""
    database_session_mock = mock.MagicMock(Session)
    workflow_unit: DatabaseUnitOfWork = app_container.unit_of_work()
    workflow_unit.session = database_session_mock
    workflow_unit.__exit__(None, None, None)
    database_session_mock.close.assert_called()


def test_commit(app_container: ApplicationContainer):
    """When commit workflow unit should work"""
    database_session_mock = mock.MagicMock(Session)
    workflow_unit: DatabaseUnitOfWork = app_container.unit_of_work()
    workflow_unit.session = database_session_mock
    workflow_unit.commit()
    database_session_mock.commit.assert_called()


def test_rollback(app_container: ApplicationContainer):
    """When rollback workflow unit should work"""
    database_session_mock = mock.MagicMock(Session)
    workflow_unit: DatabaseUnitOfWork = app_container.unit_of_work()
    workflow_unit.session = database_session_mock
    workflow_unit.rollback()
    database_session_mock.rollback.assert_called()
