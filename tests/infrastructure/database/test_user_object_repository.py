from typing import Callable

import pytest

from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.domain.project_variables.project_variable_entities import (
    ProjectVariable,
)
from ryax.studio.domain.project_variables.project_variable_exceptions import (
    ProjectVariableNotFoundException,
)
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionInput,
)
from ryax.studio.infrastructure.database.engine import Session
from ryax.studio.infrastructure.database.repositories.project_variable_repository import (
    DatabaseUserObjectRepository,
)


def test_list_project_variables(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_project_variable_raw: Callable,
):
    user_obj1 = ProjectVariable(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="obj_name",
        value=123,
        type=ActionIOType.INTEGER,
        project_id="b262b130-caf0-43b6-b495-4cc21201321b",
        description="description",
    )
    generate_project_variable_raw(
        id=user_obj1.id,
        name=user_obj1.name,
        value=user_obj1.value,
        type=user_obj1.type,
        project_id=user_obj1.project_id,
        description=user_obj1.description,
    )
    user_obj2 = ProjectVariable(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef563",
        name="obj_name2",
        value="456",
        type=ActionIOType.STRING,
        project_id=user_obj1.project_id,
        description="description2",
    )
    generate_project_variable_raw(
        id=user_obj2.id,
        name=user_obj2.name,
        value=user_obj2.value,
        type=user_obj2.type,
        project_id=user_obj2.project_id,
        description=user_obj2.description,
    )

    project_variable_repository: DatabaseUserObjectRepository = (
        app_container.project_variable_repository(session=database_session)
    )

    result = project_variable_repository.list(user_obj1.project_id, type_search=None)
    assert result == [user_obj1, user_obj2]


def test_list_project_variables_with_type_search(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_project_variable_raw: Callable,
):
    type_search: str = ActionIOType.INTEGER.value

    user_obj1 = ProjectVariable(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="obj_name",
        value=123,
        type=ActionIOType.INTEGER,
        project_id="b262b130-caf0-43b6-b495-4cc21201321b",
        description="description",
    )
    generate_project_variable_raw(
        id=user_obj1.id,
        name=user_obj1.name,
        value=user_obj1.value,
        type=user_obj1.type,
        project_id=user_obj1.project_id,
        description=user_obj1.description,
    )
    user_obj2 = ProjectVariable(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef563",
        name="obj_name2",
        value="456",
        type=ActionIOType.STRING,
        project_id=user_obj1.project_id,
        description="description2",
    )
    generate_project_variable_raw(
        id=user_obj2.id,
        name=user_obj2.name,
        value=user_obj2.value,
        type=user_obj2.type,
        project_id=user_obj2.project_id,
        description=user_obj2.description,
    )

    project_variable_repository: DatabaseUserObjectRepository = (
        app_container.project_variable_repository(session=database_session)
    )

    result = project_variable_repository.list(
        user_obj1.project_id, type_search=type_search
    )
    assert result == [user_obj1]


def test_get_project_variable(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_project_variable_raw: Callable,
):
    user_obj1 = ProjectVariable(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="obj_name",
        value=123,
        type=ActionIOType.INTEGER,
        project_id="b262b130-caf0-43b6-b495-4cc21201321b",
        description="description",
    )
    generate_project_variable_raw(
        id=user_obj1.id,
        name=user_obj1.name,
        value=user_obj1.value,
        type=user_obj1.type,
        project_id=user_obj1.project_id,
        description=user_obj1.description,
    )

    project_variable_repository: DatabaseUserObjectRepository = (
        app_container.project_variable_repository(session=database_session)
    )

    result = project_variable_repository.get(user_obj1.id, user_obj1.project_id)
    assert result == user_obj1


def test_get_project_variable_when_no_result(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_project_variable_raw: Callable,
):
    id_to_use = "foo"
    project_id = "project_id"

    project_variable_repository: DatabaseUserObjectRepository = (
        app_container.project_variable_repository(session=database_session)
    )
    with pytest.raises(ProjectVariableNotFoundException):
        project_variable_repository.get(id_to_use, project_id)


def test_get_linked_workflows(
    app_container: ApplicationContainer,
    database_session: Session,
):
    project_variable_1 = ProjectVariable(
        id="1",
        description="",
        name="",
        value="21",
        project_id="12",
        file_path=None,
        type=ActionIOType.INTEGER,
    )
    workflow_action_1 = WorkflowAction(
        id="workflow_action_1",
        name="a1",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
        inputs=[
            WorkflowActionInput(
                id="wfmin1",
                type=ActionIOType.INTEGER,
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                project_variable_value=project_variable_1,
                optional=False,
            )
        ],
    )
    workflow_action_2 = WorkflowAction(
        id="workflow_action_2",
        name="a2",
        technical_name="wfmtname",
        description="wfmdescr",
        version="wfmver",
        kind=ActionKind.PROCESSOR,
        position_x=2,
        position_y=2,
        action_id="action_id",
    )
    workflow1 = Workflow(
        id="1",
        name="wf1",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action_1, workflow_action_2],
    )

    workflow2 = Workflow(
        id="2",
        name="wf2",
        description="wfdescr",
        owner="wfowner",
        project="wfproject",
        actions=[workflow_action_2],
    )

    database_session.add(workflow1)
    database_session.add(workflow2)
    database_session.commit()

    project_variable_repository: DatabaseUserObjectRepository = (
        app_container.project_variable_repository(session=database_session)
    )
    assert [workflow1] == project_variable_repository.get_linked_workflows(
        project_variable_1
    )
