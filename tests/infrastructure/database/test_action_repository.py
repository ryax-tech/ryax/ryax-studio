from datetime import datetime
from operator import attrgetter
from typing import Callable

import pytest

from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_category import ActionCategory
from ryax.studio.domain.action.action_exceptions import ActionNotFoundException
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.domain.action.views.action import ActionView
from ryax.studio.infrastructure.database.engine import Session
from ryax.studio.infrastructure.database.repositories.action_repository import (
    DatabaseActionRepository,
)


def test_get_actions_views(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
):
    """List actions should return actions from database"""
    action_raw: dict = generate_action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        lockfile=b"",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    assert action_repository.get_actions_views(
        "b262b130-caf0-43b6-b495-4cc21201321b"
    ) == [
        ActionView(
            id=action_raw["id"],
            name=action_raw["name"],
            version=action_raw["version"],
            lockfile=action_raw["lockfile"],
            kind=ActionKind(action_raw["kind"]),
            description=action_raw["description"],
        )
    ]


def test_get_actions_views_when_multiple_projects(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
):
    """List actions should return actions from database"""
    action_raw: dict = generate_action(
        id="7e47bd04-bb8d-448b-94c7-d63d6763d3b2",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    _: dict = generate_action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="action 2",
        technical_name="action technical name 2",
        description="action description 2",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="91ab3fad-6d6f-4f40-ba7f-2fb7cdaf56a2",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    assert action_repository.get_actions_views(
        "b262b130-caf0-43b6-b495-4cc21201321b"
    ) == [
        ActionView(
            id=action_raw["id"],
            name=action_raw["name"],
            version=action_raw["version"],
            lockfile=action_raw["lockfile"],
            kind=ActionKind(action_raw["kind"]),
            description=action_raw["description"],
        )
    ]


def test_get_actions_when_multiple_versions_and_take_most_recent(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
):
    """List actions with search should return matching ones from db"""
    expected_action = generate_action(
        id="09e31c75-4cc0-41d7-becb-01dfe3d7d8e6",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        lockfile=b"",
        version="2.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    generate_action(
        id="51252fca-c479-485a-891d-f71677eb84c0",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime(2003, 5, 12, 19, 19, 6, 988946),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    expected_action_2 = generate_action(
        id="d63244ed-09ec-4eff-ae55-e753bcd1f044",
        name="action 2",
        technical_name="action 2",
        description="action 2 technical name",
        lockfile=b"",
        version="3.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    assert action_repository.get_actions_views(
        "b262b130-caf0-43b6-b495-4cc21201321b"
    ) == [
        ActionView(
            id=expected_action["id"],
            name=expected_action["name"],
            version=expected_action["version"],
            lockfile=expected_action["lockfile"],
            kind=ActionKind.PROCESSOR,
            description=expected_action["description"],
        ),
        ActionView(
            id=expected_action_2["id"],
            name=expected_action_2["name"],
            lockfile=expected_action_2["lockfile"],
            version=expected_action_2["version"],
            kind=ActionKind.PROCESSOR,
            description=expected_action_2["description"],
        ),
    ]


def test_get_actions_with_search(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
):
    """List actions with search should return matching ones from db"""
    search = "test"
    generate_action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        lockfile=b"",
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    assert (
        action_repository.get_actions_views(
            "b262b130-caf0-43b6-b495-4cc21201321b", search=search
        )
        == []
    )


def test_get_actions_with_search_when_valid_search_and_take_most_recent(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
):
    """List actions with search should return matching ones from db"""
    search = "action"
    expected_action = generate_action(
        id="09e31c75-4cc0-41d7-becb-01dfe3d7d8e6",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        lockfile=b"",
        version="2.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    generate_action(
        id="51252fca-c479-485a-891d-f71677eb84c0",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime(2003, 5, 12, 19, 19, 6, 988946),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    expected_action_2 = generate_action(
        id="1af8b9c2-b486-4e3d-9d0b-39b1fc80c47c",
        name="action 2",
        technical_name="action tecnical_name 2",
        lockfile=b"",
        description="action description 2",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime(2003, 5, 12, 11, 19, 6, 988946),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    generate_action(
        id="d63244ed-09ec-4eff-ae55-e753bcd1f044",
        name="Unwanted",
        technical_name="Unwanted",
        description="Unwanted",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime(2002, 5, 12, 19, 19, 6, 988946),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )

    expected_result_items = [
        ActionView(
            id=expected_action["id"],
            name=expected_action["name"],
            version=expected_action["version"],
            kind=ActionKind.PROCESSOR,
            description=expected_action["description"],
            lockfile=expected_action["lockfile"],
        ),
        ActionView(
            id=expected_action_2["id"],
            name=expected_action_2["name"],
            version=expected_action_2["version"],
            kind=ActionKind.PROCESSOR,
            description=expected_action_2["description"],
            lockfile=expected_action["lockfile"],
        ),
    ]
    result = action_repository.get_actions_views(
        "b262b130-caf0-43b6-b495-4cc21201321b", search=search
    )
    assert all(item in result for item in expected_result_items)
    assert len(expected_result_items) == len(result)


def test_get_actions_with_search_when_valid_search_and_no_result(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
):
    """List actions with search should return matching ones from db"""
    search = "action"
    generate_action(
        id="09e31c75-4cc0-41d7-becb-01dfe3d7d8e6",
        name="Unwanted",
        technical_name="Unwanted",
        description="Unwanted",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
        lockfile=b"lockfile",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    assert (
        action_repository.get_actions_views(
            "b262b130-caf0-43b6-b495-4cc21201321b", search=search
        )
        == []
    )


def test_get_actions_views_with_category_filter(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
    generate_action_category: Callable,
    generate_action_category_association: Callable,
):
    """List actions should return actions from database"""
    action_raw_1: dict = generate_action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
        lockfile=b"",
    )
    action_raw_2: dict = generate_action(
        id="06d748af-6118-4fac-844d-3931b80aa79c",
        name="action 2",
        technical_name="action technical name 2",
        description="action description 2",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        lockfile=b"",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    action_raw_3: dict = generate_action(
        id="2460fe4b-c996-4c79-9596-e5c8b3ded6ec",
        name="action 3",
        technical_name="action technical name 3",
        description="action description 3",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        lockfile=b"",
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    action_category_raw_1: dict = generate_action_category(
        id="8dcd2029-5ad6-4d9e-a6a1-7caea2946d4c",
        name="category_1",
    )
    action_category_raw_2: dict = generate_action_category(
        id="529f2c0b-6944-447e-9623-e355c556e99a",
        name="category_2",
    )
    action_category_raw_3: dict = generate_action_category(
        id="4a9bd3c9-bc7f-4b6e-8149-5e8f60e21993",
        name="category_3",
    )
    generate_action_category_association(
        action_raw_1["id"], action_category_raw_1["id"]
    )
    generate_action_category_association(
        action_raw_1["id"], action_category_raw_2["id"]
    )
    generate_action_category_association(
        action_raw_2["id"], action_category_raw_2["id"]
    )
    generate_action_category_association(
        action_raw_3["id"], action_category_raw_3["id"]
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    assert action_repository.get_actions_views(
        "b262b130-caf0-43b6-b495-4cc21201321b", category="category_1"
    ) == [
        ActionView(
            id=action_raw_1["id"],
            name=action_raw_1["name"],
            version=action_raw_1["version"],
            lockfile=action_raw_1["lockfile"],
            kind=ActionKind(action_raw_1["kind"]),
            description=action_raw_1["description"],
        )
    ]
    assert action_repository.get_actions_views(
        "b262b130-caf0-43b6-b495-4cc21201321b", category="category_2"
    ) == [
        ActionView(
            id=action_raw_1["id"],
            name=action_raw_1["name"],
            version=action_raw_1["version"],
            lockfile=action_raw_1["lockfile"],
            kind=ActionKind(action_raw_1["kind"]),
            description=action_raw_1["description"],
        ),
        ActionView(
            id=action_raw_2["id"],
            name=action_raw_2["name"],
            lockfile=action_raw_2["lockfile"],
            version=action_raw_2["version"],
            kind=ActionKind(action_raw_2["kind"]),
            description=action_raw_2["description"],
        ),
    ]
    assert action_repository.get_actions_views(
        "b262b130-caf0-43b6-b495-4cc21201321b", category="category_3"
    ) == [
        ActionView(
            id=action_raw_3["id"],
            name=action_raw_3["name"],
            version=action_raw_3["version"],
            lockfile=action_raw_3["lockfile"],
            kind=ActionKind(action_raw_3["kind"]),
            description=action_raw_3["description"],
        )
    ]


def test_get_actions_views_with_category_filter_and_search(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
    generate_action_category: Callable,
    generate_action_category_association: Callable,
):
    """List actions should return actions from database"""
    action_raw_1: dict = generate_action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    action_raw_2: dict = generate_action(
        id="06d748af-6118-4fac-844d-3931b80aa79c",
        name="action 2",
        technical_name="action technical name 2",
        description="action description 2",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    action_category_raw_1: dict = generate_action_category(
        id="8dcd2029-5ad6-4d9e-a6a1-7caea2946d4c",
        name="category_1",
    )
    action_category_raw_2: dict = generate_action_category(
        id="529f2c0b-6944-447e-9623-e355c556e99a",
        name="category_2",
    )
    generate_action_category_association(
        action_raw_1["id"], action_category_raw_1["id"]
    )
    generate_action_category_association(
        action_raw_1["id"], action_category_raw_2["id"]
    )
    generate_action_category_association(
        action_raw_2["id"], action_category_raw_2["id"]
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    assert action_repository.get_actions_views(
        "b262b130-caf0-43b6-b495-4cc21201321b", search="action 1", category="category_1"
    ) == [
        ActionView(
            id=action_raw_1["id"],
            name=action_raw_1["name"],
            version=action_raw_1["version"],
            lockfile=action_raw_1["lockfile"],
            kind=ActionKind(action_raw_1["kind"]),
            description=action_raw_1["description"],
        )
    ]
    assert (
        action_repository.get_actions_views(
            "b262b130-caf0-43b6-b495-4cc21201321b",
            search="action 2",
            category="category_1",
        )
        == []
    )
    assert action_repository.get_actions_views(
        "b262b130-caf0-43b6-b495-4cc21201321b", search="action 2", category="category_2"
    ) == [
        ActionView(
            id=action_raw_2["id"],
            name=action_raw_2["name"],
            version=action_raw_2["version"],
            lockfile=action_raw_2["lockfile"],
            kind=ActionKind(action_raw_2["kind"]),
            description=action_raw_2["description"],
        )
    ]


def test_get_action(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
):
    """Get action by id should return action when exists"""
    action_raw = generate_action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    result = action_repository.get_action(action_raw["id"])
    assert result.id == action_raw["id"]


def test_get_action_when_not_exists(
    app_container: ApplicationContainer, database_session: Session
):
    """Get action by id should return None when not exists"""
    action_id = "608f5b39-f383-40f7-8c1b-cac79e7324f0"
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    with pytest.raises(ActionNotFoundException):
        action_repository.get_action(action_id)


def test_get_action_versions_when_one_action(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
):
    action_data = {
        "id": "c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        "name": "action 1",
        "technical_name": "action technical name",
        "description": "action description",
        "version": "1.0",
        "kind": ActionKind.PROCESSOR,
        "has_dynamic_outputs": True,
        "lockfile": b"",
        "build_date": datetime.utcnow(),
        "project": "b262b130-caf0-43b6-b495-4cc21201321b",
        "addons": {},
    }
    action_raw = generate_action(**action_data)
    expected_result = [Action(**action_data)]
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    result = action_repository.get_action_versions(action_raw["id"])
    assert result == expected_result


def test_get_action_versions_when_multiple_actions(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
):
    action_data_1 = {
        "id": "c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        "name": "action 1",
        "technical_name": "action technical name is same",
        "description": "action description",
        "lockfile": b"",
        "version": "1.0",
        "kind": ActionKind.PROCESSOR,
        "has_dynamic_outputs": True,
        "build_date": datetime.utcnow(),
        "project": "b262b130-caf0-43b6-b495-4cc21201321b",
        "addons": {},
    }
    action_data_2 = {
        "id": "c50f8fae-e116-4db0-8c7b-d6a790a48ef3",
        "name": "action 2",
        "technical_name": "action technical name is same",
        "description": "action description",
        "lockfile": b"",
        "version": "2.0",
        "kind": ActionKind.PROCESSOR,
        "has_dynamic_outputs": True,
        "build_date": datetime.utcnow(),
        "project": "b262b130-caf0-43b6-b495-4cc21201321b",
        "addons": {},
    }
    action_data_3 = {
        "id": "975660e8-5471-413f-85c2-d91fce375dbb",
        "name": "action 3",
        "technical_name": "action technical name is same",
        "description": "action description",
        "lockfile": b"",
        "version": "3.0",
        "kind": ActionKind.PROCESSOR,
        "has_dynamic_outputs": True,
        "build_date": datetime.utcnow(),
        "project": "b262b130-caf0-43b6-b495-4cc21201321b",
        "addons": {},
    }
    action_data_4 = {
        "id": "9427f53a-94d9-4457-bdae-31dd22f9c63c",
        "name": "action 4",
        "technical_name": "action technical name is same",
        "description": "action description",
        "lockfile": b"",
        "version": "4.0",
        "kind": ActionKind.PROCESSOR,
        "has_dynamic_outputs": True,
        "build_date": datetime.utcnow(),
        "project": "b262b130-caf0-43b6-b495-4cc21201321b",
        "addons": {},
    }
    action_raw_1 = generate_action(**action_data_1)
    generate_action(**action_data_2)
    generate_action(**action_data_3)
    generate_action(**action_data_4)
    expected_result = [
        Action(**action_data_4),
        Action(**action_data_3),
        Action(**action_data_2),
        Action(**action_data_1),
    ]
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    result = action_repository.get_action_versions(action_raw_1["id"])
    assert sorted(result, key=attrgetter("id")) == sorted(
        expected_result, key=attrgetter("id")
    )


def test_get_action_versions_when_multiple_actions_and_unwanted_action(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
):
    action_data_1 = {
        "id": "c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        "name": "action 1",
        "technical_name": "action technical name is same",
        "description": "action description",
        "lockfile": b"",
        "version": "1.0",
        "kind": ActionKind.PROCESSOR,
        "has_dynamic_outputs": True,
        "build_date": datetime.utcnow(),
        "project": "b262b130-caf0-43b6-b495-4cc21201321b",
        "addons": {},
    }
    action_data_2 = {
        "id": "c50f8fae-e116-4db0-8c7b-d6a790a48ef3",
        "name": "action 2",
        "technical_name": "action technical name is same",
        "description": "action description",
        "lockfile": b"",
        "version": "2.0",
        "kind": ActionKind.PROCESSOR,
        "has_dynamic_outputs": True,
        "build_date": datetime.utcnow(),
        "project": "b262b130-caf0-43b6-b495-4cc21201321b",
        "addons": {},
    }
    unwanted_action_data = {
        "id": "c50f8fae-e116-4db0-8c7b-d6a790a48ef1",
        "name": "action 3",
        "technical_name": "action technical name is different",
        "description": "action description unwanted",
        "lockfile": b"",
        "version": "1.0",
        "kind": ActionKind.PROCESSOR,
        "has_dynamic_outputs": True,
        "build_date": datetime.utcnow(),
        "project": "b262b130-caf0-43b6-b495-4cc21201321b",
        "addons": {},
    }
    action_raw = generate_action(**action_data_1)
    generate_action(**action_data_2)
    generate_action(**unwanted_action_data)
    expected_result = [Action(**action_data_2), Action(**action_data_1)]
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    result = action_repository.get_action_versions(action_raw["id"])
    assert sorted(result, key=attrgetter("id")) == sorted(
        expected_result, key=attrgetter("id")
    )


def test_get_action_versions_when_action_not_exists(
    app_container: ApplicationContainer, database_session: Session
):
    """Get action versions should raise exception when not exists"""
    action_id = "608f5b39-f383-40f7-8c1b-cac79e7324f0"
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    with pytest.raises(ActionNotFoundException):
        action_repository.get_action_versions(action_id)


def test_add_action(app_container: ApplicationContainer, database_session: Session):
    """List actions should return all actions from database"""
    new_action = Action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="name",
        technical_name="action technical name",
        version="1.0",
        logo=None,
        kind=ActionKind.PROCESSOR,
        description="desc",
        lockfile=b"",
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    action_repository.add_action(new_action)
    database_session.commit()

    assert list(database_session.execute("SELECT id FROM action")) == [(new_action.id,)]


def test_add_action_existing(
    app_container: ApplicationContainer, database_session: Session
):
    """List actions should return all actions from database"""
    new_action = Action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="name",
        technical_name="action technical name",
        version="1.0",
        logo=None,
        kind=ActionKind.PROCESSOR,
        description="desc",
        lockfile=b"",
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    action_repository.add_action(new_action)
    database_session.commit()

    other_action_with_same_id = Action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="name",
        technical_name="action technical name",
        version="2.0",
        logo=None,
        kind=ActionKind.PROCESSOR,
        description="desc",
        lockfile=b"",
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    action_repository.add_action(other_action_with_same_id)
    database_session.commit()

    assert list(database_session.execute("SELECT id,version FROM action")) == [
        (new_action.id, other_action_with_same_id.version)
    ]


def test_delete_action(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
    generate_action_input: Callable,
    generate_action_output: Callable,
):
    """Delete action should remove action from database"""
    action_raw = generate_action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    generate_action_input(
        id="73ab05c5-2dc5-47bf-a768-62764474a814",
        display_name="Input 1",
        technical_name="input-1",
        type=ActionIOType.STRING,
        action_id=action_raw["id"],
    )
    generate_action_output(
        id="73ab05c5-2dc5-47bf-a768-62764474a815",
        display_name="Output 1",
        technical_name="output-1",
        type=ActionIOType.STRING,
        action_id=action_raw["id"],
    )
    action = database_session.query(Action).get(action_raw["id"])
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    action_repository.delete_action(action)
    database_session.commit()
    assert list(database_session.execute("SELECT * FROM action")) == []
    assert list(database_session.execute("SELECT * FROM action_io")) == []


def test_get_action_by_name_and_version(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
):
    """Get action by id should return action when exists"""
    action_raw = generate_action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    result = action_repository.get_action_by_name_version_project(
        action_raw["technical_name"], action_raw["version"], action_raw["project"]
    )
    assert result.id == action_raw["id"]


def test_get_action_by_name_and_version_when_no_action_in_project(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
):
    """Get action by id should return action when exists"""
    action_raw = generate_action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        lockfile=b"",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    with pytest.raises(ActionNotFoundException):
        action_repository.get_action_by_name_version_project(
            action_raw["technical_name"],
            action_raw["version"],
            "0a53d3db-43bb-4deb-848c-2794078d2a5d",
        )


def test_get_action_by_name_and_version_when_not_exists(
    app_container: ApplicationContainer, database_session: Session
):
    """Get action by technical_name and version should return None when not exists"""
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    with pytest.raises(ActionNotFoundException):
        action_repository.get_action_by_name_version_project(
            "technical", "1.0", "0a53d3db-43bb-4deb-848c-2794078d2a5d"
        )


def test_get_action_category_by_name(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action_category: Callable,
):
    """Get action categories by name should return category names"""
    action_category_raw = generate_action_category(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="test_name",
    )
    generate_action_category(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef5",
        name="other_name",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    assert action_repository.get_action_category_by_name(
        action_category_raw["name"]
    ) == ActionCategory(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="test_name",
    )


def test_get_action_category_by_name_when_not_exists(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action_category: Callable,
):
    """Get action categories by name should return category names when not exists"""
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    assert not action_repository.get_action_category_by_name("random_name")


def test_list_categories(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action_category: Callable,
):
    category_1 = generate_action_category(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="test_name_1",
    )
    category_2 = generate_action_category(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef5",
        name="test_name_2",
    )
    category_3 = generate_action_category(
        id="89c43b2b-36d4-49c3-a738-fa69f1c30849",
        name="test_name_3",
    )
    action_repository: DatabaseActionRepository = app_container.action_repository(
        session=database_session
    )
    assert action_repository.list_categories() == [
        ActionCategory(id=category_1["id"], name=category_1["name"]),
        ActionCategory(id=category_2["id"], name=category_2["name"]),
        ActionCategory(id=category_3["id"], name=category_3["name"]),
    ]
