import datetime
from typing import Callable

import pytest

from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.workflow_exceptions import (
    EndpointAlreadyExistsException,
    WorkflowActionNotFoundException,
    WorkflowNotFoundException,
)
from ryax.studio.domain.workflow.workflow_values import (
    WorkflowActionDeployStatus,
    WorkflowActionValidityStatus,
    WorkflowDeploymentStatus,
    WorkflowErrorCode,
    WorkflowValidityStatus,
)
from ryax.studio.infrastructure.database.engine import Session
from ryax.studio.infrastructure.database.repositories.workflow_repository import (
    DatabaseWorkflowRepository,
)


def test_list_workflows(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_workflow: Callable,
):
    workflow_1 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )

    workflow_2 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef345",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )

    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )

    result = workflow_repository.list_workflows("b262b130-caf0-43b6-b495-4cc21201321b")
    result_ids = set(map(lambda item: item.id, result))
    assert result_ids == {workflow_1["id"], workflow_2["id"]}


def test_list_workflows_sorted_alphabetically_uncased(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_workflow: Callable,
):
    workflow_1 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow Z",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )

    workflow_2 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef345",
        name="workflow A",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )

    workflow_3 = generate_workflow(
        id="90f221c4-b0f3-4c1f-ab0b-3919933c9a43",
        name="Workflow M",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )

    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )

    result = workflow_repository.list_workflows("b262b130-caf0-43b6-b495-4cc21201321b")
    result_names = [workflow_view.name for workflow_view in result]
    assert result_names == [workflow_2["name"], workflow_3["name"], workflow_1["name"]]


def test_list_workflows_from_only_given_project(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_workflow: Callable,
):
    workflow_1 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )

    workflow_2 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef345",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    _ = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef346",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="91ab3fad-6d6f-4f40-ba7f-2fb7cdaf56a2",
    )
    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )

    result = workflow_repository.list_workflows("b262b130-caf0-43b6-b495-4cc21201321b")
    result_ids = set(map(lambda item: item.id, result))
    assert result_ids == {workflow_1["id"], workflow_2["id"]}


def test_list_workflows_with_search(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_workflow: Callable,
):
    workflow_1 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow test",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )

    generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef345",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )

    workflow_3 = generate_workflow(
        id="2dd8bfc8-01ce-47b3-abc2-da0398fef345",
        name="Workflow name",
        description="test description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )

    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    result = workflow_repository.list_workflows(
        "b262b130-caf0-43b6-b495-4cc21201321b", search="test"
    )
    result_ids = set(map(lambda item: item.id, result))
    assert result_ids == {workflow_1["id"], workflow_3["id"]}


def test_list_workflows_with_deployment_status_filter(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_workflow: Callable,
):
    workflow_1 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow test",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.DEPLOYED,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )

    generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef345",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )

    workflow_3 = generate_workflow(
        id="2dd8bfc8-01ce-47b3-abc2-da0398fef345",
        name="Workflow name",
        description="test description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.DEPLOYED,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )

    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    result = workflow_repository.list_workflows(
        "b262b130-caf0-43b6-b495-4cc21201321b",
        deployment_status=WorkflowDeploymentStatus.DEPLOYED,
    )
    result_ids = set(map(lambda item: item.id, result))
    assert result_ids == {workflow_1["id"], workflow_3["id"]}


def test_list_workflows_with_search_and_status_filter(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_workflow: Callable,
):
    workflow_1 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow test",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.DEPLOYED,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )

    generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef345",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.DEPLOYED,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )

    generate_workflow(
        id="2dd8bfc8-01ce-47b3-abc2-da0398fef345",
        name="Workflow name",
        description="test description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )

    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    result = workflow_repository.list_workflows(
        "b262b130-caf0-43b6-b495-4cc21201321b",
        search="test",
        deployment_status=WorkflowDeploymentStatus.DEPLOYED,
    )
    result_ids = set(map(lambda item: item.id, result))
    assert result_ids == {workflow_1["id"]}


def test_get_workflow(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_workflow: Callable,
):
    workflow_raw: dict = generate_workflow(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        name="Workflow 1",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    result = workflow_repository.get_workflow(workflow_raw["id"])
    assert result.id == workflow_raw["id"]


def test_get_workflow_when_not_exists(
    app_container: ApplicationContainer, database_session: Session
):
    """Get workflow by id should return None when not exists"""
    workflow_id = "608f5b39-f383-40f7-8c1b-cac79e7324f0"
    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    with pytest.raises(WorkflowNotFoundException):
        workflow_repository.get_workflow(workflow_id)


def test_add_workflow(app_container: ApplicationContainer, database_session: Session):
    """List workflows should return workflows from database"""
    new_workflow = Workflow(
        id="608f5b39-f383-40f7-8c1b-cac79e7324f0",
        name="Workflow1",
        description="Workflow Desc1",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="user",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    workflow_repository.add_workflow(new_workflow)
    database_session.commit()

    rows = list(
        database_session.execute(
            "SELECT id, name, description, validity_status, deployment_status, owner, project FROM workflow"
        )
    )

    assert rows == [
        (
            new_workflow.id,
            new_workflow.name,
            new_workflow.description,
            new_workflow.validity_status.value,
            new_workflow.deployment_status.value,
            new_workflow.owner,
            new_workflow.project,
        )
    ]


def test_delete_workflow(
    app_container: ApplicationContainer, database_session: Session
):
    """Delete workflow should remove workflow from database when model exists in database"""
    raw_workflow = {
        "id": "608f5b39-f383-40f7-8c1b-cac79e7324f0",
        "name": "Workflow 1",
        "description": "Workflow description",
        "errors": [],
        "status": "valid",
        "owner": "user",
        "project": "b262b130-caf0-43b6-b495-4cc21201321b",
    }
    database_session.execute(
        "INSERT INTO workflow (id, name, description, owner, project) VALUES (:id, :name, :description, :owner, :project)",
        raw_workflow,
    )
    workflow = database_session.query(Workflow).get(raw_workflow["id"])
    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    workflow_repository.delete_workflow(workflow)
    database_session.commit()

    rows = list(database_session.execute("SELECT id, name, description FROM workflow"))
    assert rows == []


def test_get_workflow_details_view(
    app_container: ApplicationContainer,
    database_session: Session,
    workflow_raw: dict,
):
    """Get workflow by id should return workflow when exists"""
    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    result = workflow_repository.get_workflow_view(workflow_raw["id"])
    assert result.id == workflow_raw["id"]


def test_workflow_exists(
    app_container: ApplicationContainer, database_session: Session, workflow_raw: dict
):
    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    assert workflow_repository.check_workflow_exists(workflow_raw["id"]) is None


def test_workflow_not_exists(
    app_container: ApplicationContainer,
    database_session: Session,
):
    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    with pytest.raises(WorkflowNotFoundException):
        workflow_repository.check_workflow_exists(
            "5c5e61af-1077-4806-bdd9-c02e5617468e"
        )


def test_workflow_action_exists(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
):
    generate_action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    workflow_raw: dict = generate_workflow(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        name="Workflow 1",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    workflow_action_raw: dict = generate_workflow_action(
        id="82bd3d3b-cafe-419b-9d0b-c4c5943af151",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_raw["id"],
    )
    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    assert (
        workflow_repository.check_workflow_action_exists(workflow_action_raw["id"])
        is None
    )


def test_workflow_action_not_exists(
    app_container: ApplicationContainer, database_session: Session
):
    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    with pytest.raises(WorkflowActionNotFoundException):
        workflow_repository.check_workflow_action_exists(
            "5c5e61af-1077-4806-bdd9-c02e5617468e"
        )


def test_list_workflow_action_inputs_view(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
    generate_workflow_action_input: Callable,
):
    workflow_1 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    workflow_action_1 = generate_workflow_action(
        id="0e5d679c-78fb-43ae-ae49-e5e1fa46f431",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_1["id"],
    )
    workflow_action_1_input_1 = generate_workflow_action_input(
        id="e331a6b3-a86d-4675-8a66-5948cdca244a",
        workflow_action_id=workflow_action_1["id"],
    )
    workflow_action_1_input_2 = generate_workflow_action_input(
        id="73ab05c5-2dc5-47bf-a768-62764474a814",
        workflow_action_id=workflow_action_1["id"],
    )
    workflow_action_2 = generate_workflow_action(
        id="0e5d679c-78fb-43ae-ae49-e5e1fa46f432",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_1["id"],
    )
    generate_workflow_action_input(
        id="e331a6b3-a86d-4675-8a66-5948cdca246a",
        workflow_action_id=workflow_action_2["id"],
    )
    generate_workflow_action_input(
        id="73ab05c5-2dc5-47bf-a768-62764474a811",
        workflow_action_id=workflow_action_2["id"],
    )

    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )

    result = workflow_repository.list_workflow_action_inputs_view(
        workflow_action_1["id"]
    )
    assert set(map(lambda item: item.id, result)) == {
        workflow_action_1_input_2["id"],
        workflow_action_1_input_1["id"],
    }


def test_list_workflow_action_outputs_view(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
    generate_workflow_action_output: Callable,
):
    workflow_1 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    workflow_action_1 = generate_workflow_action(
        id="0e5d679c-78fb-43ae-ae49-e5e1fa46f431",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_1["id"],
    )
    workflow_action_1_output_1 = generate_workflow_action_output(
        id="11",
        display_name="Output 1",
        technical_name="output-1",
        type=ActionIOType.STRING.value,
        workflow_action_id=workflow_action_1["id"],
    )
    workflow_action_1_output_2 = generate_workflow_action_output(
        id="12",
        display_name="Output 2",
        technical_name="output-2",
        type=ActionIOType.INTEGER.value,
        workflow_action_id=workflow_action_1["id"],
    )
    generate_workflow_action_output(
        id="1_excluded",
        technical_name="ryax_output-3",
        workflow_action_id=workflow_action_1["id"],
    )
    workflow_action_1_dyn_output_1 = generate_workflow_action_output(
        id="dyn",
        technical_name="dyn_test",
        workflow_action_id=workflow_action_1["id"],
        is_dynamic=True,
    )
    workflow_action_2 = generate_workflow_action(
        id="0e5d679c-78fb-43ae-ae49-e5e1fa46f432",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_1["id"],
    )
    workflow_action_2_output_1 = generate_workflow_action_output(
        id="21",
        technical_name="output-1",
        workflow_action_id=workflow_action_2["id"],
    )
    workflow_action_2_output_2 = generate_workflow_action_output(
        id="22",
        technical_name="output-2",
        workflow_action_id=workflow_action_2["id"],
    )

    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    result = workflow_repository.list_workflow_action_outputs_view(
        workflow_action_1["id"]
    )
    expected_result = set(map(lambda item: item.id, result))
    assert expected_result == {
        workflow_action_1_output_1["id"],
        workflow_action_1_output_2["id"],
        workflow_action_1_dyn_output_1["id"],
    }
    result = list(
        workflow_repository.list_workflow_action_outputs_view(workflow_action_2["id"])
    )
    expected_result = set(map(lambda item: item.id, result))
    assert expected_result == {
        workflow_action_2_output_1["id"],
        workflow_action_2_output_2["id"],
    }


def test_search_workflow_action_outputs_view(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
    generate_workflow_action_output: Callable,
):
    workflow_1 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    workflow_action_1 = generate_workflow_action(
        id="0e5d679c-78fb-43ae-ae49-e5e1fa46f431",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_1["id"],
    )
    workflow_action_1_output_1 = generate_workflow_action_output(
        id="e331a6b3-a86d-4675-8a66-5948cdca244a",
        display_name="Output 1",
        technical_name="output-1",
        type=ActionIOType.STRING.value,
        workflow_action_id=workflow_action_1["id"],
    )
    workflow_action_1_output_2 = generate_workflow_action_output(
        id="73ab05c5-2dc5-47bf-a768-62764474a814",
        display_name="Output 2",
        technical_name="output-2",
        type=ActionIOType.INTEGER.value,
        workflow_action_id=workflow_action_1["id"],
    )
    workflow_action_1_dyn_output_1 = generate_workflow_action_output(
        id="dyn",
        workflow_action_id=workflow_action_1["id"],
        is_dynamic=True,
    )
    workflow_action_2 = generate_workflow_action(
        id="0e5d679c-78fb-43ae-ae49-e5e1fa46f432",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_1["id"],
    )
    workflow_action_2_output_1 = generate_workflow_action_output(
        id="30af2fcf-bb34-4617-90ef-b8037d921e36",
        workflow_action_id=workflow_action_2["id"],
    )
    workflow_action_2_output_2 = generate_workflow_action_output(
        id="30af2fcf-bb34-4617-90ef-b8037d921e37",
        workflow_action_id=workflow_action_2["id"],
    )
    workflow_2 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef555",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    workflow_2_action_1 = generate_workflow_action(
        id="0e5d679c-78fb-43ae-ae49-e5e1fa46f439",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_2["id"],
    )
    generate_workflow_action_output(
        id="30af2fcf-bb34-4617-90ef-b8037d921e55",
        workflow_action_id=workflow_2_action_1["id"],
    )
    generate_workflow_action_output(
        id="30af2fcf-bb34-4617-90ef-b8037d921e56",
        workflow_action_id=workflow_2_action_1["id"],
    )

    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    result = list(
        workflow_repository.search_workflow_action_outputs_view(workflow_1["id"])
    )
    expected_result = set(map(lambda item: item.id, result))
    assert expected_result == {
        workflow_action_1_output_1["id"],
        workflow_action_1_output_2["id"],
        workflow_action_2_output_1["id"],
        workflow_action_2_output_2["id"],
        workflow_action_1_dyn_output_1["id"],
    }


def test_search_workflow_action_outputs_view_with_type_filter(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
    generate_workflow_action_output: Callable,
):
    workflow_1 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    workflow_action_1 = generate_workflow_action(
        id="0e5d679c-78fb-43ae-ae49-e5e1fa46f431",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_1["id"],
    )
    workflow_action_1_output_1 = generate_workflow_action_output(
        id="e331a6b3-a86d-4675-8a66-5948cdca244a",
        display_name="Output 1",
        technical_name="output-1",
        type=ActionIOType.STRING.value,
        workflow_action_id=workflow_action_1["id"],
    )
    generate_workflow_action_output(
        id="73ab05c5-2dc5-47bf-a768-62764474a814",
        display_name="Output 2",
        technical_name="output-2",
        type=ActionIOType.INTEGER.value,
        workflow_action_id=workflow_action_1["id"],
    )

    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    result = list(
        workflow_repository.search_workflow_action_outputs_view(
            workflow_1["id"], with_type=ActionIOType.STRING
        )
    )
    expected_result = list(map(lambda item: item.id, result))
    assert workflow_action_1_output_1["id"] in expected_result


def test_search_workflow_action_outputs_view_in_workflow_actions(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
    generate_workflow_action_output: Callable,
):
    workflow_1 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    workflow_action_1 = generate_workflow_action(
        id="0e5d679c-78fb-43ae-ae49-e5e1fa46f431",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_1["id"],
    )
    generate_workflow_action_output(
        id="e331a6b3-a86d-4675-8a66-5948cdca244a",
        display_name="Output 1",
        technical_name="output-1",
        type=ActionIOType.STRING.value,
        workflow_action_id=workflow_action_1["id"],
    )
    generate_workflow_action_output(
        id="73ab05c5-2dc5-47bf-a768-62764474a814",
        display_name="Output 2",
        technical_name="output-2",
        type=ActionIOType.INTEGER.value,
        workflow_action_id=workflow_action_1["id"],
    )
    workflow_action_2 = generate_workflow_action(
        id="0e5d679c-78fb-43ae-ae49-e5e1fa46f432",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_1["id"],
    )
    workflow_action_2_output_1 = generate_workflow_action_output(
        id="e331a6b3-a86d-4675-8a66-5948cdca245c",
        display_name="Output 1",
        technical_name="output-1",
        type=ActionIOType.STRING.value,
        workflow_action_id=workflow_action_2["id"],
    )
    workflow_action_2_output_2 = generate_workflow_action_output(
        id="73ab05c5-2dc5-47bf-a768-62764474a835",
        display_name="Output 2",
        technical_name="output-2",
        type=ActionIOType.INTEGER.value,
        workflow_action_id=workflow_action_2["id"],
    )

    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    result = list(
        workflow_repository.search_workflow_action_outputs_view(
            workflow_1["id"], in_workflow_actions=[workflow_action_2["id"]]
        )
    )
    expected_result = set(map(lambda item: item.id, result))
    assert expected_result == {
        workflow_action_2_output_1["id"],
        workflow_action_2_output_2["id"],
    }


def test_search_workflow_action_outputs_view_with_exclude_workflow_actions_filter(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
    generate_workflow_action_output: Callable,
):
    workflow_1 = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    workflow_action_1 = generate_workflow_action(
        id="0e5d679c-78fb-43ae-ae49-e5e1fa46f431",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_1["id"],
    )
    workflow_action_1_output_1 = generate_workflow_action_output(
        id="e331a6b3-a86d-4675-8a66-5948cdca244a",
        display_name="Output 1",
        technical_name="output-1",
        type=ActionIOType.STRING.value,
        workflow_action_id=workflow_action_1["id"],
    )
    workflow_action_1_output_2 = generate_workflow_action_output(
        id="73ab05c5-2dc5-47bf-a768-62764474a814",
        display_name="Output 2",
        technical_name="output-2",
        type=ActionIOType.INTEGER.value,
        workflow_action_id=workflow_action_1["id"],
    )
    workflow_action_2 = generate_workflow_action(
        id="0e5d679c-78fb-43ae-ae49-e5e1fa46f432",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_1["id"],
    )
    generate_workflow_action_output(
        id="e331a6b3-a86d-4675-8a66-5948cdca245c",
        display_name="Output 1",
        technical_name="output-1",
        type=ActionIOType.STRING.value,
        workflow_action_id=workflow_action_2["id"],
    )
    generate_workflow_action_output(
        id="73ab05c5-2dc5-47bf-a768-62764474a835",
        display_name="Output 2",
        technical_name="output-2",
        type=ActionIOType.INTEGER.value,
        workflow_action_id=workflow_action_2["id"],
    )

    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    result = list(
        workflow_repository.search_workflow_action_outputs_view(
            workflow_1["id"], exclude_workflow_actions=[workflow_action_2["id"]]
        )
    )
    expected_result = set(map(lambda item: item.id, result))
    assert expected_result == {
        workflow_action_1_output_1["id"],
        workflow_action_1_output_2["id"],
    }


def test_get_workflow_errors(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
    generate_workflow_error: Callable,
):
    generate_action(
        id="4d9c95fa-56ae-4703-8cc1-57c285a08009",
        name="action 1",
        technical_name="action technical name",
        version="1.0",
        kind=ActionKind.SOURCE,
        has_dynamic_outputs=True,
        build_date=datetime.datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    workflow = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    workflow_action = generate_workflow_action(
        id="0e5d679c-78fb-43ae-ae49-e5e1fa46f431",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow["id"],
    )
    workflow_error_1 = generate_workflow_error(
        id="error1",
        code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED.value,
        workflow_id=workflow["id"],
    )
    workflow_error_2 = generate_workflow_error(
        id="error2",
        code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED.value,
        workflow_action_id=workflow_action["id"],
    )

    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    result = workflow_repository.get_workflow_errors(workflow["id"])
    error = result[0]
    assert error.id == workflow_error_1["id"]
    assert error.code == workflow_error_1["code"]
    assert error.workflow_id == workflow_error_1["workflow_id"]
    assert error.workflow_action_id is None

    error = result[1]
    assert error.id == workflow_error_2["id"]
    assert error.code == workflow_error_2["code"]
    assert error.workflow_id == workflow["id"]
    assert error.workflow_action_id == workflow_error_2["workflow_action_id"]


def test_list_workflows_where_action_used(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
):
    action = generate_action(
        id="4d9c95fa-56ae-4703-8cc1-57c285a08009",
        name="action 1",
        technical_name="technical_name",
        version="1.0",
        kind=ActionKind.SOURCE,
        has_dynamic_outputs=True,
        build_date=datetime.datetime.utcnow(),
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    workflow = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-ab0398fef987",
        name="Workflow name 2",
        description="Workflow description 2",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="b262b130-caf0-43b6-b495-4cc21201321b",
    )
    workflow_action = generate_workflow_action(
        id="0e5d679c-78fb-43ae-ae49-e5e1fa46f431",
        name="action 1",
        technical_name="action_technical_name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id=action["id"],
        workflow_id=workflow["id"],
        validity_status=WorkflowActionValidityStatus.INVALID.value,
        deployment_status=WorkflowActionDeployStatus.NONE.value,
    )
    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )
    result = workflow_repository.list_workflows_where_action_used(action["id"])
    assert result == [
        Workflow(
            id=workflow["id"],
            name=workflow["name"],
            description=workflow["description"],
            validity_status=WorkflowValidityStatus(workflow["validity_status"]),
            deployment_status=WorkflowDeploymentStatus(workflow["deployment_status"]),
            actions=[
                WorkflowAction(
                    id=workflow_action["id"],
                    name=workflow_action["name"],
                    technical_name=workflow_action["technical_name"],
                    description=workflow_action["description"],
                    version=workflow_action["version"],
                    kind=ActionKind(workflow_action["kind"]),
                    has_dynamic_outputs=workflow_action["has_dynamic_outputs"],
                    custom_name=workflow_action["custom_name"],
                    position_x=workflow_action["position_x"],
                    position_y=workflow_action["position_y"],
                    action_id=workflow_action["action_id"],
                    validity_status=WorkflowActionValidityStatus.INVALID,
                    deployment_status=WorkflowActionDeployStatus.NONE,
                    workflow_id=workflow["id"],
                    addons={},
                    constraints=None,
                    objectives=None,
                )
            ],
            owner=workflow["owner"],
            project="b262b130-caf0-43b6-b495-4cc21201321b",
        )
    ]


def test_check_if_endpoint_exists_when_exists(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
):
    existing_endpoint = "/foobar/baz"
    generate_action(
        id="m1",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.datetime.utcnow(),
        project="123",
    )
    workflow_raw: dict = generate_workflow(
        id="wf1",
        name="Workflow 1",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="123",
    )
    generate_workflow_action(
        id="wfm1",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="m1",
        workflow_id=workflow_raw["id"],
        endpoint=existing_endpoint,
    )
    # Create another workflow in another project
    generate_action(
        id="m2",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.datetime.utcnow(),
        project="other project",
    )
    workflow_raw: dict = generate_workflow(
        id="wf2",
        name="Workflow 1",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="other project",
    )
    generate_workflow_action(
        id="wfm2",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="m2",
        workflow_id=workflow_raw["id"],
        endpoint=existing_endpoint,
    )
    workflow_raw: dict = generate_workflow(
        id="wf3",
        name="Workflow 1",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="other project",
    )
    generate_workflow_action(
        id="wfm3",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="m2",
        workflow_id=workflow_raw["id"],
        endpoint=None,
    )

    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )

    workflow_repository.check_if_endpoint_exists(
        existing_endpoint, project_id="not existing project"
    )

    workflow_repository.check_if_endpoint_exists(None, project_id="123")
    workflow_repository.check_if_endpoint_exists(None, project_id="other project")

    workflow_repository.check_if_endpoint_exists("not-exists", project_id="123")

    with pytest.raises(EndpointAlreadyExistsException):
        workflow_repository.check_if_endpoint_exists(
            existing_endpoint, project_id="other project"
        )

    with pytest.raises(EndpointAlreadyExistsException):
        workflow_repository.check_if_endpoint_exists(
            existing_endpoint, project_id="123"
        )


def test_check_if_endpoint_exists_when_doesnt(
    app_container: ApplicationContainer,
    database_session: Session,
    generate_action: Callable,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
):
    existing_endpoint = "/foobar/baz"
    generate_action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.datetime.utcnow(),
        project="123",
    )
    workflow_raw: dict = generate_workflow(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        name="Workflow 1",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="123",
    )
    generate_workflow_action(
        id="82bd3d3b-cafe-419b-9d0b-c4c5943af151",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="123",
        workflow_id=workflow_raw["id"],
        endpoint="/A/DIFFERENT/ENDPOINT",
    )
    workflow_repository: DatabaseWorkflowRepository = app_container.workflow_repository(
        session=database_session
    )

    workflow_repository.check_if_endpoint_exists(existing_endpoint, project_id="123")
