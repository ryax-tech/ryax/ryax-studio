import datetime
import json
from typing import Callable
from uuid import uuid4

from sqlalchemy.orm import Session
from sqlalchemy_utils import ScalarListType

from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_category import ActionCategory
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.domain.logo.logo import Logo
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_category import (
    WorkflowActionCategory,
)
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionInput,
)
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.entities.workflow_error import WorkflowError
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.entities.workflow_result import WorkflowResult
from ryax.studio.domain.workflow.workflow_values import (
    WorkflowActionDeployStatus,
    WorkflowActionValidityStatus,
    WorkflowDeploymentStatus,
    WorkflowErrorCode,
    WorkflowValidityStatus,
)


def test_action_mapper(
    generate_action: Callable, generate_logo: Callable, database_session: Session
):
    """action mapper should return action"""
    custom_action_id = "c50f8fae-e116-4db0-8c7b-d6a790a48ef4"
    logo_raw: dict = generate_logo(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef5",
        name="logo_name",
        action_id=custom_action_id,
        extension="png",
        content=b"123",
    )

    action_raw: dict = generate_action(
        id=custom_action_id,
        name="action 1",
        technical_name="action technical name",
        description="action description",
        logo=logo_raw["id"],
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.datetime.utcnow(),
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    action: Action = database_session.query(Action).get(action_raw["id"])
    assert action == Action(
        id=action_raw["id"],
        name=action_raw["name"],
        technical_name=action_raw["technical_name"],
        version=action_raw["version"],
        kind=ActionKind(action_raw["kind"]),
        build_date=action_raw["build_date"],
        logo=Logo(
            id=logo_raw["id"],
            name=logo_raw["name"],
            action_id=custom_action_id,
            extension=logo_raw["extension"],
            content=logo_raw["content"],
        ),
        description=action_raw["description"],
        lockfile=action_raw["lockfile"],
        has_dynamic_outputs=action_raw["has_dynamic_outputs"],
        project=action_raw["project"],
        addons=json.loads(action_raw["addons"]),
    )


def test_delete_action(
    generate_action: Callable,
    generate_action_input: Callable,
    database_session: Session,
):
    action_raw: dict = generate_action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.datetime.utcnow(),
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    generate_action_input(
        optional=False,
        id="73ab05c5-2dc5-47bf-a768-62764474a814",
        display_name="Output 1",
        technical_name="output-1",
        type=ActionIOType.STRING,
        action_id=action_raw["id"],
    )
    action: Action = database_session.query(Action).get(action_raw["id"])
    database_session.delete(action)
    database_session.commit()
    assert list(database_session.execute("SELECT * FROM action")) == []
    assert list(database_session.execute("SELECT * FROM action_io")) == []


def test_logo_mapper(logo_raw: dict, database_session: Session):
    """Logo mapper should return logo"""
    logo_id = logo_raw["id"]
    logo: Logo = database_session.query(Logo).get(logo_id)
    assert logo == Logo(
        id=logo_id,
        name=logo_raw.get("name"),
        action_id=logo_raw.get("action_id"),
        extension=logo_raw.get("extension"),
        content=logo_raw.get("content"),
    )


def test_workflow(database_session: Session, generate_workflow: Callable):
    workflow_raw: dict = generate_workflow(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        name="Workflow 1",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    workflow = database_session.query(Workflow).get(workflow_raw["id"])
    assert workflow == Workflow(
        id=workflow_raw["id"],
        name=workflow_raw["name"],
        description=workflow_raw["description"],
        validity_status=WorkflowValidityStatus(workflow_raw["validity_status"]),
        deployment_status=WorkflowDeploymentStatus(workflow_raw["deployment_status"]),
        actions=[],
        actions_links=[],
        owner=workflow_raw["owner"],
        events=[],
        project=workflow_raw["project"],
    )


def test_workflow_result(
    database_session: Session,
    generate_workflow: Callable,
    generate_workflow_result: Callable,
    generate_workflow_action_input: Callable,
    generate_workflow_action: Callable,
    generate_workflow_action_output: Callable,
):
    workflow_raw: dict = generate_workflow(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        name="Workflow 1",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    workflow_action_raw = generate_workflow_action(
        id="82bd3d3b-cafe-419b-9d0b-c4c5943af151",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_raw["id"],
    )
    workflow_action_output_raw = generate_workflow_action_output(
        id="30af2fcf-bb34-4617-90ef-b8037d921e37",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING.value,
        enum_values=["option1", "option2"],
        workflow_action_id=workflow_action_raw["id"],
    )
    workflow_action_input_raw = generate_workflow_action_input(
        id="0e5d679c-78fb-43ae-ae49-e5e1fa46f455",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING.value,
        enum_values=["option1", "option2"],
        static_value="test_value",
        workflow_action_output_id=workflow_action_output_raw["id"],
        workflow_action_id=workflow_action_raw["id"],
    )
    workflow_result: dict = generate_workflow_result(
        id="d91ed78f-b2d2-4469-a845-8e0a8d5f1fe1",
        workflow_id=workflow_raw["id"],
        key="key",
        workflow_action_input_id=workflow_action_input_raw["id"],
        workflow_action_output_id=None,
    )
    workflow = database_session.query(Workflow).get(workflow_raw["id"])
    assert workflow.results == [WorkflowResult(**workflow_result)]
    workflow_result_in_db = database_session.query(WorkflowResult).get(
        workflow_result["id"]
    )
    assert workflow_result_in_db == WorkflowResult(
        id=workflow_result["id"],
        workflow_id=workflow_result["workflow_id"],
        key=workflow_result["key"],
        workflow_action_input_id=workflow_result["workflow_action_input_id"],
        workflow_action_output_id=workflow_result["workflow_action_output_id"],
    )


def test_workflow_action(
    database_session: Session,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
):
    workflow_raw: dict = generate_workflow(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        name="Workflow 1",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    workflow_action_raw: dict = generate_workflow_action(
        id="82bd3d3b-cafe-419b-9d0b-c4c5943af151",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        validity_status=WorkflowActionValidityStatus.VALID.value,
        deployment_status=WorkflowActionDeployStatus.NONE.value,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_raw["id"],
        addons={},
    )
    workflow_action: WorkflowAction = database_session.query(WorkflowAction).get(
        workflow_action_raw["id"]
    )
    assert workflow_action == WorkflowAction(
        id=workflow_action_raw["id"],
        name=workflow_action_raw["name"],
        technical_name=workflow_action_raw["technical_name"],
        description=workflow_action_raw["description"],
        version=workflow_action_raw["version"],
        kind=ActionKind(workflow_action_raw["kind"]),
        has_dynamic_outputs=workflow_action_raw["has_dynamic_outputs"],
        validity_status=WorkflowActionValidityStatus.VALID,
        deployment_status=WorkflowActionDeployStatus.NONE,
        custom_name=workflow_action_raw["custom_name"],
        position_x=workflow_action_raw["position_x"],
        position_y=workflow_action_raw["position_y"],
        action_id=workflow_action_raw["action_id"],
        workflow_id=workflow_raw["id"],
        addons=json.loads(workflow_action_raw["addons"]),
        constraints=None,
        objectives=None,
    )


def test_add_workflow_action_error(
    database_session: Session,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
):
    workflow_raw: dict = generate_workflow(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        name="Workflow 1",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    workflow_action_raw: dict = generate_workflow_action(
        id="82bd3d3b-cafe-419b-9d0b-c4c5943af151",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_raw["id"],
    )

    workflow_action_error = WorkflowError(
        id="0dd5b60f-ef96-43c0-9081-14a7f74d01b4",
        code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED,
    )
    workflow_action: WorkflowAction = database_session.query(WorkflowAction).get(
        workflow_action_raw["id"]
    )
    workflow_action.errors.append(workflow_action_error)
    database_session.commit()
    assert list(
        database_session.execute(
            "SELECT id, code, workflow_id, workflow_action_id FROM workflow_error"
        )
    ) == [
        (
            workflow_action_error.id,
            workflow_action_error.code.value,
            None,
            workflow_action.id,
        )
    ]


def test_delete_workflow_action_error(
    database_session: Session,
    generate_workflow: Callable,
    generate_workflow_error: Callable,
    generate_workflow_action: Callable,
    generate_workflow_action_error: Callable,
):
    workflow_raw: dict = generate_workflow(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        name="Workflow 1",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    workflow_action_raw: dict = generate_workflow_action(
        id="82bd3d3b-cafe-419b-9d0b-c4c5943af151",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_raw["id"],
    )
    generate_workflow_error(
        id="e4b1e9b0-8359-496c-a8ac-83c3c4210b7d",
        code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED.value,
        workflow_action_id=workflow_action_raw["id"],
    )

    workflow_action: WorkflowAction = database_session.query(WorkflowAction).get(
        workflow_action_raw["id"]
    )
    workflow_action.errors.pop()
    database_session.commit()
    assert list(database_session.execute("SELECT * FROM workflow_error")) == []


def get_workflow_with_workflow_action_input():
    workflow = Workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    output1 = WorkflowActionOutput(
        id="out1",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING.value,
        enum_values=["option1", "option2"],
        position=0,
        optional=False,
    )
    workflow_action = WorkflowAction(
        id="82bd3d3b-cafe-419b-9d0b-c4c5943af151",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        outputs=[output1],
        inputs=[
            WorkflowActionInput(
                id=str(uuid4()),
                technical_name="technical_name",
                display_name="display_name",
                help="help",
                type=ActionIOType.STRING.value,
                enum_values=["option1", "option2"],
                static_value="static_value",
                reference_value=output1,
                file_value=WorkflowFile(
                    id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
                    name="file",
                    extension="txt",
                    path="inputs/7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
                    size=128.12,
                ),
                position=0,
                optional=False,
            )
        ],
    )
    workflow.actions.append(workflow_action)
    return workflow


def check_workflow(database_session, workflow):
    workflow_from_db: Workflow = database_session.query(Workflow).get(workflow.id)
    assert workflow == workflow_from_db


def test_add_workflow_action_input(
    database_session: Session,
):
    workflow = get_workflow_with_workflow_action_input()
    database_session.add(workflow)
    database_session.commit()

    check_workflow(database_session, workflow)


def test_delete_workflow_action_input(
    database_session: Session,
):
    workflow = get_workflow_with_workflow_action_input()
    database_session.add(workflow)
    database_session.commit()

    check_workflow(database_session, workflow)

    workflow.actions[0].inputs.pop()
    database_session.commit()
    assert database_session.query(WorkflowActionInput).all() == []


def test_set_static_value_in_workflow_action_input(
    database_session: Session,
):
    workflow = get_workflow_with_workflow_action_input()
    database_session.add(workflow)
    database_session.commit()
    check_workflow(database_session, workflow)

    workflow.actions[0].inputs[0].static_value = True
    database_session.commit()
    check_workflow(database_session, workflow)

    workflow.actions[0].inputs[0].static_value = 12
    database_session.commit()
    check_workflow(database_session, workflow)

    workflow.actions[0].inputs[0].static_value = 12.23
    database_session.commit()
    check_workflow(database_session, workflow)

    workflow.actions[0].inputs[0].static_value = b"123"
    database_session.commit()
    check_workflow(database_session, workflow)


def test_add_workflow_action_output(
    database_session: Session,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
):
    workflow_raw = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    workflow_action_raw = generate_workflow_action(
        id="82bd3d3b-cafe-419b-9d0b-c4c5943af151",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_raw["id"],
    )

    workflow_action_output = WorkflowActionOutput(
        id=str(uuid4()),
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING,
        enum_values=["option1", "option2"],
        optional=False,
    )
    workflow_action: WorkflowAction = database_session.query(WorkflowAction).get(
        workflow_action_raw["id"]
    )
    workflow_action.outputs.append(workflow_action_output)
    database_session.commit()

    assert list(database_session.execute("SELECT * FROM workflow_action_output")) == [
        (
            workflow_action_output.id,
            workflow_action_output.technical_name,
            workflow_action_output.display_name,
            workflow_action_output.help,
            workflow_action_output.type.value,
            ScalarListType().process_bind_param(
                workflow_action_output.enum_values, None
            ),
            workflow_action_raw["id"],
            workflow_action_output.is_dynamic,
            workflow_action_output.origin,
            0,
            workflow_action_output.optional,
        )
    ]


def test_delete_workflow_action_output(
    database_session: Session,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
    generate_workflow_action_output: Callable,
):
    workflow_raw = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    workflow_action_raw = generate_workflow_action(
        id="82bd3d3b-cafe-419b-9d0b-c4c5943af151",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_raw["id"],
    )
    generate_workflow_action_output(
        id="30af2fcf-bb34-4617-90ef-b8037d921e37",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING.value,
        enum_values=["option1", "option2"],
        workflow_action_id=workflow_action_raw["id"],
    )
    workflow_action_raw: WorkflowAction = database_session.query(WorkflowAction).get(
        workflow_action_raw["id"]
    )
    workflow_action_raw.outputs.pop()
    database_session.commit()
    assert list(database_session.execute("SELECT * FROM workflow_action_output")) == []


def test_workflow_action_output(
    database_session: Session,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
    generate_workflow_action_output: Callable,
):
    workflow_raw = generate_workflow(
        id="2cc8bfc8-01ce-47b3-abc2-da0398fef562",
        name="Workflow name",
        description="Workflow description",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    workflow_action_raw = generate_workflow_action(
        id="82bd3d3b-cafe-419b-9d0b-c4c5943af151",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_raw["id"],
    )
    workflow_action_output_raw = generate_workflow_action_output(
        id="30af2fcf-bb34-4617-90ef-b8037d921e37",
        technical_name="technical_name",
        display_name="display_name",
        help="help",
        type=ActionIOType.STRING.value,
        enum_values=["option1", "option2"],
        workflow_action_id=workflow_action_raw["id"],
    )
    result: WorkflowActionOutput = database_session.query(WorkflowActionOutput).get(
        workflow_action_output_raw["id"]
    )
    assert result == WorkflowActionOutput(
        id=workflow_action_output_raw["id"],
        technical_name=workflow_action_output_raw["technical_name"],
        display_name=workflow_action_output_raw["display_name"],
        help=workflow_action_output_raw["help"],
        type=ActionIOType(workflow_action_output_raw["type"]),
        enum_values=workflow_action_output_raw["enum_values"],
        optional=workflow_action_output_raw["optional"],
    )


def test_workflow_error(
    database_session: Session,
    generate_action: Callable,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
    generate_workflow_error: Callable,
):
    generate_action(
        id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        name="action 1",
        technical_name="action technical name",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.datetime.utcnow(),
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    workflow_raw: dict = generate_workflow(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        name="Workflow 1",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    workflow_action_raw: dict = generate_workflow_action(
        id="82bd3d3b-cafe-419b-9d0b-c4c5943af151",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_raw["id"],
    )
    workflow_action_error_raw: dict = generate_workflow_error(
        id="e4b1e9b0-8359-496c-a8ac-83c3c4210b7d",
        code=WorkflowErrorCode.WORKFLOW_NOT_CONNECTED.value,
        workflow_id=workflow_raw["id"],
        workflow_action_id=workflow_action_raw["id"],
    )
    result: WorkflowAction = database_session.query(WorkflowError).get(
        workflow_action_error_raw["id"]
    )
    assert result == WorkflowError(
        id=workflow_action_error_raw["id"],
        code=WorkflowErrorCode(workflow_action_error_raw["code"]),
    )


def test_workflow_file(
    database_session: Session,
    generate_workflow_file: Callable,
):
    workflow_input_file: dict = generate_workflow_file(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        name="file",
        extension="txt",
        path="inputs/7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        size=128.12,
    )
    workflow_file: WorkflowFile = database_session.query(WorkflowFile).get(
        workflow_input_file["id"]
    )
    assert workflow_file == WorkflowFile(
        id=workflow_input_file["id"],
        name=workflow_input_file["name"],
        extension=workflow_input_file["extension"],
        path=workflow_input_file["path"],
        size=workflow_input_file["size"],
    )


def test_action_category(
    database_session: Session,
    generate_action_category: Callable,
):
    action_category_dict: dict = generate_action_category(
        id="8dcd2029-5ad6-4d9e-a6a1-7caea2946d4c",
        name="category_test",
    )
    action_category: ActionCategory = database_session.query(ActionCategory).get(
        action_category_dict["id"]
    )
    assert action_category == ActionCategory(
        id=action_category_dict["id"],
        name=action_category_dict["name"],
    )


def test_action_with_categories(
    database_session: Session,
    generate_action: Callable,
    generate_action_category: Callable,
    generate_action_category_association: Callable,
):
    action_category_dict: dict = generate_action_category(
        id="8dcd2029-5ad6-4d9e-a6a1-7caea2946d4a",
        name="category_test",
    )
    action_category_2_dict: dict = generate_action_category(
        id="8dcd2029-5ad6-4d9e-a6a1-7caea2946d4b",
        name="category_test_2",
    )
    action_raw: dict = generate_action(
        id="8dcd2029-5ad6-4d9e-a6a1-7caea2946d4c",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.datetime.utcnow(),
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    generate_action_category_association(action_raw["id"], action_category_dict["id"])
    generate_action_category_association(action_raw["id"], action_category_2_dict["id"])
    action: Action = database_session.query(Action).get(action_raw["id"])
    assert action == Action(
        id=action_raw["id"],
        name=action_raw["name"],
        technical_name=action_raw["technical_name"],
        version=action_raw["version"],
        kind=ActionKind(action_raw["kind"]),
        build_date=action_raw["build_date"],
        description=action_raw["description"],
        lockfile=action_raw["lockfile"],
        has_dynamic_outputs=action_raw["has_dynamic_outputs"],
        categories=[
            ActionCategory(
                id=action_category_dict["id"],
                name=action_category_dict["name"],
            ),
            ActionCategory(
                id=action_category_2_dict["id"],
                name=action_category_2_dict["name"],
            ),
        ],
        project=action_raw["project"],
        addons=json.loads(action_raw["addons"]),
    )


def test_workflow_action_category(
    database_session: Session,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
    generate_workflow_action_category: Callable,
):
    workflow_raw: dict = generate_workflow(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        name="Workflow 1",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    workflow_action_raw = generate_workflow_action(
        id="82bd3d3b-cafe-419b-9d0b-c4c5943af151",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_raw["id"],
    )
    workflow_action_category_dict: dict = generate_workflow_action_category(
        id="8dcd2029-5ad6-4d9e-a6a1-7caea2946d4c",
        name="category_test",
        workflow_action_id=workflow_action_raw["id"],
    )
    workflow_action_category: WorkflowActionCategory = database_session.query(
        WorkflowActionCategory
    ).get(workflow_action_category_dict["id"])
    assert workflow_action_category == WorkflowActionCategory(
        id=workflow_action_category_dict["id"],
        name=workflow_action_category_dict["name"],
    )


def test_workflow_action_with_categories(
    database_session: Session,
    generate_workflow: Callable,
    generate_workflow_action: Callable,
    generate_workflow_action_category: Callable,
    generate_workflow_action_category_association: Callable,
):
    workflow_raw: dict = generate_workflow(
        id="7df4e921-ba92-4cf5-9336-c8d9b4ea397b",
        name="Workflow 1",
        validity_status=WorkflowValidityStatus.VALID,
        deployment_status=WorkflowDeploymentStatus.NONE,
        owner="owner_id",
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    workflow_action_raw: dict = generate_workflow_action(
        id="82bd3d3b-cafe-419b-9d0b-c4c5943af151",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR.value,
        has_dynamic_outputs=True,
        custom_name="Custom Name",
        position_x=0,
        position_y=0,
        validity_status=WorkflowActionValidityStatus.VALID.value,
        deployment_status=WorkflowActionDeployStatus.NONE.value,
        action_id="c50f8fae-e116-4db0-8c7b-d6a790a48ef4",
        workflow_id=workflow_raw["id"],
        addons={},
    )
    action_category_dict: dict = generate_workflow_action_category(
        id="8dcd2029-5ad6-4d9e-a6a1-7caea2946d4a",
        name="category_test",
        workflow_action_id=workflow_action_raw["id"],
    )
    action_category_2_dict: dict = generate_workflow_action_category(
        id="8dcd2029-5ad6-4d9e-a6a1-7caea2946d4b",
        name="category_test_2",
        workflow_action_id=workflow_action_raw["id"],
    )
    workflow_action: WorkflowAction = database_session.query(WorkflowAction).get(
        workflow_action_raw["id"]
    )
    assert workflow_action == WorkflowAction(
        id=workflow_action_raw["id"],
        name=workflow_action_raw["name"],
        technical_name=workflow_action_raw["technical_name"],
        description=workflow_action_raw["description"],
        version=workflow_action_raw["version"],
        kind=ActionKind(workflow_action_raw["kind"]),
        has_dynamic_outputs=workflow_action_raw["has_dynamic_outputs"],
        validity_status=WorkflowActionValidityStatus.VALID,
        deployment_status=WorkflowActionDeployStatus.NONE,
        custom_name=workflow_action_raw["custom_name"],
        position_x=workflow_action_raw["position_x"],
        position_y=workflow_action_raw["position_y"],
        action_id=workflow_action_raw["action_id"],
        categories=[
            WorkflowActionCategory(
                id=action_category_dict["id"],
                name=action_category_dict["name"],
            ),
            WorkflowActionCategory(
                id=action_category_2_dict["id"],
                name=action_category_2_dict["name"],
            ),
        ],
        workflow_id=workflow_raw["id"],
        addons=json.loads(workflow_action_raw["addons"]),
        objectives=None,
        constraints=None,
    )


def test_remove_action_categories_when_not_used(
    database_session: Session,
    generate_action: Callable,
    generate_action_category: Callable,
    generate_action_category_association: Callable,
):
    action_category_dict: dict = generate_action_category(
        id="8dcd2029-5ad6-4d9e-a6a1-7caea2946d4a",
        name="Category 1",
    )
    action_category_2_dict: dict = generate_action_category(
        id="8dcd2029-5ad6-4d9e-a6a1-7caea2946d4b",
        name="Category 2",
    )
    action_raw: dict = generate_action(
        id="8dcd2029-5ad6-4d9e-a6a1-7caea2946d4c",
        name="action 1",
        technical_name="action technical name",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=True,
        build_date=datetime.datetime.utcnow(),
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    action_raw_2: dict = generate_action(
        id="8dcd2029-5ad6-4d9e-a6a1-7caea2946d4d",
        name="action 2",
        technical_name="action technical name 2",
        description="action description",
        version="1.0",
        kind=ActionKind.PROCESSOR,
        has_dynamic_outputs=False,
        build_date=datetime.datetime.utcnow(),
        project="a65532f2-3110-4e0b-8f79-4b66db848d15",
    )
    generate_action_category_association(action_raw["id"], action_category_dict["id"])
    generate_action_category_association(action_raw["id"], action_category_2_dict["id"])
    generate_action_category_association(action_raw_2["id"], action_category_dict["id"])

    # CHECK CATEGORIES
    action_categories: [ActionCategory] = database_session.query(ActionCategory).all()
    assert action_categories == [
        ActionCategory(
            id=action_category_dict["id"],
            name=action_category_dict["name"],
        ),
        ActionCategory(
            id=action_category_2_dict["id"],
            name=action_category_2_dict["name"],
        ),
    ]

    # DELETE action 1. Expect action 1 and Category 2 to disappear
    action: Action = database_session.query(Action).get(action_raw["id"])
    database_session.delete(action)

    # CHECK actionS
    actions: [Action] = database_session.query(Action).all()
    assert actions == [
        Action(
            id=action_raw_2["id"],
            name=action_raw_2["name"],
            technical_name=action_raw_2["technical_name"],
            version=action_raw_2["version"],
            lockfile=action_raw_2["lockfile"],
            kind=ActionKind(action_raw_2["kind"]),
            build_date=action_raw_2["build_date"],
            description=action_raw_2["description"],
            has_dynamic_outputs=action_raw_2["has_dynamic_outputs"],
            categories=[
                ActionCategory(
                    id=action_category_dict["id"],
                    name=action_category_dict["name"],
                )
            ],
            project=action_raw_2["project"],
            addons=json.loads(action_raw_2["addons"]),
        )
    ]

    # CHECK CATEGORIES
    action_categories: [ActionCategory] = database_session.query(ActionCategory).all()
    assert action_categories == [
        ActionCategory(
            id=action_category_dict["id"],
            name=action_category_dict["name"],
        ),
    ]

    # DELETE action 2. Expect action 2 and Category 1 to disappear
    action: Action = database_session.query(Action).get(action_raw_2["id"])
    database_session.delete(action)

    # CHECK actionS
    actions: [Action] = database_session.query(Action).all()
    assert actions == []

    # CHECK CATEGORIES
    action_categories: [ActionCategory] = database_session.query(ActionCategory).all()
    assert action_categories == []


def test_workflow_action_list_is_sorted(
    database_session: Session,
):
    database_session.add(
        Action(
            id="test",
            project="toto",
            name="test",
            lockfile=b"",
            technical_name="test",
            version="1",
            kind=ActionKind.PROCESSOR,
            description="toto",
            build_date=datetime.datetime.utcnow(),
        )
    )
    database_session.add(
        Workflow(id="1234", description="", name="", owner="", project="")
    )
    database_session.commit()

    wf: Workflow = database_session.query(Workflow).get("1234")
    wfm1 = WorkflowAction(
        id="wfm1",
        kind=ActionKind.PROCESSOR,
        action_id="test",
        technical_name="",
        name="",
        description="",
        version="1",
        position_x=0,
        position_y=0,
    )
    wfm2 = WorkflowAction(
        id="wfm2",
        kind=ActionKind.PROCESSOR,
        action_id="test",
        technical_name="",
        name="",
        description="",
        version="1",
        position_x=1,
        position_y=1,
    )
    wfm3 = WorkflowAction(
        id="wfm3",
        kind=ActionKind.PROCESSOR,
        action_id="test",
        technical_name="",
        name="",
        description="",
        version="1",
        position_x=2,
        position_y=2,
    )
    wfm4 = WorkflowAction(
        id="wfm4",
        kind=ActionKind.PROCESSOR,
        action_id="test",
        technical_name="",
        name="",
        description="",
        version="1",
        position_x=3,
        position_y=3,
    )
    wf.actions.append(wfm2)
    wf.actions.append(wfm3)
    wf.actions.append(wfm4)
    wf.actions.append(wfm1)
    wf.add_action_link(wfm1.id, wfm2.id)
    wf.add_action_link(wfm2.id, wfm3.id)
    wf.add_action_link(wfm3.id, wfm4.id)
    database_session.commit()

    wf = database_session.query(Workflow).get("1234")
    assert wf.actions == [wfm1, wfm2, wfm3, wfm4]
