# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import asyncio
import functools
import io
import logging
import uuid
from typing import BinaryIO

from minio import Minio, error
from minio.commonconfig import CopySource

from ryax.studio.domain.services.filestore_service import IFilestoreService
from ryax.studio.domain.workflow.workflow_exceptions import (
    FilestoreEntryNotFoundException,
)
from ryax.studio.infrastructure.filestore.engine import FileStoreEngine

logger = logging.getLogger(__name__)


class FilestoreService(IFilestoreService):
    def __init__(self, engine: FileStoreEngine):
        self.connection: Minio | None = engine.connection
        self.bucket: str = engine.bucket
        self.buffer_size: int = 8 * 1024 * 1024  # 8MB

    def generate_file_path(self, file_name: str) -> str:
        return f"iodata/{uuid.uuid4()}/{file_name}"

    async def upload_file(self, file_path: str, file: bytes, file_size: int) -> None:
        logger.debug(f"Start uploading {file_path}. Size: {file_size}")
        loop = asyncio.get_running_loop()
        assert self.connection is not None
        await loop.run_in_executor(
            None,
            functools.partial(
                self.connection.put_object,
                self.bucket,
                file_path,
                io.BytesIO(file),
                file_size,
            ),
        )
        logger.debug(f"End uploading {file_path}. Size: {file_size}")

    async def write(self, file_path: str, stream: BinaryIO) -> None:
        logger.debug("Start Uploading stream to %s", file_path)
        loop = asyncio.get_running_loop()
        assert self.connection is not None
        result = await loop.run_in_executor(
            None,
            functools.partial(
                self.connection.put_object,
                self.bucket,
                file_path,
                stream,
                -1,
                part_size=self.buffer_size,
            ),
        )
        logger.debug(
            "End Uploading stream to %s. Uploaded file details: %s",
            file_path,
            str(result),
        )

    async def remove_file(self, file_path: str) -> None:
        try:
            loop = asyncio.get_running_loop()
            assert self.connection is not None
            await loop.run_in_executor(
                None,
                functools.partial(self.connection.stat_object, self.bucket, file_path),
            )
            await loop.run_in_executor(
                None,
                functools.partial(
                    self.connection.remove_object, self.bucket, file_path
                ),
            )
        except error.S3Error as err:
            logger.warning(
                "Error while deleting file at path %s, with error %s ignoring...",
                file_path,
                str(err),
            )

    async def get_file(self, file_path: str) -> bytes:
        try:
            logger.debug("Reading file to %s", file_path)
            loop = asyncio.get_running_loop()
            assert self.connection is not None
            data = await loop.run_in_executor(
                None,
                functools.partial(
                    self.connection.get_object,
                    self.bucket,
                    file_path,
                ),
            )
            return data.read()
        except error.S3Error:
            raise FilestoreEntryNotFoundException()

    async def copy_file(self, source_file: str, destination_path: str) -> None:
        logger.info(f"Copying {source_file} to {destination_path}")
        loop = asyncio.get_running_loop()
        assert self.connection is not None
        await loop.run_in_executor(
            None,
            functools.partial(
                self.connection.copy_object,
                self.bucket,
                destination_path,
                CopySource(self.bucket, source_file),
            ),
        )
