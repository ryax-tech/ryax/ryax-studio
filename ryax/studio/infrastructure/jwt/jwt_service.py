# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
from typing import Optional

import jwt

from ryax.studio.domain.auth.auth_token import AuthToken
from ryax.studio.domain.services import ISecurityService


class JwtService(ISecurityService):
    def __init__(self, secret_key: str):
        self.logger = logging.getLogger("AuthService")
        self.secret_key = secret_key
        self.algorithms = ["HS256"]

    def get_auth_token(self, token: str) -> Optional[AuthToken]:
        try:
            decoded_token = jwt.decode(
                jwt=token,
                key=self.secret_key,
                verify=True,
                algorithms=self.algorithms,
            )
            self.logger.debug("Token decoded - %s", decoded_token)
            return AuthToken(user_id=decoded_token["user_id"])
        except jwt.DecodeError as err:
            self.logger.warning("Token decoding failed - %s", err)
            return None
        except jwt.ExpiredSignatureError as err:
            self.logger.warning("Token expired - %s", err)
            return None
