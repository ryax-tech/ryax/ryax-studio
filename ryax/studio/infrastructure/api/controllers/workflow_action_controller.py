# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import tempfile
from pathlib import Path
from typing import IO, Optional, Union

from aiohttp import BodyPartReader, MultipartReader
from aiohttp.web_request import Request
from aiohttp.web_response import Response, json_response
from aiohttp_apispec import docs, request_schema, response_schema
from dependency_injector.wiring import Provide

from ryax.studio.application.workflow_action_service import WorkflowActionService
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action_exceptions import ActionNotFoundException
from ryax.studio.domain.action.action_values import ActionIOType, DynamicOutputOrigin
from ryax.studio.domain.workflow.entities.workflow_action_constraints import (
    WorkflowActionConstraints,
)
from ryax.studio.domain.workflow.entities.workflow_action_objectives import (
    WorkflowActionObjectives,
)
from ryax.studio.domain.workflow.workflow_exceptions import (
    ActionIOTypeInvalidException,
    DirectoryInputsMustBeZipfileException,
    DynamicOutputOriginInvalidException,
    FilestoreEntryNotFoundException,
    InvalidInputValueException,
    NotImplementedException,
    WorkflowActionDynamicOutputNotFoundException,
    WorkflowActionHasNoDynamicOutputsException,
    WorkflowActionInputNotFoundException,
    WorkflowActionLinkNotFoundException,
    WorkflowActionNotFoundException,
    WorkflowActionOutputNotFoundException,
    WorkflowFileAlreadyPresentException,
    WorkflowFileTooLargeException,
    WorkflowInputHasNoStaticFileException,
    WorkflowNotFoundException,
    WorkflowNotUpdatableException,
    WorkflowActionNonOptionalInputReferenceValueException,
    UnknownAddonParameterError,
)
from ryax.studio.domain.workflow.workflow_values import (
    AddWorkflowAction,
    AddWorkflowActionDynamicOutput,
    AddWorkflowActionLink,
    AddWorkflowActionWithLink,
    UpdateAllWorkflowActionIOData,
    UpdateWorkflowAction,
    UpdateWorkflowActionDynamicOutput,
    UpdateWorkflowActionInputValue,
    UpdateWorkflowActionLinks,
)
from ryax.studio.infrastructure.api.schemas.add_workflow_action_dynamic_output_schema import (
    AddWorkflowModuleDynamicOutputSchema,
)
from ryax.studio.infrastructure.api.schemas.add_workflow_action_link_schema import (
    AddWorkflowModuleLinkSchema,
)
from ryax.studio.infrastructure.api.schemas.add_workflow_action_schema import (
    AddWorkflowModuleSchema,
    AddWorkflowModuleWithLinkSchema,
)
from ryax.studio.infrastructure.api.schemas.change_action_version_schema import (
    ChangeModuleVersionRequestSchema,
    ChangeModuleVersionResponseSchema,
)
from ryax.studio.infrastructure.api.schemas.error_schema import ErrorSchema
from ryax.studio.infrastructure.api.schemas.workflow_action_objectives_schema import (
    WorkflowActionObjectivesSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_action_constraints_schema import (
    WorkflowActionConstraintsSchema,
)
from ryax.studio.infrastructure.api.schemas.update_workflow_action_dynamic_output_schema import (
    UpdateWorkflowModuleDynamicOutputSchema,
)
from ryax.studio.infrastructure.api.schemas.update_workflow_action_input_value_schema import (
    UpdateWorkflowModuleInputValueSchema,
)
from ryax.studio.infrastructure.api.schemas.update_workflow_action_io_schema import (
    UpdateAllWorkflowModuleIOSchema,
)
from ryax.studio.infrastructure.api.schemas.update_workflow_action_schema import (
    UpdateWorkflowModuleSchema,
)
from ryax.studio.infrastructure.api.schemas.update_workflow_actions_links_schema import (
    UpdateWorkflowModulesLinksSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_action_input_schema import (
    WorkflowModuleInputSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_action_output_schema import (
    WorkflowModuleOutputSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_action_schema import (
    WorkflowModuleExtendedSchema,
)


@docs(
    tags=["Workflow Actions"],
    summary="Add action to workflow",
    description="Add a action by ID to a workflow by ID",
    responses={
        201: {"description": "Workflow action added successfully"},
        404: {"description": "Workflow not found", "schema": ErrorSchema},
        400: {"description": "Workflow not updatable", "schema": ErrorSchema},
    },
)
@request_schema(AddWorkflowModuleSchema())
async def add_workflow_action(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        data = AddWorkflowAction(
            action_id=request["data"]["module_id"],
            custom_name=request["data"].get("custom_name"),
            position_x=request["data"].get("position_x"),
            position_y=request["data"].get("position_y"),
        )
        new_action_id = service.add_workflow_action(workflow_id=workflow_id, data=data)
        return json_response({"id": new_action_id}, status=201)
    except (WorkflowNotFoundException, ActionNotFoundException) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
    except (
        WorkflowNotUpdatableException,
        NotImplementedException,
        InvalidInputValueException,
        UnknownAddonParameterError,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Workflow Actions"],
    summary="Add action to workflow with a link",
    description="Add a action by ID to a workflow by ID and replace if needed.",
    responses={
        201: {"description": "Workflow action added successfully"},
        404: {"description": "Workflow data not found", "schema": ErrorSchema},
        400: {"description": "Workflow not updatable", "schema": ErrorSchema},
    },
)
@request_schema(AddWorkflowModuleWithLinkSchema())
@response_schema(WorkflowModuleExtendedSchema())
async def add_workflow_action_with_link(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        if (
            "replace_workflow_module_id" in request["data"]
            and "parent_workflow_module_id" in request["data"]
        ):
            raise InvalidInputValueException(
                "You can't specify a parent workflow action when replacing a workflow action"
            )
        workflow_id = request.match_info["workflow_id"]
        data = AddWorkflowActionWithLink(
            action_definition_id=request["data"]["module_definition_id"],
            parent_workflow_action_id=request["data"].get("parent_workflow_module_id"),
            replace_workflow_action_id=request["data"].get(
                "replace_workflow_module_id"
            ),
        )
        workflow_action_extended = service.add_workflow_action_with_link(
            workflow_id=workflow_id, data=data
        )
        return json_response(
            WorkflowModuleExtendedSchema().dump(workflow_action_extended),
            status=201,
        )
    except (
        WorkflowNotFoundException,
        ActionNotFoundException,
        WorkflowActionNotFoundException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
    except (
        WorkflowNotUpdatableException,
        NotImplementedException,
        InvalidInputValueException,
        UnknownAddonParameterError,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Workflow Actions"],
    summary="Update workflow action",
    description="Update the data of action in a workflow",
    responses={
        200: {"description": "Workflow action updated successfully"},
        400: {"description": "Workflow not updatable", "schema": ErrorSchema},
        404: {"description": "Workflow or action not found", "schema": ErrorSchema},
    },
)
@request_schema(UpdateWorkflowModuleSchema())
async def update_workflow_action(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_action_id = request.match_info["workflow_module_id"]
        data = UpdateWorkflowAction(**request["data"])
        service.update_workflow_action(
            workflow_id=workflow_id,
            workflow_action_id=workflow_action_id,
            workflow_action_data=data,
        )
        return json_response(None, status=200)
    except WorkflowNotUpdatableException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)
    except (WorkflowActionNotFoundException, WorkflowNotFoundException) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflow Actions"],
    summary="Delete action from workflow",
    description="Delete a action by ID from a workflow",
    responses={
        200: {"description": "Workflow action deleted successfully"},
        400: {"description": "Workflow not updatable", "schema": ErrorSchema},
        404: {"description": "Workflow or action not found", "schema": ErrorSchema},
    },
)
async def delete_workflow_action(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_action_id = request.match_info["workflow_module_id"]
        await service.delete_workflow_action(
            workflow_id=workflow_id, workflow_action_id=workflow_action_id
        )
        return json_response(None, status=200)
    except WorkflowNotUpdatableException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)
    except (WorkflowNotFoundException, WorkflowActionNotFoundException) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflow Actions Links"],
    summary="Add action link to workflow",
    description="Add a action link with input and output id to a workflow",
    responses={
        200: {"description": "Workflow action link added successfully"},
        404: {"description": "Workflow not found", "schema": ErrorSchema},
        400: {"description": "Workflow not updatable", "schema": ErrorSchema},
    },
)
@request_schema(AddWorkflowModuleLinkSchema())
async def add_workflow_action_link(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        data = AddWorkflowActionLink(
            upstream_action_id=request["data"]["input_module_id"],
            downstream_action_id=request["data"]["output_module_id"],
        )
        service.add_workflow_action_link(workflow_id=workflow_id, data=data)
        return json_response(None, status=200)
    except (WorkflowNotFoundException, WorkflowActionNotFoundException) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
    except WorkflowNotUpdatableException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Workflow Actions Links"],
    summary="Delete action link from workflow",
    description="Delete a action link by ID from a workflow",
    responses={
        200: {"description": "Action link deleted successfully"},
        400: {"description": "Workflow not updatable", "schema": ErrorSchema},
        404: {
            "description": "Workflow or action link not found",
            "schema": ErrorSchema,
        },
    },
)
async def delete_workflow_action_link(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_action_link_id = request.match_info["workflow_module_link_id"]
        service.delete_workflow_action_link(
            workflow_id=workflow_id, workflow_action_link_id=workflow_action_link_id
        )
        return json_response(None, status=200)
    except WorkflowNotUpdatableException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)
    except (WorkflowNotFoundException, WorkflowActionLinkNotFoundException) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflow Actions Inputs"],
    summary="Update workflow action input",
    description="Update the value of action input in a workflow",
    responses={
        200: {"description": "Workflow input updated successfully"},
        404: {
            "description": "Workflow or action or input not found",
            "schema": ErrorSchema,
        },
    },
)
@request_schema(UpdateWorkflowModuleInputValueSchema())
async def update_workflow_action_input(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_action_id = request.match_info["workflow_module_id"]
        workflow_input_id = request.match_info["workflow_input_id"]
        data = UpdateWorkflowActionInputValue(**request["data"])
        service.update_workflow_action_input(
            workflow_id, workflow_action_id, workflow_input_id, data
        )
        return json_response(None, status=200)
    except (InvalidInputValueException, NotImplementedException) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)
    except (
        WorkflowActionNotFoundException,
        WorkflowNotFoundException,
        WorkflowActionInputNotFoundException,
        WorkflowActionNonOptionalInputReferenceValueException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflow Actions Inputs"],
    summary="Delete workflow action input file",
    description="Delete the static file of action input in a workflow",
    responses={
        200: {"description": "Workflow input file deleted successfully"},
        400: {"description": "Workflow not updatable", "schema": ErrorSchema},
        404: {
            "description": "Workflow or action or input or file not found",
            "schema": ErrorSchema,
        },
    },
)
async def delete_workflow_input_static_file(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_action_id = request.match_info["workflow_module_id"]
        workflow_input_id = request.match_info["workflow_input_id"]
        await service.delete_workflow_action_input_file(
            workflow_id, workflow_action_id, workflow_input_id
        )
        return json_response(None, status=200)
    except (
        WorkflowActionNotFoundException,
        WorkflowNotFoundException,
        WorkflowActionInputNotFoundException,
        WorkflowInputHasNoStaticFileException,
        FilestoreEntryNotFoundException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
    except WorkflowNotUpdatableException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Workflow Actions Outputs"],
    summary="Add output to workflow action",
    description="Create a dynamic output for a action in a workflow action",
    responses={
        200: {"description": "Dynamic output created successfully"},
        404: {
            "description": "Workflow or action not found",
            "schema": ErrorSchema,
        },
    },
)
@request_schema(AddWorkflowModuleDynamicOutputSchema())
async def add_workflow_dynamic_output(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_action_id = request.match_info["workflow_module_id"]
        data = AddWorkflowActionDynamicOutput(**request["data"])
        service.add_workflow_action_dynamic_output(
            workflow_id, workflow_action_id, data
        )
        return json_response(None, status=201)
    except (
        WorkflowActionNotFoundException,
        WorkflowNotFoundException,
        WorkflowActionHasNoDynamicOutputsException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflow Actions Outputs"],
    summary="Update output in workflow action",
    description="Update a dynamic output for a action in a workflow action",
    responses={
        200: {"description": "Dynamic output updated successfully"},
        404: {
            "description": "Workflow or action or dynamic output not found",
            "schema": ErrorSchema,
        },
    },
)
@request_schema(UpdateWorkflowModuleDynamicOutputSchema())
async def update_workflow_dynamic_output(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_action_id = request.match_info["workflow_module_id"]
        workflow_action_dynamic_output_id = request.match_info[
            "workflow_dynamic_output_id"
        ]
        data = UpdateWorkflowActionDynamicOutput(**request["data"])
        service.update_workflow_action_dynamic_output(
            workflow_id, workflow_action_id, workflow_action_dynamic_output_id, data
        )
        return json_response(None, status=200)
    except (
        WorkflowActionNotFoundException,
        WorkflowNotFoundException,
        WorkflowActionDynamicOutputNotFoundException,
        WorkflowActionHasNoDynamicOutputsException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflow Actions Outputs"],
    summary="Delete output in workflow action",
    description="Delete a dynamic output from workflow action",
    responses={
        200: {"description": "Dynamic output deleted successfully"},
        404: {
            "description": "Workflow or action or dynamic output not found",
            "schema": ErrorSchema,
        },
    },
)
async def delete_workflow_dynamic_output(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_action_id = request.match_info["workflow_module_id"]
        workflow_action_dynamic_output_id = request.match_info[
            "workflow_dynamic_output_id"
        ]
        service.delete_workflow_action_dynamic_output(
            workflow_id, workflow_action_id, workflow_action_dynamic_output_id
        )
        return json_response(None, status=200)
    except (
        WorkflowActionNotFoundException,
        WorkflowNotFoundException,
        WorkflowActionDynamicOutputNotFoundException,
        WorkflowActionHasNoDynamicOutputsException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflow Actions Inputs"],
    summary="List all workflows action inputs",
    description="List all inputs available in a workflow action",
)
@response_schema(
    WorkflowModuleInputSchema(many=True),
    code=200,
    description="Workflow action inputs fetched successfully",
)
@response_schema(
    ErrorSchema,
    code=404,
    description="Workflow action inputs not found",
)
async def list_workflow_action_inputs(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_action_id = request.match_info["workflow_module_id"]
        workflow_action_inputs = service.list_workflow_action_inputs(
            workflow_id, workflow_action_id
        )
        result = WorkflowModuleInputSchema().dump(workflow_action_inputs, many=True)
        return json_response(result, status=200)
    except (
        WorkflowActionNotFoundException,
        WorkflowNotFoundException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflow Actions Outputs"],
    summary="List all workflows action outputs",
    description="List all outputs available in a workflow action",
    responses={
        200: {
            "description": "Workflow action outputs fetched successfully",
            "schema": WorkflowModuleOutputSchema(many=True),
        },
        404: {"description": "Workflow action output not found", "schema": ErrorSchema},
    },
)
async def list_workflow_action_outputs(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_action_id = request.match_info["workflow_module_id"]
        outputs = service.list_workflow_action_outputs(workflow_id, workflow_action_id)
        result = WorkflowModuleOutputSchema().dump(outputs, many=True)
        return json_response(result, status=200)
    except (
        WorkflowActionNotFoundException,
        WorkflowNotFoundException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflow Action Outputs"],
    summary="Search in all action outputs available in workflow",
    description="Search in all action outputs available in a workflow",
    parameters=[
        {
            "in": "query",
            "name": "with_type",
            "type": "string",
            # TODO: OAS3 format
            # "schema": {"type": "string"},
            "description": "Filter workflow actions outputs by type",
        },
        {
            "in": "query",
            "name": "accessible_from",
            "type": "string",
            # TODO: OAS3 format
            # "schema": {"type": "string"},
            "description": "Filter workflow actions outputs accessible from specified workflow action id",
        },
    ],
    responses={
        200: {
            "description": "Workflow action outputs fetched successfully",
            "schema": WorkflowModuleOutputSchema(many=True),
        },
        404: {"description": "Workflow not found", "schema": ErrorSchema},
    },
)
async def search_workflow_action_outputs(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        with_type_filter = request.query.get("with_type")
        accessible_from_filter = request.query.get("accessible_from")
        workflow_action_outputs_views = service.search_workflow_action_outputs(
            workflow_id,
            with_type=with_type_filter,
            accessible_from=accessible_from_filter,
        )
        result = WorkflowModuleOutputSchema().dump(
            workflow_action_outputs_views, many=True
        )
        return json_response(result, status=200)
    except WorkflowNotFoundException as err:
        workflow_action_outputs_views = ErrorSchema().dump({"error": err.message})
        return json_response(workflow_action_outputs_views, status=404)


async def _copy_uploaded_file(
    to_read: BodyPartReader,
    to_write: IO[bytes],
    max_upload_size: int,
) -> int:
    total_size = 0
    while True:
        chunk = await to_read.read_chunk()
        if not chunk:
            break
        to_write.write(chunk)
        total_size += len(chunk)
        if total_size > max_upload_size:
            raise WorkflowFileTooLargeException(
                f"The uploaded file is too big: {total_size}. Maximum upload size is {max_upload_size}"
            )
    to_write.seek(0)
    return total_size


@docs(
    tags=["Workflow Actions Static File Input"],
    summary="Update workflow action input with static file",
    description="Update the value of action input in a workflow wihth a static file",
    responses={
        201: {"description": "Static file uploaded successfully"},
        400: {
            "description": "Error while importing the selected file",
            "schema": ErrorSchema,
        },
    },
    consumes=["multipart/form-data"],
    parameters=[
        {
            "in": "formData",
            "name": "file",
            "type": "file",
            "description": "The file to use as input.",
            "required": "true",
        },
    ],
)
async def add_workflow_static_file_input(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
    max_upload_size: int = Provide[ApplicationContainer.configuration.max_upload_size],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_action_id = request.match_info["module_id"]
        workflow_action_input_id = request.match_info["workflow_input_id"]

        reader: MultipartReader = await request.multipart()
        part: Optional[Union[MultipartReader, BodyPartReader]] = await reader.next()
        if not isinstance(part, BodyPartReader):
            raise InvalidInputValueException

        file: BodyPartReader = part
        filename = file.filename
        if filename is None:
            raise InvalidInputValueException

        filename, extension = Path(filename).stem, Path(filename).suffix
        extension = extension[1:] if extension.startswith(".") else extension

        with tempfile.TemporaryFile() as tmp_file:
            file_size = await _copy_uploaded_file(file, tmp_file, max_upload_size)

            await service.upload_static_file_input(
                workflow_id,
                workflow_action_id,
                workflow_action_input_id,
                filename,
                extension,
                tmp_file,
                file_size,
            )
        return json_response(None, status=201)
    except (
        InvalidInputValueException,
        NotImplementedException,
        WorkflowFileTooLargeException,
        WorkflowInputHasNoStaticFileException,
        WorkflowFileAlreadyPresentException,
        DirectoryInputsMustBeZipfileException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)
    except (
        WorkflowActionNotFoundException,
        WorkflowNotFoundException,
        WorkflowActionInputNotFoundException,
        WorkflowNotUpdatableException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflow Actions"],
    summary="Update workflow action version",
    description="Update the version of action in a workflow",
    responses={
        200: {
            "description": "Workflow action version changed successfully",
            "schema": ChangeModuleVersionResponseSchema,
        },
        404: {
            "description": "Workflow or action not found",
            "schema": ErrorSchema,
        },
    },
)
@request_schema(ChangeModuleVersionRequestSchema())
async def change_action_version(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_action_id = request.match_info["workflow_module_id"]
        action_id = request["data"]["module_id"]
        (
            new_workflow_action_id,
            removed_results,
        ) = service.change_workflow_action_version(
            workflow_id, workflow_action_id, action_id
        )
        result = ChangeModuleVersionResponseSchema().dump(
            {
                "workflow_module_id": new_workflow_action_id,
                "removed_results": removed_results,
            }
        )
        return json_response(result, status=200)
    except (
        WorkflowNotFoundException,
        WorkflowActionNotFoundException,
        ActionNotFoundException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
    except (WorkflowNotUpdatableException,) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Workflows"],
    summary="Update workflow links",
    description="For each given action, redo all its links.",
    responses={
        200: {"description": "Workflow links updated successfully"},
        404: {"description": "Workflow not found", "schema": ErrorSchema},
        400: {"description": "Workflow not updatable", "schema": ErrorSchema},
    },
)
@request_schema(UpdateWorkflowModulesLinksSchema())
async def update_workflow_actions_links(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info.get("workflow_id")
        if workflow_id is None:
            raise InvalidInputValueException("Missing workflow id.")
        links = request["data"]["links"]
        for link in links:
            service.update_workflow_action_links(
                workflow_id,
                UpdateWorkflowActionLinks(
                    action_id=link["module_id"],
                    next_actions_ids=link["next_modules_ids"],
                ),
            )
        return json_response(None, status=200)
    except (
        WorkflowNotFoundException,
        WorkflowActionNotFoundException,
        WorkflowActionLinkNotFoundException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
    except InvalidInputValueException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Workflow Actions IO"],
    summary="Update all IO of workflow action",
    description="Update all the inputs and all dynamic outputs of a workflow action",
)
@request_schema(UpdateAllWorkflowModuleIOSchema())
@response_schema(ErrorSchema, code=400, description="Workflow Action IO update error")
async def update_all_workflow_action_io(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_action_id = request.match_info["workflow_module_id"]
        data = UpdateAllWorkflowActionIOData(
            custom_name=request["data"].get("custom_name"),
            inputs=[
                UpdateAllWorkflowActionIOData.InputValue(
                    id=input_data["id"],
                    static_value=input_data.get("static_value"),
                    reference_value=input_data.get("reference_value"),
                    project_variable_value=input_data.get("project_variable_value"),
                )
                for input_data in request["data"]["inputs"]
            ]
            if request["data"].get("inputs")
            else None,
            addons_inputs=[
                UpdateAllWorkflowActionIOData.InputValue(
                    id=addon_input_data["id"],
                    static_value=addon_input_data.get("static_value"),
                    reference_value=addon_input_data.get("reference_value"),
                    project_variable_value=addon_input_data.get(
                        "project_variable_value"
                    ),
                )
                for addon_input_data in request["data"]["addons_inputs"]
            ]
            if request["data"].get("addons_inputs")
            else None,
            dynamic_outputs=[
                UpdateAllWorkflowActionIOData.DynamicOutputDefinition(
                    id=output_data["id"],
                    technical_name=output_data["technical_name"],
                    display_name=output_data["display_name"],
                    help=output_data["help"],
                    type=ActionIOType.string_to_enum(output_data["type"]),
                    origin=DynamicOutputOrigin.string_to_enum(output_data["origin"])
                    if output_data["origin"] is not None
                    else None,
                    enum_values=output_data.get("enum_values"),
                    optional=output_data.get("optional", False),
                )
                for output_data in request["data"]["dynamic_outputs"]
            ]
            if request["data"].get("dynamic_outputs")
            else None,
        )
        service.update_all_workflow_action_io(
            workflow_id=workflow_id, workflow_action_id=workflow_action_id, data=data
        )
        return json_response(None, status=200)
    except (
        WorkflowActionNotFoundException,
        WorkflowNotFoundException,
        WorkflowActionOutputNotFoundException,
        InvalidInputValueException,
        WorkflowActionHasNoDynamicOutputsException,
        ActionIOTypeInvalidException,
        DynamicOutputOriginInvalidException,
        WorkflowActionNonOptionalInputReferenceValueException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Workflow Actions"],
    summary="Update workflow action constraints",
    description="Update the action deployment constraints",
    responses={
        404: {
            "description": "Workflow or action not found",
            "schema": ErrorSchema,
        },
    },
)
@request_schema(WorkflowActionConstraintsSchema())
async def set_constraints(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_action_id = request.match_info["workflow_action_id"]
        service.set_workflow_action_constraints(
            workflow_id,
            workflow_action_id,
            WorkflowActionConstraints(
                site_list=request["data"]["site_list"],
                site_type_list=request["data"]["site_type_list"],
                arch_list=request["data"]["arch_list"],
                node_pool_list=request["data"]["node_pool_list"],
            ),
        )
        return json_response()
    except (
        WorkflowNotFoundException,
        WorkflowActionNotFoundException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
    except WorkflowNotUpdatableException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Workflow Actions"],
    summary="Update workflow action objectives",
    description="Update the action deployment objectives",
    responses={
        404: {
            "description": "Workflow or action not found",
            "schema": ErrorSchema,
        },
    },
)
@request_schema(WorkflowActionObjectivesSchema())
async def set_objectives(
    request: Request,
    service: WorkflowActionService = Provide[
        ApplicationContainer.workflow_action_service
    ],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_action_id = request.match_info["workflow_action_id"]
        service.set_workflow_action_objectives(
            workflow_id,
            workflow_action_id,
            WorkflowActionObjectives(
                energy=request["data"]["energy"],
                cost=request["data"]["cost"],
                performance=request["data"]["performance"],
            ),
        )
        return json_response()
    except (
        WorkflowNotFoundException,
        WorkflowActionNotFoundException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
    except WorkflowNotUpdatableException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)
