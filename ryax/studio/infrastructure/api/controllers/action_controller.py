# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from aiohttp.web_request import Request
from aiohttp.web_response import Response, json_response
from aiohttp_apispec import docs
from dependency_injector.wiring import Provide

from ryax.studio.application.action_service import ActionService
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action_exceptions import (
    ActionLogoNotFoundException,
    ActionNotDeletableException,
    ActionNotFoundException,
)
from ryax.studio.domain.action.action_values import ActionErrorCode
from ryax.studio.infrastructure.api.middlewares import authentication_middleware
from ryax.studio.infrastructure.api.schemas.action_category_schema import (
    ModuleCategorySchema,
)
from ryax.studio.infrastructure.api.schemas.error_schema import (
    ErrorSchema,
    ModuleDeleteErrorSchema,
)
from ryax.studio.infrastructure.api.schemas.module_schema import (
    ModuleDetailsSchema,
    ModuleSchema,
)
from ryax.studio.infrastructure.api.schemas.module_versions_schema import (
    ModuleVersionsSchema,
)


@docs(
    tags=["Actions"],
    summary="List all actions",
    description="List all actions available",
    parameters=[
        {
            "in": "query",
            "name": "search",
            "type": "string",
            # TODO: OAS3 format
            # "schema": {"type": "string"},
        },
        {
            "in": "query",
            "name": "category",
            "type": "string",
        },
    ],
    responses={
        200: {
            "description": "actions fetched successfully",
            "schema": ModuleSchema(many=True),
        },
    },
)
async def list_actions(
    request: Request,
    service: ActionService = Provide[ApplicationContainer.action_service],
) -> Response:
    current_project = request.get("current_project_id", None)
    search = request.rel_url.query.get("search", "")
    category = request.rel_url.query.get("category")
    actions = service.get_actions(current_project, search=search, category=category)
    result = ModuleSchema().dump(actions, many=True)
    return json_response(result, status=200)


@docs(
    tags=["Actions"],
    summary="Get one action",
    description="Get the requested action by ID",
    responses={
        200: {
            "description": "Action fetched successfully",
            "schema": ModuleDetailsSchema,
        },
        404: {"description": "Action not found", "schema": ErrorSchema},
    },
)
async def get_action(
    request: Request,
    service: ActionService = Provide[ApplicationContainer.action_service],
) -> Response:
    try:
        action_id = request.match_info["module_id"]
        action = service.get_action_extended_view(action_id)
        result = ModuleDetailsSchema().dump(action)
        return json_response(result, status=200)
    except ActionNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Actions"],
    summary="Get action versions",
    description="Get the versions of the requested action by ID",
    responses={
        200: {
            "description": "Action versions fetched successfully",
            "schema": ModuleVersionsSchema(many=True),
        },
        404: {"description": "Action not found", "schema": ErrorSchema},
    },
)
async def list_action_versions(
    request: Request,
    service: ActionService = Provide[ApplicationContainer.action_service],
) -> Response:
    try:
        action_id = request.match_info["module_id"]
        action_versions = service.list_action_versions(action_id)
        result = ModuleVersionsSchema().dump(action_versions, many=True)
        return json_response(result, status=200)
    except ActionNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Actions"],
    summary="Get one logo",
    description="Get the requested logo by action ID",
    responses={
        200: {"description": "Logo fetched successfully"},
        404: {"description": "Logo not found", "schema": ErrorSchema},
    },
)
@authentication_middleware.skip()
async def get_logo(
    request: Request,
    service: ActionService = Provide[ApplicationContainer.action_service],
) -> Response:
    try:
        action_id = request.match_info["module_id"]
        logo = service.get_logo(action_id)
        return Response(body=logo.content, content_type=f"image/{logo.extension}")
    except ActionLogoNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Actions"],
    summary="Delete a action",
    description="Delete the requested action by ID",
    responses={
        200: {"description": "Workflow fetched successfully"},
        400: {
            "description": "Action not deletable",
            "schema": ModuleDeleteErrorSchema,
        },
        404: {"description": "Action not found", "schema": ErrorSchema},
    },
)
async def delete_action(
    request: Request,
    service: ActionService = Provide[ApplicationContainer.action_service],
) -> Response:
    try:
        action_id = request.match_info["module_id"]
        await service.delete_action(action_id)
        return json_response(None, status=200)
    except ActionNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
    except ActionNotDeletableException as err:
        result = ModuleDeleteErrorSchema().dump(
            {
                "error": err.message,
                "workflows": err.workflows,
                "code": ActionErrorCode.ACTION_NOT_DELETABLE,
            }
        )
        return json_response(result, status=400)


@docs(
    tags=["Actions"],
    summary="List all action categories",
    description="List all available actions categories",
    responses={
        200: {
            "description": "Categories fetched successfully",
            "schema": ModuleCategorySchema(many=True),
        },
    },
)
async def list_categories(
    request: Request,
    service: ActionService = Provide[ApplicationContainer.action_service],
) -> Response:
    categories = service.list_categories()
    result = ModuleCategorySchema().dump(categories, many=True)
    return json_response(result, status=200)
