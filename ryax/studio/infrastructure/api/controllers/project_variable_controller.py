# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
import tempfile
from pathlib import Path
from typing import IO, Optional, Tuple, Union

from aiohttp import BodyPartReader, MultipartReader
from aiohttp.web_request import Request
from aiohttp.web_response import Response, json_response
from aiohttp_apispec import docs, request_schema
from dependency_injector.wiring import Provide

from ryax.studio.application.project_variable_service import ProjectVariableService
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.project_variables.project_variable_entities import (
    ProjectVariableTransactionData,
)
from ryax.studio.domain.project_variables.project_variable_exceptions import (
    ProjectVariableFileInvalidException,
    ProjectVariableFileTooLargeException,
    ProjectVariableIsInUseError,
    ProjectVariableNotFoundException,
)
from ryax.studio.domain.workflow.workflow_exceptions import (
    ActionIOTypeInvalidException,
    FilestoreEntryNotFoundException,
)
from ryax.studio.infrastructure.api.schemas.error_schema import ErrorSchema
from ryax.studio.infrastructure.api.schemas.project_variable_schema import (
    ProjectVariableTransactionSchema,
    ProjetcVariablesSchema,
)

logger = logging.getLogger(__name__)


@docs(
    tags=["Project Variables"],
    summary="List all project variables",
    parameters=[{"in": "query", "name": "type", "type": "string"}],
    responses={
        200: {
            "description": "Project Variables fetched successfully",
            "schema": ProjetcVariablesSchema(many=True),
            400: {"description": "Type filter is invalid", "schema": ErrorSchema},
        },
    },
)
async def list_project_variables(
    request: Request,
    service: ProjectVariableService = Provide[
        ApplicationContainer.project_variable_service
    ],
) -> Response:
    current_project = request["current_project_id"]
    type_search = request.rel_url.query.get("type")
    try:
        objects = service.list(current_project, type_search=type_search)
        result = ProjetcVariablesSchema().dump(objects, many=True)
        return json_response(result, status=200)
    except ActionIOTypeInvalidException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Project Variables"],
    summary="Get one project variable object",
    responses={
        200: {
            "description": "Project Variable fetched successfully",
            "schema": ProjetcVariablesSchema(),
        },
        404: {"description": "User object not found", "schema": ErrorSchema},
    },
)
async def get_project_variable(
    request: Request,
    service: ProjectVariableService = Provide[
        ApplicationContainer.project_variable_service
    ],
) -> Response:
    current_project = request["current_project_id"]
    project_variable_id = request.match_info["project_variable_id"]
    try:
        variable = service.get(project_variable_id, current_project)
        result = ProjetcVariablesSchema().dump(variable)
        return json_response(result, status=200)

    except ProjectVariableNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Project Variables"],
    summary="Add Project Variable",
    responses={
        201: {"description": "Project Variable added successfully"},
    },
)
@request_schema(ProjectVariableTransactionSchema())
async def add_project_variable(
    request: Request,
    service: ProjectVariableService = Provide[
        ApplicationContainer.project_variable_service
    ],
) -> Response:
    current_project = request["current_project_id"]
    data = ProjectVariableTransactionData(
        name=request["data"]["name"],
        value=request["data"]["value"],
        type=request["data"]["type"],
        project_id=current_project,
        description=request["data"].get("description"),
    )
    new_object_id = service.add(data)
    return json_response({"id": new_object_id}, status=201)


@docs(
    tags=["Project Variables"],
    summary="Delete one project variable",
    responses={
        200: {
            "description": "Project Variable deleted successfully",
        },
        404: {"description": "Project variable not found", "schema": ErrorSchema},
        400: {"description": "Project variable not found", "schema": ErrorSchema},
    },
)
async def delete_project_variable(
    request: Request,
    service: ProjectVariableService = Provide[
        ApplicationContainer.project_variable_service
    ],
) -> Response:
    current_project = request["current_project_id"]
    project_variable_id = request.match_info["project_variable_id"]
    try:
        await service.delete(project_variable_id, current_project)
        return json_response(None, status=200)

    except ProjectVariableIsInUseError as err:
        result = {
            "error": err.message,
            "workflows": [
                {"name": workflow.name, "id": workflow.id} for workflow in err.workflows
            ],
        }
        return json_response(result, status=400)
    except (ProjectVariableNotFoundException, FilestoreEntryNotFoundException) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Project Variables"],
    summary="Update a Project Variable",
    responses={
        200: {"description": "Project Variable updated successfully"},
    },
)
@request_schema(ProjectVariableTransactionSchema())
async def update_project_variable(
    request: Request,
    service: ProjectVariableService = Provide[
        ApplicationContainer.project_variable_service
    ],
) -> Response:
    project_variable_id = request.match_info["project_variable_id"]
    current_project = request["current_project_id"]
    data = ProjectVariableTransactionData(
        name=request["data"]["name"],
        value=request["data"]["value"],
        type=request["data"]["type"],
        project_id=current_project,
        description=request["data"].get("description"),
        file_path=None,
    )
    try:
        await service.update(project_variable_id, data, current_project)
        return json_response(None, status=200)
    except ProjectVariableIsInUseError as err:
        result = {
            "error": err.message,
            "workflows": [
                {"name": workflow.name, "id": workflow.id} for workflow in err.workflows
            ],
        }
        return json_response(result, status=400)
    except ProjectVariableNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


async def _read_data(
    request: Request,
    file_io: IO,
    max_upload_size: int = Provide[ApplicationContainer.configuration.max_upload_size],
) -> Tuple[dict, Optional[str], Optional[str], int]:
    metadata: dict = {}
    filename: Optional[str] = None
    extension: Optional[str] = None
    file_size = 0

    reader: MultipartReader = await request.multipart()
    part: Optional[Union["MultipartReader", BodyPartReader]]
    async for part in reader:
        if not isinstance(part, BodyPartReader):
            raise ProjectVariableFileInvalidException
        logger.debug("Field found: %s", part)
        if part.name != "file":
            metadata[part.name] = await part.text()
        else:
            file: BodyPartReader = part
            filename = file.filename
            if filename is None:
                raise ProjectVariableFileInvalidException

            filename, extension = Path(filename).stem, Path(filename).suffix
            extension = extension[1:] if extension.startswith(".") else extension
            while True:
                if file_size > max_upload_size:
                    raise ProjectVariableFileTooLargeException()
                content_part = await file.read_chunk()
                if not content_part:
                    break
                file_size += len(content_part)
                file_io.write(content_part)
    file_io.seek(0)
    return metadata, filename, extension, file_size


@docs(
    tags=["Project Variables"],
    summary="Upload file as project variable value",
    responses={
        201: {"description": "Variable created successfully"},
        400: {
            "description": "Error while importing the selected file",
            "schema": ErrorSchema,
        },
    },
    consumes=["multipart/form-data"],
    parameters=[
        {
            "in": "formData",
            "name": "file",
            "type": "file",
            "description": "The file to use as input.",
            "required": "true",
        },
        {
            "in": "formData",
            "name": "name",
            "type": "string",
            "description": "the variable name",
            "required": "true",
        },
        {
            "in": "formData",
            "name": "type",
            "type": "string",
            "description": "the variable type (file or directory)",
            "required": "true",
        },
        {
            "in": "formData",
            "name": "description",
            "type": "string",
            "description": "the variable description",
            "required": "false",
        },
    ],
)
async def add_project_variable_filetype(
    request: Request,
    service: ProjectVariableService = Provide[
        ApplicationContainer.project_variable_service
    ],
) -> Response:
    try:
        current_project = request["current_project_id"]

        with tempfile.TemporaryFile() as file_io:
            metadata, filename, extension, file_size = await _read_data(
                request, file_io
            )

            data = ProjectVariableTransactionData(
                name=metadata["name"],
                type=metadata["type"],
                description=metadata.get("description"),
                project_id=current_project,
                value=None,
            )
            if filename is None or extension is None or file_size == 0:
                raise ProjectVariableFileInvalidException
            new_object_id = await service.add_with_file(
                filename, extension, file_io, data
            )
        return json_response({"id": new_object_id}, status=201)
    except (
        ProjectVariableFileInvalidException,
        ProjectVariableFileTooLargeException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Project Variables"],
    summary="Update file project variable value",
    responses={
        201: {"description": "Variable updated successfully"},
        400: {
            "description": "Error while importing the selected file",
            "schema": ErrorSchema,
        },
    },
    consumes=["multipart/form-data"],
    parameters=[
        {
            "in": "formData",
            "name": "file",
            "type": "file",
            "description": "The file to use as input.",
            "required": "true",
        },
        {
            "in": "formData",
            "name": "name",
            "type": "string",
            "description": "the variable name",
            "required": "true",
        },
        {
            "in": "formData",
            "name": "type",
            "type": "string",
            "description": "the variable type (file or directory)",
            "required": "true",
        },
        {
            "in": "formData",
            "name": "description",
            "type": "string",
            "description": "the variable description",
            "required": "false",
        },
    ],
)
async def update_project_variable_filetype(
    request: Request,
    service: ProjectVariableService = Provide[
        ApplicationContainer.project_variable_service
    ],
) -> Response:
    try:
        current_project = request["current_project_id"]
        project_variable_id = request.match_info["project_variable_id"]

        with tempfile.TemporaryFile() as file_io:
            metadata, filename, extension, file_content = await _read_data(
                request, file_io
            )

            data = ProjectVariableTransactionData(
                name=metadata["name"],
                type=metadata["type"],
                description=metadata.get("description"),
                project_id=current_project,
                value=None,
            )
            if filename is None or extension is None or file_content is None:
                raise ProjectVariableFileInvalidException
            await service.update_with_file(
                filename, extension, file_io, data, project_variable_id
            )
        return json_response({"id": project_variable_id}, status=200)
    except ProjectVariableIsInUseError as err:
        result = {
            "error": err.message,
            "workflows": [
                {"name": workflow.name, "id": workflow.id} for workflow in err.workflows
            ],
        }
        return json_response(result, status=400)
    except (
        ProjectVariableFileInvalidException,
        ProjectVariableFileTooLargeException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)
