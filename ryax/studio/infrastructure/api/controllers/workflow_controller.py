# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import Optional, Union

from aiohttp import BodyPartReader, MultipartReader
from aiohttp.web_fileresponse import FileResponse
from aiohttp.web_request import Request
from aiohttp.web_response import Response, json_response
from aiohttp_apispec import docs, request_schema, response_schema
from dependency_injector.wiring import Provide

from ryax.studio.application.workflow_service import WorkflowService
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action_exceptions import ActionNotFoundException
from ryax.studio.domain.workflow.entities.workflow_result import WorkflowResultType
from ryax.studio.domain.workflow.workflow_exceptions import (
    EndpointAlreadyExistsException,
    FilestoreEntryNotFoundException,
    InvalidUserDefinedEndpointException,
    NotAllKeysAreUniqueInResults,
    SchemaValidWorkflowInvalidException,
    WorkflowActionIONotFoundException,
    WorkflowBadZipFile,
    WorkflowHasNoTriggerException,
    WorkflowInvalidSchemaException,
    WorkflowNotDeployable,
    WorkflowNotFoundException,
    WorkflowNotValidException,
    WorkflowParsingYamlException,
    WorkflowSchemaFileTooLargeException,
    WorkflowYamlNotFound,
    WrongStatusFilterException,
)
from ryax.studio.domain.workflow.workflow_values import (
    CopyWorkflowData,
    CreateWorkflowData,
    CreateWorkflowResultData,
    WorkflowInformations,
)
from ryax.studio.infrastructure.api.schemas.add_workflow_schema import (
    AddWorkflowRequestSchema,
    AddWorkflowResponseSchema,
)
from ryax.studio.infrastructure.api.schemas.error_schema import ErrorSchema
from ryax.studio.infrastructure.api.schemas.update_workflow_schema import (
    UpdateWorkflowRequestSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_error_schema import (
    WorkflowErrorSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_result_schema import (
    AddManyWorkflowResultRequestSchema,
    AddWorkflowResultResponseSchema,
    WorkflowResultConfigurationSchema,
    WorkflowResultSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_schema import (
    WorkflowDetailsSchema,
    WorkflowEndpointSchema,
    WorkflowExtendedSchema,
    WorkflowSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_validation_schema import (
    WorkflowValidationSchema,
)


@docs(
    tags=["Workflows"],
    summary="List all workflows",
    description="List all workflows available",
    parameters=[
        {
            "in": "query",
            "name": "search",
            "type": "string",
            # TODO: OAS3 format
            # "schema": {"type": "string"},
        },
        {
            "in": "query",
            "name": "deployment_status",
            "type": "string",
        },
    ],
    responses={
        200: {
            "description": "Workflows fetched successfully",
            "schema": WorkflowSchema(many=True),
        },
        400: {
            "description": "Deployment status filter is incorrect",
            "schema": ErrorSchema,
        },
    },
)
async def list_workflows(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    current_project = request.get("current_project_id", None)
    search = request.rel_url.query.get("search")
    deployment_status_filter = request.rel_url.query.get("deployment_status")
    try:
        workflows = service.list_workflows(
            current_project,
            search=search,
            deployment_status_filter_value=deployment_status_filter,
        )
        result = WorkflowSchema().dump(workflows, many=True)
        return json_response(result, status=200)
    except WrongStatusFilterException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


# WARN: DEPRECATED in favor of get_workflow_results_configuration
# TODO: Remove in next version
@docs(
    tags=["Workflows"],
    summary="Get workflow results",
    description="Get all workflow results",
)
@response_schema(WorkflowResultSchema(many=True), code=200)
@response_schema(ErrorSchema, code=400, description="Workflow result get error")
async def get_workflow_results(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    workflow_id = request.match_info["workflow_id"]
    try:
        workflow_results = service.list_workflow_results(workflow_id)
        result = WorkflowResultSchema().dump(workflow_results, many=True)
        return json_response(result, status=200)
    except WorkflowNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Workflows"],
    summary="Get workflow results configuration",
    description="Get all workflow results configuration",
)
@response_schema(WorkflowResultConfigurationSchema, code=200)
@response_schema(
    ErrorSchema,
    code=400,
    description="Error while getting workflow results configuration",
)
async def get_workflow_results_with_global_type(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    workflow_id = request.match_info["workflow_id"]
    try:
        workflow_results = service.list_workflow_results(workflow_id)

        # Because all([]) == True we have to handle the empty case to default to JSON
        if not workflow_results:
            result = WorkflowResultConfigurationSchema().dump(
                {"type": WorkflowResultType.JSON, "workflow_results": []}
            )
        else:
            result = WorkflowResultConfigurationSchema().dump(
                {
                    "type": WorkflowResultType.FILEORDIR
                    if all([result.type.is_file() for result in workflow_results])
                    else WorkflowResultType.JSON,
                    "workflow_results": workflow_results,
                }
            )
        return json_response(result, status=200)
    except WorkflowNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Workflows"],
    summary="Overwrite workflow results",
    description="Overwrite workflow results",
)
@request_schema(AddManyWorkflowResultRequestSchema())
@response_schema(AddWorkflowResultResponseSchema(many=True), code=200)
@response_schema(ErrorSchema, code=400, description="Workflow result add error")
async def overwrite_workflow_results(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    workflow_id = request.match_info["workflow_id"]
    data = [
        CreateWorkflowResultData(**workflow_result_to_add, workflow_id=workflow_id)
        for workflow_result_to_add in request["data"]["workflow_results_to_add"]
    ]
    try:
        workflow_results_created = service.overwrite_workflow_results(workflow_id, data)
        result = AddWorkflowResultResponseSchema().dump(
            workflow_results_created, many=True
        )
        return json_response(result, status=200)
    except NotAllKeysAreUniqueInResults as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)
    except (WorkflowNotFoundException, WorkflowActionIONotFoundException) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflows"],
    summary="Create a workflows",
    description="Create a new workflow",
)
@request_schema(AddWorkflowRequestSchema)
@response_schema(
    AddWorkflowResponseSchema, code=201, description="Workflow created successfully"
)
@response_schema(ErrorSchema, code=401, description="Authentication failed")
async def create_workflow(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    data = request["data"]
    user_id = request["user_id"]
    current_project = request.get("current_project_id", None)
    if "from_workflow" in data:
        try:
            copy_workflow_data = CopyWorkflowData(**data)
            workflow_id = await service.copy_workflow(
                copy_workflow_data, user_id, current_project
            )
            result = AddWorkflowResponseSchema().dump({"workflow_id": workflow_id})
            return json_response(result, status=201)
        except WorkflowNotFoundException as err:
            result = ErrorSchema().dump({"error": err.message})
            return json_response(result, status=400)
    else:
        create_workflow_data = CreateWorkflowData(**data)
        workflow_id = service.create_workflow(
            create_workflow_data, user_id, current_project
        )
        result = AddWorkflowResponseSchema().dump({"workflow_id": workflow_id})
        return json_response(result, status=201)


@docs(
    tags=["Workflows"],
    summary="Get one workflow",
    description="Get the requested workflow by ID",
)
@response_schema(
    WorkflowDetailsSchema, code=200, description="Workflow fetched successfully"
)
@response_schema(ErrorSchema, code=404, description="Workflow not found")
async def get_workflow(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow = service.get_workflow(workflow_id)
        result = WorkflowDetailsSchema().dump(workflow)
        return json_response(result, status=200)
    except WorkflowNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflows", "v2"],
    summary="Get one workflow with details",
    description="Get the requested workflow by ID with all details",
)
@response_schema(
    WorkflowExtendedSchema, code=200, description="Workflow fetched successfully"
)
@response_schema(ErrorSchema, code=404, description="Workflow not found")
async def get_workflow_detailed(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow = service.get_workflow_extended(workflow_id)
        result = WorkflowExtendedSchema().dump(workflow)
        return json_response(result, status=200)
    except WorkflowNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflows"],
    summary="Update workflow",
    description="Update the data of a workflow",
    responses={
        200: {"description": "Workflow updated successfully"},
        404: {"description": "Workflow not found", "schema": ErrorSchema},
    },
)
@request_schema(UpdateWorkflowRequestSchema())
async def update_workflow(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    user_id = request["user_id"]
    try:
        workflow_id = request.match_info["workflow_id"]
        workflow_informations = WorkflowInformations(**request["data"])
        service.update_workflow(workflow_id, workflow_informations, user_id)
        return json_response(None, status=200)
    except WorkflowNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflows"],
    summary="Delete workflow",
    description="Delete a workflow by ID",
    responses={
        200: {"description": "Workflow deleted successfully"},
        404: {"description": "Workflow not found", "schema": ErrorSchema},
    },
)
async def delete_workflow(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        authorization = request.headers.get("authorization", "")
        await service.delete_workflow(workflow_id, authorization)
        return json_response(None, status=200)
    except WorkflowNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflows errors"],
    summary="Get workflow errors",
    description="Get the list of workflow errors by ID",
    responses={
        200: {
            "description": "Workflow errors fetched successfully",
            "schema": WorkflowErrorSchema(many=True),
        },
        404: {"description": "Workflow not found", "schema": ErrorSchema},
    },
)
async def get_workflow_errors(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        errors = service.get_workflow_errors(workflow_id)
        result = WorkflowErrorSchema().dump(errors, many=True)
        return json_response(result, status=200)
    except WorkflowNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflows"],
    summary="Deploy workflow",
    description="Deploy a workflow by ID",
    responses={
        200: {"description": "Workflow deploying triggered"},
        400: {"description": "Workflow not valid", "schema": ErrorSchema},
        404: {"description": "Workflow not found", "schema": ErrorSchema},
    },
)
async def deploy_workflow(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        await service.deploy_workflow(workflow_id)
        return json_response(None, status=200)
    except WorkflowNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)
    except (WorkflowNotValidException, WorkflowNotDeployable) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Workflows"],
    summary="Stop workflow deployment",
    description="Stop a workflow deployment by workflow ID. If graceful is set to 'true' the runs in progress will finish before stop, else they will be cancelled",
    parameters=[
        {
            "in": "query",
            "name": "graceful",
            "type": "string",
            # TODO: OAS3 format
            # "schema": {"type": "string"},
        },
    ],
    responses={
        200: {"description": "Stop workflow deployment triggered"},
        404: {"description": "Workflow not found", "schema": ErrorSchema},
    },
)
async def stop_deploy_workflow(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    try:
        workflow_id = request.match_info["workflow_id"]
        graceful = request.rel_url.query.get("graceful") == "true"
        await service.stop_deploy_workflow(workflow_id, graceful)
        return json_response(None, status=200)
    except WorkflowNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflows"],
    summary="Import workflow",
    description="Import workflow packaging file",
    consumes=["multipart/form-data"],
    parameters=[
        {
            "in": "formData",
            "name": "workflow packaging file",
            "type": "file",
            "description": "The workflow packaging file to upload.",
            "required": "true",
        }
    ],
    responses={
        201: {
            "description": "Workflow imported successfully",
            "schema": AddWorkflowResponseSchema,
        },
        400: {
            "description": "Error when importing the workflow",
            "schema": ErrorSchema,
        },
        404: {"description": "Action not found", "schema": ErrorSchema},
    },
)
async def import_workflow(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    try:
        user_id = request["user_id"]
        current_project = request.get("current_project_id", None)
        reader: MultipartReader = await request.multipart()
        part = await reader.next()
        if not isinstance(part, BodyPartReader):
            raise WorkflowBadZipFile
        else:
            file: BodyPartReader = part
        file_content = bytearray()
        while True:
            content_part = await file.read_chunk()
            if not content_part:
                break
            file_content.extend(content_part)
        workflow_id = await service.import_workflow(
            file_content, user_id, current_project
        )
        result = AddWorkflowResponseSchema().dump({"workflow_id": workflow_id})
        return json_response(result, status=201)
    except (
        WorkflowParsingYamlException,
        WorkflowInvalidSchemaException,
        WorkflowBadZipFile,
        WorkflowYamlNotFound,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)
    except ActionNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflows"],
    summary="Export a workflow",
    description="Export the given workflow zip file by id",
    produces=["application/zip"],
    responses={
        200: {"description": "Workflow exported successfully"},
        404: {"description": "Workflow not found", "schema": ErrorSchema},
    },
)
async def export_workflow(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Union[Response, FileResponse]:
    try:
        workflow_id = request.match_info["workflow_id"]
        exported_workflow_path = await service.export_workflow(workflow_id)
        with open(exported_workflow_path, "r"):
            return FileResponse(
                exported_workflow_path,
                headers={
                    "Content-Disposition": "attachment;filename=workflow.zip",
                    "Content-Type": "application/zip",
                },
            )
    except (WorkflowNotFoundException, FilestoreEntryNotFoundException) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflows"],
    summary="Get JSON workflow schema",
    description="Get the JSON workflow schema that is used to validate workflows",
    produces=["application/json"],
    responses={
        200: {"description": "Workflow schema fetched successfully."},
        404: {"description": "Workflow schema not found.", "schema": ErrorSchema},
    },
)
async def get_schema(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    try:
        schema = service.get_workflow_schema()
        return json_response(schema, status=200)
    except AttributeError as err:
        result = ErrorSchema().dump({"error": err})
        return json_response(result, status=404)


@docs(
    tags=["Workflows"],
    summary="Check if a given workflow schema is valid",
    description="Check the validity of a given workflow specification file",
    consumes=["multipart/form-data"],
    parameters=[
        {
            "in": "formData",
            "name": "workflow schema file",
            "type": "file",
            "description": "The workflow schema file to check.",
            "required": "true",
        }
    ],
    responses={
        200: {
            "description": "Workflow schema checked.",
            "schema": WorkflowValidationSchema,
        },
        400: {
            "description": "Workflow schema validation failed.",
            "schema": ErrorSchema,
        },
    },
)
async def validate_schema(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
    max_upload_size: int = Provide[ApplicationContainer.configuration.max_upload_size],
) -> Response:
    try:
        user_id = request["user_id"]
        current_project = request.get("current_project_id", None)
        reader: MultipartReader = await request.multipart()
        part: Optional[Union["MultipartReader", BodyPartReader]] = await reader.next()
        if not isinstance(part, BodyPartReader):
            raise WorkflowBadZipFile
        else:
            file: BodyPartReader = part
        file_content = bytearray()
        content_part: bytes
        while True:
            content_part = await file.read_chunk()
            if len(file_content) > max_upload_size:
                raise WorkflowSchemaFileTooLargeException()
            elif not content_part:
                break
            file_content.extend(content_part)
        service.validate_workflow_schema(file_content, user_id, current_project)
        result = WorkflowValidationSchema().dump(
            {"status": "Valid", "message": "Workflow packaging is valid"}
        )
        return json_response(result, status=200)
    except SchemaValidWorkflowInvalidException as err:
        result = WorkflowValidationSchema().dump(
            {"status": "Valid", "message": err.message}
        )
        return json_response(result, status=200)
    except (
        WorkflowParsingYamlException,
        WorkflowInvalidSchemaException,
        ActionNotFoundException,
    ) as err:
        result = WorkflowValidationSchema().dump(
            {"status": "Invalid", "message": err.message}
        )
        return json_response(result, status=200)
    except (WorkflowSchemaFileTooLargeException, WorkflowBadZipFile) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)


@docs(
    tags=["Workflows"],
    summary="Get workflow endpoint",
    description="Get the workflow endpoint if this workflow support it",
)
@response_schema(WorkflowEndpointSchema(), code=200)
@response_schema(ErrorSchema, code=404, description="Workflow not found")
@response_schema(
    ErrorSchema, code=400, description="This workflow does not support endpoint"
)
async def get_workflow_endpoint(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    workflow_id = request.match_info["workflow_id"]
    try:
        endpoint = service.get_workflow_endpoint(workflow_id)
        result = WorkflowEndpointSchema().dump({"path": endpoint})
        return json_response(result, status=200)
    except WorkflowNotFoundException as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=404)


@docs(
    tags=["Workflows"],
    summary="Update workflow endpoint",
    description="Update workflow endpoint",
)
@request_schema(WorkflowEndpointSchema())
@response_schema(WorkflowEndpointSchema(), code=200)
@response_schema(
    ErrorSchema, code=400, description="Workflow endpoint could not be updated"
)
async def overwrite_workflow_endpoint(
    request: Request,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> Response:
    workflow_id = request.match_info["workflow_id"]
    new_path = request["data"]["path"]
    try:
        path = service.overwrite_workflow_endpoint(workflow_id, new_path)
        result = WorkflowEndpointSchema().dump({"path": path})
        return json_response(result, status=200)
    except (
        WorkflowNotFoundException,
        WorkflowHasNoTriggerException,
        InvalidUserDefinedEndpointException,
        EndpointAlreadyExistsException,
    ) as err:
        result = ErrorSchema().dump({"error": err.message})
        return json_response(result, status=400)
