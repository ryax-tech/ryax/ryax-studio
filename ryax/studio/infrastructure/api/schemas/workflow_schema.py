# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields, validate

from ryax.studio.domain.workflow.workflow_values import (
    WorkflowDeploymentStatus,
    WorkflowValidityStatus,
)
from ryax.studio.infrastructure.api.schemas.workflow_action_link_schema import (
    WorkflowModuleLinkSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_action_schema import (
    WorkflowModuleExtendedSchema,
    WorkflowModuleSchema,
)


class WorkflowListParametersSchema(Schema):
    name = fields.String(
        metadata={
            "description": "Workflow trigger parameter name",
            "example": "My param",
        }
    )
    value = fields.String(
        metadata={
            "description": "Workflow trigger parameter value",
            "example": "This is a value",
        }
    )


class WorkflowSchema(Schema):
    id = fields.String(
        description="ID of the workflow", example="3ff80197-c975-4f20-a0ca-c35bda9f092b"
    )

    name = fields.String(
        description="Name of the workflow",
        example="Video detection",
        validate=[validate.Length(3, 128)],
    )

    description = fields.String(
        description="Description of the workflow",
        example="Detect object in videos in real time",
        validate=[validate.Length(0, 512)],
        allow_none=True,
    )

    status = fields.String(
        description="Validity status of the workflow",
        example="valid",
        enum=list(item.value for item in WorkflowValidityStatus),
        attribute="validity_status.value",
    )

    deployment_status = fields.String(
        description="deployment status of the workflow",
        example="Deploying",
        enum=list(item.value for item in WorkflowDeploymentStatus),
        attribute="deployment_status.value",
    )

    deployment_error = fields.String(
        metadata={"description": "Deployment error details if any"},
        allow_none=True,
    )

    has_form = fields.Boolean(
        metadata={
            "description": "Whether or not this workflow has a source action with a form"
        }
    )

    endpoint_prefix = fields.String(
        metadata={
            "description": "Trigger endpoint prefix associated to this Workflow",
            "example": "/path/to/somwhere",
        },
        allow_none=True,
    )

    trigger = fields.String(
        metadata={
            "description": "Workflow trigger action name",
            "example": "My trigger",
        },
        allow_none=True,
    )

    categories = fields.List(
        fields.String(),
        metadata={
            "description": "All categories associated to one or more actions in the workflow",
            "example": ["Default", "Users"],
        },
    )

    parameters = fields.Nested(
        WorkflowListParametersSchema,
        metadata={
            "description": "Trigger inputs representation. Only for inputs that are not null and set to a non default values",
            "example": [
                {"name": "foo", "value": "false"},
                {"name": "bar", "value": "toto.zip (12MB)"},
            ],
        },
        many=True,
    )


class WorkflowDetailsSchema(WorkflowSchema):
    modules = fields.Nested(
        WorkflowModuleSchema,
        description="Modules used in workflow",
        many=True,
        attribute="actions",
    )

    modules_links = fields.Nested(
        WorkflowModuleLinkSchema,
        description="Module links used in workflow",
        many=True,
        attribute="actions_links",
    )


class WorkflowExtendedSchema(WorkflowSchema):
    modules = fields.Nested(
        WorkflowModuleExtendedSchema,
        description="Modules used in workflow",
        many=True,
        attribute="actions",
    )


class WorkflowEndpointSchema(Schema):
    path = fields.String(
        metadata={
            "description": "Path used to trigger the workflow",
            "example": "/my/custom/path",
        },
        allow_none=True,
    )
