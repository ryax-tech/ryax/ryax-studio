# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields

from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.infrastructure.api.schemas.workflow_action_input_file_schema import (
    WorkflowModuleInputFileSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_action_output_schema import (
    WorkflowModuleOutputSchema,
)


class WorkflowModuleInputSchema(Schema):
    id = fields.String(
        description="ID of the workflow action input",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
    )

    technical_name = fields.String(
        description="Workflow action input technical name", example="input-name"
    )

    display_name = fields.String(
        description="Workflow action input display name",
        example="Module to send data on cloud server",
    )

    help = fields.String(
        description="Workflow action input help",
        example="Workflow action input help for user",
    )

    type = fields.String(
        description="Module input type",
        example="string",
        enum=list(item.value for item in ActionIOType),
        attribute="type.value",
    )

    enum_values = fields.List(
        fields.String,
        description="Module input available options for enum type",
        example=['"option1"', '"option2"'],
    )

    static_value = fields.String(
        description="Module input value to reference other input",
        example="string",
        allow_none=True,
    )

    reference_value = fields.String(
        description="Module input value to reference other output",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092c",
        allow_none=True,
    )

    reference_output = fields.Nested(
        WorkflowModuleOutputSchema,
        description="Definition of the workflow action output referenced",
        allow_none=True,
    )

    workflow_file = fields.Nested(
        WorkflowModuleInputFileSchema,
        description="Metadata of the input static file",
        allow_none=True,
    )
