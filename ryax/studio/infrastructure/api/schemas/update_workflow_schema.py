# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields, validate


class UpdateWorkflowRequestSchema(Schema):
    name = fields.String(
        description="Name of the workflow",
        example="Video detection",
        required=False,
        validate=[validate.Length(3, 128)],
    )

    description = fields.String(
        description="Description of the workflow",
        example="Detect object in videos in real time",
        validate=[validate.Length(0, 512)],
        required=False,
    )
