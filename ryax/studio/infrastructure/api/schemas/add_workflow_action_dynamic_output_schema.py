# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields

from ryax.studio.domain.action.action_values import ActionIOType


class AddWorkflowModuleDynamicOutputSchema(Schema):
    technical_name = fields.String(
        description="Output technical name", example="string", required=True
    )

    display_name = fields.String(
        description="Output name to display on Webui", example="Name", required=True
    )

    help = fields.String(
        description="Output help text",
        example="Description of the output",
        required=True,
    )

    type = fields.String(
        description="Type of the output",
        enum=list(item.value for item in ActionIOType),
        example="integer",
        required=True,
    )

    enum_values = fields.List(
        fields.String(),
        description="Values of output if the type is Enum",
        example=["value1"],
    )
