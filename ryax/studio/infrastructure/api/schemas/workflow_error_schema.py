# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields

from ryax.studio.domain.workflow.workflow_values import WorkflowErrorCode


class WorkflowErrorSchema(Schema):
    id = fields.String(
        description="ID of the workflow error",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
    )

    code = fields.Integer(
        description="Workflow error message",
        example="Workflow has cycle",
        enum=list(item.value for item in WorkflowErrorCode),
        attribute="code.value",
    )

    workflow_module_id = fields.String(
        description="Workflow action id (used if the error is attached to a action)",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
        allow_none=True,
        attribute="workflow_action_id",
    )
