# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields


class ModuleVersionsSchema(Schema):
    id = fields.String(
        description="ID of the action", example="3ff80197-c975-4f20-a0ca-c35bda9f092b"
    )
    version = fields.String(
        description="Module version",
        example="1.0",
    )
