# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields

from ryax.studio.domain.action.action_values import ActionIOType, DynamicOutputOrigin


class UpdateWorkflowModuleDynamicOutputWithIdSchema(Schema):
    id = fields.String(description="Output id", example="1234")
    technical_name = fields.String(
        description="Output technical name",
        example="string",
        required=True,
    )

    display_name = fields.String(
        description="Output name to display on Webui",
        example="Name",
        required=True,
    )

    help = fields.String(
        description="Output help text",
        example="Description of the output",
        required=True,
    )

    type = fields.String(
        description="Type of the output",
        enum=list(item.value for item in ActionIOType),
        example="integer",
        required=True,
    )

    enum_values = fields.List(
        fields.String(),
        description="Module input options for enum type",
        example=["option_1", "option_2"],
        required=False,
    )
    origin = fields.String(
        description="Origin of this parameter",
        enum=list(item.value for item in DynamicOutputOrigin),
        example="path",
        required=False,
        allow_none=True,
    )
    optional = fields.Boolean(description="Whether this dynamic output is optional")


class UpdateWorkflowModuleInputValueWithIdSchema(Schema):
    id = fields.String(
        description="Input id",
        example="1234",
        required=True,
    )
    static_value = fields.Raw(
        description="Static value to set for the input",
        example='"value"',
        required=False,
        allow_none=True,
    )
    reference_value = fields.String(
        description="Referencce id to set for the input",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
        required=False,
        allow_none=True,
    )
    project_variable_value = fields.String(
        description="Project Variable id to set for the input",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
        required=False,
        allow_none=True,
    )


class UpdateAllWorkflowModuleIOSchema(Schema):
    custom_name = fields.String(
        description="Module new name",
        example="string",
    )
    inputs = fields.Nested(
        UpdateWorkflowModuleInputValueWithIdSchema,
        many=True,
    )
    dynamic_outputs = fields.Nested(
        UpdateWorkflowModuleDynamicOutputWithIdSchema,
        many=True,
    )
    addons_inputs = fields.Nested(
        UpdateWorkflowModuleInputValueWithIdSchema,
        many=True,
    )
