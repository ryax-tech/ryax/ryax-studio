# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields


class AddWorkflowModuleLinkSchema(Schema):
    input_module_id = fields.String(
        description="Identifier of the input workflow action",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
        required=True,
    )

    output_module_id = fields.String(
        description="Identifier of the output workflow action",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
        required=True,
    )
