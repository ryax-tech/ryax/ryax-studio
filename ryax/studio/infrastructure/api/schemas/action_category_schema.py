# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields


class ModuleCategorySchema(Schema):
    id = fields.String(
        description="ID of the category", example="3ff80197-c975-4f20-a0ca-c35bda9f092b"
    )

    name = fields.String(
        description="Name of the category",
        example="Internet of Things",
    )
