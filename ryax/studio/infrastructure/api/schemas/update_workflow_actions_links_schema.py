from marshmallow import Schema, fields


class UpdateWorkflowModuleLinksSchema(Schema):
    module_id = fields.String(
        required=True,
        metadata={
            "description": "ID of the action you want the links to originate from",
            "example": "3ff80197-c975-4f20-a0ca-c35bda9f092b",
        },
    )

    next_modules_ids = fields.List(
        fields.String(allow_none=True),
        required=True,
        metadata={
            "description": "List of ids for the modules you want the link to point to",
            "example": ["3ff80197-c975-4f20-a0ca-c35bda9f092b"],
        },
    )


class UpdateWorkflowModulesLinksSchema(Schema):
    links = fields.Nested(
        UpdateWorkflowModuleLinksSchema,
        metadata={
            "description": "List of the modules with their links that you want to update."
        },
        required=True,
        many=True,
    )
