# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields

from ryax.studio.domain.action.action_values import ActionErrorCode


class ErrorSchema(Schema):
    error = fields.String(description="Error name", example="Workflow not found")


class LightWorkflowSchema(Schema):
    id = fields.String(
        description="ID of workflow", example="88813c34-6c69-4eed-a3a1-4994d7a6a3b0"
    )

    name = fields.String(
        description="Name of workflow", example="Video detection workflow"
    )


class ModuleDeleteErrorSchema(ErrorSchema):
    workflows = fields.Nested(
        LightWorkflowSchema,
        description="List of the workflows where the action is used",
        many=True,
    )

    code = fields.Integer(
        description="Value of the error code",
        enum=list(item.value for item in ActionErrorCode),
        attribute="code.value",
        example=1,
    )
