# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import Optional, Union

from marshmallow import Schema, fields
from marshmallow_enum import EnumField

from ryax.studio.domain.action.action_values import ActionIOType, DynamicOutputOrigin
from ryax.studio.domain.workflow.views.workflow_action_input_view import (
    WorkflowActionInputView,
)


class ModuleIOSchema(Schema):
    id = fields.String(
        description="ID of the workflow action input",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
    )

    technical_name = fields.String(
        description="Workflow action input technical name", example="input-name"
    )

    display_name = fields.String(
        description="Workflow action input display name",
        example="Module to send data on cloud server",
    )

    help = fields.String(
        description="Workflow action input help",
        example="Workflow action input help for user",
    )

    type = EnumField(ActionIOType, by_value=True)

    enum_values = fields.List(
        fields.String(),
        description="Module input options for enum type",
        example=["option_1", "option_2"],
    )

    default_value = fields.Raw(
        metadata={"description": "Default value for this entry"},
        allow_none=True,
    )

    optional = fields.Boolean(metadata={"description": "Wether this IO is optional"})


class ModuleIOSchemaWithOrigin(ModuleIOSchema):
    origin = EnumField(DynamicOutputOrigin, by_value=True)


class ModuleIOWithValueSchema(ModuleIOSchema):
    reference_value = fields.String(allow_none=True)
    project_variable_value = fields.String(allow_none=True)
    static_value = fields.Method("get_static_value_with_file", allow_none=True)

    @staticmethod
    def get_static_value_with_file(
        obj: WorkflowActionInputView,
    ) -> Optional[Union[str, float, int, bool, bytes]]:
        if obj.workflow_file is None:
            return obj.static_value
        else:
            return obj.workflow_file.name + "." + obj.workflow_file.extension
