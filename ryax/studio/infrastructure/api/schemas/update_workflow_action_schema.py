# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields


class UpdateWorkflowModuleSchema(Schema):
    custom_name = fields.String(
        description="Human friendly name of the action to be displayed in the users interfaces",
        example="Video detection inside workflow",
        required=False,
    )
    position_x = fields.Integer(
        description="The position on x-axis of the action on the Web UI grid",
        example=0,
        required=False,
    )
    position_y = fields.Integer(
        description="The position on y-axis of the action on the Web UI grid",
        example=1,
        required=False,
    )
