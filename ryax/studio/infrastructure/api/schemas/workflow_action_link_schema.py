# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields, pre_dump

from ryax.studio.domain.workflow.views.workflow_action_link_view import (
    WorkflowActionLinkView,
)


class WorkflowModuleLinkSchema(Schema):
    id = fields.String(
        description="ID of the action link",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
    )

    input_module_id = fields.String(
        description="ID of the input action of the link",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
        attribute="upstream_action_id",
    )

    output_module_id = fields.String(
        description="ID of the output action of the link",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
        attribute="downstream_action_id",
    )

    @pre_dump
    def dump_upstream_downstream_to_input_output(
        self, workflow_module_link: WorkflowActionLinkView, **kwargs: dict
    ) -> WorkflowActionLinkView:
        workflow_module_link.input_module_id = workflow_module_link.upstream_action_id  # type: ignore
        workflow_module_link.output_module_id = workflow_module_link.downstream_action_id  # type: ignore
        return workflow_module_link
