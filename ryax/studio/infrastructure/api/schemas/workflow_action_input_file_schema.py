# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields


class WorkflowModuleInputFileSchema(Schema):
    id = fields.String(
        description="ID of the workflow action input static file",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
    )

    name = fields.String(description="Name of the static file", example="data-file")

    extension = fields.String(description="Extension of the static file", example="txt")

    size = fields.Float(description="Size of the static file", example=12.10)

    path = fields.String(
        description="Path to static file in the filestore",
        example="/iodata/080b83e9-9ecc-4b08-aa65-706cdca43b8a/file.txt",
    )
