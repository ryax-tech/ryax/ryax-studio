# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields, validate


class AddWorkflowRequestSchema(Schema):
    name = fields.String(
        description="Name of the workflow",
        example="Video detection",
        required=True,
        error_messages={"required": "Workflow name required", "code": "400"},
        validate=[validate.Length(3, 128)],
    )

    description = fields.String(
        description="Description of the workflow",
        example="Detect object in videos in real time",
        validate=[validate.Length(0, 512)],
        error_messages={"required": "Workflow description required", "code": "400"},
        default=None,
        required=False,
    )

    from_workflow = fields.String(
        description="Workflow identifier used for templating new workflow",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
        error_messages={"required": "Workflow description required", "code": "400"},
        default=None,
        required=False,
    )


class AddWorkflowResponseSchema(Schema):
    workflow_id = fields.String(
        description="Identifier of created workflow",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
    )
