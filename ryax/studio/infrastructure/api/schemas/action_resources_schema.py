from marshmallow import Schema, fields


class ActionResourcesSchema(Schema):
    cpu = fields.Float()
    memory = fields.Integer()
    time = fields.Float()
    gpu = fields.Integer()
