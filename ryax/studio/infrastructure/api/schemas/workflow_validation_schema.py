# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields, validate


class WorkflowValidationSchema(Schema):
    status = fields.String(
        description="Validity of the workflow file",
        example="Valid",
        enum=["Valid", "Invalid"],
    )
    message = fields.String(
        description="Reason for the status",
        example="Workflow file is valid",
        validate=[validate.Length(3, 128)],
    )
