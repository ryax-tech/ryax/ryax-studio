from marshmallow import Schema, fields


class WorkflowActionConstraintsSchema(Schema):
    site_list = fields.List(fields.String)
    site_type_list = fields.List(fields.String)
    node_pool_list = fields.List(fields.String)
    arch_list = fields.List(fields.String)
