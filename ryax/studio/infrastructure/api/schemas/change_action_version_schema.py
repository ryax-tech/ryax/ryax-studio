# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields


class ChangeModuleVersionRequestSchema(Schema):
    module_id = fields.String(
        description="The new action id that correspond to the version to update",
        example="30a197ce-6db1-4cc3-a895-27aeb40979dd",
    )


class ChangeModuleVersionResponseSchema(Schema):
    workflow_module_id = fields.String(
        description="Identifier of the new workflow action",
        example="5tf80197-c975-4f20-a0ca-c35bda9f054u",
    )
    removed_results = fields.List(fields.String())
