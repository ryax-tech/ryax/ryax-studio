# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields
from marshmallow_enum import EnumField

from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.workflow.entities.workflow_result import WorkflowResultType


class WorkflowResultSchema(Schema):
    key = fields.String(
        description="Name of the result",
        example="Result Value",
        required=True,
    )

    workflow_module_io_id = fields.String(
        description="Identifier of the workflow action io associated.",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
        required=True,
        attribute="workflow_action_io_id",
    )

    description = fields.String(
        metadata={
            "description": "Textual description of the action IO",
            "example": 'Value from input "My input name" of action "My action name"',
        },
    )

    type = EnumField(
        ActionIOType,
        by_value=True,
        metadata={
            "description": "IO type",
            "example": 'string"',
        },
    )


class AddWorkflowResultResponseSchema(Schema):
    id = fields.String(
        description="Identifier of the result created",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
        data_key="id",
    )


class AddManyWorkflowResultRequestSchema(Schema):
    workflow_results_to_add = fields.Nested(WorkflowResultSchema, many=True)


class WorkflowResultConfigurationSchema(Schema):
    workflow_results = fields.Nested(WorkflowResultSchema, many=True)
    type = EnumField(WorkflowResultType, by_value=True)
