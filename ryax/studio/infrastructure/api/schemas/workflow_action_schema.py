# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields

from ryax.studio.domain.action.action_values import ActionKind
from ryax.studio.domain.workflow.workflow_values import (
    WorkflowActionDeployStatus,
    WorkflowActionValidityStatus,
)
from ryax.studio.infrastructure.api.schemas.action_category_schema import (
    ModuleCategorySchema,
)
from ryax.studio.infrastructure.api.schemas.action_resources_schema import (
    ActionResourcesSchema,
)
from ryax.studio.infrastructure.api.schemas.module_io_schema import (
    ModuleIOSchema,
    ModuleIOSchemaWithOrigin,
    ModuleIOWithValueSchema,
)
from ryax.studio.infrastructure.api.schemas.module_schema import ModuleDetailsSchema
from ryax.studio.infrastructure.api.schemas.workflow_action_constraints_schema import (
    WorkflowActionConstraintsSchema,
)
from ryax.studio.infrastructure.api.schemas.workflow_action_objectives_schema import (
    WorkflowActionObjectivesSchema,
)


class WorkflowModuleSchema(Schema):
    id = fields.String(
        description="ID of the action", example="3ff80197-c975-4f20-a0ca-c35bda9f092b"
    )

    name = fields.String(description="Module name", example="Module 1")

    technical_name = fields.String(
        description="Technical name of the action", example="mqttgw"
    )

    description = fields.String(
        description="Module description", example="Module to send data on cloud server"
    )

    version = fields.String(description="Module version", example="1.0.0")

    kind = fields.String(
        description="Module kind",
        example="Processor",
        enum=list(item.value for item in ActionKind),
        attribute="kind.value",
    )

    has_dynamic_outputs = fields.Boolean(
        description="Flag to indicate that dynamic outputs configuration allowed or not",
    )

    module_id = fields.String(
        description="Module identifier",
        example="3ff80197-c975-4f20-a0ca-c35bda9f093c",
        attribute="action_id",
    )

    custom_name = fields.String(
        description="Human friendly name of the action to be displayed in the users interfaces",
        example="Video detection inside workflow",
        allow_none=True,
    )

    position_x = fields.Integer(
        description="The position on x-axis of the action on the Web UI grid",
        example=0,
    )

    position_y = fields.Integer(
        description="The position on y-axis of the action on the Web UI grid",
        example=1,
    )

    status = fields.String(
        description="Validity status of the workflow action",
        example="valid",
        enum=list(item.value for item in WorkflowActionValidityStatus),
        attribute="validity_status.value",
    )

    deployment_status = fields.String(
        description="Deployment status of the workflow action",
        example="Deploying",
        enum=list(item.value for item in WorkflowActionDeployStatus),
        attribute="deployment_status.value",
    )
    categories = fields.Nested(
        ModuleCategorySchema, description="Module Categories", many=True
    )

    endpoint = fields.String(
        description="Trigger endpoint. Is null if no endpoint is defined for this trigger.",
        example="/path/to/somwhere",
        allow_none=True,
    )

    resources = fields.Nested(
        ActionResourcesSchema,
        allow_none=True,
    )


class WorkflowModuleExtendedSchema(ModuleDetailsSchema):
    inputs = fields.Nested(
        ModuleIOWithValueSchema,
        description="Modules inputs",
        many=True,
    )

    outputs = fields.Nested(
        ModuleIOSchema,
        description="Module outputs",
        many=True,
    )

    dynamic_outputs = fields.Nested(
        ModuleIOSchemaWithOrigin,
        description="Module dynamic outputs",
        many=True,
    )

    addons_inputs = fields.Nested(
        ModuleIOWithValueSchema,
        description="Module addons inputs",
        many=True,
    )

    status = fields.String(
        description="Validity status of the workflow action",
        example="valid",
        enum=list(item.value for item in WorkflowActionValidityStatus),
        attribute="validity_status.value",
    )

    custom_name = fields.String(
        description="Human friendly name of the action to be displayed in the users interfaces",
        example="Video detection inside workflow",
        allow_none=True,
    )

    module_id = fields.String(
        description="Action identifier",
        example="3ff80197-c975-4f20-a0ca-c35bda9f093c",
        attribute="action_id",
    )

    constraints = fields.Nested(
        WorkflowActionConstraintsSchema,
        allow_none=True,
    )
    objectives = fields.Nested(
        WorkflowActionObjectivesSchema,
        allow_none=True,
    )
