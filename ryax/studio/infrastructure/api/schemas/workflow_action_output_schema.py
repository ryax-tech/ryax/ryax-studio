# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields

from ryax.studio.domain.action.action_values import ActionIOType


class WorkflowModuleOutputSchema(Schema):
    id = fields.String(
        description="ID of the workflow action output",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
    )

    technical_name = fields.String(
        description="Workflow action output technical name", example="input-name"
    )

    display_name = fields.String(
        description="Workflow action output display name",
        example="Module to send data on cloud server",
    )

    help = fields.String(
        description="Workflow action output help",
        example="Workflow action output help for user",
    )

    type = fields.String(
        description="Module output type",
        example="string",
        enum=list(item.value for item in ActionIOType),
        attribute="type.value",
    )

    enum_values = fields.List(
        fields.String,
        description="Module output available values for enum type",
        example="option1",
    )
