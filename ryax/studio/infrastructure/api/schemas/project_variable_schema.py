# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields
from marshmallow_enum import EnumField

from ryax.studio.domain.action.action_values import ActionIOType


class ProjetcVariablesSchema(Schema):
    id = fields.String(
        description="ID of the user object",
        example="3ff80197-c975-4f20-a0ca-c35bda9f092b",
    )

    name = fields.String(
        description="Name of this object",
        example="My username",
        required=True,
        allow_none=False,
    )

    value = fields.Raw(
        description="Object Value (will show up if not of filetype)",
        example="value",
        required=False,
    )

    description = fields.String(
        description="Description of this value",
        example="My username for the database client",
        required=False,
        allow_none=True,
    )

    type = EnumField(ActionIOType, by_value=True, required=True)


class ProjectVariableTransactionSchema(Schema):
    name = fields.String(
        description="Name of this object",
        example="My username",
        required=True,
        allow_none=False,
    )

    value = fields.Raw(
        description="Object Value (will show up if not of filetype)",
        example="value",
        required=False,
    )

    description = fields.String(
        description="Description of this value",
        example="My username for the database client",
        required=False,
        allow_none=True,
    )
    type = fields.String(description="type of the user object", required=True)
