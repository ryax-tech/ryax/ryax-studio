from marshmallow import Schema, fields


class WorkflowActionObjectivesSchema(Schema):
    energy = fields.Float()
    cost = fields.Float()
    performance = fields.Float()
