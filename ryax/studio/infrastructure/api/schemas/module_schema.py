# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from marshmallow import Schema, fields

from ryax.studio.domain.action.action_values import ActionKind
from ryax.studio.infrastructure.api.schemas.action_category_schema import (
    ModuleCategorySchema,
)
from ryax.studio.infrastructure.api.schemas.action_resources_schema import (
    ActionResourcesSchema,
)
from ryax.studio.infrastructure.api.schemas.module_io_schema import ModuleIOSchema
from ryax.studio.infrastructure.api.schemas.module_versions_schema import (
    ModuleVersionsSchema,
)


class ModuleSchema(Schema):
    id = fields.String(
        description="ID of the action", example="3ff80197-c975-4f20-a0ca-c35bda9f092b"
    )

    name = fields.String(
        description="Name of the action",
        example="MQTT Gateway",
    )

    version = fields.String(
        description="Module version",
        example="1.0",
    )

    kind = fields.String(
        description="Module kind",
        example="Source",
        enum=list(item.value for item in ActionKind),
        attribute="kind.value",
    )
    description = fields.String(
        description="Description of the action",
        example="This action creates a MQTT Gateway",
    )


class ModuleDetailsSchema(ModuleSchema):
    inputs = fields.Nested(
        ModuleIOSchema,
        description="Modules inputs",
        many=True,
    )

    outputs = fields.Nested(
        ModuleIOSchema,
        description="Module outputs",
        many=True,
    )

    addons = fields.Dict(
        keys=fields.String,
        description="Addons parameters",
    )

    technical_name = fields.String(
        description="Technical name of the action", example="mqttgw"
    )

    lockfile = fields.String(
        allow_none=False,
    )

    categories = fields.Nested(
        ModuleCategorySchema, description="Module Categories", many=True
    )

    versions = fields.Nested(ModuleVersionsSchema, many=True)

    has_dynamic_outputs = fields.Bool(
        metadata={"description": "True if this action has dynamic outputs"}
    )

    resources = fields.Nested(
        ActionResourcesSchema,
        allow_none=True,
    )
