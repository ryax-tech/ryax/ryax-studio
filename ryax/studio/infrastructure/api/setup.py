# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from aiohttp import web
from aiohttp_apispec import setup_aiohttp_apispec, validation_middleware

from ryax.studio.container import ApplicationContainer
from ryax.studio.infrastructure.api.controllers import (
    action_controller,
    project_variable_controller,
    util_controller,
    workflow_action_controller,
    workflow_controller,
)
from ryax.studio.infrastructure.api.middlewares import (
    authentication_middleware,
    current_project_id_middleware,
)


def setup(app: web.Application, container: ApplicationContainer) -> None:
    """Method to setup api"""
    # Configure application container for wiring
    container.wire(
        modules=[
            authentication_middleware,
            workflow_controller,
            action_controller,
            workflow_action_controller,
            project_variable_controller,
            util_controller,
            current_project_id_middleware,
        ]
    )

    # Setup server middlewares
    app.middlewares.extend(
        [
            authentication_middleware.check_token(
                public_paths=[
                    "/docs",
                    "/static",
                    "/healthz",
                    "/workflow-modules",
                ]
            ),
            current_project_id_middleware.handle(
                public_paths=[
                    "/docs",
                    "/static",
                    "/healthz",
                    "/workflow-modules",
                ]
            ),
            validation_middleware,
        ]
    )

    # Configure api routing
    app.add_routes(
        [
            web.get("/healthz", util_controller.health_check, allow_head=False),
            web.get("/workflows", workflow_controller.list_workflows, allow_head=False),
            web.post("/workflows", workflow_controller.create_workflow),
            web.get(
                "/workflows/schema/get",
                workflow_controller.get_schema,
                allow_head=False,
            ),
            web.get(
                "/workflows/{workflow_id}",
                workflow_controller.get_workflow,
                allow_head=False,
            ),
            web.put("/workflows/{workflow_id}", workflow_controller.update_workflow),
            web.delete("/workflows/{workflow_id}", workflow_controller.delete_workflow),
            web.post("/workflows/import", workflow_controller.import_workflow),
            web.get(
                "/workflows/{workflow_id}/errors",
                workflow_controller.get_workflow_errors,
                allow_head=False,
            ),
            web.post(
                "/workflows/{workflow_id}/deploy", workflow_controller.deploy_workflow
            ),
            web.post(
                "/workflows/{workflow_id}/stop",
                workflow_controller.stop_deploy_workflow,
            ),
            web.get("/modules", action_controller.list_actions, allow_head=False),
            web.get(
                "/modules/list_categories",
                action_controller.list_categories,
                allow_head=False,
            ),
            web.get(
                "/modules/{module_id}",
                action_controller.get_action,
                allow_head=False,
            ),
            web.delete(
                "/modules/{module_id}",
                action_controller.delete_action,
            ),
            web.post(
                "/workflows/{workflow_id}/modules",
                workflow_action_controller.add_workflow_action,
            ),
            web.put(
                "/workflows/{workflow_id}/modules/{workflow_module_id}",
                workflow_action_controller.update_workflow_action,
            ),
            web.delete(
                "/workflows/{workflow_id}/modules/{workflow_module_id}",
                workflow_action_controller.delete_workflow_action,
            ),
            web.post(
                "/workflows/{workflow_id}/modules-links",
                workflow_action_controller.add_workflow_action_link,
            ),
            web.delete(
                "/workflows/{workflow_id}/modules-links/{workflow_module_link_id}",
                workflow_action_controller.delete_workflow_action_link,
            ),
            web.get(
                "/workflows/{workflow_id}/modules-outputs",
                workflow_action_controller.search_workflow_action_outputs,
                allow_head=False,
            ),
            web.post(
                "/workflows/{workflow_id}/modules/{workflow_module_id}/change_version",
                workflow_action_controller.change_action_version,
            ),
            web.get(
                "/workflows/{workflow_id}/modules/{workflow_module_id}/inputs",
                workflow_action_controller.list_workflow_action_inputs,
                allow_head=False,
            ),
            web.put(
                "/workflows/{workflow_id}/modules/{workflow_module_id}/inputs/{workflow_input_id}/value",
                workflow_action_controller.update_workflow_action_input,
            ),
            web.delete(
                "/workflows/{workflow_id}/modules/{workflow_module_id}/inputs/{workflow_input_id}/file",
                workflow_action_controller.delete_workflow_input_static_file,
            ),
            web.get(
                "/workflows/{workflow_id}/modules/{workflow_module_id}/outputs",
                workflow_action_controller.list_workflow_action_outputs,
                allow_head=False,
            ),
            web.get(
                "/portals/{workflow_id}/modules/{workflow_module_id}/outputs",
                workflow_action_controller.list_workflow_action_outputs,
                allow_head=False,
            ),
            web.post(
                "/workflows/{workflow_id}/modules/{workflow_module_id}/outputs",
                workflow_action_controller.add_workflow_dynamic_output,
            ),
            web.put(
                "/workflows/{workflow_id}/modules/{workflow_module_id}/outputs/{workflow_dynamic_output_id}",
                workflow_action_controller.update_workflow_dynamic_output,
            ),
            web.delete(
                "/workflows/{workflow_id}/modules/{workflow_module_id}/outputs/{workflow_dynamic_output_id}",
                workflow_action_controller.delete_workflow_dynamic_output,
            ),
            web.get(
                "/workflows/{workflow_id}/export", workflow_controller.export_workflow
            ),
            web.post(
                "/workflows/{workflow_id}/modules/{module_id}/inputs/{workflow_input_id}/file",
                workflow_action_controller.add_workflow_static_file_input,
            ),
            web.get("/modules/{module_id}/logo", action_controller.get_logo),
            web.get("/static/logo/{module_id}", action_controller.get_logo),
            web.post("/workflows/schema/validate", workflow_controller.validate_schema),
            web.get(
                "/modules/{module_id}/list_versions",
                action_controller.list_action_versions,
            ),
            # v2 API
            web.get(
                "/v2/workflows/{workflow_id}",
                workflow_controller.get_workflow_detailed,
                allow_head=False,
            ),
            web.post(
                "/v2/workflows/{workflow_id}/modules",
                workflow_action_controller.add_workflow_action_with_link,
            ),
            web.put(
                "/v2/workflows/{workflow_id}/links",
                workflow_action_controller.update_workflow_actions_links,
            ),
            web.get(
                "/v2/workflows/{workflow_id}/results",
                workflow_controller.get_workflow_results,
            ),
            web.get(
                "/v2/workflows/{workflow_id}/run-results",
                workflow_controller.get_workflow_results_with_global_type,
            ),
            web.put(
                "/v2/workflows/{workflow_id}/results",
                workflow_controller.overwrite_workflow_results,
            ),
            web.put(
                "/v2/workflows/{workflow_id}/modules/{workflow_module_id}",
                workflow_action_controller.update_all_workflow_action_io,
            ),
            web.put(
                "/v2/workflows/{workflow_id}/modules/{workflow_action_id}/objectives",
                workflow_action_controller.set_objectives,
            ),
            web.put(
                "/v2/workflows/{workflow_id}/modules/{workflow_action_id}/constraints",
                workflow_action_controller.set_constraints,
            ),
            web.get(
                "/v2/workflows/{workflow_id}/endpoint",
                workflow_controller.get_workflow_endpoint,
            ),
            web.put(
                "/v2/workflows/{workflow_id}/endpoint",
                workflow_controller.overwrite_workflow_endpoint,
            ),
            web.get(
                "/v2/user-objects", project_variable_controller.list_project_variables
            ),
            web.get(
                "/v2/user-objects/{project_variable_id}",
                project_variable_controller.get_project_variable,
            ),
            web.put(
                "/v2/user-objects/{project_variable_id}",
                project_variable_controller.update_project_variable,
            ),
            web.delete(
                "/v2/user-objects/{project_variable_id}",
                project_variable_controller.delete_project_variable,
            ),
            web.post(
                "/v2/user-objects", project_variable_controller.add_project_variable
            ),
            web.post(
                "/v2/user-objects/file",
                project_variable_controller.add_project_variable_filetype,
            ),
            web.put(
                "/v2/user-objects/{project_variable_id}/file",
                project_variable_controller.update_project_variable_filetype,
            ),
        ]
    )

    # Configure api documentation
    setup_aiohttp_apispec(
        app,
        title="Studio Api",
        # FIXME unable to laod this from a stange bug with pytest in Nix leading to this error:
        # importlib.metadata.PackageNotFoundError: studio
        # version=__version__,
        url="/docs/swagger.json",
        swagger_path="/docs",
        static_path="/static/swagger",
        securityDefinitions={
            "bearer": {
                "type": "apiKey",
                "name": "Authorization",
                "in": "header",
                "description": "Ryax token",
            }
        },
        security=[{"bearer": []}],
    )
