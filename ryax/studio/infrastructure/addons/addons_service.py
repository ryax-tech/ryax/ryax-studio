from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.addon.addon_entities import AddOn, AddonInput, AddOns
from ryax.studio.domain.addon.addons_service import IAddonsService


class PoCAddonsService(IAddonsService):
    # Addons
    # TODO: We use direct values here but in the future this will come from external repo of addons
    HttpServiceAddon = AddOn(
        id="1",
        display_name="HTTP Service",
        description="Enable external access to HTTP service using a endpoint prefix",
        technical_name="http_service",
        parameters=[
            AddonInput.create(
                technical_name="endpoint_prefix",
                type=ActionIOType.STRING,
                display_name="Endpoint Prefix [HTTP Service Addon]",
                help="All API call that starts with this prefix will be sent to your service. The full prefix with Ryax instance prefix added will be exposed is user's action in the `ryax_endpoint_prefix` variable",
            ),
            AddonInput.create(
                technical_name="ryax_endpoint_port",
                alias="port",
                display_name="Service Port [HTTP Service Addon]",
                help="The service TCP port that you should serve on.",
                type=ActionIOType.INTEGER,
                default_value=8080,
            ),
            AddonInput.create(
                technical_name="base_url",
                display_name="Base URL [HTTP Service Addon]",
                help="The base URL your endpoint will be on",
                type=ActionIOType.STRING,
                editable=False,
            ),
            AddonInput.create(
                technical_name="ryax_endpoint_prefix",
                type=ActionIOType.STRING,
                display_name="Full Endpoint Prefix with Ryax instance included",
                help="Generated from user defined prefix and Ryax instance prefix. Expose is user's action",
                editable=False,
            ),
            AddonInput.create(
                technical_name="strip_path_prefix",
                display_name="Strip Prefix of Path [HTTP Service Addon]",
                help="If enabled, strip the prefix path of the request URL",
                type=ActionIOType.BOOLEAN,
                default_value=False,
            ),
        ],
    )

    HttpAPIAddon = AddOn(
        id="2",
        description="Create HTTP API endpoints that trigger your workflows",
        display_name="HttpAPI",
        technical_name="http_api",
        parameters=[
            AddonInput.create(
                technical_name="path",
                type=ActionIOType.STRING,
                display_name="Endpoint Path [HTTP API Addon]",
                help="Use curly brackets to add parameters in your path. Example: /users/{user_id}/assets/{assets_id}",
            ),
            AddonInput.create(
                technical_name="http_method",
                type=ActionIOType.ENUM,
                display_name="Method [HTTP API Addon]",
                help="HTTP method for your API endpoint.",
                enum_values=["GET", "PUT", "POST", "PATCH", "DELETE"],
                default_value="GET",
            ),
            AddonInput.create(
                technical_name="http_success_status_code",
                type=ActionIOType.ENUM,
                display_name="Success Code [HTTP API Addon]",
                help="A custom success status code 200 or 201, 202 is reserved by ryax in case of timeout. If timeout 202 is returned a link for pooling the result is given.",
                enum_values=["200", "201"],
                default_value="200",
            ),
            AddonInput.create(
                technical_name="async_reply_timeout",
                type=ActionIOType.FLOAT,
                display_name="Asynchronous Reply Timeout [HTTP API Addon]",
                help="A timeout in seconds after which the reply will be asynchronous: respond a 201 with a link to the future result. It negative, wait forever",
                default_value=20,
            ),
        ],
    )

    HpcOffloadingAddOn = AddOn(
        id="3",
        description="Offload the Ryax actions on an HPC cluster",
        display_name="HPC Slurm through SSH",
        technical_name="hpc",
        parameters=[
            AddonInput.create(
                technical_name="slurm_args",
                display_name="Header for Slurm Sbatch Script [HPC Addon]",
                help="Head of sbatch script",
                type=ActionIOType.LONGSTRING,
                default_value="#!/bin/bash\n#SBATCH --nodes=1\n#SBATCH --tasks=1",
            ),
            AddonInput.create(
                technical_name="custom_script",
                display_name="Custom Script [HPC Addon]",
                help=(
                    "This script will run directly on the Slurm node. It will bypass singularity run injected by Ryax. "
                    "WARNING: Inputs and outputs are imported and exported using environment variables. See documentation: https://docs.ryax.tech/reference/hpc.html"
                ),
                type=ActionIOType.LONGSTRING,
                optional=True,
            ),
            AddonInput.create(
                technical_name="username",
                display_name="Username [HPC Addon]",
                help="Override the site username used to connect (ssh) with the HPC frontend",
                type=ActionIOType.STRING,
                optional=True,
            ),
            AddonInput.create(
                technical_name="private_key",
                display_name="Private Key [HPC Addon]",
                help="Override the site private key used to connect (ssh) with the HPC frontend, DO NOT USE YOUR PERSONAL PRIVATE KEY, create a pair exclusive to grant ryax access",
                type=ActionIOType.LONGSTRING,
                optional=True,
            ),
            AddonInput.create(
                technical_name="password",
                display_name="Password [HPC Addon]",
                help="Override the site password used to connect (ssh) with the HPC frontend. Ignored when 'private_key' is set",
                type=ActionIOType.PASSWORD,
                optional=True,
            ),
        ],
    )

    KubernetesAddOn = AddOn(
        id="4",
        display_name="KubernetesAddon",
        description="Enable users to enact on kubernetes, add labels (for instance)",
        technical_name="kubernetes",
        parameters=[
            AddonInput.create(
                technical_name="labels",
                type=ActionIOType.STRING,
                display_name="Pod labels [Kubernetes Addon]",
                help="Add kubernetes labels on the deployment pod template, labels come in a single string respecting format label1=value1,label2=value2",
                optional=True,
            ),
            AddonInput.create(
                technical_name="annotations",
                type=ActionIOType.STRING,
                display_name="Pod annotations [Kubernetes Addon]",
                help="Add annotations to Kubernetes pod, for multiple annotation use single string comma separated list respecting format label1=value1,label2=value2",
                optional=True,
            ),
            AddonInput.create(
                technical_name="node_selector",
                type=ActionIOType.STRING,
                display_name="Pod nodeSelector [Kubernetes Addon]",
                help="Add nodeSelector to Kubernetes pod, it is defined on the pod spec of the deployment",
                optional=True,
            ),
            AddonInput.create(
                technical_name="service_account_name",
                type=ActionIOType.STRING,
                display_name="Pod serviceAccountName [Kubernetes Addon]",
                help="Add serviceAccountName to Kubernetes pod, it is defined on the pod spec of the deployment",
                optional=True,
            ),
            AddonInput.create(
                technical_name="service_name",
                type=ActionIOType.STRING,
                display_name="Kubernetes service name [Kubernetes Addon]",
                help="Add headless service and deploy the pod with label 'ryax.tech/kubernetes-service=service_name'",
                optional=True,
            ),
            AddonInput.create(
                technical_name="service_port",
                type=ActionIOType.INTEGER,
                display_name="Kubernetes service port [Kubernetes Addon]",
                help="TCP port for the headless service created when service_name is set, if service_name is set this parameter is mandatory",
                optional=True,
            ),
            AddonInput.create(
                technical_name="runtime_class",
                type=ActionIOType.STRING,
                display_name="Runtime Class [Kubernetes Addon]",
                help="Set the RuntimeClassName parameter of you pod to use alternate runtime like urunc of Kata. Get the list of Runtime class using : kubectl get runtimeclasses",
                optional=True,
            ),
        ],
    )

    def load(self) -> AddOns:
        addons = AddOns()
        addons.add(self.HttpServiceAddon)
        addons.add(self.HttpAPIAddon)
        addons.add(self.HpcOffloadingAddOn)
        addons.add(self.KubernetesAddOn)
        return addons
