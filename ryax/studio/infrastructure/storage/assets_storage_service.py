# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import os

from ryax.studio.domain.logo.logo import Logo
from ryax.studio.domain.services.assets_storage_service import IAssetsStorageService


class AssetsStorageService(IAssetsStorageService):
    def __init__(self, assets_path: str):
        self.assets_path = assets_path

    def get_default_logo(self) -> Logo:
        default_logo_path = os.path.join(self.assets_path, "default_logo.png")
        default_logo_content = open(default_logo_path, "rb").read()
        return Logo(
            id="default_logo",
            name="default_logo",
            extension="png",
            content=default_logo_content,
            action_id="NONE",
        )
