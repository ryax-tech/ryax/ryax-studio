# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from enum import IntEnum
from typing import Any, Optional, Type

from sqlalchemy import Integer, types


class IntEnumType(types.TypeDecorator):
    impl = Integer

    def __init__(self, enumtype: Type[IntEnum], *args: Any, **kwargs: Any) -> None:
        super().__init__(*args, **kwargs)
        self._enumtype = enumtype

    def process_bind_param(self, value: Any, dialect: Any) -> Any:
        return value.value

    def process_result_value(self, value: Any, dialect: Any) -> Optional[IntEnum]:
        return self._enumtype(value) if value is not None else None
