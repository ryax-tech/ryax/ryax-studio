# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from sqlalchemy import and_, event
from sqlalchemy.orm import Session, registry, relationship

from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_category import ActionCategory
from ryax.studio.domain.action.action_io import ActionIO
from ryax.studio.domain.action.action_resources import ActionResources
from ryax.studio.domain.logo.logo import Logo
from ryax.studio.domain.project_variables.project_variable_entities import (
    ProjectVariable,
)
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_category import (
    WorkflowActionCategory,
)
from ryax.studio.domain.workflow.entities.workflow_action_constraints import (
    WorkflowActionConstraints,
)
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionAddonInput,
    WorkflowActionInput,
)
from ryax.studio.domain.workflow.entities.workflow_action_link import WorkflowActionLink
from ryax.studio.domain.workflow.entities.workflow_action_objectives import (
    WorkflowActionObjectives,
)
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.entities.workflow_action_resources import (
    WorkflowActionResources,
)
from ryax.studio.domain.workflow.entities.workflow_error import WorkflowError
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.entities.workflow_result import WorkflowResult
from ryax.studio.infrastructure.database.metadata import (
    action_categories_association,
    action_category_table,
    action_io_table,
    action_resources_table,
    action_table,
    logo_table,
    project_variable_table,
    workflow_action_addons_input_table,
    workflow_action_category_table,
    workflow_action_input_table,
    workflow_action_output_table,
    workflow_action_resources_table,
    workflow_action_table,
    workflow_error_table,
    workflow_file_table,
    workflow_link_table,
    workflow_result_table,
    workflow_table,
    workflow_action_objectives_table,
    workflow_action_constraints_table,
)

mapper_registry = registry()
mapper = mapper_registry.map_imperatively


@event.listens_for(Workflow, "load")
def workflow_init_on_load(workflow: Workflow, _: str) -> None:
    workflow.events = []


def start_mapping() -> None:
    """Method to start mapping between database and domain models"""
    mapper(
        Workflow,
        workflow_table,
        properties={
            "actions": relationship(
                WorkflowAction,
                cascade="all, delete-orphan",
                backref="workflow",
                order_by=workflow_action_table.columns.position,
            ),
            "actions_links": relationship(
                WorkflowActionLink,
                cascade="all, delete-orphan",
            ),
            "errors": relationship(
                WorkflowError,
                cascade="all, delete-orphan",
                foreign_keys=[workflow_error_table.columns.workflow_id],
            ),
            "results": relationship(
                WorkflowResult,
                cascade="all, delete-orphan",
                order_by=workflow_result_table.columns.key,
            ),
        },
    )

    # Mapping workflow error table
    mapper(
        WorkflowError,
        workflow_error_table,
    )

    mapper(WorkflowResult, workflow_result_table)

    mapper(
        WorkflowActionOutput,
        workflow_action_output_table,
    )
    mapper(WorkflowActionObjectives, workflow_action_objectives_table)
    mapper(WorkflowActionConstraints, workflow_action_constraints_table)

    # Mapping for WorkflowAction class
    mapper(
        WorkflowAction,
        workflow_action_table,
        properties={
            "inputs": relationship(
                WorkflowActionInput,
                cascade="all, delete-orphan",
                order_by=workflow_action_input_table.columns.position,
            ),
            "outputs": relationship(
                WorkflowActionOutput,
                cascade="all, delete-orphan",
                primaryjoin=and_(
                    workflow_action_output_table.columns.workflow_action_id
                    == workflow_action_table.columns.id,
                    WorkflowActionOutput.is_dynamic == False,  # noqa
                ),
                order_by=workflow_action_output_table.columns.position,
            ),
            "dynamic_outputs": relationship(
                WorkflowActionOutput,
                cascade="all, delete-orphan",
                primaryjoin=and_(
                    workflow_action_output_table.columns.workflow_action_id
                    == workflow_action_table.columns.id,
                    WorkflowActionOutput.is_dynamic == True,  # noqa
                ),
                order_by=workflow_action_output_table.columns.position,
                overlaps="outputs",
            ),
            "addons_inputs": relationship(
                WorkflowActionAddonInput,
                cascade="all, delete-orphan",
                order_by=workflow_action_addons_input_table.columns.position,
            ),
            "errors": relationship(
                WorkflowError,
                cascade="all, delete-orphan",
                foreign_keys=[workflow_error_table.columns.workflow_action_id],
            ),
            "categories": relationship(
                WorkflowActionCategory, cascade="all, delete-orphan"
            ),
            "resources": relationship(WorkflowActionResources),
            "objectives": relationship(
                WorkflowActionObjectives, uselist=False, cascade="all, delete-orphan"
            ),
            "constraints": relationship(
                WorkflowActionConstraints, uselist=False, cascade="all, delete-orphan"
            ),
        },
    )

    mapper(
        WorkflowActionCategory,
        workflow_action_category_table,
        properties={
            "id": workflow_action_category_table.columns.id,
            "name": workflow_action_category_table.columns.name,
        },
    )

    # Mapping for WorkflowActionInput class
    mapper(
        WorkflowActionInput,
        workflow_action_input_table,
        properties={
            "reference_value": relationship(
                WorkflowActionOutput,
                foreign_keys=workflow_action_input_table.columns.workflow_action_output_id,
            ),
            "file_value": relationship(
                WorkflowFile,
                foreign_keys=workflow_action_input_table.columns.workflow_file_id,
            ),
            "project_variable_value": relationship(
                ProjectVariable,
                foreign_keys=workflow_action_input_table.columns.project_variable_id,
            ),
        },
    )

    mapper(
        WorkflowActionAddonInput,
        workflow_action_addons_input_table,
        properties={
            "reference_value": relationship(
                WorkflowActionOutput,
                foreign_keys=workflow_action_addons_input_table.columns.workflow_action_output_id,
            ),
            "file_value": relationship(
                WorkflowFile,
                foreign_keys=workflow_action_addons_input_table.columns.workflow_file_id,
            ),
            "project_variable_value": relationship(
                ProjectVariable,
                foreign_keys=workflow_action_addons_input_table.columns.project_variable_id,
            ),
        },
    )

    # Mapping for WorkflowActionLink class
    mapper(
        WorkflowActionLink,
        workflow_link_table,
    )

    # Mapping for Action class
    mapper(
        Action,
        action_table,
        properties={
            "inputs": relationship(
                ActionIO,
                foreign_keys=[action_io_table.columns.input_of_action_id],
                cascade="all, delete-orphan",
            ),
            "outputs": relationship(
                ActionIO,
                foreign_keys=[action_io_table.columns.output_of_action_id],
                cascade="all, delete-orphan",
            ),
            "logo": relationship(
                Logo, cascade="all, delete-orphan", single_parent=True
            ),
            "resources": relationship(ActionResources),
            "categories": relationship(
                ActionCategory,
                secondary=action_categories_association,
                back_populates="actions",
            ),
        },
    )

    mapper(
        ActionCategory,
        action_category_table,
        properties={
            "id": action_category_table.columns.id,
            "name": action_category_table.columns.name,
            "actions": relationship(
                Action,
                secondary=action_categories_association,
                back_populates="categories",
            ),
        },
    )

    # Mapping for ActionIO class
    mapper(
        ActionIO,
        action_io_table,
    )
    # Mapping for Logo class
    mapper(
        Logo,
        logo_table,
    )

    mapper(ActionResources, action_resources_table)
    mapper(WorkflowActionResources, workflow_action_resources_table)

    mapper(
        WorkflowFile,
        workflow_file_table,
    )
    mapper(ProjectVariable, project_variable_table)


# This function removes categories whenever they are not assigned to any actions
# TODO Make this function run only when actions are deleted. Otherwise it hurts perf. (we could just move it to the delete action func in the service)
@event.listens_for(Session, "after_flush")
def delete_unused_action_categories(session: Session, flush_context: str) -> None:
    session.query(ActionCategory).filter(~ActionCategory.actions.any()).delete(synchronize_session=False)  # type: ignore
