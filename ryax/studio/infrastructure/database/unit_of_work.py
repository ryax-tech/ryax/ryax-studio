# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import Any, Callable

from ryax.studio.domain.common.unit_of_work import IUnitOfWork
from ryax.studio.infrastructure.database.engine import DatabaseEngine
from ryax.studio.infrastructure.database.repositories.action_repository import (
    DatabaseActionRepository,
)
from ryax.studio.infrastructure.database.repositories.project_variable_repository import (
    DatabaseUserObjectRepository,
)
from ryax.studio.infrastructure.database.repositories.workflow_repository import (
    DatabaseWorkflowRepository,
)


class DatabaseUnitOfWork(IUnitOfWork):
    def __init__(
        self,
        engine: DatabaseEngine,
        workflow_repository_factory: Callable[..., DatabaseWorkflowRepository],
        action_repository_factory: Callable[..., DatabaseActionRepository],
        project_variable_repository_factory: Callable[
            ..., DatabaseUserObjectRepository
        ],
    ) -> None:
        self.engine = engine
        self.workflow_repository_factory = workflow_repository_factory
        self.action_repository_factory = action_repository_factory
        self.project_variable_repository_factory = project_variable_repository_factory

    def __enter__(self) -> None:
        self.session = self.engine.get_session()
        self.workflows = self.workflow_repository_factory(session=self.session)
        self.actions = self.action_repository_factory(session=self.session)
        self.project_variables = self.project_variable_repository_factory(
            session=self.session
        )

    def __exit__(self, *args: Any) -> None:
        self.session.close()

    def commit(self) -> None:
        self.session.commit()

    def rollback(self) -> None:
        self.session.rollback()
        self.session.refresh()
