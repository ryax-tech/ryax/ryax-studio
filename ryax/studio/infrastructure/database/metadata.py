# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from sqlalchemy import (
    JSON,
    BigInteger,
    Boolean,
    Column,
    DateTime,
    Enum,
    Float,
    ForeignKey,
    Integer,
    MetaData,
    PickleType,
    String,
    Table,
    UniqueConstraint,
)
from sqlalchemy.types import LargeBinary
from sqlalchemy_utils import ScalarListType

# Model metadata
from ryax.studio.domain.action.action_values import (
    ActionIOType,
    ActionKind,
    DynamicOutputOrigin,
)
from ryax.studio.domain.workflow.workflow_values import (
    WorkflowActionDeployStatus,
    WorkflowActionValidityStatus,
    WorkflowDeploymentStatus,
    WorkflowErrorCode,
    WorkflowValidityStatus,
)
from ryax.studio.infrastructure.database.utils import IntEnumType

metadata = MetaData()

action_kind = Enum(
    ActionKind, name="action_kind", values_callable=lambda x: [e.value for e in x]
)

dynamic_output_origin = Enum(
    DynamicOutputOrigin,
    name="dynamic_output_origin",
    values_callable=lambda x: [e.value for e in x],
)

# Action table declaration
action_table: Table = Table(
    "action",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column("technical_name", String, nullable=False),
    Column("version", String, nullable=False),
    Column("kind", action_kind, nullable=False),
    Column("description", String, nullable=True),
    Column("lockfile", LargeBinary, nullable=True),
    Column("has_dynamic_outputs", Boolean, nullable=False),
    Column("logo_id", ForeignKey("logo.id"), nullable=True),
    Column("resources_id", ForeignKey("action_resources.id"), nullable=True),
    Column("build_date", DateTime, nullable=False),
    Column("project", String, nullable=False),
    Column("addons", JSON, nullable=True),
)

# create an FK from many to one Category -> Action
action_category_table = Table(
    "action_category",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    UniqueConstraint("name"),
)

action_categories_association = Table(
    "action_categories_association",
    metadata,
    Column("action_category_id", String, ForeignKey("action_category.id")),
    Column("action_id", String, ForeignKey("action.id")),
)

action_io_type = Enum(
    ActionIOType, name="action_io_type", values_callable=lambda x: [e.value for e in x]
)

# Action io table declaration
action_io_table: Table = Table(
    "action_io",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("display_name", String, nullable=False),
    Column("technical_name", String, nullable=True),
    Column("help", String, nullable=True),
    Column("type", action_io_type, nullable=False),
    Column("enum_values", ScalarListType(str), nullable=False),
    Column("input_of_action_id", ForeignKey("action.id"), nullable=True),
    Column("output_of_action_id", ForeignKey("action.id"), nullable=True),
    Column("default_value", PickleType, nullable=True),
    Column("optional", Boolean, nullable=False),
)

# Logo table declaration
logo_table: Table = Table(
    "logo",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column("action_id", String, nullable=False),
    Column("extension", String, nullable=False),
    Column("content", LargeBinary, nullable=False),
)

action_resources_table = Table(
    "action_resources",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("cpu", Float, nullable=True),
    Column("memory", BigInteger, nullable=True),
    Column("time", Float, nullable=True),
    Column("gpu", Integer, nullable=True),
)

workflow_validity_status = Enum(
    WorkflowValidityStatus,
    name="validity status",
    values_callable=lambda x: [e.value for e in x],
)

workflow_deployment_status = Enum(
    WorkflowDeploymentStatus,
    name="deployment status",
    values_callable=lambda x: [e.value for e in x],
)

workflow_action_validity_status = Enum(
    WorkflowActionValidityStatus,
    name="validity status",
    values_callable=lambda x: [e.value for e in x],
)

workflow_action_deployment_status = Enum(
    WorkflowActionDeployStatus,
    name="deployment status",
    values_callable=lambda x: [e.value for e in x],
)

# Workflow table declaration
workflow_table: Table = Table(
    "workflow",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column("description", String, nullable=True),
    Column("validity_status", workflow_validity_status),
    Column("deployment_status", workflow_deployment_status),
    Column("deployment_error", String, nullable=True),
    Column("owner", String, nullable=False),
    Column("project", String, nullable=False),
)

# Workflow error table
workflow_error_table: Table = Table(
    "workflow_error",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("code", IntEnumType(WorkflowErrorCode), nullable=False),
    Column("message", String, nullable=True),
    Column("workflow_id", ForeignKey("workflow.id", ondelete="CASCADE"), nullable=True),
    Column(
        "workflow_action_id",
        ForeignKey("workflow_action.id", ondelete="CASCADE"),
        nullable=True,
    ),
)

workflow_action_objectives_table = Table(
    "workflow_action_objectives",
    metadata,
    Column("energy", Float),
    Column("cost", Float),
    Column("performance", Float),
    Column(
        "workflow_action_id",
        ForeignKey("workflow_action.id", ondelete="CASCADE"),
        primary_key=True,
    ),
)
workflow_action_constraints_table = Table(
    "workflow_action_constraints",
    metadata,
    Column("site_list", ScalarListType()),
    Column("site_type_list", ScalarListType()),
    Column("node_pool_list", ScalarListType()),
    Column("arch_list", ScalarListType()),
    Column(
        "workflow_action_id",
        ForeignKey("workflow_action.id", ondelete="CASCADE"),
        primary_key=True,
    ),
)

# Workflow node table declaration
workflow_action_table: Table = Table(
    "workflow_action",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column("technical_name", String, nullable=False),
    Column("version", String, nullable=False),
    Column("description", String, nullable=True),
    Column("kind", action_kind, nullable=False),
    Column("has_dynamic_outputs", Boolean, nullable=False),
    Column("custom_name", String, nullable=True),
    Column("position_x", Integer, nullable=True),
    Column("position_y", Integer, nullable=True),
    Column("validity_status", workflow_action_validity_status, nullable=True),
    Column("deployment_status", workflow_action_deployment_status, nullable=True),
    Column(
        "workflow_id",
        String,
        ForeignKey("workflow.id", ondelete="CASCADE"),
        nullable=False,
    ),
    Column("action_id", String, nullable=False),
    Column("position", Integer),
    Column("endpoint", String, nullable=True),
    Column("addons", JSON, nullable=True),
    Column("resources_id", ForeignKey("workflow_action_resources.id"), nullable=True),
)

workflow_action_category_table = Table(
    "workflow_action_category",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column(
        "workflow_action_id",
        ForeignKey("workflow_action.id", ondelete="CASCADE"),
        nullable=False,
    ),
)

workflow_action_resources_table = Table(
    "workflow_action_resources",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("cpu", Float, nullable=True),
    Column("memory", BigInteger, nullable=True),
    Column("time", Float, nullable=True),
    Column("gpu", Integer, nullable=True),
)

# Workflow action input table declaration
workflow_action_input_table: Table = Table(
    "workflow_action_input",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("technical_name", String, nullable=True),
    Column("display_name", String, nullable=True),
    Column("help", String, nullable=True),
    Column("type", action_io_type, nullable=True),
    Column("enum_values", ScalarListType(str), nullable=True),
    Column("static_value", PickleType, nullable=True),
    Column(
        "workflow_action_output_id",
        ForeignKey("workflow_action_output.id", ondelete="SET NULL"),
        nullable=True,
    ),
    Column(
        "workflow_action_id",
        ForeignKey("workflow_action.id", ondelete="CASCADE"),
        nullable=False,
    ),
    Column(
        "workflow_file_id",
        ForeignKey("workflow_file.id", ondelete="SET NULL"),
        nullable=True,
    ),
    Column(
        "project_variable_id",
        ForeignKey("project_variable.id", ondelete="SET NULL"),
        nullable=True,
    ),
    Column("position", Integer),
    Column("optional", Boolean, nullable=False),
)
workflow_action_addons_input_table: Table = Table(
    "workflow_action_addons_input",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("technical_name", String, nullable=True),
    Column("display_name", String, nullable=True),
    Column("help", String, nullable=True),
    Column("type", action_io_type, nullable=True),
    Column("enum_values", ScalarListType(str), nullable=True),
    Column("static_value", PickleType, nullable=True),
    Column(
        "workflow_action_output_id",
        ForeignKey("workflow_action_output.id", ondelete="SET NULL"),
        nullable=True,
    ),
    Column(
        "workflow_action_id",
        ForeignKey("workflow_action.id", ondelete="CASCADE"),
        nullable=False,
    ),
    Column(
        "workflow_file_id",
        ForeignKey("workflow_file.id", ondelete="SET NULL"),
        nullable=True,
    ),
    Column(
        "project_variable_id",
        ForeignKey("project_variable.id", ondelete="SET NULL"),
        nullable=True,
    ),
    Column("position", Integer),
    Column("addon_name", String),
    Column("optional", Boolean, nullable=False),
    Column("editable", Boolean, nullable=False),
)

workflow_action_output_table: Table = Table(
    "workflow_action_output",
    metadata,
    Column("id", String, primary_key=True),
    Column("technical_name", String, nullable=True),
    Column("display_name", String, nullable=True),
    Column("help", String, nullable=True),
    Column("type", action_io_type, nullable=True),
    Column("enum_values", ScalarListType(str), nullable=True),
    Column(
        "workflow_action_id",
        ForeignKey("workflow_action.id", ondelete="CASCADE"),
        nullable=False,
    ),
    Column("is_dynamic", Boolean, nullable=False),
    Column("origin", dynamic_output_origin),
    Column("position", Integer),
    Column("optional", Boolean, nullable=False),
)


# Workflow link table
workflow_link_table: Table = Table(
    "workflow_link",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column(
        "upstream_action_id",
        String,
        ForeignKey("workflow_action.id", ondelete="CASCADE"),
        nullable=False,
    ),
    Column(
        "downstream_action_id",
        String,
        ForeignKey("workflow_action.id", ondelete="CASCADE"),
        nullable=False,
    ),
    Column(
        "workflow_id",
        String,
        ForeignKey("workflow.id", ondelete="CASCADE"),
        nullable=False,
    ),
)

workflow_file_table: Table = Table(
    "workflow_file",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column("extension", String, nullable=False),
    Column("path", String, nullable=False),
    Column("size", Float, nullable=False),
)

workflow_result_table: Table = Table(
    "workflow_result",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("key", String, nullable=False),
    Column(
        "workflow_action_input_id",
        String,
        ForeignKey("workflow_action_input.id", ondelete="CASCADE"),
        nullable=True,
    ),
    Column(
        "workflow_action_output_id",
        String,
        ForeignKey("workflow_action_output.id", ondelete="CASCADE"),
        nullable=True,
    ),
    Column(
        "workflow_id",
        String,
        ForeignKey("workflow.id", ondelete="CASCADE"),
        nullable=False,
    ),
)

project_variable_table: Table = Table(
    "project_variable",
    metadata,
    Column("id", String, primary_key=True, nullable=False),
    Column("name", String, nullable=False),
    Column("value", PickleType, nullable=False),
    Column("type", action_io_type, nullable=False),
    Column("project_id", String, nullable=False),
    Column("description", String, nullable=True),
    Column("file_path", String, nullable=True),
)
