# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
from typing import Optional

from sqlalchemy.engine import Engine, create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.session import _SessionClassMethods

from ryax.studio.infrastructure.database.mapper import start_mapping
from ryax.studio.infrastructure.database.metadata import metadata

Session = sessionmaker()


def _remove_password_from_URI(uri: str) -> str:
    passw = uri.split("://")[1].split("@")[0].split(":")[1]
    return uri.replace(passw, "******")


class DatabaseEngine:
    def __init__(self, connection_url: str) -> None:
        self.logger = logging.getLogger(DatabaseEngine.__name__)
        self.connection_url: str = connection_url
        self.connection: Optional[Engine] = None

    def connect(self) -> None:
        self.logger.info("Connecting to database")
        self.logger.debug(
            "Database connection url: %s",
            _remove_password_from_URI(self.connection_url),
        )
        if self.connection_url.startswith("sqlite"):
            self.connection = create_engine(self.connection_url)
        else:
            self.connection = create_engine(
                self.connection_url, pool_size=10, max_overflow=90
            )
        metadata.create_all(bind=self.connection)
        start_mapping()

    def disconnect(self) -> None:
        self.logger.info("Disconnecting from database")
        if self.connection:
            self.connection.dispose()

    def get_session(self) -> _SessionClassMethods:
        return Session(bind=self.connection)
