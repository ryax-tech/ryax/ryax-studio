# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import List, Optional

from sqlalchemy import and_, desc, func, or_
from sqlalchemy.orm import Session

from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_category import ActionCategory
from ryax.studio.domain.action.action_exceptions import ActionNotFoundException
from ryax.studio.domain.action.action_repository import IActionRepository
from ryax.studio.domain.action.views.action import ActionView


class DatabaseActionRepository(IActionRepository):
    """Postgres implementation of action repository"""

    def __init__(self, session: Session):
        self.session = session

    def get_actions_views(
        self, project_id: str, search: str = "", category: Optional[str] = None
    ) -> List[ActionView]:
        """Explanation in 3 steps:
        1. If there's a search, we gather our search filters in a var search_filters
        2. Create subquery called max_versions to get the latest version of a action, grouped by technical name
        3. Get the ActionViews, taking into account the subquery (.join) and the list of search filters (.filter).
        """
        all_filters = []
        all_filters.append(Action.project == project_id)
        if search:
            search = search.lower()
            all_filters.append(
                or_(
                    func.lower(Action.name).contains(search),
                    func.lower(Action.description).contains(search),
                )
            )
        if category is not None:
            all_filters.append(Action.categories.any(name=category))  # type: ignore
        max_versions = (
            self.session.query(
                Action.technical_name,
                func.max(Action.build_date).label("max_version"),
            )
            .filter(Action.project == project_id)
            .group_by(Action.technical_name)
            .subquery()
        )
        actions = (
            self.session.query(Action)
            .join(
                max_versions,
                and_(
                    Action.technical_name == max_versions.c.technical_name,
                    Action.build_date == max_versions.c.max_version,
                ),
            )
            .filter(*all_filters)
            .order_by(Action.name)
            .all()
        )
        return [
            ActionView(
                id=action.id,
                description=action.description,
                kind=action.kind,
                lockfile=action.lockfile,
                version=action.version,
                name=action.name,
            )
            for action in actions
        ]

    def get_action(self, action_id: str) -> Action:
        action = self.session.query(Action).get(action_id)
        if not action:
            raise ActionNotFoundException()
        return action

    def add_action(self, action: Action) -> None:
        self.session.merge(action)

    def delete_action(self, action: Action) -> None:
        self.session.delete(action)

    def get_action_by_name_version_project(
        self, technical_name: str, version: str, project: str
    ) -> Action:
        action = (
            self.session.query(Action)
            .filter(
                and_(
                    Action.technical_name == technical_name,
                    Action.version == version,
                    Action.project == project,
                )
            )
            .first()
        )
        if not action:
            raise ActionNotFoundException()
        return action

    def get_action_versions(self, action_id: str) -> List[Action]:
        action = self.session.query(Action).get(action_id)
        if not action:
            raise ActionNotFoundException()
        actions = (
            self.session.query(Action)
            .filter(Action.technical_name == action.technical_name)
            .filter(Action.project == action.project)
            .order_by(desc(Action.build_date))
            .all()
        )
        return actions

    def get_action_category_by_name(
        self, category_name: str
    ) -> Optional[ActionCategory]:
        return (
            self.session.query(ActionCategory)
            .filter(ActionCategory.name == category_name)
            .first()
        )

    def list_categories(self) -> List[ActionCategory]:
        return self.session.query(ActionCategory).order_by(ActionCategory.name).all()
