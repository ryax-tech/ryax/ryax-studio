# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import List, Optional

from sqlalchemy import and_, select
from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import Session

from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.project_variables.project_variable_entities import (
    ProjectVariable,
)
from ryax.studio.domain.project_variables.project_variable_exceptions import (
    ProjectVariableNotFoundException,
)
from ryax.studio.domain.project_variables.project_variable_repository import (
    IUserObjectRepository,
)
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionInput,
)


class DatabaseUserObjectRepository(IUserObjectRepository):
    """Postgres implementation of user object repository"""

    def __init__(self, session: Session):
        self.session = session

    def list(
        self, project_id: str, type_search: Optional[str]
    ) -> List[ProjectVariable]:
        all_filters = []
        all_filters.append(ProjectVariable.project_id == project_id)
        if type_search is not None:
            all_filters.append(
                ProjectVariable.type == ActionIOType.string_to_enum(type_search)
            )
        return (
            self.session.query(ProjectVariable)
            .filter(*all_filters)
            .order_by(ProjectVariable.id)
            .all()
        )

    def get(self, project_variable_id: str, project_id: str) -> ProjectVariable:
        try:
            project_variable = (
                self.session.query(ProjectVariable)
                .filter(
                    and_(
                        ProjectVariable.id == project_variable_id,
                        ProjectVariable.project_id == project_id,
                    )
                )
                .one()
            )
            return project_variable
        except NoResultFound:
            raise ProjectVariableNotFoundException

    def get_linked_workflows(self, project_variable: ProjectVariable) -> List[Workflow]:
        return (
            self.session.execute(
                select(Workflow)
                .join(Workflow.actions)
                .join(WorkflowAction.inputs)
                .where(WorkflowActionInput.project_variable_value == project_variable)
            )
            .scalars()
            .all()
        )

    def add(self, project_variable: ProjectVariable) -> None:
        self.session.add(project_variable)

    def delete(self, project_variable: ProjectVariable) -> None:
        self.session.delete(project_variable)
