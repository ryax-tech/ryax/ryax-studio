# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import List, Optional, Sequence

from sqlalchemy import and_, exists, func, not_, or_
from sqlalchemy.orm import Session

from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionInput,
)
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.entities.workflow_error import WorkflowError
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.views.workflow_action_input_view import (
    WorkflowActionInputView,
)
from ryax.studio.domain.workflow.views.workflow_action_link_view import (
    WorkflowActionLinkView,
)
from ryax.studio.domain.workflow.views.workflow_action_output_view import (
    WorkflowActionOutputView,
)
from ryax.studio.domain.workflow.views.workflow_action_view import WorkflowActionView
from ryax.studio.domain.workflow.views.workflow_error_view import WorkflowErrorView
from ryax.studio.domain.workflow.views.workflow_view import WorkflowView
from ryax.studio.domain.workflow.workflow_exceptions import (
    EndpointAlreadyExistsException,
    WorkflowActionNotFoundException,
    WorkflowNotFoundException,
)
from ryax.studio.domain.workflow.workflow_repository import IWorkflowRepository
from ryax.studio.domain.workflow.workflow_values import WorkflowDeploymentStatus
from ryax.studio.infrastructure.database.metadata import (
    workflow_action_input_table,
    workflow_action_output_table,
    workflow_action_table,
    workflow_error_table,
)


class DatabaseWorkflowRepository(IWorkflowRepository):
    """Postgres implementation of workflow repository"""

    def __init__(self, session: Session):
        self.session = session

    def check_workflow_exists(self, workflow_id: str) -> None:
        workflow_exists = self.session.query(
            exists().where(Workflow.id == workflow_id)
        ).scalar()
        if not workflow_exists:
            raise WorkflowNotFoundException()

    def check_workflow_action_exists(self, workflow_action_id: str) -> None:
        workflow_action_exists = self.session.query(
            exists().where(WorkflowAction.id == workflow_action_id)
        ).scalar()
        if not workflow_action_exists:
            raise WorkflowActionNotFoundException()

    def list_workflows(
        self,
        current_project_id: str,
        search: Optional[str] = None,
        deployment_status: Optional[WorkflowDeploymentStatus] = None,
    ) -> List[Workflow]:
        filters = []
        filters.append(Workflow.project == current_project_id)
        if search is not None:
            filters.append(
                or_(
                    func.lower(Workflow.name).contains(search),
                    func.lower(Workflow.description).contains(search),
                )
            )
        if deployment_status is not None:
            filters.append(Workflow.deployment_status == deployment_status.value)
        workflows: list = (
            self.session.query(Workflow)
            .filter(*filters)
            .order_by(func.lower(Workflow.name))
            .all()
        )
        return workflows

    def get_workflow_view(self, workflow_id: str) -> WorkflowView:
        workflow = self.session.query(Workflow).get(workflow_id)
        if workflow is None:
            raise WorkflowNotFoundException()
        return WorkflowView.from_workflow(workflow)

    def list_previous_workflow_actions_view(self, workflow_action_id: str) -> List[str]:
        root_action_query = (
            self.session.query(WorkflowAction.id)
            .filter(WorkflowAction.id == workflow_action_id)
            .cte(name="root_action", recursive=True)
        )

        previous_actions_query = self.session.query(WorkflowActionView.id).filter(
            and_(
                WorkflowActionView.id == WorkflowActionLinkView.upstream_action_id,
                root_action_query.columns.id
                == WorkflowActionLinkView.downstream_action_id,
            )
        )

        result_query = root_action_query.union(previous_actions_query)
        result = (
            self.session.query(result_query)
            .filter(result_query.columns.id != workflow_action_id)
            .all()
        )
        return [item[0] for item in result]

    def get_workflow_errors(self, workflow_id: str) -> List[WorkflowErrorView]:
        errors = (
            self.session.query(
                WorkflowError, WorkflowAction.id, WorkflowAction.workflow_id
            )
            .join(WorkflowAction, isouter=True)
            .filter(
                or_(
                    workflow_error_table.columns.workflow_id == workflow_id,
                    workflow_action_table.columns.workflow_id == workflow_id,
                )
            )
            .all()
        )
        return [
            WorkflowErrorView(
                id=error[0].id,
                code=error[0].code,
                workflow_action_id=error[1],
                workflow_id=workflow_id,
            )
            for error in errors
        ]

    def get_workflow(self, workflow_id: str) -> Workflow:
        workflow = self.session.query(Workflow).get(workflow_id)
        if not workflow:
            raise WorkflowNotFoundException()
        else:
            return workflow

    def delete_static_file(self, workflow_file: WorkflowFile) -> None:
        self.session.delete(workflow_file)

    def add_workflow(self, workflow: Workflow) -> None:
        self.session.add(workflow)

    def delete_workflow(self, workflow: Workflow) -> None:
        self.session.delete(workflow)

    def list_workflow_action_inputs_view(
        self, workflow_action_id: str
    ) -> List[WorkflowActionInputView]:
        inputs: list[WorkflowActionInput] = (
            self.session.query(WorkflowActionInput)
            .filter(
                workflow_action_input_table.columns.workflow_action_id
                == workflow_action_id
            )
            .all()
        )
        return [
            WorkflowActionInputView.from_workflow_action_input(input)
            for input in inputs
        ]

    def list_workflow_action_outputs_view(
        self, workflow_action_id: str
    ) -> Sequence[WorkflowActionOutputView]:
        outputs = (
            self.session.query(WorkflowActionOutput)
            .filter(
                workflow_action_output_table.columns.workflow_action_id
                == workflow_action_id
            )
            # FIXME: This is a hack use an additional field for this
            .filter(not_(WorkflowActionOutput.technical_name.startswith("ryax_")))
            .all()
        )
        return [
            WorkflowActionOutputView.from_workflow_action_output(output)
            for output in outputs
        ]

    def search_workflow_action_outputs_view(
        self,
        workflow_id: str,
        with_type: Optional[ActionIOType] = None,
        exclude_workflow_actions: Optional[List[str]] = None,
        in_workflow_actions: Optional[List[str]] = None,
    ) -> Sequence[WorkflowActionOutputView]:
        filters = [WorkflowAction.workflow_id == workflow_id]

        if with_type is not None:
            filters.append(
                WorkflowActionOutput.type == with_type.value,
            )

        if exclude_workflow_actions is not None:
            filters.append(~WorkflowActionOutput.workflow_action_id.in_(exclude_workflow_actions))  # type: ignore

        if in_workflow_actions is not None:
            filters.append(WorkflowActionOutput.workflow_action_id.in_(in_workflow_actions))  # type: ignore

        outputs = (
            self.session.query(WorkflowActionOutput)
            .join(
                WorkflowAction,
                workflow_action_output_table.columns.workflow_action_id
                == WorkflowAction.id,
            )
            .filter(*filters)
            .all()
        )
        return [
            WorkflowActionOutputView.from_workflow_action_output(output)
            for output in outputs
        ]

    def list_workflows_where_action_used(self, action_id: str) -> List[Workflow]:
        return (
            self.session.query(Workflow)
            .join(WorkflowAction)
            .filter(WorkflowAction.action_id == action_id)
            .all()
        )

    def check_if_endpoint_exists(
        self, endpoint: Optional[str], project_id: str
    ) -> None:
        if endpoint is None:
            return

        endpoint_exists = (
            self.session.query(WorkflowAction)
            .join(Workflow)
            .filter(Workflow.project == project_id)
            .filter(WorkflowAction.endpoint == endpoint)
            .all()
        )
        if len(endpoint_exists) > 0:
            raise EndpointAlreadyExistsException()
