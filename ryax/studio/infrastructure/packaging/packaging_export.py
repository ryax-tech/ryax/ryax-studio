# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
import tempfile
import zipfile
from typing import Any, Dict, Optional
from uuid import uuid4

import yaml

from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.services.workflow_export_service import (
    IWorkflowExportService,
)


class PackagingExportService(IWorkflowExportService):
    def __init__(self, tmp_directory_path: str):
        self.logger = logging.getLogger("YamlExportService")
        self.tmp_directory_path = tmp_directory_path
        self.workflow_zip_file: Optional[zipfile.ZipFile] = None

    def _format_workflow(
        self,
        workflow: Workflow,
        all_inputs_files: Dict[str, bytes],
    ) -> dict:
        return {
            "apiVersion": "ryax.tech/v1",
            "kind": "Workflows",
            "spec": {
                "human_name": workflow.name,
                "description": workflow.description,
                "functions": [
                    self.format_workflow_action(workflow, item, all_inputs_files)
                    for item in workflow.actions
                ],
            },
        }

    def format_workflow_action(
        self,
        workflow: Workflow,
        workflow_action: WorkflowAction,
        all_inputs_files: Dict[str, bytes],
    ) -> dict:
        workflow_action_export: Dict[str, Any] = {
            "id": workflow_action.id,
            "from": workflow_action.technical_name,
            "position": {
                "x": workflow_action.position_x,
                "y": workflow_action.position_y,
            },
            "version": workflow_action.version,
            "inputs_values": self.format_workflow_input_values(
                workflow, workflow_action, all_inputs_files
            ),
            "streams_to": workflow.get_workflow_action_streams_to(workflow_action.id),
        }
        if workflow_action.has_dynamic_outputs:
            workflow_action_export["outputs"] = [
                PackagingExportService._format_workflow_action_outputs(item)
                for item in workflow_action.dynamic_outputs
            ]
        return workflow_action_export

    def format_workflow_input_values(
        self,
        workflow: Workflow,
        workflow_action: WorkflowAction,
        all_inputs_files: Dict[str, bytes],
    ) -> dict:
        outputs_map = {}
        outputs_parent_map = {}

        for action in workflow.actions:
            for output in action.get_outputs():
                outputs_map[output.id] = output
                outputs_parent_map[output.id] = action

        inputs_values = {}
        for item in workflow_action.inputs:
            if item.has_defined_value():
                if item.has_static_value():
                    inputs_values[item.technical_name] = item.static_value
                elif item.reference_value is not None:
                    workflow_action_output_id = item.reference_value.id
                    reference_action = outputs_parent_map[workflow_action_output_id]
                    reference_output = outputs_map[workflow_action_output_id]
                    inputs_values[
                        item.technical_name
                    ] = f"={reference_action.id}.{reference_output.technical_name}"
                elif item.file_value is not None:
                    input_file_name = (
                        f"{uuid4()}-{item.file_value.name}.{item.file_value.extension}"
                    )
                    input_file = all_inputs_files[item.file_value.path]
                    self.add_file_to_workflow_zip(input_file, input_file_name)
                    inputs_values[item.technical_name] = input_file_name
        return inputs_values

    @staticmethod
    def _format_workflow_action_outputs(
        workflow_action_output: WorkflowActionOutput,
    ) -> dict:
        return {
            "name": workflow_action_output.technical_name,
            "human_name": workflow_action_output.display_name,
            "type": workflow_action_output.type.value,
            "help": workflow_action_output.help,
            "enum_values": workflow_action_output.enum_values,
        }

    def add_file_to_workflow_zip(self, file_data: bytes, name: str) -> None:
        assert self.workflow_zip_file is not None
        yaml_tmp_file = tempfile.NamedTemporaryFile()
        yaml_tmp_file.write(file_data)
        yaml_tmp_file.seek(0)
        self.workflow_zip_file.write(yaml_tmp_file.name, arcname=name)

    def export_workflow(
        self, workflow: Workflow, all_inputs_files: Dict[str, bytes]
    ) -> None:
        assert self.workflow_zip_file is not None
        workflow_dict = self._format_workflow(workflow, all_inputs_files)
        workflow_yaml = yaml.safe_dump(workflow_dict)
        self.add_file_to_workflow_zip(workflow_yaml.encode(), "workflow.yaml")
        self.workflow_zip_file.close()

    def generate_workflow_zip_file(self) -> str:
        path_to_workflow_zip: str = f"{self.tmp_directory_path}/{uuid4()}-workflow.zip"
        self.workflow_zip_file = zipfile.ZipFile(path_to_workflow_zip, mode="w")
        return path_to_workflow_zip
