# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
import os
import uuid
import zipfile
from typing import Any, Dict, List, Optional, Tuple

import yaml
from jsonschema import ValidationError, validate

from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_io import ActionIO
from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionInput,
)
from ryax.studio.domain.workflow.entities.workflow_action_link import WorkflowActionLink
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.entities.workflow_action_resources import (
    WorkflowActionResources,
)
from ryax.studio.domain.workflow.services.workflow_import_service import (
    IWorkflowImportService,
)
from ryax.studio.domain.workflow.workflow_exceptions import (
    WorkflowBadZipFile,
    WorkflowInvalidSchemaException,
    WorkflowParsingYamlException,
    WorkflowYamlNotFound,
)
from ryax.studio.domain.workflow.workflow_values import (
    WorkflowActionDeployStatus,
    WorkflowDeploymentStatus,
)


class PackagingImportService(IWorkflowImportService):
    def __init__(self, tmp_directory_path: str):
        self.tmp_directory_path = tmp_directory_path
        self.zip_file: Optional[zipfile.ZipFile] = None
        self.yaml_data: Dict[str, Any] = {}
        self._schema = {
            "type": "object",
            "properties": {
                "apiVersion": {"type": "string", "const": "ryax.tech/v1"},
                "kind": {"type": "string", "const": "Workflows"},
                "spec": {
                    "type": "object",
                    "properties": {
                        "human_name": {
                            "type": "string",
                            "minLength": 3,
                            "maxLength": 128,
                        },
                        "description": {
                            "anyOf": [
                                {"type": "string", "minLength": 0, "maxLength": 512},
                                {"type": "null"},
                            ]
                        },
                        "functions": {
                            "type": "array",
                            "maxItems": 128,
                            "description": "The list of all actions of this workflow.",
                            "items": {
                                "type": "object",
                                "properties": {
                                    "id": {
                                        "type": "string",
                                        "minLength": 3,
                                        "maxLength": 64,
                                    },
                                    "from": {"type": "string", "minLength": 1},
                                    "position": {
                                        "type": "object",
                                        "properties": {
                                            "x": {
                                                "type": ["null", "integer"],
                                                "minimum": 0,
                                                "exclusiveMaximum": 32,
                                            },
                                            "y": {
                                                "type": ["null", "integer"],
                                                "minimum": 0,
                                                "exclusiveMaximum": 32,
                                            },
                                        },
                                    },
                                    "version": {"type": "string"},
                                    "inputs_values": {"type": "object"},
                                    "streams_to": {
                                        "type": "array",
                                        "items": {
                                            "type": "string",
                                            "minLength": 3,
                                            "maxLength": 64,
                                        },
                                        "maxItems": 64,
                                    },
                                    "outputs": {
                                        "type": "array",
                                        "description": "If a action supports dynamic outputs, set them here.",
                                        "maxItems": 64,
                                        "items": {
                                            "type": "object",
                                            "properties": {
                                                "optional": {"type": "boolean"},
                                                "human_name": {
                                                    "type": "string",
                                                    "minLength": 0,
                                                    "maxLength": 128,
                                                },
                                                "help": {
                                                    "type": "string",
                                                    "minLength": 0,
                                                    "maxLength": 10_000,
                                                },
                                                "name": {
                                                    "type": "string",
                                                    "minLength": 1,
                                                    "maxLength": 64,
                                                },
                                                "type": {
                                                    "type": "string",
                                                    "enum": [
                                                        "string",
                                                        "longstring",
                                                        "integer",
                                                        "float",
                                                        "password",
                                                        "enum",
                                                        "file",
                                                        "directory",
                                                    ],
                                                },
                                                "enum_values": {
                                                    "type": "array",
                                                    "items": {
                                                        "type": "string",
                                                        "minLength": 1,
                                                        "maxLength": 10_000,
                                                    },
                                                    "maxItems": 64,
                                                },
                                            },
                                            "required": [
                                                "human_name",
                                                "help",
                                                "name",
                                                "type",
                                            ],
                                        },
                                    },
                                },
                                "required": [
                                    "id",
                                    "from",
                                    "position",
                                    "version",
                                ],
                            },
                        },
                    },
                    "required": ["human_name"],
                },
            },
            "required": ["apiVersion", "kind", "spec"],
        }
        self.logger = logging.getLogger(self.__class__.__name__)

    @property
    def schema(self) -> dict:
        return self._schema

    def get_schema(self) -> dict:
        return self.schema

    @staticmethod
    def _build_workflow_action_dynamic_output(
        action_output: dict,
    ) -> WorkflowActionOutput:
        workflow_action_output = WorkflowActionOutput(
            id=str(uuid.uuid4()),
            technical_name=action_output["name"],
            display_name=action_output["human_name"],
            help=action_output["help"],
            type=ActionIOType(action_output["type"]),
            enum_values=action_output.get("enum_values", []),
            is_dynamic=True,
            optional=action_output.get("optional", False),
        )
        return workflow_action_output

    @staticmethod
    def _build_workflow_action_output(action_output: ActionIO) -> WorkflowActionOutput:
        # Construct workflow action using action
        workflow_action_output = WorkflowActionOutput(
            id=str(uuid.uuid4()),
            technical_name=action_output.technical_name,
            display_name=action_output.display_name,
            help=action_output.help,
            type=action_output.type,
            enum_values=action_output.enum_values,
            optional=action_output.optional,
        )
        return workflow_action_output

    @staticmethod
    def _build_workflow_action_input(
        workflow_action_input_value_yaml: str,
        action_input: ActionIO,
        workflow_action_output_map: Dict[str, WorkflowActionOutput],
        workflow_file_metadata: Dict[str, Dict],
        position: int,
    ) -> WorkflowActionInput:
        # Construct workflow action using action
        workflow_action_input = WorkflowActionInput(
            id=str(uuid.uuid4()),
            technical_name=action_input.technical_name,
            display_name=action_input.display_name,
            help=action_input.help,
            type=action_input.type,
            enum_values=action_input.enum_values,
            optional=action_input.optional,
            position=position,
        )
        if workflow_action_input_value_yaml is not None:
            if workflow_action_input_value_yaml.startswith("="):
                workflow_action_output_key = workflow_action_input_value_yaml[1:]
                workflow_action_output = workflow_action_output_map[
                    workflow_action_output_key
                ]
                workflow_action_input.set_reference_value(workflow_action_output)
            else:
                if action_input.type == ActionIOType.FILE:
                    file = workflow_file_metadata.get(
                        workflow_action_input_value_yaml, None
                    )
                    if file is not None:
                        filename, extension = os.path.splitext(
                            workflow_action_input_value_yaml
                        )
                        extension = extension[1:]
                        workflow_action_input.add_input_file(
                            filename, extension, file["path"], file["size"]
                        )
                    else:
                        pass
                else:
                    workflow_action_input.set_static_value(
                        workflow_action_input_value_yaml
                    )
        return workflow_action_input

    def _build_workflow_action(
        self,
        workflow_action_yaml: Dict,
        action: Action,
        workflow_action_input_map: Dict[str, WorkflowActionInput],
        workflow_action_output_map: Dict[str, WorkflowActionOutput],
    ) -> WorkflowAction:
        workflow_action = WorkflowAction(
            id=str(uuid.uuid4()),
            name=action.name,
            technical_name=action.technical_name,
            description=action.description,
            kind=action.kind,
            version=action.version,
            has_dynamic_outputs=action.has_dynamic_outputs,
            position_x=workflow_action_yaml["position"]["x"],
            position_y=workflow_action_yaml["position"]["y"],
            inputs=[
                workflow_action_input_map[
                    f"{workflow_action_yaml['id']}.{item.technical_name}"
                ]
                for item in action.inputs
            ],
            outputs=[
                workflow_action_output_map[
                    f"{workflow_action_yaml['id']}.{item.technical_name}"
                ]
                for item in action.outputs
            ],
            action_id=action.id,
            deployment_status=WorkflowActionDeployStatus.NONE,
            resources=WorkflowActionResources(
                id=action.resources.id,
                time=action.resources.time,
                memory=action.resources.memory,
                cpu=action.resources.cpu,
                gpu=action.resources.gpu,
            )
            if action.resources is not None
            else None,
        )

        if action.has_dynamic_outputs:
            for item in workflow_action_yaml.get("outputs", []):
                workflow_action.dynamic_outputs.append(
                    workflow_action_output_map[
                        f"{workflow_action_yaml['id']}.{item['name']}"
                    ]
                )
        self.logger.info(f"Workflow action generated: {workflow_action}")
        return workflow_action

    @staticmethod
    def _build_workflow_action_link(
        input_action_yaml_id: str,
        output_action_yaml_id: str,
        workflow_action_map: Dict[str, WorkflowAction],
    ) -> WorkflowActionLink:
        return WorkflowActionLink.create_from_workflow_actions(
            upstream_action=workflow_action_map[input_action_yaml_id],
            downstream_action=workflow_action_map[output_action_yaml_id],
        )

    def _build_workflow(
        self,
        owner: str,
        actions: List[Action],
        workflow_file_metadata: Dict[str, Dict],
        project: str,
    ) -> Workflow:
        action_map: Dict[str, Action] = {
            f"{action_item.technical_name}.{action_item.version}": action_item
            for action_item in actions
        }

        workflow_action_output_map: Dict[str, WorkflowActionOutput] = dict()
        for function_item in self.yaml_data["spec"]["functions"]:
            current_action = action_map[
                f"{function_item['from']}.{function_item['version']}"
            ]
            for action_output_item in current_action.outputs:
                workflow_action_output: WorkflowActionOutput = (
                    self._build_workflow_action_output(action_output_item)
                )
                workflow_action_output_map[
                    f"{function_item['id']}.{action_output_item.technical_name}"
                ] = workflow_action_output
            if current_action.has_dynamic_outputs:
                for function_item_output in function_item.get("outputs", []):
                    workflow_action_dynamic_output: WorkflowActionOutput = (
                        self._build_workflow_action_dynamic_output(function_item_output)
                    )
                    workflow_action_output_map[
                        f"{function_item['id']}.{function_item_output['name']}"
                    ] = workflow_action_dynamic_output

        workflow_action_input_map: Dict[str, WorkflowActionInput] = dict()
        for function_item in self.yaml_data["spec"]["functions"]:
            current_action = action_map[
                f"{function_item['from']}.{function_item['version']}"
            ]
            for position, item in enumerate(current_action.inputs):
                function_input_value_yaml = function_item.get("inputs_values", {}).get(
                    item.technical_name, None
                )
                workflow_action_input = self._build_workflow_action_input(
                    function_input_value_yaml,
                    item,
                    workflow_action_output_map,
                    workflow_file_metadata,
                    position,
                )
                workflow_action_input_map[
                    f"{function_item['id']}.{item.technical_name}"
                ] = workflow_action_input

        workflow_action_map: Dict[str, WorkflowAction] = dict()
        for function_item in self.yaml_data["spec"]["functions"]:
            current_action = action_map[
                f"{function_item['from']}.{function_item['version']}"
            ]
            workflow_action_map[function_item["id"]] = self._build_workflow_action(
                function_item,
                current_action,
                workflow_action_input_map,
                workflow_action_output_map,
            )

        workflow_action_link_map: Dict[str, WorkflowActionLink] = dict()
        for function_item in self.yaml_data["spec"]["functions"]:
            for stream_item in function_item.get("streams_to", []):
                workflow_action_link_map[
                    function_item["id"]
                ] = self._build_workflow_action_link(
                    function_item["id"], stream_item, workflow_action_map
                )

        workflow = Workflow(
            id=str(uuid.uuid4()),
            name=self.yaml_data["spec"]["human_name"],
            description=self.yaml_data["spec"]["description"],
            actions=[item for item in workflow_action_map.values()],
            actions_links=[item for item in workflow_action_link_map.values()],
            deployment_status=WorkflowDeploymentStatus.NONE,
            owner=owner,
            project=project,
        )
        return workflow

    def load_workflow_package(self, workflow_data: bytes) -> None:
        try:
            self.logger.info("Loading workflow Zip file")
            path: str = f"{self.tmp_directory_path}/{uuid.uuid4()}-workflow.zip"
            with open(path, "wb") as zip_file:
                zip_file.write(workflow_data)
            self.zip_file = zipfile.ZipFile(path, "r")
            if "workflow.yaml" not in self.zip_file.namelist():
                raise WorkflowYamlNotFound()
            workflow_yaml = self.zip_file.open("workflow.yaml").read()
            self.yaml_data = yaml.safe_load(workflow_yaml.decode("utf8"))
        except yaml.YAMLError as err:
            self.logger.debug(f"Yaml load error: {err}. data: {str(workflow_data)}")
            raise WorkflowParsingYamlException()
        except zipfile.BadZipfile:
            raise WorkflowBadZipFile()

    def load_workflow_schema(self, workflow_data: bytes) -> None:
        try:
            self.yaml_data = yaml.safe_load(workflow_data.decode("utf8"))
        except yaml.YAMLError:
            raise WorkflowParsingYamlException()

    def check_workflow_schema_validity(self) -> None:
        try:
            validate(instance=self.yaml_data, schema=self.schema)
        except ValidationError as err:
            self.logger.debug(f"Schema validation error: {err}. data: {self.yaml_data}")
            raise WorkflowInvalidSchemaException()

    # Use to fetch actions in application layer
    def get_actions_references(self) -> List[Tuple[str, str]]:
        workflow_yaml_functions = self.yaml_data["spec"]["functions"]
        return [(item["from"], item["version"]) for item in workflow_yaml_functions]

    def get_workflow_io_files(self) -> List[Dict[str, Any]]:
        assert self.zip_file is not None
        all_files = []
        file: Dict[str, Any]
        for name in self.zip_file.namelist():
            if name != "workflow.yaml":
                self.logger.info(f"Extracting file {name} from workflow Zip file")
                file = {
                    "name": name,
                    "content": self.zip_file.open(name).read(),
                    "size": self.zip_file.getinfo(name).file_size,
                }
                all_files.append(file)
        return all_files

    def process(
        self,
        owner: str,
        actions: List[Action],
        workflow_file_metadata: Dict[str, Dict],
        project: str,
    ) -> Workflow:
        return self._build_workflow(owner, actions, workflow_file_metadata, project)
