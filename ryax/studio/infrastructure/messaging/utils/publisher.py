# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from logging import getLogger
from typing import List, Union

from aio_pika import ExchangeType, Message
from google.protobuf.struct_pb2 import Struct

from ryax.studio.domain.action.action_events import ActionDeletedEvent
from ryax.studio.domain.common.base_event import BaseEvent
from ryax.studio.domain.common.event_publisher import IEventPublisher
from ryax.studio.domain.workflow.workflow_events import (
    WorkflowDeletedEvent,
    WorkflowDeployEvent,
    WorkflowUndeployEvent,
)
from ryax.studio.infrastructure.messaging.messages.studio_messages_pb2 import (
    ActionDeleted,
    WorkflowDeleted,
    WorkflowDeploy,
    WorkflowUndeploy,
)
from ryax.studio.infrastructure.messaging.utils.engine import MessagingEngine

logger = getLogger(__name__)


class MessagingPublisher(IEventPublisher):
    def __init__(self, engine: MessagingEngine):
        self.engine: MessagingEngine = engine

    @staticmethod
    def _handle_workflow_deploy(event: WorkflowDeployEvent) -> WorkflowDeploy:
        actions_to_deploy: list = []
        for action in event.workflow_actions:
            new_action = WorkflowDeploy.WorkflowAction(
                workflow_action_id=action.id,
                action_id=action.action_id,
                human_name=action.human_name,
                description=action.description,
                technical_name=action.technical_name,
                version=action.action_version,
                kind=WorkflowDeploy.WorkflowAction.Kind.Value(action.action_kind.name),
                inputs=[
                    WorkflowDeploy.WorkflowActionIO(
                        display_name=action_input.display_name,
                        help=action_input.help,
                        technical_name=action_input.technical_name,
                        type=WorkflowDeploy.WorkflowActionIO.IOType.Value(
                            action_input.type.name
                        ),
                        enum_values=action_input.enum_values,
                        optional=action_input.optional,
                    )
                    for action_input in action.action_inputs
                ],
                addons_inputs=[
                    WorkflowDeploy.WorkflowActionIO(
                        display_name=action_input.display_name,
                        help=action_input.help,
                        technical_name=action_input.technical_name,
                        type=WorkflowDeploy.WorkflowActionIO.IOType.Value(
                            action_input.type.name
                        ),
                        enum_values=action_input.enum_values,
                        addon_name=action_input.addon_name,
                        optional=action_input.optional,
                    )
                    for action_input in action.action_addons_inputs
                ],
                outputs=[
                    WorkflowDeploy.WorkflowActionIO(
                        display_name=action_output.display_name,
                        help=action_output.help,
                        technical_name=action_output.technical_name,
                        type=WorkflowDeploy.WorkflowActionIO.IOType.Value(
                            action_output.type.name
                        ),
                        enum_values=action_output.enum_values,
                        optional=action_output.optional,
                    )
                    for action_output in action.action_outputs
                ],
                dynamic_outputs=[
                    WorkflowDeploy.WorkflowActionIO(
                        display_name=action_output.display_name,
                        help=action_output.help,
                        technical_name=action_output.technical_name,
                        type=WorkflowDeploy.WorkflowActionIO.IOType.Value(
                            action_output.type.name
                        ),
                        enum_values=action_output.enum_values,
                        origin=WorkflowDeploy.WorkflowActionIO.ORIGIN.Value(
                            action_output.origin.name
                        )
                        if action_output.origin
                        else None,
                        optional=action_output.optional,
                    )
                    for action_output in action.dynamic_outputs
                ],
                has_dynamic_outputs=action.has_dynamic_outputs,
                project_id=event.workflow_project_id,
                next_actions=action.streams_to,
                reference_values={
                    k: WorkflowDeploy.ReferenceValue(
                        workflow_action_id=v["workflow_action_id"],
                        output_technical_name=v["output_technical_name"],
                    )
                    for k, v in action.reference_inputs_values.items()
                },
                file_values=action.file_inputs_values,
                resources=WorkflowDeploy.WorkflowActionResources(
                    cpu=action.resources.cpu,
                    time=action.resources.time,
                    memory=action.resources.memory,
                    gpu=action.resources.gpu,
                ),
                objectives=WorkflowDeploy.WorkflowActionObjectives(
                    energy=action.objectives.energy,
                    performance=action.objectives.performance,
                    cost=action.objectives.cost,
                ),
                constraints=WorkflowDeploy.WorkflowActionConstraints(
                    site_list=action.constraints.site_list,
                    arch_list=action.constraints.arch_list,
                    site_type_list=action.constraints.site_type_list,
                    node_pool_list=action.constraints.node_pool_list,
                ),
            )
            # Add addons
            # FIXME: Only the addon name should be needed because values are in static_values and def in addons_inputs
            for addon_name, addon_spec in action.addons.items():
                new_action.action_addons.get_or_create(addon_name)
                value_struct = Struct()
                value_struct.update(addon_spec)
                new_action.action_addons[addon_name].CopyFrom(value_struct)
            # Add static inputs
            tmp_struct = Struct()
            tmp_struct.update(action.static_inputs_values)
            new_action.static_values.CopyFrom(tmp_struct)
            # add the action
            actions_to_deploy.append(new_action)
        workflow_deploy = WorkflowDeploy(
            id=event.workflow_id,
            name=event.workflow_name,
            project_id=event.workflow_project_id,
            actions=actions_to_deploy,
            results=[
                WorkflowDeploy.WorkflowResult(
                    workflow_result_id=workflow_result.id,
                    workflow_action_io_id=workflow_result.workflow_action_io_id,
                    key=workflow_result.key,
                    technical_name=workflow_result.technical_name,
                    action_id=workflow_result.action_id,
                )
                for workflow_result in event.results
            ],
        )
        return workflow_deploy

    @staticmethod
    def _handle_workflow_undeploy(event: WorkflowUndeployEvent) -> WorkflowUndeploy:
        return WorkflowUndeploy(
            workflow_id=event.workflow_id,
            project_id=event.workflow_project_id,
            graceful=event.graceful,
        )

    @staticmethod
    def _handle_action_deleted(event: ActionDeletedEvent) -> ActionDeleted:
        action_deleted = ActionDeleted()
        action_deleted.action_id = event.action_id
        action_deleted.version = event.version
        return action_deleted

    @staticmethod
    def _handle_workflow_deleted(event: WorkflowDeletedEvent) -> WorkflowDeleted:
        workflow_deleted = WorkflowDeleted(
            workflow_id=event.workflow_id, project_id=event.project_id
        )
        return workflow_deleted

    def handle_event(
        self, event: BaseEvent
    ) -> Union[ActionDeleted, WorkflowDeploy, WorkflowUndeploy, WorkflowDeleted]:
        if isinstance(event, ActionDeletedEvent):
            return self._handle_action_deleted(event)
        elif isinstance(event, WorkflowDeployEvent):
            return self._handle_workflow_deploy(event)

        elif isinstance(event, WorkflowUndeployEvent):
            return self._handle_workflow_undeploy(event)

        elif isinstance(event, WorkflowDeletedEvent):
            return self._handle_workflow_deleted(event)
        else:
            raise Exception("Event is serializable into message")

    async def publish(self, events: List[BaseEvent]) -> None:
        channel = await self.engine.get_channel()
        exchange = await channel.declare_exchange(
            "domain_events", ExchangeType.TOPIC, durable=True
        )
        for item in events:
            logger.info(f"Publishing event: {item}")
            message_content = self.handle_event(item)
            logger.info(f"Publishing converted message: {message_content}")
            message = Message(
                type=item.event_type, body=message_content.SerializeToString()
            )
            await exchange.publish(message, routing_key=f"Studio.{item.event_type}")
        await channel.close()
