# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging

from aio_pika.abc import AbstractIncomingMessage
from dependency_injector.wiring import Provide

from ryax.studio.application.workflow_service import WorkflowService
from ryax.studio.container import ApplicationContainer
from ryax.studio.infrastructure.messaging.messages.runner_messages_pb2 import (
    WorkflowDeployed,
    WorkflowUndeployed,
)

logger = logging.getLogger(__name__)


async def on_workflow_deployed_successfully(
    message: AbstractIncomingMessage,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> None:
    message_content = WorkflowDeployed()
    message_content.ParseFromString(message.body)
    logger.debug("Handling Workflow deployed successfully message: %s", message_content)
    service.deploy_workflow_success(workflow_id=message_content.workflow_id)


async def on_workflow_undeployed(
    message: AbstractIncomingMessage,
    service: WorkflowService = Provide[ApplicationContainer.workflow_service],
) -> None:
    """Workflow undeployed message handler"""
    message_content = WorkflowUndeployed()
    message_content.ParseFromString(message.body)
    logger.debug("Handling Workflow undeployed message: %s", message_content)
    service.undeploy_workflow_success(
        workflow_id=message_content.workflow_id,
        error=message_content.error,
    )
