# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging

from aio_pika.abc import AbstractIncomingMessage
from dependency_injector.wiring import Provide
from google.protobuf.json_format import MessageToDict

from ryax.studio.application.action_service import ActionService
from ryax.studio.container import ApplicationContainer
from ryax.studio.domain.action.action_events import ActionReadyEvent
from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.infrastructure.messaging.messages.action_builder_messages_pb2 import (
    ActionReady,
)

logger = logging.getLogger(__name__)


async def on_action_ready(
    message: AbstractIncomingMessage,
    service: ActionService = Provide[ApplicationContainer.action_service],
) -> None:
    """Action ready event controller"""
    message_content = ActionReady()
    message_content.ParseFromString(message.body)
    message_dict = MessageToDict(message_content, preserving_proto_field_name=True)
    logger.debug(f"ActionReady event received: {message_dict}")
    action_ready_event = ActionReadyEvent(
        id=message_content.action_id,
        name=message_content.action_name,
        technical_name=message_content.action_technical_name,
        version=message_content.action_version,
        kind=ActionKind[ActionReady.Kind.Name(message_content.action_kind)],
        description=message_content.action_description,
        lockfile=message_content.lockfile,
        build_date=message_content.action_build_date.ToDatetime(),
        logo=ActionReadyEvent.Logo(
            id=message_content.action_logo.action_logo_id,
            name=message_content.action_logo.action_logo_name,
            extension=message_content.action_logo.action_logo_extension,
            content=message_content.action_logo.action_logo_content,
        )
        if message_content.action_logo.action_logo_id
        else None,
        owner_id=message_content.action_owner_id,
        dynamic_outputs=message_content.action_dynamic_outputs,
        inputs=[
            ActionReadyEvent.ActionIO(
                id=item.id,
                technical_name=item.technical_name,
                display_name=item.display_name,
                help=item.help,
                type=ActionIOType[ActionReady.ActionIO.Type.Name(item.type)],
                enum_values=list(item.enum_values),
                default_value=item.default_value,
                optional=item.optional,
            )
            for item in message_content.action_inputs
        ],
        outputs=[
            ActionReadyEvent.ActionIO(
                id=item.id,
                technical_name=item.technical_name,
                display_name=item.display_name,
                help=item.help,
                type=ActionIOType[ActionReady.ActionIO.Type.Name(item.type)],
                enum_values=list(item.enum_values),
                optional=item.optional,
            )
            for item in message_content.action_outputs
        ],
        categories=list(message_content.action_categories),
        project_id=message_content.action_project_id,
        addons=message_dict.get("action_addons", {}),
        resources=ActionReadyEvent.ActionResources(
            cpu=message_content.action_resources.cpu
            if message_content.action_resources.HasField("cpu")
            else None,
            memory=message_content.action_resources.memory
            if message_content.action_resources.HasField("memory")
            else None,
            time=message_content.action_resources.time
            if message_content.action_resources.HasField("time")
            else None,
            gpu=message_content.action_resources.gpu
            if message_content.action_resources.HasField("gpu")
            else None,
        )
        if message_content.HasField("action_resources")
        else None,
    )
    service.add_action(action_ready_event)
