# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from ryax.studio.container import ApplicationContainer
from ryax.studio.infrastructure.messaging.handlers import (
    action_handler,
    workflow_handler,
)
from ryax.studio.infrastructure.messaging.utils.consumer import MessagingConsumer


def setup(consumer: MessagingConsumer, container: ApplicationContainer) -> None:
    """Method to setup messaging mapper (event type/messages mapping)"""
    container.wire(modules=[action_handler, workflow_handler])

    # Register message controller
    consumer.register_handler("ActionReady", action_handler.on_action_ready)
    consumer.register_handler(
        "WorkflowDeployed",
        workflow_handler.on_workflow_deployed_successfully,
    )
    consumer.register_handler(
        "WorkflowUndeployed", workflow_handler.on_workflow_undeployed
    )
