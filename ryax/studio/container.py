# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dependency_injector import containers, providers

from ryax.studio.application.action_service import ActionService
from ryax.studio.application.authentication_service import AuthenticationService
from ryax.studio.application.project_authorization_service import (
    ProjectAuthorizationService,
)
from ryax.studio.application.project_variable_service import ProjectVariableService
from ryax.studio.application.util_service import UtilService
from ryax.studio.application.workflow_action_service import WorkflowActionService
from ryax.studio.application.workflow_service import WorkflowService
from ryax.studio.domain.action.action_factory import ActionFactory
from ryax.studio.domain.action.action_repository import IActionRepository
from ryax.studio.domain.addon.addons_service import IAddonsService
from ryax.studio.domain.common.unit_of_work import IUnitOfWork
from ryax.studio.domain.project_variables.project_variable_repository import (
    IUserObjectRepository,
)
from ryax.studio.domain.services.filestore_service import IFilestoreService
from ryax.studio.domain.workflow.services.workflow_export_service import (
    IWorkflowExportService,
)
from ryax.studio.domain.workflow.services.workflow_import_service import (
    IWorkflowImportService,
)
from ryax.studio.domain.workflow.workflow_factory import WorkflowFactory
from ryax.studio.domain.workflow.workflow_repository import IWorkflowRepository
from ryax.studio.infrastructure.addons.addons_service import PoCAddonsService
from ryax.studio.infrastructure.authorization.authorization_api_service import (
    AuthorizationApiService,
)
from ryax.studio.infrastructure.database.engine import DatabaseEngine
from ryax.studio.infrastructure.database.repositories.action_repository import (
    DatabaseActionRepository,
)
from ryax.studio.infrastructure.database.repositories.project_variable_repository import (
    DatabaseUserObjectRepository,
)
from ryax.studio.infrastructure.database.repositories.workflow_repository import (
    DatabaseWorkflowRepository,
)
from ryax.studio.infrastructure.database.unit_of_work import DatabaseUnitOfWork
from ryax.studio.infrastructure.filestore.engine import FileStoreEngine
from ryax.studio.infrastructure.filestore.filestore_service import FilestoreService
from ryax.studio.infrastructure.jwt import JwtService
from ryax.studio.infrastructure.messaging.utils.consumer import MessagingConsumer
from ryax.studio.infrastructure.messaging.utils.engine import MessagingEngine
from ryax.studio.infrastructure.messaging.utils.publisher import MessagingPublisher
from ryax.studio.infrastructure.packaging.packaging_export import PackagingExportService
from ryax.studio.infrastructure.packaging.packaging_import import PackagingImportService
from ryax.studio.infrastructure.storage.assets_storage_service import (
    AssetsStorageService,
)


class ApplicationContainer(containers.DeclarativeContainer):
    """Application container for dependency injection"""

    # Define configuration provider
    configuration = providers.Configuration()

    database_engine: providers.Singleton[DatabaseEngine] = providers.Singleton(
        DatabaseEngine, connection_url=configuration.ryax_datastore
    )

    filestore_engine: providers.Singleton[FileStoreEngine] = providers.Singleton(
        FileStoreEngine,
        connection_url=configuration.ryax_filestore,
        access_key=configuration.ryax_filestore_access_key,
        secret_key=configuration.ryax_filestore_secret_key,
        bucket=configuration.ryax_filestore_bucket,
    )

    filestore_service: providers.Factory[IFilestoreService] = providers.Factory(
        FilestoreService,
        engine=filestore_engine,
    )

    action_repository: providers.Factory[IActionRepository] = providers.Factory(
        DatabaseActionRepository
    )

    workflow_repository: providers.Factory[IWorkflowRepository] = providers.Factory(
        DatabaseWorkflowRepository
    )
    project_variable_repository: providers.Factory[
        IUserObjectRepository
    ] = providers.Factory(DatabaseUserObjectRepository)

    unit_of_work: providers.Factory[IUnitOfWork] = providers.Factory(
        DatabaseUnitOfWork,
        engine=database_engine,
        workflow_repository_factory=workflow_repository.provider,
        action_repository_factory=action_repository.provider,
        project_variable_repository_factory=project_variable_repository.provider,
    )

    action_factory: providers.Singleton[ActionFactory] = providers.Singleton(
        ActionFactory,
    )

    workflow_factory: providers.Singleton[WorkflowFactory] = providers.Singleton(
        WorkflowFactory,
    )

    messaging_engine: providers.Singleton[MessagingEngine] = providers.Singleton(
        MessagingEngine, connection_url=configuration.ryax_broker
    )

    messaging_consumer: providers.Singleton[MessagingConsumer] = providers.Singleton(
        MessagingConsumer, engine=messaging_engine
    )

    messaging_publisher = providers.Singleton(
        MessagingPublisher, engine=messaging_engine
    )

    # Jwt service
    jwt_service = providers.Singleton(
        JwtService, secret_key=configuration.jwt_secret_key
    )

    authentication_service = providers.Singleton(
        AuthenticationService, security_service=jwt_service
    )

    util_service = providers.Singleton(UtilService)

    packaging_import_service: providers.Factory[
        IWorkflowImportService
    ] = providers.Factory(
        PackagingImportService, tmp_directory_path=configuration.tmp_directory_path
    )
    packaging_export_service: providers.Factory[
        IWorkflowExportService
    ] = providers.Factory(
        PackagingExportService, tmp_directory_path=configuration.tmp_directory_path
    )

    # Workflow application service
    workflow_service: providers.Factory[WorkflowService] = providers.Factory(
        WorkflowService,
        uow=unit_of_work,
        factory=workflow_factory,
        messaging_publisher=messaging_publisher,
        workflow_import_service=packaging_import_service,
        workflow_export_service=packaging_export_service,
        filestore_service=filestore_service,
    )

    # Addons service
    addons_service: providers.Singleton[IAddonsService] = providers.Singleton(
        PoCAddonsService
    )

    # Workflow action application service
    workflow_action_service: providers.Factory[
        WorkflowActionService
    ] = providers.Factory(
        WorkflowActionService,
        uow=unit_of_work,
        filestore_service=filestore_service,
        addons_service=addons_service,
    )

    assets_storage_service: providers.Factory[AssetsStorageService] = providers.Factory(
        AssetsStorageService, assets_path=configuration.ryax_assets_path
    )

    # Action application service
    action_service: providers.Factory[ActionService] = providers.Factory(
        ActionService,
        uow=unit_of_work,
        factory=action_factory,
        assets_storage_service=assets_storage_service,
        event_publisher=messaging_publisher,
    )

    project_variable_service: providers.Factory[
        ProjectVariableService
    ] = providers.Factory(
        ProjectVariableService, uow=unit_of_work, filestore_service=filestore_service
    )

    authorization_api_service = providers.Singleton(
        AuthorizationApiService, configuration.authorization_api_base_url
    )
    project_authorization_service = providers.Singleton(
        ProjectAuthorizationService, authorization_api_service=authorization_api_service
    )
