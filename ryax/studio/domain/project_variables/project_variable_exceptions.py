from dataclasses import dataclass

from ryax.studio.domain.workflow.entities.workflow import Workflow


@dataclass
class ProjectVariableNotFoundException(Exception):
    message: str = "Project variable not found"


@dataclass
class ProjectVariableFileInvalidException(Exception):
    message: str = "File value is invalid"


@dataclass
class ProjectVariableFileTooLargeException(Exception):
    message: str = "File is too large."


@dataclass
class ProjectVariableIsInUseError(Exception):
    workflows: list[Workflow]
    message: str = "The variable is in use by one or more workflows"
