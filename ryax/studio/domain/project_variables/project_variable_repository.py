# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import List, Optional

from ryax.studio.domain.project_variables.project_variable_entities import (
    ProjectVariable,
)
from ryax.studio.domain.workflow.entities.workflow import Workflow


class IUserObjectRepository(abc.ABC):
    @abc.abstractmethod
    def list(
        self, project_id: str, type_search: Optional[str]
    ) -> List[ProjectVariable]:
        pass

    @abc.abstractmethod
    def get(self, project_variable_id: str, project_id: str) -> ProjectVariable:
        pass

    @abc.abstractmethod
    def get_linked_workflows(self, project_variable: ProjectVariable) -> List[Workflow]:
        pass

    @abc.abstractmethod
    def add(self, project_variable: ProjectVariable) -> None:
        pass

    @abc.abstractmethod
    def delete(self, project_variable: ProjectVariable) -> None:
        pass
