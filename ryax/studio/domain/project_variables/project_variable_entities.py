# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from typing import Any, Optional

from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.common.ids import get_random_id


@dataclass
class ProjectVariableTransactionData:
    name: str
    value: Any
    type: str
    project_id: str
    description: Optional[str] = None
    file_path: Optional[str] = None


@dataclass
class ProjectVariable:
    id: str
    name: str
    value: Any
    type: ActionIOType
    project_id: str
    description: Optional[str] = None
    file_path: Optional[str] = None

    @staticmethod
    def from_add_project_variable_data(
        project_variable_data: ProjectVariableTransactionData,
    ) -> "ProjectVariable":
        return ProjectVariable(
            id="var-" + get_random_id(),
            name=project_variable_data.name,
            value=project_variable_data.value,
            type=ActionIOType.string_to_enum(project_variable_data.type),
            project_id=project_variable_data.project_id,
            description=project_variable_data.description,
            file_path=project_variable_data.file_path,
        )

    def update(self, project_variable_data: ProjectVariableTransactionData) -> None:
        """
        Update the User object properties. The type cannot be changed to avoid workflow IO errors
        """
        self.name = project_variable_data.name
        self.value = project_variable_data.value
        self.description = project_variable_data.description
        self.file_path = project_variable_data.file_path
