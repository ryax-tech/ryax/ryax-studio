# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import BinaryIO


class IFilestoreService(abc.ABC):
    @abc.abstractmethod
    def generate_file_path(self, file_name: str) -> str:
        pass

    @abc.abstractmethod
    async def upload_file(self, file_path: str, file: bytes, file_size: int) -> None:
        pass

    @abc.abstractmethod
    async def write(self, file_path: str, stream: BinaryIO) -> None:
        """Write into the file_path the content of the byte stream."""
        raise NotImplementedError()

    @abc.abstractmethod
    async def remove_file(self, file_path: str) -> None:
        pass

    @abc.abstractmethod
    async def get_file(self, file_path: str) -> bytes:
        pass

    @abc.abstractmethod
    async def copy_file(self, source_file: str, destination_path: str) -> None:
        pass
