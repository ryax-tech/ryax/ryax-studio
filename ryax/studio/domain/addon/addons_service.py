import abc

from ryax.studio.domain.addon.addon_entities import AddOns


class IAddonsService(abc.ABC):
    @abc.abstractmethod
    def load(self) -> AddOns:
        ...
