import uuid
from dataclasses import dataclass, field
from typing import Optional, Union

from ryax.studio.domain.action.action_io import ActionIO
from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.workflow.workflow_exceptions import UndefinedAddonError


@dataclass
class AddonInput(ActionIO):
    editable: bool = True
    alias: str | None = None

    @classmethod
    def create(
        cls,
        technical_name: str,
        type: ActionIOType,
        display_name: str,
        help: str,
        alias: str | None = None,
        enum_values: Optional[list[str]] = None,
        default_value: Optional[Union[str, int, float, bool, bytes]] = None,
        editable: bool = True,
        optional: bool = False,
    ) -> "AddonInput":
        if enum_values is None:
            enum_values = []
        return AddonInput(
            id=str(uuid.uuid4()),
            technical_name=technical_name,
            display_name=display_name,
            help=help,
            type=type,
            enum_values=enum_values,
            default_value=default_value,
            editable=editable,
            optional=optional,
            alias=alias,
        )


@dataclass
class AddOn:
    id: str
    technical_name: str
    display_name: str
    description: str
    parameters: list[AddonInput]


@dataclass
class AddOns:
    addons: list[AddOn] = field(default_factory=list)

    def __str__(self) -> str:
        return ", ".join([addon.technical_name for addon in self.addons])

    def get(self, technical_name: str) -> AddOn:
        for addon in self.addons:
            if addon.technical_name == technical_name:
                return addon
        raise UndefinedAddonError(
            f"The addon {technical_name} is not registered or cannot be found. Available addons are {self.addons}"
        )

    def add(self, addon: AddOn) -> None:
        self.addons.append(addon)
