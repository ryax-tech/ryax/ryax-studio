# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass


@dataclass
class WorkflowHasNoTriggerException(Exception):
    message: str = "Workflow has no trigger"


@dataclass
class WorkflowNotFoundException(Exception):
    message: str = "Workflow not found"


@dataclass
class WorkflowNotValidException(Exception):
    message: str = "Workflow not valid"


@dataclass
class WorkflowNotDeployable(Exception):
    message: str = "Workflow can't be deployed at this status"


@dataclass
class WorkflowNotDeletableException(Exception):
    message: str = "Workflow can not be deleted at this status"


@dataclass
class WorkflowNotUpdatableException(Exception):
    message: str = "Workflow can not be modified at this status"


@dataclass
class ActionNotFoundException(Exception):
    message: str = "Action not found"


@dataclass
class WorkflowActionNotFoundException(Exception):
    message: str = "Action not found in the workflow"


@dataclass
class WorkflowActionLinkNotFoundException(Exception):
    message: str = "Action link not found in the workflow"


@dataclass
class WorkflowActionInputNotFoundException(Exception):
    message: str = "Input not found in the workflow"


@dataclass
class WorkflowActionNonOptionalInputReferenceValueException(Exception):
    message: str = "A non optional input may not take an optional reference value."


@dataclass
class WorkflowActionOutputNotFoundException(Exception):
    message: str = "Output not found in the workflow"


@dataclass
class WorkflowActionIONotFoundException(Exception):
    message: str = "Workflow IO not found in the workflow."


@dataclass
class WorkflowActionDynamicOutputNotFoundException(Exception):
    message: str = "Dynamic output not found in the workflow"


@dataclass
class InvalidInputValueException(Exception):
    message: str = "Input value is invalid"


@dataclass
class NotImplementedException(Exception):
    message: str = "Feature not implemented"


@dataclass
class WorkflowActionHasNoDynamicOutputsException(Exception):
    message: str = "The selected action can't take dynamic outputs"


@dataclass
class WrongStatusFilterException(Exception):
    message: str = "The status filter is not correct"


@dataclass
class WorkflowParsingYamlException(Exception):
    message: str = "Error while parsing workflow packaging data"


@dataclass
class WorkflowInvalidSchemaException(Exception):
    message: str = "The workflow schema is not valid"


@dataclass
class FilestoreEntryNotFoundException(Exception):
    message: str = "Static file is not found"


@dataclass
class WorkflowInputHasNoStaticFileException(Exception):
    message: str = "Input has no static file"


@dataclass
class DirectoryInputsMustBeZipfileException(Exception):
    message: str = "Static input directories must be compressed as .zip files"


@dataclass
class WorkflowFileTooLargeException(Exception):
    message: str = "The selected input file exceeds the maximum size"


@dataclass
class WorkflowFileAlreadyPresentException(Exception):
    message: str = "The workflow action input already has a file uploaded"


@dataclass
class WorkflowSchemaFileTooLargeException(Exception):
    message: str = "The workflow file exceeds the maximum size"


@dataclass
class SchemaValidWorkflowInvalidException(Exception):
    message: str = "Packaging is valid, resulting workflow would be in state: invalid"


@dataclass
class WorkflowBadZipFile(Exception):
    message: str = "Workflow importing bad file"


@dataclass
class WorkflowYamlNotFound(Exception):
    message: str = "Workflow yaml file not found"


@dataclass
class InvalidUserDefinedEndpointException(Exception):
    message: str = (
        "Endpoint must be alphanumeric and may not contain more than 1 consecutive /"
    )


@dataclass
class EndpointAlreadyExistsException(Exception):
    message: str = "Endpoint must be unique. This endpoint already exists."


@dataclass
class NotAllKeysAreUniqueInResults(Exception):
    message: str = "Not all keys are unique in the results."


@dataclass
class DynamicOutputOriginInvalidException(Exception):
    message: str = "Dynamic output origin invalid."


@dataclass
class ActionIOTypeInvalidException(Exception):
    message: str = "Action IO has invalid IO type."


class UndefinedAddonError(Exception):
    pass


@dataclass
class UnknownAddonParameterError(Exception):
    message: str
