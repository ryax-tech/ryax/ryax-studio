# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import Dict

from ryax.studio.domain.workflow.entities.workflow import Workflow


class IWorkflowExportService(abc.ABC):
    @abc.abstractmethod
    def export_workflow(
        self, workflow: Workflow, all_inputs_files: Dict[str, bytes]
    ) -> None:
        pass

    @abc.abstractmethod
    def generate_workflow_zip_file(self) -> str:
        pass
