# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import Any, Dict, List, Tuple

from ryax.studio.domain.action.action import Action
from ryax.studio.domain.workflow.entities.workflow import Workflow


class IWorkflowImportService(abc.ABC):
    @abc.abstractmethod
    def get_schema(self) -> dict:
        pass

    @abc.abstractmethod
    def load_workflow_package(self, workflow_data: bytes) -> None:
        pass

    @abc.abstractmethod
    def load_workflow_schema(self, workflow_data: bytes) -> None:
        pass

    @abc.abstractmethod
    def check_workflow_schema_validity(self) -> None:
        pass

    @abc.abstractmethod
    def get_actions_references(self) -> List[Tuple[str, str]]:
        pass

    @abc.abstractmethod
    def get_workflow_io_files(self) -> List[Dict[str, Any]]:
        pass

    @abc.abstractmethod
    def process(
        self,
        owner: str,
        actions: List[Action],
        workflow_file_metadata: Dict[str, Dict],
        project: str,
    ) -> Workflow:
        pass
