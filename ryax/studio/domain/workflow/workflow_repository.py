# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import List, Optional, Sequence

from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.views.workflow_action_input_view import (
    WorkflowActionInputView,
)
from ryax.studio.domain.workflow.views.workflow_action_output_view import (
    WorkflowActionOutputView,
)
from ryax.studio.domain.workflow.views.workflow_error_view import WorkflowErrorView
from ryax.studio.domain.workflow.views.workflow_view import WorkflowView
from ryax.studio.domain.workflow.workflow_values import WorkflowDeploymentStatus


class IWorkflowRepository(abc.ABC):
    @abc.abstractmethod
    def check_workflow_exists(self, workflow_id: str) -> None:
        pass

    @abc.abstractmethod
    def check_workflow_action_exists(self, workflow_action_id: str) -> None:
        pass

    @abc.abstractmethod
    def list_workflows(
        self,
        project_id: str,
        search: Optional[str] = None,
        deployment_status: Optional[WorkflowDeploymentStatus] = None,
    ) -> List[Workflow]:
        pass

    @abc.abstractmethod
    def get_workflow_view(self, workflow_id: str) -> WorkflowView:
        pass

    @abc.abstractmethod
    def list_previous_workflow_actions_view(self, workflow_action_id: str) -> List[str]:
        pass

    @abc.abstractmethod
    def get_workflow_errors(self, workflow_id: str) -> List[WorkflowErrorView]:
        pass

    @abc.abstractmethod
    def get_workflow(self, workflow_id: str) -> Workflow:
        pass

    @abc.abstractmethod
    def delete_static_file(self, workflow_file: WorkflowFile) -> None:
        pass

    @abc.abstractmethod
    def add_workflow(self, workflow: Workflow) -> None:
        pass

    @abc.abstractmethod
    def delete_workflow(self, workflow: Workflow) -> None:
        pass

    @abc.abstractmethod
    def list_workflow_action_inputs_view(
        self, workflow_action_id: str
    ) -> List[WorkflowActionInputView]:
        pass

    @abc.abstractmethod
    def list_workflow_action_outputs_view(
        self, workflow_action_id: str
    ) -> Sequence[WorkflowActionOutputView]:
        pass

    @abc.abstractmethod
    def search_workflow_action_outputs_view(
        self,
        workflow_id: str,
        with_type: Optional[ActionIOType] = None,
        exclude_workflow_actions: Optional[List[str]] = None,
        in_workflow_actions: Optional[List[str]] = None,
    ) -> Sequence[WorkflowActionOutputView]:
        pass

    @abc.abstractmethod
    def list_workflows_where_action_used(self, action_id: str) -> List[Workflow]:
        pass

    @abc.abstractmethod
    def check_if_endpoint_exists(
        self, endpoint: Optional[str], project_id: str
    ) -> None:
        pass
