# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import enum
from dataclasses import dataclass, field
from typing import Optional, Union

from ryax.studio.domain.action.action_values import ActionIOType, DynamicOutputOrigin
from ryax.studio.domain.project_variables.project_variable_entities import (
    ProjectVariable,
)


@dataclass
class CreateWorkflowResultData:
    key: str
    workflow_id: str
    workflow_action_io_id: str


@dataclass
class UpdateAllWorkflowActionIOData:
    @dataclass
    class InputValue:
        id: str
        static_value: Optional[Union[str, int, float, bool]] = None
        reference_value: Optional[str] = None
        project_variable_value: Optional[str] = None

    @dataclass
    class DynamicOutputDefinition:
        id: str
        technical_name: str
        display_name: str
        help: str
        type: ActionIOType
        origin: DynamicOutputOrigin | None
        optional: bool
        enum_values: Optional[list[str]] = None

    custom_name: Optional[str]
    inputs: Optional[list[InputValue]]
    dynamic_outputs: Optional[list[DynamicOutputDefinition]]
    addons_inputs: Optional[list[InputValue]]


@dataclass
class CreateWorkflowData:
    """Value used for workflow creation"""

    name: str
    description: Optional[str] = None


@dataclass
class CopyWorkflowData:
    """Value used for workflow copy"""

    from_workflow: str
    name: Optional[str] = None
    description: Optional[str] = None


@dataclass
class WorkflowInformations:
    """Update basic workflow information"""

    name: Optional[str] = None
    description: Optional[str] = None


@dataclass
class AddWorkflowAction:
    """Value to add a action in a workflow"""

    action_id: str
    custom_name: Optional[str] = None
    position_x: Optional[int] = None
    position_y: Optional[int] = None


@dataclass
class AddWorkflowActionWithLink:
    """Value to add a action in a workflow with a link to its parent"""

    action_definition_id: str
    parent_workflow_action_id: Optional[str] = None
    replace_workflow_action_id: Optional[str] = None


@dataclass
class UpdateWorkflowActionLinks:
    action_id: str
    next_actions_ids: list[str] = field(default_factory=list)


@dataclass
class UpdateWorkflowAction:
    """Value to update a action in a workflow"""

    custom_name: Optional[str] = None
    position_x: Optional[int] = None
    position_y: Optional[int] = None


@dataclass
class AddWorkflowActionLink:
    """Value to add a action link in a workflow"""

    upstream_action_id: str
    downstream_action_id: str


@dataclass
class UpdateWorkflowActionInputValue:
    static_value: Optional[Union[str, float, int, bool, bytes]] = None
    reference_value: Optional[str] = None
    project_variable_value: Optional[ProjectVariable] = None


@dataclass
class AddWorkflowActionDynamicOutput:
    technical_name: str
    display_name: str
    help: str
    type: ActionIOType
    optional: bool = False
    enum_values: list[str] = field(init=True, default_factory=list)
    origin: Optional[DynamicOutputOrigin] = None


@dataclass
class UpdateWorkflowActionDynamicOutput:
    technical_name: Optional[str] = None
    display_name: Optional[str] = None
    help: Optional[str] = None
    type: Optional[ActionIOType] = None
    enum_values: Optional[list[str]] = None
    optional: Optional[bool] = None
    origin: Optional[DynamicOutputOrigin] = None


class WorkflowValidityStatus(enum.Enum):
    VALID = "valid"
    INVALID = "invalid"


class WorkflowDeploymentStatus(enum.Enum):
    NONE = "None"
    DEPLOYING = "Deploying"
    DEPLOYED = "Deployed"
    UNDEPLOYING = "Undeploying"
    ERROR = "Error"

    @classmethod
    def has_value(cls, value: str) -> bool:
        return any(value == var.value for var in cls)


class WorkflowActionValidityStatus(enum.Enum):
    VALID = "valid"
    INVALID = "invalid"


class WorkflowActionDeployStatus(enum.Enum):
    NONE = "None"
    DEPLOYING = "Deploying"
    DEPLOYED = "Deployed"
    UNDEPLOYING = "Undeploying"
    ERROR = "Error"


class WorkflowErrorCode(enum.IntEnum):
    WORKFLOW_NOT_CONNECTED = 1
    WORKFLOW_HAS_CYCLES = 2
    WORKFLOW_ACTIONS_UPPER_LIMIT_REACHED = 3
    WORKFLOW_SOURCES_LOWER_LIMIT_REACHED = 4

    WORKFLOW_ACTION_LINK_INPUT_UPPER_LIMIT_REACHED = 101
    WORKFLOW_ACTION_LINK_INPUT_LOWER_LIMIT_REACHED = 102
    WORKFLOW_ACTION_INPUTS_NOT_DEFINED = 105
