import uuid
from dataclasses import dataclass
from enum import Enum
from typing import Optional

from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.workflow.workflow_values import CreateWorkflowResultData


class WorkflowResultType(Enum):
    JSON = "JSON"
    FILEORDIR = "FileOrDir"


@dataclass
class WorkflowResultInfo:
    action_name: str
    action_io_name: str
    action_type: ActionIOType


@dataclass
class WorkflowResult:
    id: str
    workflow_id: str
    key: str
    workflow_action_input_id: Optional[str]
    workflow_action_output_id: Optional[str]

    @staticmethod
    def create_from_data(
        data: CreateWorkflowResultData, is_an_output: bool
    ) -> "WorkflowResult":
        return WorkflowResult(
            id=str(uuid.uuid4()),
            workflow_id=data.workflow_id,
            key=data.key,
            workflow_action_input_id=data.workflow_action_io_id
            if not is_an_output
            else None,
            workflow_action_output_id=data.workflow_action_io_id
            if is_an_output
            else None,
        )

    @property
    def workflow_action_io_id(self) -> str:
        io_id = (
            self.workflow_action_output_id
            if self.is_an_output()
            else self.workflow_action_input_id
        )
        assert isinstance(io_id, str)
        return io_id

    def is_an_output(self) -> bool:
        return self.workflow_action_output_id is not None
