# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import uuid
from dataclasses import dataclass, field
from typing import Any, Dict, List, Optional, Sequence, cast
from urllib.parse import urlparse

from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_category import ActionCategory
from ryax.studio.domain.action.action_io import ActionIO
from ryax.studio.domain.action.action_values import ActionKind
from ryax.studio.domain.addon.addon_entities import AddOns
from ryax.studio.domain.project_variables.project_variable_entities import (
    ProjectVariable,
)
from ryax.studio.domain.workflow.entities.workflow_action_category import (
    WorkflowActionCategory,
)
from ryax.studio.domain.workflow.entities.workflow_action_constraints import (
    WorkflowActionConstraints,
)
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionAddonInput,
    WorkflowActionInput,
)
from ryax.studio.domain.workflow.entities.workflow_action_io import WorkflowActionIO
from ryax.studio.domain.workflow.entities.workflow_action_objectives import (
    WorkflowActionObjectives,
)
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.entities.workflow_action_resources import (
    WorkflowActionResources,
)
from ryax.studio.domain.workflow.entities.workflow_error import WorkflowError
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.workflow_exceptions import (
    InvalidUserDefinedEndpointException,
    UnknownAddonParameterError,
    WorkflowActionHasNoDynamicOutputsException,
    WorkflowActionInputNotFoundException,
    WorkflowActionIONotFoundException,
    WorkflowActionOutputNotFoundException,
    WorkflowActionNonOptionalInputReferenceValueException,
)
from ryax.studio.domain.workflow.workflow_values import (
    AddWorkflowActionDynamicOutput,
    UpdateWorkflowAction,
    UpdateWorkflowActionDynamicOutput,
    WorkflowActionDeployStatus,
    WorkflowActionValidityStatus,
    WorkflowErrorCode,
)


@dataclass
class WorkflowAction:
    """Class to modeling actions in workflow"""

    id: str
    name: str
    technical_name: str
    description: str
    version: str
    kind: ActionKind
    action_id: str
    resources: Optional[WorkflowActionResources] = None
    workflow_id: Optional[str] = None
    position_x: Optional[int] = None
    position_y: Optional[int] = None
    has_dynamic_outputs: bool = False
    custom_name: Optional[str] = None
    inputs: list[WorkflowActionInput] = field(default_factory=list)
    addons_inputs: list[WorkflowActionAddonInput] = field(default_factory=list)
    outputs: list[WorkflowActionOutput] = field(default_factory=list)
    dynamic_outputs: list[WorkflowActionOutput] = field(default_factory=list)
    errors: list[WorkflowError] = field(default_factory=list)
    validity_status: Optional[WorkflowActionValidityStatus] = None
    deployment_status: WorkflowActionDeployStatus = WorkflowActionDeployStatus.NONE
    categories: list[WorkflowActionCategory] = field(init=True, default_factory=list)
    # Attribute use by the database to keep action list ordered
    position: Optional[int] = None
    endpoint: Optional[str] = None
    addons: Dict[str, dict] = field(default_factory=dict)
    objectives: WorkflowActionObjectives | None = field(
        default_factory=WorkflowActionObjectives
    )
    constraints: WorkflowActionConstraints | None = field(
        default_factory=WorkflowActionConstraints
    )

    @staticmethod
    def create_from_action(
        action: Action,
        custom_name: Optional[str] = None,
        position_x: Optional[int] = None,
        position_y: Optional[int] = None,
    ) -> "WorkflowAction":
        new_workflow_action = WorkflowAction(
            id=str(uuid.uuid4()),
            name=action.name,
            technical_name=action.technical_name,
            version=action.version,
            description=action.description,
            kind=action.kind,
            has_dynamic_outputs=action.has_dynamic_outputs,
            custom_name=custom_name,
            position_x=position_x,
            position_y=position_y,
            deployment_status=WorkflowActionDeployStatus.NONE,
            action_id=action.id,
            addons=action.addons,
            resources=WorkflowActionResources(
                id=str(uuid.uuid4()),
                cpu=action.resources.cpu,
                time=action.resources.time,
                memory=action.resources.memory,
                gpu=action.resources.gpu,
            )
            if action.resources is not None
            else None,
        )
        if action.kind == ActionKind.SOURCE:
            new_workflow_action.update_endpoint(new_workflow_action.id)
        return new_workflow_action

    def _get_error(self, error_code: WorkflowErrorCode) -> Optional[WorkflowError]:
        return next((item for item in self.errors if item.code == error_code), None)

    @staticmethod
    def _endpoint_is_valid(endpoint: str) -> bool:
        """
        Checks that there are no invalid characters or consecutive slashes in the endpoint
        """
        invalid_chars = [":", "?", "#", "[", "]", "@"]
        if any(invalid_char in endpoint for invalid_char in invalid_chars) or any(
            char == "/" and endpoint[idx + 1] == "/"
            for idx, char in enumerate(endpoint[:-1])
        ):
            return False
        return True

    def update_endpoint(self, new_endpoint: Optional[str]) -> None:
        if new_endpoint is None:
            if self.kind == ActionKind.SOURCE:
                raise InvalidUserDefinedEndpointException()
            self.endpoint = None
        elif not self._endpoint_is_valid(new_endpoint):
            raise InvalidUserDefinedEndpointException()
        else:
            verified_new_endpoint = urlparse(new_endpoint, allow_fragments=False).path
            self.endpoint = verified_new_endpoint

    def add_error(self, error_code: WorkflowErrorCode) -> None:
        error = self._get_error(error_code)
        if error is None:
            new_error = WorkflowError(id=str(uuid.uuid4()), code=error_code)
            self.errors.append(new_error)

    def remove_error(self, error_code: WorkflowErrorCode) -> None:
        error = self._get_error(error_code)
        if error is not None:
            self.errors.remove(error)

    def check_action_links_inputs(self, action_links_inputs_number: int) -> None:
        """Method to check links of a action"""

        class ActionKindLimits:
            SOURCE = {"lower": 0, "upper": 0}
            PROCESSOR = {"lower": 1, "upper": 1}
            PUBLISHER = {"lower": 1, "upper": 1}

            @classmethod
            def get_limit(cls, kind: ActionKind) -> Dict:
                return cls.__getattribute__(cls, kind.name)

        if action_links_inputs_number > ActionKindLimits.get_limit(self.kind)["upper"]:
            self.add_error(
                WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_UPPER_LIMIT_REACHED
            )
        else:
            self.remove_error(
                WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_UPPER_LIMIT_REACHED
            )

        if action_links_inputs_number < ActionKindLimits.get_limit(self.kind)["lower"]:
            self.add_error(
                WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_LOWER_LIMIT_REACHED
            )
        else:
            self.remove_error(
                WorkflowErrorCode.WORKFLOW_ACTION_LINK_INPUT_LOWER_LIMIT_REACHED
            )

    def check_action_inputs(self) -> None:
        for item in self.get_all_inputs(filter_not_editable=True):
            if item.optional:
                continue
            elif not item.has_defined_value():
                self.add_error(WorkflowErrorCode.WORKFLOW_ACTION_INPUTS_NOT_DEFINED)
                return
        self.remove_error(WorkflowErrorCode.WORKFLOW_ACTION_INPUTS_NOT_DEFINED)

    def update_validity_status(self) -> None:
        if len(self.errors) > 0:
            self.validity_status = WorkflowActionValidityStatus.INVALID
        else:
            self.validity_status = WorkflowActionValidityStatus.VALID

    def update_data(self, data: UpdateWorkflowAction) -> None:
        """Method to update workflow action data"""
        self.custom_name = (
            data.custom_name if data.custom_name is not None else self.custom_name
        )
        self.position_x = (
            data.position_x if data.position_x is not None else self.position_x
        )
        self.position_y = (
            data.position_y if data.position_y is not None else self.position_y
        )

    def add_input(self, action_input: ActionIO, position: int) -> None:
        new_input = WorkflowActionInput(
            id=str(uuid.uuid4()),
            technical_name=action_input.technical_name,
            display_name=action_input.display_name,
            help=action_input.help,
            type=action_input.type,
            enum_values=action_input.enum_values,
            default_value=action_input.default_value,
            position=position,
            optional=action_input.optional,
        )
        if action_input.default_value:
            new_input.set_static_value(action_input.default_value)
        self.inputs.append(new_input)

    def add_addon(self, addon_name: str, addon_args: dict, addons: AddOns) -> None:
        addon = addons.get(addon_name)

        # Initialize addon inputs

        position = 0
        all_addon_param_names = []
        for parameter_definition in addon.parameters:
            all_addon_param_names.append(parameter_definition.technical_name)
            if parameter_definition.alias:
                all_addon_param_names.append(parameter_definition.alias)

            new_input = WorkflowActionAddonInput(
                id=str(uuid.uuid4()),
                technical_name=parameter_definition.technical_name,
                display_name=parameter_definition.display_name,
                help=parameter_definition.help,
                type=parameter_definition.type,
                enum_values=parameter_definition.enum_values,
                default_value=parameter_definition.default_value,
                position=position,
                addon_name=addon_name,
                optional=parameter_definition.optional,
                editable=parameter_definition.editable,
            )
            # Get value from addon args or default values
            found = False
            for parameter_name, parameter_value in addon_args.items():
                if (
                    parameter_definition.technical_name == parameter_name
                    or parameter_definition.alias == parameter_name
                ):
                    new_input.set_static_value(parameter_value)
                    found = True
                    break
            if not found:
                if parameter_definition.default_value is not None:
                    new_input.set_static_value(parameter_definition.default_value)

            position += 1
            self.addons_inputs.append(new_input)

        # check that all given args are in the definition
        for arg_name in addon_args:
            if arg_name not in all_addon_param_names:
                raise UnknownAddonParameterError(
                    message=f"The addon parameter '{arg_name}' is not a parameter in the addon '{addon_name}'."
                )

    def get_all_inputs(
        self, filter_not_editable: bool = False
    ) -> list[WorkflowActionInput]:
        normal_inputs = [input for input in self.inputs]
        addon_inputs = []
        for input in self.addons_inputs:
            if filter_not_editable:
                if input.editable is True:
                    addon_inputs.append(cast(WorkflowActionInput, input))
            else:
                addon_inputs.append(cast(WorkflowActionInput, input))
        return normal_inputs + addon_inputs

    def get_input(self, workflow_action_input_id: str) -> WorkflowActionInput:
        workflow_action_input = None
        for item in self.get_all_inputs():
            if item.id == workflow_action_input_id:
                workflow_action_input = item
        if not workflow_action_input:
            raise WorkflowActionInputNotFoundException
        return workflow_action_input

    def update_input_static_value(
        self, workflow_action_input_id: str, static_value: Any
    ) -> None:
        self.get_input(workflow_action_input_id).set_static_value(static_value)

    def update_input_project_variable_value(
        self, workflow_action_input_id: str, project_variable_value: ProjectVariable
    ) -> None:
        self.get_input(workflow_action_input_id).set_project_variable_value(
            project_variable_value
        )

    def update_input_reference_value(
        self,
        workflow_action_input_id: str,
        workflow_action_output: WorkflowActionOutput,
    ) -> None:
        input = self.get_input(workflow_action_input_id)
        if workflow_action_output.optional and not input.optional:
            raise WorkflowActionNonOptionalInputReferenceValueException()
        input.set_reference_value(workflow_action_output)

    def reset_input_value(
        self,
        workflow_action_input_id: str,
    ) -> None:
        input = self.get_input(workflow_action_input_id)
        input.reset_value()

    def clear_inputs_reference_values_for(
        self, workflow_action_outputs_deleted: List[WorkflowActionOutput]
    ) -> None:
        for item in self.get_all_inputs():
            item.clear_reference_value_for(workflow_action_outputs_deleted)

    def delete_input_file(self, workflow_input_id: str) -> None:
        workflow_action_input: WorkflowActionInput = self.get_input(workflow_input_id)
        workflow_action_input.delete_input_file()

    def get_outputs(self) -> List[WorkflowActionOutput]:
        return self.outputs + self.dynamic_outputs

    def has_output(self, workflow_action_output_id: str) -> bool:
        return any(item.id == workflow_action_output_id for item in self.get_outputs())

    def has_io(self, workflow_action_io_id: str) -> bool:
        return any(item.id == workflow_action_io_id for item in self.get_all_io())

    def get_all_io(self) -> Sequence[WorkflowActionIO]:
        return cast(List[WorkflowActionIO], self.get_all_inputs()) + cast(
            List[WorkflowActionIO], self.get_outputs()
        )

    def get_io_from_technical_name(self, name: str) -> WorkflowActionIO:
        for io in self.get_all_io():
            if io.technical_name == name:
                return io
        raise WorkflowActionIONotFoundException()

    def get_output(self, action_output_id: str) -> WorkflowActionOutput:
        action_output = None
        all_outputs = self.get_outputs()
        for item in all_outputs:
            if item.id == action_output_id:
                action_output = item
        if action_output is None:
            raise WorkflowActionOutputNotFoundException()
        return action_output

    def add_output(self, action_output: ActionIO, position: int) -> None:
        output = WorkflowActionOutput(
            id=str(uuid.uuid4()),
            technical_name=action_output.technical_name,
            display_name=action_output.display_name,
            help=action_output.help,
            type=action_output.type,
            enum_values=action_output.enum_values,
            position=position,
            optional=action_output.optional,
        )
        self.outputs.append(output)

    def add_category(self, action_category: ActionCategory) -> None:
        self.categories.append(
            WorkflowActionCategory(
                id=str(uuid.uuid4()),
                name=action_category.name,
            )
        )

    def add_dynamic_output(
        self, data: AddWorkflowActionDynamicOutput, position: int
    ) -> WorkflowActionOutput:
        if not self.has_dynamic_outputs:
            raise WorkflowActionHasNoDynamicOutputsException()
        else:
            output = WorkflowActionOutput(
                id=str(uuid.uuid4()),
                technical_name=str(uuid.uuid4()),
                display_name=data.display_name,
                help=data.help,
                type=data.type,
                enum_values=data.enum_values,
                is_dynamic=True,
                position=position,
                optional=data.optional,
            )
            self.dynamic_outputs.append(output)
            return output

    def update_dynamic_output(
        self, action_output_id: str, data: UpdateWorkflowActionDynamicOutput
    ) -> None:
        if not self.has_dynamic_outputs:
            raise WorkflowActionHasNoDynamicOutputsException()
        action_output = self.get_output(action_output_id)
        action_output.update(data)

    def delete_dynamic_output(self, action_output_id: str) -> None:
        if not self.has_dynamic_outputs:
            raise WorkflowActionHasNoDynamicOutputsException()
        action_output = self.get_output(action_output_id)
        self.dynamic_outputs.remove(action_output)

    def copy(self, io_and_errors: dict) -> "WorkflowAction":
        return WorkflowAction(
            id=str(uuid.uuid4()),
            name=self.name,
            technical_name=self.technical_name,
            description=self.description,
            version=self.version,
            kind=self.kind,
            has_dynamic_outputs=self.has_dynamic_outputs,
            custom_name=self.custom_name,
            position_x=self.position_x,
            position_y=self.position_y,
            inputs=[io_and_errors[item.id] for item in self.inputs],
            outputs=[io_and_errors[item.id] for item in self.outputs],
            dynamic_outputs=[io_and_errors[item.id] for item in self.dynamic_outputs],
            errors=[io_and_errors[item.id] for item in self.errors],
            action_id=self.action_id,
            validity_status=WorkflowActionValidityStatus(self.validity_status),
            deployment_status=WorkflowActionDeployStatus.NONE,
            categories=self.categories,
            endpoint=self.endpoint,
            addons_inputs=self.addons_inputs,
            addons=self.addons,
            resources=self.resources,
            objectives=self.objectives.copy()
            if self.objectives
            else WorkflowActionObjectives(),
            constraints=self.constraints.copy()
            if self.constraints
            else WorkflowActionConstraints(),
        )

    def add_input_file(
        self, input_id: str, name: str, extension: str, path: str, size: float
    ) -> None:
        workflow_action_input: WorkflowActionInput = self.get_input(input_id)
        workflow_action_input.add_input_file(name, extension, path, size)

    def patch_workflow_action_ios_values(
        self, old_workflow_action: "WorkflowAction"
    ) -> None:
        """This function patch the inputs/outputs values of workflow action on a new workflow action"""
        input: WorkflowActionInput
        new_input: WorkflowActionInput
        for input in old_workflow_action.get_all_inputs():
            for new_input in self.get_all_inputs():
                if (
                    new_input.technical_name == input.technical_name
                    and new_input.type == input.type
                ):
                    if input.has_static_value():
                        assert input.static_value is not None
                        new_input.set_static_value(input.static_value)
                    elif input.has_reference_value():
                        assert input.reference_value is not None
                        new_input.set_reference_value(input.reference_value)
                    elif input.has_file_value():
                        assert input.file_value is not None
                        new_input.set_file_value(input.file_value)
                    elif input.has_project_variable_value():
                        assert input.project_variable_value is not None
                        new_input.set_project_variable_value(
                            input.project_variable_value
                        )
                    break

    def get_static_files(self) -> List[WorkflowFile]:
        return [
            item.file_value
            for item in self.get_all_inputs()
            if item.file_value is not None
        ]

    def get_name(self) -> str:
        return self.custom_name if self.custom_name else self.name

    def transfer_dynamic_outputs(self, old_workflow_action: "WorkflowAction") -> None:
        for dyn_output in old_workflow_action.dynamic_outputs:
            self.dynamic_outputs.append(dyn_output.copy())

    def set_constraints(self, constraints: WorkflowActionConstraints) -> None:
        self.constraints = constraints

    def set_objectives(self, objectives: WorkflowActionObjectives) -> None:
        self.objectives = objectives
