# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import uuid
from dataclasses import dataclass

from ryax.studio.domain.workflow.workflow_values import WorkflowErrorCode


@dataclass
class WorkflowError:
    """Class for modeling a workflow error"""

    id: str
    code: WorkflowErrorCode

    def copy(self) -> "WorkflowError":
        return WorkflowError(id=str(uuid.uuid4()), code=self.code)
