from dataclasses import dataclass

from ryax.studio.domain.action.action_io import ActionIO


@dataclass
class WorkflowActionIO(ActionIO):
    position: int = 0
