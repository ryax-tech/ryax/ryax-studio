from dataclasses import dataclass


@dataclass
class WorkflowActionObjectives:
    energy: float = 0
    cost: float = 0
    performance: float = 0

    def copy(self) -> "WorkflowActionObjectives":
        return WorkflowActionObjectives(
            energy=self.energy,
            cost=self.cost,
            performance=self.performance,
        )
