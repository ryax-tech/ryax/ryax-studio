from dataclasses import dataclass, field


@dataclass
class WorkflowActionConstraints:
    site_list: list[str] = field(default_factory=list)
    site_type_list: list[str] = field(default_factory=list)
    node_pool_list: list[str] = field(default_factory=list)
    # FIXME arch constraints are not usable right now because Ryax is only able to build actions for one arch
    arch_list: list[str] = field(default_factory=list)

    def copy(self) -> "WorkflowActionConstraints":
        return WorkflowActionConstraints(
            site_list=self.site_list,
            arch_list=self.arch_list,
            site_type_list=self.site_type_list,
            node_pool_list=self.node_pool_list,
        )
