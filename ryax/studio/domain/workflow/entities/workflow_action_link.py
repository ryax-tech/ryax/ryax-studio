# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import uuid
from dataclasses import dataclass, field
from typing import Dict

from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction


@dataclass
class WorkflowActionLink:
    """Class to modeling link between actions in workflow"""

    upstream_action_id: str
    downstream_action_id: str
    id: str = field(default_factory=lambda: str(uuid.uuid4()))

    @staticmethod
    def create_from_workflow_actions(
        upstream_action: WorkflowAction, downstream_action: WorkflowAction
    ) -> "WorkflowActionLink":
        return WorkflowActionLink(
            upstream_action_id=upstream_action.id,
            downstream_action_id=downstream_action.id,
        )

    def get_output_action_id(self) -> str:
        return self.downstream_action_id

    def get_input_action_id(self) -> str:
        return self.upstream_action_id

    def has_action(self, workflow_action_id: str) -> bool:
        return (
            workflow_action_id == self.upstream_action_id
            or workflow_action_id == self.downstream_action_id
        )

    def has_input_action(self, workflow_action_id: str) -> bool:
        return workflow_action_id == self.upstream_action_id

    def has_output_action(self, workflow_action_id: str) -> bool:
        return workflow_action_id == self.downstream_action_id

    def copy(
        self, workflow_action_map: Dict[str, WorkflowAction]
    ) -> "WorkflowActionLink":
        return WorkflowActionLink.create_from_workflow_actions(
            upstream_action=workflow_action_map[self.upstream_action_id],
            downstream_action=workflow_action_map[self.downstream_action_id],
        )
