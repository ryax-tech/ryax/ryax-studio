# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
import uuid
from dataclasses import dataclass, field
from graphlib import CycleError, TopologicalSorter
from typing import Any, Dict, List, Optional, Set, Tuple

from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_values import ActionKind
from ryax.studio.domain.addon.addon_entities import AddOns
from ryax.studio.domain.common.base_event import BaseEvent
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_constraints import (
    WorkflowActionConstraints,
)
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionInput,
)
from ryax.studio.domain.workflow.entities.workflow_action_io import WorkflowActionIO
from ryax.studio.domain.workflow.entities.workflow_action_link import WorkflowActionLink
from ryax.studio.domain.workflow.entities.workflow_action_objectives import (
    WorkflowActionObjectives,
)
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.entities.workflow_error import WorkflowError
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.entities.workflow_result import (
    WorkflowResult,
    WorkflowResultInfo,
)
from ryax.studio.domain.workflow.workflow_events import (
    WorkflowDeletedEvent,
    WorkflowDeployEvent,
    WorkflowUndeployEvent,
)
from ryax.studio.domain.workflow.workflow_exceptions import (
    FilestoreEntryNotFoundException,
    InvalidInputValueException,
    WorkflowActionIONotFoundException,
    WorkflowActionLinkNotFoundException,
    WorkflowActionNotFoundException,
    WorkflowActionOutputNotFoundException,
    WorkflowHasNoTriggerException,
    WorkflowNotDeployable,
    WorkflowNotUpdatableException,
    WorkflowNotValidException,
)
from ryax.studio.domain.workflow.workflow_values import (
    AddWorkflowAction,
    AddWorkflowActionDynamicOutput,
    CopyWorkflowData,
    UpdateWorkflowAction,
    UpdateWorkflowActionDynamicOutput,
    UpdateWorkflowActionInputValue,
    WorkflowActionValidityStatus,
    WorkflowDeploymentStatus,
    WorkflowErrorCode,
    WorkflowInformations,
    WorkflowValidityStatus,
)

logger = logging.getLogger(__name__)


@dataclass
class Workflow:
    """Class to modeling a workflow + check_ methods to assess its validity"""

    name: str
    description: Optional[str]
    owner: str
    project: str
    validity_status: WorkflowValidityStatus = WorkflowValidityStatus.VALID
    deployment_status: WorkflowDeploymentStatus = WorkflowDeploymentStatus.NONE
    deployment_error: Optional[str] = None
    id: str = field(default=str(uuid.uuid4()))
    actions: List[WorkflowAction] = field(default_factory=list)
    actions_links: List[WorkflowActionLink] = field(default_factory=list)
    errors: List[WorkflowError] = field(default_factory=list)
    events: List[BaseEvent] = field(default_factory=list)
    results: List[WorkflowResult] = field(default_factory=list)

    @property
    def has_form(self) -> bool:
        # FIXME: This is a hack use an internal action Kind instead
        return any(action.technical_name == "postgw" for action in self.actions)

    def get_trigger_action(self) -> WorkflowAction:
        for action in self.actions:
            if action.kind == ActionKind.SOURCE:
                return action
        else:
            raise WorkflowHasNoTriggerException()

    def _get_error_by_code(
        self, error_code: WorkflowErrorCode
    ) -> Optional[WorkflowError]:
        return next((item for item in self.errors if item.code == error_code), None)

    def add_error(self, error_code: WorkflowErrorCode) -> None:
        error: Optional[WorkflowError] = self._get_error_by_code(error_code)
        if error is None:
            new_error = WorkflowError(id=str(uuid.uuid4()), code=error_code)
            self.errors.append(new_error)

    def remove_error(self, error_code: WorkflowErrorCode) -> None:
        error: Optional[WorkflowError] = self._get_error_by_code(error_code)
        if error is not None:
            self.errors.remove(error)

    def check_graph_cyclicity(self) -> None:
        """Check that all actions have at least one link to other"""

        def _explore(
            workflow_action_id: str,
            workflow_action_origin_id: str,
            workflow_action_visits: Set[str],
        ) -> bool:
            """Function to proceed deep workflow exploration following links"""
            workflow_actions_links = [
                link
                for link in self.actions_links
                if link.has_input_action(workflow_action_id)
            ]
            for link in workflow_actions_links:
                if link.has_output_action(workflow_action_origin_id):
                    return True

                if link.get_output_action_id() not in workflow_action_visits:
                    item_output_action_id = link.get_output_action_id()
                    workflow_action_visits.add(item_output_action_id)
                    has_cycle = _explore(
                        item_output_action_id,
                        workflow_action_origin_id,
                        workflow_action_visits,
                    )
                    workflow_action_visits.remove(item_output_action_id)
                    return has_cycle
                else:
                    return True
            return False

        source_actions = list(
            filter(lambda item: item.kind == ActionKind.SOURCE, self.actions)
        )
        if len(source_actions):
            for item in source_actions:
                source_has_cycle = _explore(item.id, item.id, set())
                if source_has_cycle:
                    self.add_error(WorkflowErrorCode.WORKFLOW_HAS_CYCLES)
                else:
                    self.remove_error(WorkflowErrorCode.WORKFLOW_HAS_CYCLES)

    def check_graph_connexity(self) -> None:
        """Check that all actions have at least one link to other"""

        def _explore(workflow_action_id: str, workflow_action_marks: Set[str]) -> None:
            """Function to proceed deep workflow exploration"""
            workflow_action_marks.add(workflow_action_id)
            workflow_actions_links = [
                item
                for item in self.actions_links
                if item.has_action(workflow_action_id)
            ]
            for item in workflow_actions_links:
                if item.get_output_action_id() not in workflow_action_marks:
                    _explore(item.get_output_action_id(), workflow_action_marks)
                if item.get_input_action_id() not in workflow_action_marks:
                    _explore(item.get_input_action_id(), workflow_action_marks)
                else:
                    continue

        if len(self.actions):
            workflow_action_marks: Set[str] = set()
            _explore(self.actions[0].id, workflow_action_marks)
            if len(workflow_action_marks) < len(self.actions):
                self.add_error(WorkflowErrorCode.WORKFLOW_NOT_CONNECTED)
            else:
                self.remove_error(WorkflowErrorCode.WORKFLOW_NOT_CONNECTED)

    def check_actions_limit(self) -> None:
        if len(self.actions) > 128:
            self.add_error(WorkflowErrorCode.WORKFLOW_ACTIONS_UPPER_LIMIT_REACHED)
        else:
            self.remove_error(WorkflowErrorCode.WORKFLOW_ACTIONS_UPPER_LIMIT_REACHED)

    def check_sources_limit(self) -> None:
        source_actions = list(
            filter(lambda item: item.kind == ActionKind.SOURCE, self.actions)
        )
        if len(source_actions) < 1:
            self.add_error(WorkflowErrorCode.WORKFLOW_SOURCES_LOWER_LIMIT_REACHED)
        else:
            self.remove_error(WorkflowErrorCode.WORKFLOW_SOURCES_LOWER_LIMIT_REACHED)

    def update_validity_status(self) -> None:
        if len(self.errors) > 0:
            self.validity_status = WorkflowValidityStatus.INVALID
        elif not all(
            [
                item.validity_status == WorkflowActionValidityStatus.VALID
                for item in self.actions
            ]
        ):
            self.validity_status = WorkflowValidityStatus.INVALID
        else:
            self.validity_status = WorkflowValidityStatus.VALID

    def check_all(self) -> None:
        """Set workflow and actions to valid, reset errors, and call all check_ methods"""
        self.check_graph_connexity()
        self.check_graph_cyclicity()
        self.check_actions_limit()
        self.check_sources_limit()

        for action_item in self.actions:
            workflow_action_links_inputs = [
                item
                for item in self.actions_links
                if item.has_output_action(action_item.id)
            ]
            action_item.check_action_links_inputs(len(workflow_action_links_inputs))
            action_item.check_action_inputs()
            action_item.update_validity_status()

        self.update_validity_status()

    def is_deletable(self) -> bool:
        return self.deployment_status not in [
            WorkflowDeploymentStatus.DEPLOYING,
            WorkflowDeploymentStatus.DEPLOYED,
        ]

    def is_updatable(self) -> bool:
        return self.deployment_status not in [
            WorkflowDeploymentStatus.DEPLOYING,
            WorkflowDeploymentStatus.DEPLOYED,
        ]

    def update_informations(
        self, informations: WorkflowInformations, owner: str
    ) -> None:
        """Method to update workflow informations"""
        if informations.name is not None:
            self.name = informations.name
        if informations.description is not None:
            self.description = informations.description
        self.owner = owner

    def get_actions(self) -> List[WorkflowAction]:
        return self.actions

    def get_action(self, workflow_action_id: str) -> WorkflowAction:
        workflow_action: Optional[WorkflowAction] = next(
            (item for item in self.actions if item.id == workflow_action_id), None
        )
        if not workflow_action:
            raise WorkflowActionNotFoundException()
        else:
            return workflow_action

    def sort_actions(self) -> None:
        sorted_actions = []
        graph = {
            action.id: set(self.get_workflow_action_streams_to(action.id))
            for action in self.get_actions()
        }

        sorter = TopologicalSorter(graph)
        try:
            order = sorter.static_order()
            for item in order:
                sorted_actions.append(self.get_action(item))
            # Sort from root not from leaf
            sorted_actions.reverse()
            for position, action in enumerate(sorted_actions):
                action.position = position + 1
            self.actions = sorted_actions
        except CycleError:
            logger.exception(f"Cycle detected in the workflow {self}")

    def add_action(
        self, action: Action, data: AddWorkflowAction, addons: AddOns
    ) -> WorkflowAction:
        new_workflow_action = WorkflowAction.create_from_action(
            action, data.custom_name, data.position_x, data.position_y
        )
        for position, input_item in enumerate(action.inputs):
            new_workflow_action.add_input(input_item, position)
        for addon_name, addon_args in action.addons.items():
            new_workflow_action.add_addon(addon_name, addon_args, addons)
        for position, output_item in enumerate(action.outputs):
            new_workflow_action.add_output(output_item, position)
        for category_item in action.categories:
            new_workflow_action.add_category(category_item)
        self.actions.append(new_workflow_action)
        self.sort_actions()
        return new_workflow_action

    def delete_action(self, workflow_action_id: str) -> None:
        """Method to delete a action from a workflow"""
        workflow_action = self.get_action(workflow_action_id)
        links_to_deleted_action = self.get_links_to_workflow_action(workflow_action.id)
        links_from_deleted_action = self.get_links_from_workflow_action(
            workflow_action.id
        )
        for upstream_link in links_to_deleted_action:
            for downstream_link in links_from_deleted_action:
                upstream_action = self.get_action(upstream_link.upstream_action_id)
                downstream_action = self.get_action(
                    downstream_link.downstream_action_id
                )
                self.actions_links.append(
                    WorkflowActionLink.create_from_workflow_actions(
                        upstream_action=upstream_action,
                        downstream_action=downstream_action,
                    )
                )
        self.actions_links = [
            item
            for item in self.actions_links
            if not item.has_action(workflow_action.id)
        ]
        for item in self.actions:
            item.clear_inputs_reference_values_for(workflow_action.get_outputs())
        self.actions.remove(workflow_action)
        self.sort_actions()

    def update_action(
        self, workflow_action_id: str, data: UpdateWorkflowAction
    ) -> None:
        workflow_action = next(
            (item for item in self.actions if item.id == workflow_action_id), None
        )
        if not workflow_action:
            raise WorkflowActionNotFoundException()
        else:
            workflow_action.update_data(data)

    def add_action_link(
        self, upstream_action_id: str, downstream_action_id: str
    ) -> None:
        """Method to add a link between actions"""
        upstream_action = self.get_action(upstream_action_id)
        downstream_action = self.get_action(downstream_action_id)
        new_action_link = WorkflowActionLink.create_from_workflow_actions(
            upstream_action=upstream_action, downstream_action=downstream_action
        )
        self.actions_links.append(new_action_link)
        self.sort_actions()

    def transfer_workflow_action_links(
        self, workflow_action_1: WorkflowAction, workflow_action_2: WorkflowAction
    ) -> None:
        """This function replace a action "workflow_action_1" by another "workflow_action_2" in all workflow links"""
        link: WorkflowActionLink
        for link in self.actions_links:
            if link.get_input_action_id() == workflow_action_1.id:
                link.upstream_action_id = workflow_action_2.id
            if link.get_output_action_id() == workflow_action_1.id:
                link.downstream_action_id = workflow_action_2.id

        self.sort_actions()

    def delete_action_link(self, link_id: str) -> None:
        """Method to delete a link in a workflow"""
        workflow_action_link = next(
            (item for item in self.actions_links if item.id == link_id), None
        )
        if not workflow_action_link:
            raise WorkflowActionLinkNotFoundException()
        else:
            self.actions_links.remove(workflow_action_link)
        self.sort_actions()

    def get_all_outputs(self) -> list[WorkflowActionOutput]:
        outputs: list[WorkflowActionOutput] = []
        for action in self.actions:
            outputs.extend(action.get_outputs())
        return outputs

    def get_all_inputs(self) -> list[WorkflowActionInput]:
        inputs: list[WorkflowActionInput] = []
        for action in self.actions:
            inputs.extend(action.get_all_inputs())
        return inputs

    def get_all_ios(self) -> list[WorkflowActionIO]:
        ios: list[WorkflowActionIO] = []
        for action in self.actions:
            ios.extend(action.get_all_io())
        return ios

    def get_input_file(self, workflow_action_input_id: str) -> WorkflowFile:
        for action_input in self.get_all_inputs():
            if (
                action_input.id == workflow_action_input_id
                and action_input.file_value is not None
            ):
                return action_input.file_value
        else:
            raise FilestoreEntryNotFoundException

    def get_all_errors(self) -> List[WorkflowError]:
        errors: List[WorkflowError] = []
        for item in self.actions:
            errors.extend(item.errors)
        errors.extend(self.errors)
        return errors

    def copy(self, data: CopyWorkflowData, owner: str) -> "Workflow":
        copy_object_map: Dict[str, Any] = {}

        output_item: WorkflowActionOutput
        for output_item in self.get_all_outputs():
            copy_object_map[output_item.id] = output_item.copy()

        input_item: WorkflowActionInput
        for input_item in self.get_all_inputs():
            copy_object_map[input_item.id] = input_item.copy(copy_object_map)

        error_item: WorkflowError
        for error_item in self.get_all_errors():
            copy_object_map[error_item.id] = error_item.copy()

        action_item: WorkflowAction
        for action_item in self.actions:
            copy_object_map[action_item.id] = action_item.copy(copy_object_map)

        link_item: WorkflowActionLink
        for link_item in self.actions_links:
            copy_object_map[link_item.id] = link_item.copy(copy_object_map)

        workflow = Workflow(
            id=str(uuid.uuid4()),
            name=data.name if data.name else self.name,
            description=data.description if data.description else self.description,
            actions=[copy_object_map[item.id] for item in self.actions],
            actions_links=[copy_object_map[item.id] for item in self.actions_links],
            errors=[copy_object_map[item.id] for item in self.errors],
            owner=owner,
            deployment_status=WorkflowDeploymentStatus.NONE,
            validity_status=self.validity_status,
            project=self.project,
        )
        workflow.sort_actions()
        return workflow

    def add_action_dynamic_output(
        self, workflow_action_id: str, data: AddWorkflowActionDynamicOutput
    ) -> None:
        workflow_action = self.get_action(workflow_action_id)
        workflow_action.add_dynamic_output(data, len(workflow_action.dynamic_outputs))

    def update_action_dynamic_output(
        self,
        workflow_action_id: str,
        workflow_dynamic_output_id: str,
        data: UpdateWorkflowActionDynamicOutput,
    ) -> None:
        workflow_action = self.get_action(workflow_action_id)
        workflow_action.update_dynamic_output(workflow_dynamic_output_id, data)

    def delete_action_dynamic_output(
        self, workflow_action_id: str, workflow_dynamic_output_id: str
    ) -> None:
        workflow_action = self.get_action(workflow_action_id)
        workflow_action.delete_dynamic_output(workflow_dynamic_output_id)

    def transfer_actions_references(
        self, old_workflow_action: WorkflowAction, new_workflow_action: WorkflowAction
    ) -> None:
        """
        For all actions in workflow, if any action has a reference to the old workflow action outputs,
        it must refer the same output in the new workflow action if it exists otherwise it refers nothing.
        """
        item: WorkflowAction
        input: WorkflowActionInput
        for item in self.actions:
            for input in item.inputs:
                if (
                    input.has_reference_value()
                    and input.reference_value in old_workflow_action.get_outputs()
                ):
                    for new_action_output in new_workflow_action.get_outputs():
                        if (
                            input.reference_value.technical_name
                            == new_action_output.technical_name
                        ):
                            input.reference_value = new_action_output

    def update_action_input(
        self,
        workflow_action_id: str,
        workflow_action_input_id: str,
        data: UpdateWorkflowActionInputValue,
    ) -> None:
        workflow_action = self.get_action(workflow_action_id)

        # Only one of the 3 kind of input should be set
        if (
            data.reference_value is None
            and data.static_value is None
            and data.project_variable_value is None
        ):
            workflow_action.reset_input_value(workflow_action_input_id)
        elif (
            data.static_value is not None
            and data.reference_value is None
            and data.project_variable_value is None
        ):
            workflow_action.update_input_static_value(
                workflow_action_input_id, data.static_value
            )
        elif (
            data.reference_value is not None
            and data.static_value is None
            and data.project_variable_value is None
        ):
            for action in self.actions:
                if action.has_output(data.reference_value):
                    workflow_action_output = action.get_output(data.reference_value)
                    workflow_action.update_input_reference_value(
                        workflow_action_input_id, workflow_action_output
                    )
                    break
            else:
                raise WorkflowActionOutputNotFoundException()
        elif (
            data.project_variable_value is not None
            and data.reference_value is None
            and data.static_value is None
        ):
            workflow_action.update_input_project_variable_value(
                workflow_action_input_id, data.project_variable_value
            )
        else:
            raise InvalidInputValueException(
                message="Only one of the value type (static, reference, variable) must be set"
            )

    def delete_input_file(
        self, workflow_action_id: str, workflow_input_id: str
    ) -> None:
        workflow_action: WorkflowAction = self.get_action(workflow_action_id)
        workflow_action.delete_input_file(workflow_input_id)

    def get_action_input_file_path(
        self, workflow_action_id: str, workflow_input_id: str
    ) -> str:
        """Raise an exception if this input does not have a file_value"""
        workflow_action: WorkflowAction = self.get_action(workflow_action_id)
        workflow_input = workflow_action.get_input(workflow_input_id)
        assert workflow_input.file_value is not None
        return workflow_input.file_value.path

    def get_action_outputs_maps(
        self,
    ) -> Tuple[Dict[str, WorkflowActionOutput], Dict[str, WorkflowAction]]:
        """Returns a dict of {output_id: WorkflowActionOutput} and a dict of {output_id : WorkflowAction}
        for all actions in this workflow. Used when looking up reference values to deploy a workflow
        """
        outputs_map = {}
        outputs_parent_map = {}

        for action in self.actions:
            for output in action.get_outputs():
                outputs_map[output.id] = output
                outputs_parent_map[output.id] = action
        return outputs_map, outputs_parent_map

    def get_inputs_static_values_map(self, workflow_action: WorkflowAction) -> dict:
        static_inputs_map = {}
        for wf_mod_input in workflow_action.get_all_inputs():
            if wf_mod_input.has_static_value():
                static_inputs_map[
                    wf_mod_input.technical_name
                ] = wf_mod_input.static_value
            elif (
                wf_mod_input.project_variable_value is not None
                and not wf_mod_input.project_variable_value.type.is_file()
            ):
                static_inputs_map[
                    wf_mod_input.technical_name
                ] = wf_mod_input.project_variable_value.value
        return static_inputs_map

    def get_inputs_reference_values_map(self, workflow_action: WorkflowAction) -> dict:
        outputs_map, outputs_parent_map = self.get_action_outputs_maps()
        reference_inputs_map = {}
        for wf_mod_input in workflow_action.inputs + workflow_action.addons_inputs:
            if wf_mod_input.has_reference_value():
                assert wf_mod_input.reference_value is not None
                reference_action = outputs_parent_map[wf_mod_input.reference_value.id]
                reference_output = outputs_map[wf_mod_input.reference_value.id]
                reference_inputs_map[wf_mod_input.technical_name] = {
                    "workflow_action_id": reference_action.id,
                    "output_technical_name": reference_output.technical_name,
                }
        return reference_inputs_map

    def get_inputs_file_values_map(self, workflow_action: WorkflowAction) -> dict:
        file_values_map = {}
        for wf_mod_input in workflow_action.get_all_inputs():
            if wf_mod_input.file_value is not None:
                file_values_map[
                    wf_mod_input.technical_name
                ] = wf_mod_input.file_value.path
            elif (
                wf_mod_input.project_variable_value is not None
                and wf_mod_input.project_variable_value.type.is_file()
            ):
                assert wf_mod_input.project_variable_value.file_path is not None
                file_values_map[
                    wf_mod_input.technical_name
                ] = wf_mod_input.project_variable_value.file_path
        return file_values_map

    def get_workflow_action_streams_to(self, workflow_action_id: str) -> List[str]:
        return [
            item.get_output_action_id()
            for item in self.actions_links
            if item.has_input_action(workflow_action_id)
        ]

    def prepare_workflow_deleted_event(self) -> None:
        workflow_deleted_event = WorkflowDeletedEvent(
            workflow_id=self.id, project_id=self.project
        )
        self.events.append(workflow_deleted_event)

    def deploy(self) -> None:
        if self.validity_status is not WorkflowValidityStatus.VALID:
            raise WorkflowNotValidException
        if self.deployment_status not in [
            WorkflowDeploymentStatus.NONE,
            WorkflowDeploymentStatus.ERROR,
        ]:
            raise WorkflowNotDeployable

        self.deployment_error = None

        workflow_deploy_event = WorkflowDeployEvent(
            workflow_id=self.id,
            workflow_name=self.name,
            workflow_actions=[
                WorkflowDeployEvent.WorkflowAction(
                    id=workflow_action.id,
                    human_name=workflow_action.custom_name
                    if workflow_action.custom_name
                    else workflow_action.name,
                    description=workflow_action.description,
                    technical_name=workflow_action.technical_name,
                    action_id=workflow_action.action_id,
                    action_version=workflow_action.version,
                    action_kind=workflow_action.kind,
                    has_dynamic_outputs=workflow_action.has_dynamic_outputs,
                    endpoint=workflow_action.endpoint,
                    action_inputs=[
                        WorkflowDeployEvent.WorkflowActionIO(
                            technical_name=input.technical_name,
                            display_name=input.display_name,
                            help=input.help,
                            type=input.type,
                            enum_values=input.enum_values,
                            optional=input.optional,
                        )
                        for input in workflow_action.inputs
                    ],
                    action_addons_inputs=[
                        WorkflowDeployEvent.WorkflowActionIO(
                            technical_name=input.technical_name,
                            display_name=input.display_name,
                            help=input.help,
                            type=input.type,
                            enum_values=input.enum_values,
                            addon_name=input.addon_name,
                            optional=input.optional,
                        )
                        for input in workflow_action.addons_inputs
                    ],
                    action_outputs=[
                        WorkflowDeployEvent.WorkflowActionIO(
                            technical_name=output.technical_name,
                            display_name=output.display_name,
                            help=output.help,
                            type=output.type,
                            enum_values=output.enum_values,
                            optional=output.optional,
                        )
                        for output in workflow_action.outputs
                    ],
                    dynamic_outputs=[
                        WorkflowDeployEvent.WorkflowActionIO(
                            technical_name=output.technical_name,
                            display_name=output.display_name,
                            help=output.help,
                            type=output.type,
                            enum_values=output.enum_values,
                            origin=output.origin,
                            optional=output.optional,
                        )
                        for output in workflow_action.dynamic_outputs
                    ],
                    static_inputs_values=self.get_inputs_static_values_map(
                        workflow_action
                    ),
                    reference_inputs_values=self.get_inputs_reference_values_map(
                        workflow_action
                    ),
                    file_inputs_values=self.get_inputs_file_values_map(workflow_action),
                    streams_to=self.get_workflow_action_streams_to(workflow_action.id),
                    addons=workflow_action.addons,
                    resources=WorkflowDeployEvent.WorkflowActionResources(
                        cpu=workflow_action.resources.cpu,
                        memory=workflow_action.resources.memory,
                        time=workflow_action.resources.time,
                        gpu=workflow_action.resources.gpu,
                    )
                    if workflow_action.resources is not None
                    else WorkflowDeployEvent.WorkflowActionResources(),
                    constraints=workflow_action.constraints
                    if workflow_action.constraints
                    else WorkflowActionConstraints(),
                    objectives=workflow_action.objectives
                    if workflow_action.objectives
                    else WorkflowActionObjectives(),
                )
                for workflow_action in self.actions
            ],
            workflow_project_id=self.project,
            results=[
                WorkflowDeployEvent.WorkflowResult(
                    id=workflow_result.id,
                    key=workflow_result.key,
                    workflow_action_io_id=workflow_result.workflow_action_io_id,
                    technical_name=next(
                        workflow_io.technical_name
                        for workflow_io in self.get_all_ios()
                        if workflow_io.id == workflow_result.workflow_action_io_id
                    ),
                    action_id=next(
                        action.id
                        for action in self.actions
                        if action.has_io(
                            workflow_action_io_id=workflow_result.workflow_action_io_id
                        )
                    ),
                )
                for workflow_result in self.results
            ],
        )
        self.events.append(workflow_deploy_event)
        self.deployment_status = WorkflowDeploymentStatus.DEPLOYING

    def undeploy(self, graceful: bool) -> None:
        workflow_deploy_event = WorkflowUndeployEvent(
            workflow_id=self.id,
            workflow_project_id=self.project,
            graceful=graceful,
        )
        self.events.append(workflow_deploy_event)

    def undeploy_success(self) -> None:
        self.deployment_status = WorkflowDeploymentStatus.NONE

    def undeploy_with_error(self, error_message: str) -> None:
        self.deployment_status = WorkflowDeploymentStatus.ERROR
        self.deployment_error = error_message

    def deploy_workflow_success(
        self,
    ) -> None:
        self.deployment_status = WorkflowDeploymentStatus.DEPLOYED

    def add_input_file(
        self,
        action_id: str,
        input_id: str,
        name: str,
        extension: str,
        path: str,
        size: float,
    ) -> None:
        if not self.is_updatable():
            raise WorkflowNotUpdatableException()
        workflow_action: WorkflowAction = self.get_action(action_id)
        workflow_action.add_input_file(input_id, name, extension, path, size)

    def set_action_constraints(
        self, action_id: str, constraints: WorkflowActionConstraints
    ) -> None:
        if not self.is_updatable():
            raise WorkflowNotUpdatableException()
        workflow_action = self.get_action(action_id)
        workflow_action.set_constraints(constraints)

    def set_action_objectives(
        self, action_id: str, objectives: WorkflowActionObjectives
    ) -> None:
        if not self.is_updatable():
            raise WorkflowNotUpdatableException()
        workflow_action = self.get_action(action_id)
        workflow_action.set_objectives(objectives)

    def get_action_static_files(self, workflow_action_id: str) -> List[WorkflowFile]:
        workflow_action: Optional[WorkflowAction] = next(
            (item for item in self.actions if item.id == workflow_action_id), None
        )
        if workflow_action is None:
            return []
        return workflow_action.get_static_files()

    def get_all_static_files(self) -> list[WorkflowFile]:
        all_static_files: list[WorkflowFile] = []
        for action in self.actions:
            all_static_files.extend(action.get_static_files())
        return all_static_files

    def get_links_from_workflow_action(
        self, workflow_action_id: str
    ) -> List[WorkflowActionLink]:
        return [
            link
            for link in self.actions_links
            if link.upstream_action_id == workflow_action_id
        ]

    def get_links_to_workflow_action(
        self, workflow_action_id: str
    ) -> List[WorkflowActionLink]:
        return [
            link
            for link in self.actions_links
            if link.downstream_action_id == workflow_action_id
        ]

    def get_workflow_action_result_info(
        self, workflow_action_io_id: str
    ) -> WorkflowResultInfo:
        for action_ordinal, action in enumerate(self.actions, start=1):
            for action_io in action.get_all_io():
                if action_io.id == workflow_action_io_id:
                    return WorkflowResultInfo(
                        action_name=action.get_name() + f" ({action_ordinal})",
                        action_io_name=action_io.display_name
                        + f" ({action_io.type.value})",
                        action_type=action_io.type,
                    )
        raise WorkflowActionIONotFoundException()

    def remove_workflow_results_from_old_action(
        self, old_workflow_action: WorkflowAction
    ) -> list[str]:
        # For now, we just check to see if a workflow result was an IO from the old action
        # A possible implementation is to check if that IO corresponds to one in the new action, and overwrite the results
        removed_result_keys_to_return = []
        removed_result_ids_to_delete = []
        for action_io in old_workflow_action.get_all_io():
            for result in self.results:
                if result.workflow_action_io_id == action_io.id:
                    removed_result_keys_to_return.append(result.key)
                    removed_result_ids_to_delete.append(result.id)
        self.results = [
            result
            for result in self.results
            if result.id not in removed_result_ids_to_delete
        ]
        return removed_result_keys_to_return
