# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import uuid
from dataclasses import dataclass
from typing import Optional

from ryax.studio.domain.action.action_values import DynamicOutputOrigin
from ryax.studio.domain.workflow.entities.workflow_action_io import WorkflowActionIO
from ryax.studio.domain.workflow.workflow_values import (
    UpdateWorkflowActionDynamicOutput,
)


@dataclass
class WorkflowActionOutput(WorkflowActionIO):
    is_dynamic: bool = False
    origin: Optional[DynamicOutputOrigin] = None

    def update(self, data: UpdateWorkflowActionDynamicOutput) -> None:
        self.technical_name = (
            data.technical_name
            if data.technical_name is not None
            else self.technical_name
        )
        self.display_name = (
            data.display_name if data.display_name is not None else self.display_name
        )
        self.help = data.help if data.help is not None else self.help
        self.type = data.type if data.type is not None else self.type
        self.enum_values = (
            data.enum_values if data.enum_values is not None else self.enum_values
        )
        self.optional = data.optional if data.optional is not None else self.optional
        self.origin = data.origin if data.origin is not None else self.origin

    def copy(self) -> "WorkflowActionOutput":
        return WorkflowActionOutput(
            id=str(uuid.uuid4()),
            technical_name=self.technical_name,
            display_name=self.display_name,
            help=self.help,
            type=self.type,
            enum_values=self.enum_values,
            is_dynamic=self.is_dynamic,
            origin=self.origin,
            optional=self.optional,
        )
