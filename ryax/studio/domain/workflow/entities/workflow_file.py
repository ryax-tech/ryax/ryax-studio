# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import uuid
from dataclasses import dataclass


@dataclass
class WorkflowFile:
    """Class for workflow static file"""

    id: str
    name: str
    extension: str
    path: str
    size: float

    def copy(self) -> "WorkflowFile":
        return WorkflowFile(
            id=str(uuid.uuid4()),
            name=self.name,
            extension=self.extension,
            path=self.path,
            size=self.size,
        )

    def update_path(self, path: str) -> None:
        self.path = path

    @property
    def size_human_readable(self) -> str:
        # Taken from:
        # https://stackoverflow.com/a/1094933/2165830
        suffix = "B"
        num = self.size
        for unit in ["", "Ki", "Mi", "Gi", "Ti"]:
            if abs(num) < 1024.0:
                return f"{num:3.1f}{unit}{suffix}"
            num /= 1024.0
        return f"{num:.1f}Yi{suffix}"

    def __str__(self) -> str:
        filename = f"{self.name}.{self.extension}" if self.extension else self.name
        return f"{filename} ({self.size_human_readable})"
