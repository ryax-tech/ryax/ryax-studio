# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
import uuid
from dataclasses import dataclass
from typing import Any, Dict, List, Optional, Union

from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.project_variables.project_variable_entities import (
    ProjectVariable,
)
from ryax.studio.domain.workflow.entities.workflow_action_io import WorkflowActionIO
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.workflow_exceptions import (
    DirectoryInputsMustBeZipfileException,
    InvalidInputValueException,
    NotImplementedException,
    WorkflowFileAlreadyPresentException,
    WorkflowInputHasNoStaticFileException,
)

logger = logging.getLogger(__name__)


@dataclass
class WorkflowActionInput(WorkflowActionIO):
    static_value: Optional[Union[str, float, int, bool, bytes]] = None
    reference_value: Optional[WorkflowActionOutput] = None
    file_value: Optional[WorkflowFile] = None
    project_variable_value: Optional[ProjectVariable] = None

    def has_defined_value(self) -> bool:
        return (
            self.static_value is not None
            or self.reference_value is not None
            or self.file_value is not None
            or self.project_variable_value is not None
        )

    def has_static_value(self) -> bool:
        return self.static_value is not None

    def has_reference_value(self) -> bool:
        return self.reference_value is not None

    def has_file_value(self) -> bool:
        return self.file_value is not None

    def has_project_variable_value(self) -> bool:
        return self.project_variable_value is not None

    def set_static_value(self, static_value: Any) -> None:
        try:
            self.reference_value = None
            self.file_value = None
            self.project_variable_value = None
            if self.optional and static_value is None:
                pass
            elif self.type in (
                ActionIOType.STRING,
                ActionIOType.LONGSTRING,
                ActionIOType.PASSWORD,
            ):
                self.static_value = str(static_value)
            elif self.type == ActionIOType.INTEGER:
                self.static_value = int(static_value)  # type: ignore
            elif self.type == ActionIOType.FLOAT:
                self.static_value = float(static_value)  # type: ignore
            elif self.type == ActionIOType.ENUM:
                self.static_value = (
                    str(static_value) if static_value in self.enum_values else None
                )
            elif self.type == ActionIOType.BOOLEAN:
                self.static_value = bool(static_value)
            elif self.type == ActionIOType.BYTES:
                self.static_value = bytes(static_value.encode()).decode()
            elif self.type in (
                ActionIOType.FILE,
                ActionIOType.DIRECTORY,
                ActionIOType.TABLE,
            ):
                raise NotImplementedException(
                    "Use WorkflowActionInput.add_input_file for file and dir."
                )
            else:
                raise InvalidInputValueException
        except ValueError:
            raise InvalidInputValueException

    def reset_value(self) -> None:
        self.static_value = None
        self.file_value = None
        self.reference_value = None
        self.project_variable_value = None

    def set_reference_value(self, workflow_action_output: WorkflowActionOutput) -> None:
        """Method to set reference value (workflow action output id)"""
        self.reset_value()
        self.reference_value = workflow_action_output

    def set_file_value(self, workflow_file: WorkflowFile) -> None:
        self.reset_value()
        self.file_value = workflow_file

    def set_project_variable_value(self, project_variable: ProjectVariable) -> None:
        self.reset_value()
        self.project_variable_value = project_variable

    def clear_reference_value_for(
        self, workflow_action_outputs: List[WorkflowActionOutput]
    ) -> None:
        if self.reference_value is not None:
            if any(
                [item.id == self.reference_value.id for item in workflow_action_outputs]
            ):
                self.reference_value = None
        else:
            self.reference_value = None

    def copy(
        self, copy_object: Dict[str, WorkflowActionOutput]
    ) -> "WorkflowActionInput":
        input = WorkflowActionInput(
            id=str(uuid.uuid4()),
            technical_name=self.technical_name,
            display_name=self.display_name,
            help=self.help,
            type=self.type,
            enum_values=self.enum_values,
            static_value=self.static_value,
            reference_value=copy_object.get(self.reference_value.id)
            if self.reference_value is not None
            else None,
            file_value=self.file_value.copy() if self.file_value is not None else None,
            project_variable_value=self.project_variable_value
            if self.project_variable_value is not None
            else None,
            position=self.position,
            optional=self.optional,
        )
        return input

    def delete_input_file(self) -> None:
        if not self.has_file_value() or self.type not in [
            ActionIOType.FILE,
            ActionIOType.DIRECTORY,
        ]:
            raise WorkflowInputHasNoStaticFileException
        else:
            self.file_value = None

    def add_input_file(self, name: str, extension: str, path: str, size: float) -> None:
        if self.has_file_value():
            raise WorkflowFileAlreadyPresentException()
        elif self.type not in (
            ActionIOType.FILE,
            ActionIOType.TABLE,
            ActionIOType.DIRECTORY,
        ):
            raise WorkflowInputHasNoStaticFileException
        elif self.type == ActionIOType.DIRECTORY and extension != "zip":
            raise DirectoryInputsMustBeZipfileException

        self.reset_value()
        self.file_value = WorkflowFile(
            id=str(uuid.uuid4()), name=name, extension=extension, path=path, size=size
        )


@dataclass
class WorkflowActionAddonInput(WorkflowActionInput):
    addon_name: Optional[str] = None
    editable: bool = False
