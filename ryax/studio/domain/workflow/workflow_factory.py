# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import uuid

from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.workflow_values import CreateWorkflowData


class WorkflowFactory:
    """Workflow Factory"""

    def __init__(self) -> None:
        pass

    def from_create_workflow_data(
        self, create_workflow_data: CreateWorkflowData, owner: str, project_id: str
    ) -> Workflow:
        return Workflow(
            id=str(uuid.uuid4()),
            name=create_workflow_data.name,
            description=create_workflow_data.description,
            owner=owner,
            project=project_id,
        )
