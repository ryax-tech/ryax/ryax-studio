# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass, field
from typing import Optional, Union

from ryax.studio.domain.action.action_values import (
    ActionIOType,
    ActionKind,
    DynamicOutputOrigin,
)
from ryax.studio.domain.common.base_event import BaseEvent
from ryax.studio.domain.workflow.entities.workflow_action_constraints import (
    WorkflowActionConstraints,
)
from ryax.studio.domain.workflow.entities.workflow_action_objectives import (
    WorkflowActionObjectives,
)


@dataclass
class WorkflowDeployEvent(BaseEvent):
    @dataclass
    class WorkflowActionResources:
        cpu: Optional[float] = None
        memory: Optional[int] = None
        time: Optional[float] = None
        gpu: Optional[int] = None

    @dataclass
    class WorkflowActionIO:
        technical_name: str
        display_name: str
        help: str
        type: ActionIOType
        optional: bool
        enum_values: list[str] = field(init=True, default_factory=list)
        origin: Optional[DynamicOutputOrigin] = None
        addon_name: Optional[str] = None

    @dataclass
    class WorkflowResult:
        id: str
        key: str
        workflow_action_io_id: str
        technical_name: str
        action_id: str

    @dataclass
    class WorkflowAction:
        id: str
        technical_name: str
        human_name: str
        description: str
        action_id: str
        action_version: str
        action_kind: ActionKind
        action_inputs: list["WorkflowDeployEvent.WorkflowActionIO"]
        action_outputs: list["WorkflowDeployEvent.WorkflowActionIO"]
        dynamic_outputs: list["WorkflowDeployEvent.WorkflowActionIO"]
        action_addons_inputs: list["WorkflowDeployEvent.WorkflowActionIO"]
        static_inputs_values: dict[str, Union[str, float, int, bool, bytes]]
        has_dynamic_outputs: bool
        reference_inputs_values: dict[str, dict[str, str]]
        file_inputs_values: dict[str, str]
        streams_to: list[str]
        endpoint: Optional[str]
        addons: dict[str, dict]
        resources: "WorkflowDeployEvent.WorkflowActionResources"
        objectives: WorkflowActionObjectives
        constraints: WorkflowActionConstraints

    workflow_id: str
    workflow_actions: list[WorkflowAction]
    workflow_project_id: str
    workflow_name: str
    results: list[WorkflowResult]
    event_type: str = "WorkflowDeploy"


@dataclass
class WorkflowUndeployEvent(BaseEvent):
    workflow_id: str
    workflow_project_id: str
    graceful: bool
    event_type: str = "WorkflowUndeploy"


@dataclass
class WorkflowDeletedEvent(BaseEvent):
    workflow_id: str
    project_id: str
    event_type: str = "WorkflowDeleted"
