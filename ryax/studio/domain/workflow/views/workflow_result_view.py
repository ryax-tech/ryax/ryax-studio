from dataclasses import dataclass

from ryax.studio.domain.action.action_values import ActionIOType


@dataclass
class WorkflowResultView:
    key: str
    workflow_action_io_id: str
    description: str
    type: ActionIOType
