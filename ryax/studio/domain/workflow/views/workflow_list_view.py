from dataclasses import dataclass
from typing import Optional

from ryax.studio.domain.workflow.workflow_values import (
    WorkflowDeploymentStatus,
    WorkflowValidityStatus,
)


@dataclass
class WorkflowListView:
    id: str
    name: str
    description: Optional[str]
    validity_status: WorkflowValidityStatus
    deployment_status: WorkflowDeploymentStatus
    has_form: bool
    endpoint_prefix: Optional[str]
    categories: list[str]
    trigger: Optional[str]
    parameters: list[dict]
