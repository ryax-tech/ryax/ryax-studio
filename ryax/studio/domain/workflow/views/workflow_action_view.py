# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from typing import Optional, cast

from ryax.studio.domain.action.action_values import ActionKind
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_category import (
    WorkflowActionCategory,
)
from ryax.studio.domain.workflow.entities.workflow_action_constraints import (
    WorkflowActionConstraints,
)
from ryax.studio.domain.workflow.entities.workflow_action_objectives import (
    WorkflowActionObjectives,
)
from ryax.studio.domain.workflow.entities.workflow_action_resources import (
    WorkflowActionResources,
)
from ryax.studio.domain.workflow.views.workflow_action_input_view import (
    WorkflowActionInputView,
)
from ryax.studio.domain.workflow.views.workflow_action_output_view import (
    WorkflowActionOutputView,
)
from ryax.studio.domain.workflow.views.workflow_action_version_view import (
    WorkflowActionVersion,
)
from ryax.studio.domain.workflow.workflow_values import (
    WorkflowActionDeployStatus,
    WorkflowActionValidityStatus,
)


@dataclass
class WorkflowActionCategoryView:
    id: str
    name: str

    @staticmethod
    def from_category(category: WorkflowActionCategory) -> "WorkflowActionCategoryView":
        return WorkflowActionCategoryView(category.id, category.name)


@dataclass
class WorkflowActionResourcesView:
    cpu: Optional[float]
    gpu: Optional[int]
    memory: Optional[int]
    time: Optional[float]

    @staticmethod
    def from_resources(
        resources: Optional[WorkflowActionResources],
    ) -> Optional["WorkflowActionResourcesView"]:
        if resources is None:
            return None
        return WorkflowActionResourcesView(
            cpu=resources.cpu,
            gpu=resources.gpu,
            time=resources.time,
            memory=resources.memory,
        )


@dataclass
class WorkflowActionView:
    id: str
    name: str
    technical_name: str
    description: str
    version: str
    kind: ActionKind
    has_dynamic_outputs: bool
    validity_status: WorkflowActionValidityStatus
    deployment_status: WorkflowActionDeployStatus
    action_id: str
    custom_name: Optional[str]
    position_x: Optional[int]
    position_y: Optional[int]
    workflow_id: Optional[str]
    categories: list[WorkflowActionCategoryView]
    endpoint: Optional[str]
    resources: Optional[WorkflowActionResourcesView]
    constraints: WorkflowActionConstraints
    objectives: WorkflowActionObjectives

    @classmethod
    def from_workflow_action(cls, action: WorkflowAction) -> "WorkflowActionView":
        return cls(
            id=action.id,
            name=action.name,
            kind=action.kind,
            technical_name=action.technical_name,
            description=action.description,
            deployment_status=action.deployment_status,
            validity_status=action.validity_status
            if action.validity_status
            else WorkflowActionValidityStatus.INVALID,
            action_id=action.action_id,
            workflow_id=action.workflow_id,
            custom_name=action.custom_name,
            categories=[
                WorkflowActionCategoryView.from_category(category)
                for category in action.categories
            ],
            version=action.version,
            endpoint=action.endpoint,
            has_dynamic_outputs=action.has_dynamic_outputs,
            position_y=action.position_y,
            position_x=action.position_x,
            resources=WorkflowActionResourcesView.from_resources(action.resources),
            objectives=WorkflowActionObjectives(
                energy=action.objectives.energy,
                cost=action.objectives.cost,
                performance=action.objectives.performance,
            )
            if action.objectives
            else WorkflowActionObjectives(),
            constraints=WorkflowActionConstraints(
                node_pool_list=action.constraints.node_pool_list,
                site_list=action.constraints.site_list,
                site_type_list=action.constraints.site_type_list,
                arch_list=action.constraints.arch_list,
            )
            if action.constraints
            else WorkflowActionConstraints(),
        )


@dataclass
class WorkflowActionExtendedView(WorkflowActionView):
    versions: list[WorkflowActionVersion]
    inputs: list[WorkflowActionInputView]
    addons_inputs: list[WorkflowActionInputView]
    outputs: list[WorkflowActionOutputView]
    dynamic_outputs: list[WorkflowActionOutputView]

    @classmethod
    def from_workflow_action(
        cls,
        workflow_action: WorkflowAction,
    ) -> "WorkflowActionExtendedView":
        view: WorkflowActionExtendedView = cast(
            WorkflowActionExtendedView,
            WorkflowActionView.from_workflow_action(workflow_action),
        )
        view.inputs = [
            WorkflowActionInputView.from_workflow_action_input(input)
            for input in workflow_action.inputs
        ]
        view.addons_inputs = [
            WorkflowActionInputView.from_workflow_action_input(input)
            for input in workflow_action.addons_inputs
            if input.editable is True or input.editable is None
        ]
        view.outputs = [
            WorkflowActionOutputView.from_workflow_action_output(output)
            for output in workflow_action.outputs
        ]
        view.dynamic_outputs = [
            WorkflowActionOutputView.from_workflow_action_output(output)
            for output in workflow_action.dynamic_outputs
        ]
        return view
