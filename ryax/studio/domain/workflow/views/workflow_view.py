# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass, field
from typing import List, Optional

from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.views.workflow_action_link_view import (
    WorkflowActionLinkView,
)
from ryax.studio.domain.workflow.views.workflow_action_view import (
    WorkflowActionExtendedView,
    WorkflowActionView,
)
from ryax.studio.domain.workflow.workflow_values import (
    WorkflowDeploymentStatus,
    WorkflowValidityStatus,
)


@dataclass
class WorkflowView:
    """Class to create a workflow detailed view"""

    id: str
    name: str
    description: Optional[str]
    validity_status: WorkflowValidityStatus
    deployment_status: WorkflowDeploymentStatus
    project: str
    actions: List[WorkflowActionView] = field(default_factory=list)
    actions_links: List[WorkflowActionLinkView] = field(default_factory=list)
    deployment_error: Optional[str] = None

    @property
    def has_form(self) -> bool:
        # FIXME: This is a hack use an internal action Kind instead
        return any(action.technical_name == "postgw" for action in self.actions)

    @staticmethod
    def from_workflow(workflow: Workflow) -> "WorkflowView":
        view = WorkflowView(
            id=workflow.id,
            name=workflow.name,
            description=workflow.description,
            validity_status=workflow.validity_status,
            deployment_status=workflow.deployment_status,
            project=workflow.project,
            actions=[
                WorkflowActionView.from_workflow_action(action)
                for action in workflow.actions
            ],
            actions_links=[
                WorkflowActionLinkView.from_workflow_action(action)
                for action in workflow.actions_links
            ],
            deployment_error=workflow.deployment_error
            if workflow.deployment_error
            else None,
        )
        return view


@dataclass
class WorkflowExtendedView(WorkflowView):
    actions: List[WorkflowActionExtendedView] = field(default_factory=list)  # type: ignore

    @staticmethod
    def from_workflow(workflow: Workflow) -> "WorkflowExtendedView":
        view = WorkflowExtendedView(
            id=workflow.id,
            name=workflow.name,
            description=workflow.description,
            validity_status=workflow.validity_status,
            deployment_status=workflow.deployment_status,
            deployment_error=workflow.deployment_error
            if workflow.deployment_error
            else None,
            project=workflow.project,
            actions=[
                WorkflowActionExtendedView.from_workflow_action(action)
                for action in workflow.actions
            ],
            actions_links=[
                WorkflowActionLinkView.from_workflow_action(action)
                for action in workflow.actions_links
            ],
        )
        return view
