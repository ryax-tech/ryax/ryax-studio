# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass, field
from typing import List, Optional

from ryax.studio.domain.action.action_values import ActionIOType, DynamicOutputOrigin
from ryax.studio.domain.workflow.entities.workflow_action_output import (
    WorkflowActionOutput,
)


@dataclass
class WorkflowActionOutputView:
    id: str
    technical_name: str
    display_name: str
    help: str
    type: ActionIOType
    optional: bool
    enum_values: List[str] = field(default_factory=list)
    origin: Optional[DynamicOutputOrigin] = None

    @staticmethod
    def from_workflow_action_output(
        output: WorkflowActionOutput,
    ) -> "WorkflowActionOutputView":
        return WorkflowActionOutputView(
            id=output.id,
            display_name=output.display_name,
            technical_name=output.technical_name,
            enum_values=output.enum_values,
            help=output.help,
            type=output.type,
            origin=output.origin,
            optional=output.optional,
        )
