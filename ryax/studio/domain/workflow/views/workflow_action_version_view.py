from dataclasses import dataclass


@dataclass
class WorkflowActionVersion:
    id: str
    version: str
