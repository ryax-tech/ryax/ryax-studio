# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from typing import Optional

from ryax.studio.domain.workflow.workflow_values import WorkflowErrorCode


@dataclass
class WorkflowErrorView:
    """Class for modeling a workflow error view"""

    id: str
    code: WorkflowErrorCode
    workflow_id: Optional[str] = None
    workflow_action_id: Optional[str] = None
