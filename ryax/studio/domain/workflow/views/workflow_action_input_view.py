# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from typing import List, Optional, Union

from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionInput,
)
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.views.workflow_action_output_view import (
    WorkflowActionOutputView,
)


@dataclass
class WorkflowActionInputView:
    id: str
    technical_name: str
    display_name: str
    help: str
    type: ActionIOType
    enum_values: List[str]
    static_value: Optional[Union[str, float, int, bool, bytes]]
    reference_value: Optional[str]
    reference_output: Optional[WorkflowActionOutputView]
    workflow_file: Optional[WorkflowFile]
    project_variable_value: Optional[str]
    optional: bool

    @staticmethod
    def from_workflow_action_input(
        input: WorkflowActionInput,
    ) -> "WorkflowActionInputView":
        return WorkflowActionInputView(
            id=input.id,
            display_name=input.display_name,
            technical_name=input.technical_name,
            enum_values=input.enum_values,
            help=input.help,
            type=input.type,
            reference_output=WorkflowActionOutputView.from_workflow_action_output(
                input.reference_value
            )
            if input.reference_value
            else None,
            reference_value=input.reference_value.id if input.reference_value else None,
            project_variable_value=input.project_variable_value.id
            if input.project_variable_value
            else None,
            workflow_file=input.file_value,
            static_value=input.static_value,
            optional=input.optional,
        )
