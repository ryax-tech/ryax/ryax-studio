# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass

from ryax.studio.domain.workflow.entities.workflow_action_link import WorkflowActionLink


@dataclass
class WorkflowActionLinkView:
    id: str
    upstream_action_id: str
    downstream_action_id: str

    @staticmethod
    def from_workflow_action(link: WorkflowActionLink) -> "WorkflowActionLinkView":
        return WorkflowActionLinkView(
            id=link.id,
            upstream_action_id=link.upstream_action_id,
            downstream_action_id=link.downstream_action_id,
        )
