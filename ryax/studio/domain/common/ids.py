import random
import string
import time


def get_random_id(length: int = 4) -> str:
    letters = string.digits + string.ascii_lowercase
    new_id = "".join(random.choice(letters) for _ in range(length))
    now = str(int(time.time()))
    return now + "-" + new_id
