# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import Any

from ryax.studio.domain.action.action_repository import IActionRepository
from ryax.studio.domain.project_variables.project_variable_repository import (
    IUserObjectRepository,
)
from ryax.studio.domain.workflow.workflow_repository import IWorkflowRepository


class IUnitOfWork(abc.ABC):
    workflows: IWorkflowRepository
    actions: IActionRepository
    project_variables: IUserObjectRepository

    @abc.abstractmethod
    def __enter__(self) -> None:
        pass

    @abc.abstractmethod
    def __exit__(self, *args: Any) -> None:
        pass

    @abc.abstractmethod
    def commit(self) -> None:
        pass

    @abc.abstractmethod
    def rollback(self) -> None:
        pass
