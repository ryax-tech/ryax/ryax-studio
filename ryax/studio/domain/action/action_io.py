# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass, field
from typing import Optional, Union

from ryax.studio.domain.action.action_values import ActionIOType


@dataclass
class ActionIO:
    id: str
    technical_name: str
    display_name: str
    help: str
    type: ActionIOType
    optional: bool
    enum_values: list[str] = field(default_factory=list)
    default_value: Optional[Union[str, float, int, bool, bytes]] = None
