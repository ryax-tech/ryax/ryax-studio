from dataclasses import dataclass
from typing import Optional


@dataclass
class ActionResources:
    id: str
    cpu: Optional[float] = None
    memory: Optional[int] = None
    time: Optional[float] = None
    gpu: Optional[int] = None
