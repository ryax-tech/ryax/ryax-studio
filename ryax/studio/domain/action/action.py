# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import uuid
from dataclasses import dataclass, field
from datetime import datetime
from typing import Any, Optional

from ryax.studio.domain.action.action_category import ActionCategory
from ryax.studio.domain.action.action_events import ActionDeletedEvent
from ryax.studio.domain.action.action_io import ActionIO
from ryax.studio.domain.action.action_resources import ActionResources
from ryax.studio.domain.action.action_values import ActionKind
from ryax.studio.domain.common.base_event import BaseEvent
from ryax.studio.domain.logo.logo import Logo


@dataclass
class Action:
    id: str
    project: str
    name: str
    technical_name: str
    version: str
    kind: ActionKind
    description: str
    build_date: datetime
    lockfile: bytes
    resources: Optional[ActionResources] = None
    logo: Optional[Logo] = None
    has_dynamic_outputs: bool = False
    inputs: list[ActionIO] = field(default_factory=list)
    outputs: list[ActionIO] = field(default_factory=list)
    events: list[BaseEvent] = field(init=False)
    categories: list[ActionCategory] = field(default_factory=list)
    addons: dict[str, dict] = field(default_factory=dict)

    def __new__(cls, *args: Any, **kwargs: Any) -> "Action":
        instance = super(Action, cls).__new__(cls)
        instance.events = []
        return instance

    def add_delete_event(self) -> None:
        self.events.append(ActionDeletedEvent(action_id=self.id, version=self.version))

    def add_new_category(self, category_name: str) -> None:
        self.categories.append(ActionCategory(id=str(uuid.uuid4()), name=category_name))
