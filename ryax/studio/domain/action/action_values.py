# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import enum

from ryax.studio.domain.workflow.workflow_exceptions import (
    ActionIOTypeInvalidException,
    DynamicOutputOriginInvalidException,
)


class ActionIOType(enum.Enum):
    STRING = "string"
    LONGSTRING = "longstring"
    INTEGER = "integer"
    FLOAT = "float"
    PASSWORD = "password"
    ENUM = "enum"
    FILE = "file"
    DIRECTORY = "directory"
    TABLE = "table"
    BOOLEAN = "boolean"
    BYTES = "bytes"

    @classmethod
    def string_to_enum(cls, value: str) -> "ActionIOType":
        for name in cls:
            if name.value == value:
                return name
        else:
            raise ActionIOTypeInvalidException

    def is_file(self) -> bool:
        return self in [ActionIOType.FILE, ActionIOType.DIRECTORY]

    def is_a_secret(self) -> bool:
        return self in [ActionIOType.PASSWORD]


class ActionKind(enum.Enum):
    SOURCE = "Source"
    PROCESSOR = "Processor"
    PUBLISHER = "Publisher"


class ActionErrorCode(enum.IntEnum):
    ACTION_NOT_DELETABLE = 1


class DynamicOutputOrigin(enum.Enum):
    PATH = "path"
    HEADER = "header"
    QUERY = "query"
    COOKIE = "cookie"
    BODY = "body"

    @classmethod
    def string_to_enum(cls, value: str) -> "DynamicOutputOrigin":
        for name in cls:
            if name.value == value:
                return name
        else:
            raise DynamicOutputOriginInvalidException
