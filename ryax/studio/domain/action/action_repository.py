# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import abc
from typing import List, Optional

from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_category import ActionCategory
from ryax.studio.domain.action.views.action import ActionView


class IActionRepository(abc.ABC):
    @abc.abstractmethod
    def get_actions_views(
        self, project_id: str, search: str = "", category: Optional[str] = None
    ) -> List[ActionView]:
        pass

    @abc.abstractmethod
    def get_action(self, action_id: str) -> Action:
        pass

    @abc.abstractmethod
    def add_action(self, action: Action) -> None:
        pass

    @abc.abstractmethod
    def delete_action(self, action: Action) -> None:
        pass

    @abc.abstractmethod
    def get_action_by_name_version_project(
        self, technical_name: str, version: str, project: str
    ) -> Action:
        pass

    @abc.abstractmethod
    def get_action_versions(self, action_id: str) -> List[Action]:
        pass

    @abc.abstractmethod
    def get_action_category_by_name(
        self, category_name: str
    ) -> Optional[ActionCategory]:
        pass

    @abc.abstractmethod
    def list_categories(self) -> List[ActionCategory]:
        pass
