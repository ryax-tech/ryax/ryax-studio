# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import asdict, dataclass, field
from typing import Optional

from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_category import ActionCategory
from ryax.studio.domain.action.action_io import ActionIO
from ryax.studio.domain.action.action_resources import ActionResources
from ryax.studio.domain.action.action_values import ActionKind


@dataclass
class ActionView:
    id: str
    name: str
    version: str
    kind: ActionKind
    lockfile: bytes
    description: Optional[str]


class ActionIOView(ActionIO):
    ...

    @staticmethod
    def from_action_io(io: ActionIO) -> "ActionIOView":
        return ActionIOView(**asdict(io))


class ActionCategoryView(ActionCategory):
    ...

    @staticmethod
    def from_action_category(category: ActionCategory) -> "ActionCategoryView":
        return ActionCategoryView(**asdict(category))


@dataclass
class ActionVersionView:
    id: str
    version: str

    @staticmethod
    def from_action_list(actions: list[Action]) -> list["ActionVersionView"]:
        return [
            ActionVersionView(id=action.id, version=action.version)
            for action in actions
        ]


@dataclass
class ActionExtendedView(ActionView):
    technical_name: str
    inputs: list[ActionIOView]
    outputs: list[ActionIOView]
    addons: dict[str, dict]
    categories: list[ActionCategoryView] = field(default_factory=list)
    has_dynamic_outputs: bool = False
    versions: list[ActionVersionView] = field(default_factory=list)
    resources: Optional[ActionResources] = None

    @staticmethod
    def from_action(action: Action) -> "ActionExtendedView":
        return ActionExtendedView(
            id=action.id,
            version=action.version,
            technical_name=action.technical_name,
            description=action.description,
            kind=action.kind,
            name=action.name,
            addons=action.addons,
            lockfile=action.lockfile,
            inputs=[ActionIOView.from_action_io(input) for input in action.inputs],
            outputs=[ActionIOView.from_action_io(output) for output in action.outputs],
            categories=[
                ActionCategoryView.from_action_category(category)
                for category in action.categories
            ],
            has_dynamic_outputs=action.has_dynamic_outputs,
            resources=action.resources,
        )
