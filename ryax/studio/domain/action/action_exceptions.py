# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from typing import List

from ryax.studio.domain.workflow.entities.workflow import Workflow


@dataclass
class ActionNotFoundException(Exception):
    message: str = "Action not found"


@dataclass
class ActionNotDeletableException(Exception):
    workflows: List[Workflow]
    message: str = "Action not deletable"


@dataclass
class ActionLogoNotFoundException(Exception):
    message: str = "Action logo not found"
