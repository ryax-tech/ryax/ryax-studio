# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass, field
from datetime import datetime
from typing import Dict, List, Optional

from ryax.studio.domain.action.action_values import ActionIOType, ActionKind
from ryax.studio.domain.common.base_event import BaseEvent


@dataclass
class ActionReadyEvent(BaseEvent):
    """Class to modeling action ready event"""

    @dataclass
    class ActionResources:
        cpu: Optional[float] = None
        memory: Optional[int] = None
        time: Optional[float] = None
        gpu: Optional[int] = None

    @dataclass
    class ActionIO:
        id: str
        technical_name: str
        display_name: str
        help: str
        type: ActionIOType
        optional: bool
        enum_values: List[str] = field(init=True, default_factory=list)
        default_value: Optional[str] = None

    @dataclass
    class Logo:
        id: str
        name: str
        extension: str
        content: bytes

    id: str
    name: str
    technical_name: str
    version: str
    kind: ActionKind
    description: str
    logo: Optional[Logo]
    dynamic_outputs: bool
    inputs: List[ActionIO]
    outputs: List[ActionIO]
    owner_id: str
    build_date: datetime
    categories: List[str]
    project_id: str
    lockfile: bytes
    resources: Optional[ActionResources] = None
    addons: Dict[str, dict] = field(init=True, default_factory=dict)
    event_type: str = "ActionReady"


@dataclass
class ActionDeletedEvent(BaseEvent):
    action_id: str
    version: str
    event_type: str = "ActionDeleted"
