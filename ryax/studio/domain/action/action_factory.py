# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import uuid

from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_events import ActionReadyEvent
from ryax.studio.domain.action.action_io import ActionIO
from ryax.studio.domain.action.action_resources import ActionResources
from ryax.studio.domain.logo.logo import Logo


class ActionFactory:
    def from_action_ready_event(self, event: ActionReadyEvent) -> Action:
        return Action(
            id=event.id,
            name=event.name,
            version=event.version,
            technical_name=event.technical_name,
            kind=event.kind,
            build_date=event.build_date,
            lockfile=event.lockfile,
            logo=Logo(
                id=event.logo.id,
                name=event.logo.name,
                action_id=event.id,
                extension=event.logo.extension,
                content=event.logo.content,
            )
            if event.logo
            else None,
            description=event.description,
            has_dynamic_outputs=event.dynamic_outputs,
            inputs=[
                ActionIO(
                    id=item.id,
                    technical_name=item.technical_name,
                    display_name=item.display_name,
                    help=item.help,
                    type=item.type,
                    enum_values=item.enum_values,
                    default_value=item.default_value,
                    optional=item.optional,
                )
                for item in event.inputs
            ],
            outputs=[
                ActionIO(
                    id=item.id,
                    technical_name=item.technical_name,
                    display_name=item.display_name,
                    help=item.help,
                    type=item.type,
                    enum_values=item.enum_values,
                    optional=item.optional,
                )
                for item in event.outputs
            ],
            project=event.project_id,
            addons=event.addons,
            resources=ActionResources(
                id=str(uuid.uuid4()),
                cpu=event.resources.cpu,
                time=event.resources.time,
                memory=event.resources.memory,
                gpu=event.resources.gpu,
            )
            if event.resources is not None
            else None,
        )
