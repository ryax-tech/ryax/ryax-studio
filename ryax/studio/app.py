# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import logging
import concurrent.futures
from pathlib import Path

import alembic.config
from aiohttp import web

from ryax.studio.container import ApplicationContainer
from ryax.studio.infrastructure.api.setup import setup as api_setup
from ryax.studio.infrastructure.messaging.setup import setup as messaging_setup

from .version import __version__

logger = logging.getLogger(__name__)


def init() -> web.Application:
    """Init application"""
    # Init application container
    container = ApplicationContainer()

    container.configuration.jwt_secret_key.from_env(
        "RYAX_JWT_SECRET_KEY", required=True
    )
    container.configuration.ryax_datastore.from_env("RYAX_DATASTORE", required=True)
    container.configuration.ryax_filestore.from_env("RYAX_FILESTORE", required=True)
    container.configuration.ryax_filestore_access_key.from_env(
        "RYAX_FILESTORE_ACCESS_KEY", required=True
    )
    container.configuration.ryax_filestore_secret_key.from_env(
        "RYAX_FILESTORE_SECRET_KEY", required=True
    )
    container.configuration.ryax_filestore_bucket.from_env(
        "RYAX_FILESTORE_BUCKET", required=True
    )
    container.configuration.ryax_broker.from_env("RYAX_BROKER", required=True)
    container.configuration.ryax_assets_path.from_env(
        "RYAX_ASSETS_PATH", str(Path(__file__).parent / "assets")
    )
    container.configuration.log_level.from_env("RYAX_LOG_LEVEL", "INFO")
    container.configuration.tmp_directory_path.from_env("RYAX_TMP_DIR", required=True)
    container.configuration.authorization_api_base_url.from_env(
        "RYAX_AUTHORIZATION_API_BASE_URL", required=True
    )
    container.configuration.api_port.from_env(
        "RYAX_STUDIO_API_PORT", default=8080, as_=int
    )
    container.configuration.max_upload_size.from_env(
        "RYAX_MAX_UPLOAD_SIZE", default=1073741824, as_=int
    )  # 1GB

    # Setup logging
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s.%(msecs)03d %(levelname)-7s %(name)-22s %(message)s",
        datefmt="%Y-%m-%d,%H:%M:%S",
    )
    str_level = container.configuration.log_level()
    numeric_level = getattr(logging, str_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError("Invalid log level: %s" % str_level)
    logging.getLogger("ryax").setLevel(numeric_level)
    logger = logging.getLogger(__name__)
    logger.info("Logging level is set to %s" % str_level.upper())

    app: web.Application = web.Application()
    api_setup(app, container)
    app["container"] = container

    consumer = container.messaging_consumer()
    messaging_setup(consumer, container)

    return app


async def on_startup(app: web.Application) -> None:
    """Hooks for application startup"""
    container: ApplicationContainer = app["container"]
    container.database_engine().connect()
    container.filestore_engine().connect()
    await container.filestore_engine().create_bucket()
    await container.messaging_engine().connect()
    await container.messaging_consumer().start()


async def on_cleanup(app: web.Application) -> None:
    """Define hook when application stop"""
    container: ApplicationContainer = app["container"]
    container.database_engine().disconnect()
    await container.messaging_engine().disconnect()


def db_migration() -> None:
    alembic_args = ("upgrade", "head")
    alembic.config.main(argv=alembic_args)


def start() -> None:
    """Start application"""
    with concurrent.futures.ProcessPoolExecutor() as executor:
        executor.submit(db_migration)
    app: web.Application = init()
    logger.info(f"Ryax Studio version: {__version__}")
    app.on_startup.append(on_startup)
    app.on_cleanup.append(on_cleanup)
    web.run_app(app, port=app["container"].configuration.api_port())
