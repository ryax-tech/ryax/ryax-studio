# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from typing import List, Optional, Sequence, Tuple, BinaryIO

from ryax.studio.domain.action.action_values import ActionIOType
from ryax.studio.domain.common.unit_of_work import IUnitOfWork
from ryax.studio.domain.services.filestore_service import IFilestoreService
from ryax.studio.domain.workflow.entities.workflow import Workflow
from ryax.studio.domain.workflow.entities.workflow_action import WorkflowAction
from ryax.studio.domain.workflow.entities.workflow_action_constraints import (
    WorkflowActionConstraints,
)
from ryax.studio.domain.workflow.entities.workflow_action_objectives import (
    WorkflowActionObjectives,
)
from ryax.studio.domain.workflow.entities.workflow_file import WorkflowFile
from ryax.studio.domain.workflow.views.workflow_action_input_view import (
    WorkflowActionInputView,
)
from ryax.studio.domain.workflow.views.workflow_action_output_view import (
    WorkflowActionOutputView,
)
from ryax.studio.domain.workflow.views.workflow_action_view import (
    WorkflowActionExtendedView,
    WorkflowActionVersion,
)
from ryax.studio.domain.workflow.workflow_exceptions import (
    ActionNotFoundException,
    WorkflowNotFoundException,
    WorkflowNotUpdatableException,
)
from ryax.studio.domain.workflow.workflow_values import (
    AddWorkflowAction,
    AddWorkflowActionDynamicOutput,
    AddWorkflowActionLink,
    AddWorkflowActionWithLink,
    UpdateAllWorkflowActionIOData,
    UpdateWorkflowAction,
    UpdateWorkflowActionDynamicOutput,
    UpdateWorkflowActionInputValue,
    UpdateWorkflowActionLinks,
)
from ryax.studio.infrastructure.addons.addons_service import IAddonsService


@dataclass
class WorkflowActionService:
    """Workflow action application service"""

    uow: IUnitOfWork
    filestore_service: IFilestoreService
    addons_service: IAddonsService

    def add_workflow_action(self, workflow_id: str, data: AddWorkflowAction) -> str:
        """Method to add a action to a workflow"""
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            if not workflow:
                raise WorkflowNotFoundException()
            if not workflow.is_updatable():
                raise WorkflowNotUpdatableException()

            action = self.uow.actions.get_action(data.action_id)
            if not action:
                raise ActionNotFoundException()

            addons = self.addons_service.load()
            new_action_id = workflow.add_action(action, data, addons).id
            workflow.check_all()
            self.uow.commit()
        return new_action_id

    def add_workflow_action_with_link(
        self, workflow_id: str, data: AddWorkflowActionWithLink
    ) -> WorkflowActionExtendedView:
        """Method to add a action to a workflow with a link"""
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            if not workflow:
                raise WorkflowNotFoundException()
            if not workflow.is_updatable():
                raise WorkflowNotUpdatableException()
            action = self.uow.actions.get_action(data.action_definition_id)
            if not action:
                raise ActionNotFoundException()

            new_workflow_action: WorkflowAction = workflow.add_action(
                action,
                AddWorkflowAction(
                    action_id=data.action_definition_id,
                    custom_name=action.name,
                    position_x=None,
                    position_y=None,
                ),
                self.addons_service.load(),
            )

            if data.replace_workflow_action_id is not None:
                old_workflow_action = workflow.get_action(
                    data.replace_workflow_action_id
                )
                workflow.transfer_actions_references(
                    old_workflow_action=old_workflow_action,
                    new_workflow_action=new_workflow_action,
                )
                workflow.transfer_workflow_action_links(
                    workflow_action_1=old_workflow_action,
                    workflow_action_2=new_workflow_action,
                )
                workflow.delete_action(old_workflow_action.id)
            else:
                if data.parent_workflow_action_id is not None:
                    # It's not the first action of the workflow
                    workflow.add_action_link(
                        upstream_action_id=data.parent_workflow_action_id,
                        downstream_action_id=new_workflow_action.id,
                    )

            action_versions = self.uow.actions.get_action_versions(
                new_workflow_action.action_id
            )

            new_workflow_action_view = WorkflowActionExtendedView.from_workflow_action(
                new_workflow_action
            )
            new_workflow_action_view.versions = [
                WorkflowActionVersion(action.id, action.version)
                for action in action_versions
            ]
            workflow.check_all()
            self.uow.commit()

        return new_workflow_action_view

    async def delete_workflow_action(
        self, workflow_id: str, workflow_action_id: str
    ) -> None:
        """Method to delete an action from a workflow"""
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            if not workflow:
                raise WorkflowNotFoundException()
            if not workflow.is_updatable():
                raise WorkflowNotUpdatableException()
            action_static_files: List[WorkflowFile] = workflow.get_action_static_files(
                workflow_action_id
            )
            for item in action_static_files:
                await self.filestore_service.remove_file(item.path)
            workflow.delete_action(workflow_action_id)
            workflow.check_all()
            self.uow.commit()

    def add_workflow_action_link(
        self, workflow_id: str, data: AddWorkflowActionLink
    ) -> None:
        """Method to add a action link in a workflow"""
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            if not workflow:
                raise WorkflowNotFoundException()
            if not workflow.is_updatable():
                raise WorkflowNotUpdatableException()
            workflow.add_action_link(data.upstream_action_id, data.downstream_action_id)
            workflow.check_all()
            self.uow.commit()

    def delete_workflow_action_link(
        self, workflow_id: str, workflow_action_link_id: str
    ) -> None:
        """Method to delete a action link in a workflow"""
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            if not workflow:
                raise WorkflowNotFoundException()
            if not workflow.is_updatable():
                raise WorkflowNotUpdatableException()
            else:
                workflow.delete_action_link(workflow_action_link_id)
                workflow.check_all()
                self.uow.commit()

    def update_workflow_action(
        self,
        workflow_id: str,
        workflow_action_id: str,
        workflow_action_data: UpdateWorkflowAction,
    ) -> None:
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            if not workflow:
                raise WorkflowNotFoundException()
            if not workflow.is_updatable():
                raise WorkflowNotUpdatableException()
            else:
                workflow.update_action(workflow_action_id, workflow_action_data)
                self.uow.commit()

    def change_workflow_action_version(
        self,
        workflow_id: str,
        workflow_action_id: str,
        action_id: str,
    ) -> Tuple[str, list[str]]:
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            if not workflow.is_updatable():
                raise WorkflowNotUpdatableException()
            action = self.uow.actions.get_action(action_id)
            workflow_action = workflow.get_action(workflow_action_id)
            data = AddWorkflowAction(
                action_id=action.id,
                custom_name=workflow_action.custom_name,
                position_x=workflow_action.position_x,
                position_y=workflow_action.position_y,
            )
            new_workflow_action = workflow.add_action(
                action, data, self.addons_service.load()
            )
            workflow.transfer_workflow_action_links(
                workflow_action, new_workflow_action
            )
            new_workflow_action.patch_workflow_action_ios_values(workflow_action)
            new_workflow_action.transfer_dynamic_outputs(workflow_action)
            workflow.transfer_actions_references(workflow_action, new_workflow_action)

            new_workflow_action.endpoint = workflow_action.endpoint
            removed_results = workflow.remove_workflow_results_from_old_action(
                workflow_action
            )
            workflow.delete_action(workflow_action_id)
            workflow.check_all()
            self.uow.commit()
            return new_workflow_action.id, removed_results

    def update_all_workflow_action_io(
        self,
        workflow_id: str,
        workflow_action_id: str,
        data: UpdateAllWorkflowActionIOData,
    ) -> None:
        # in this function, if we have a user object passed as an input, it can be a file or not...
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            if data.custom_name is not None:
                workflow.update_action(
                    workflow_action_id,
                    UpdateWorkflowAction(custom_name=data.custom_name),
                )
            if data.inputs is not None:
                for workflow_action_input in data.inputs:
                    project_variable_value_to_pass = (
                        self.uow.project_variables.get(
                            workflow_action_input.project_variable_value,
                            workflow.project,
                        )
                        if workflow_action_input.project_variable_value is not None
                        else None
                    )
                    workflow.update_action_input(
                        workflow_action_id=workflow_action_id,
                        workflow_action_input_id=workflow_action_input.id,
                        data=UpdateWorkflowActionInputValue(
                            static_value=workflow_action_input.static_value,
                            reference_value=workflow_action_input.reference_value,
                            project_variable_value=project_variable_value_to_pass,
                        ),
                    )

            if data.addons_inputs is not None:
                for workflow_action_addons_input in data.addons_inputs:
                    project_variable_addon_value_to_pass = (
                        self.uow.project_variables.get(
                            workflow_action_addons_input.project_variable_value,
                            workflow.project,
                        )
                        if workflow_action_addons_input.project_variable_value
                        is not None
                        else None
                    )
                    workflow.update_action_input(
                        workflow_action_id=workflow_action_id,
                        workflow_action_input_id=workflow_action_addons_input.id,
                        data=UpdateWorkflowActionInputValue(
                            static_value=workflow_action_addons_input.static_value,
                            reference_value=workflow_action_addons_input.reference_value,
                            project_variable_value=project_variable_addon_value_to_pass,
                        ),
                    )

            if data.dynamic_outputs is not None:
                for workflow_action_output in data.dynamic_outputs:
                    workflow.update_action_dynamic_output(
                        workflow_action_id=workflow_action_id,
                        workflow_dynamic_output_id=workflow_action_output.id,
                        data=UpdateWorkflowActionDynamicOutput(
                            technical_name=workflow_action_output.technical_name,
                            display_name=workflow_action_output.display_name,
                            help=workflow_action_output.help,
                            type=workflow_action_output.type,
                            enum_values=workflow_action_output.enum_values,
                            origin=workflow_action_output.origin,
                            optional=workflow_action_output.optional,
                        ),
                    )
            workflow.check_all()
            self.uow.commit()

    def update_workflow_action_input(
        self,
        workflow_id: str,
        workflow_action_id: str,
        workflow_action_input_id: str,
        data: UpdateWorkflowActionInputValue,
    ) -> None:
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            if not workflow:
                raise WorkflowNotFoundException()
            else:
                workflow.update_action_input(
                    workflow_action_id, workflow_action_input_id, data
                )
                workflow.check_all()
                self.uow.commit()

    async def delete_workflow_action_input_file(
        self, workflow_id: str, workflow_action_id: str, workflow_input_id: str
    ) -> None:
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            if not workflow.is_updatable():
                raise WorkflowNotUpdatableException()
            file_path = workflow.get_action_input_file_path(
                workflow_action_id, workflow_input_id
            )
            await self.filestore_service.remove_file(file_path)
            workflow_file = workflow.get_input_file(workflow_input_id)
            self.uow.workflows.delete_static_file(workflow_file)
            workflow.check_all()
            self.uow.commit()

    def add_workflow_action_dynamic_output(
        self,
        workflow_id: str,
        workflow_action_id: str,
        data: AddWorkflowActionDynamicOutput,
    ) -> None:
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            if not workflow:
                raise WorkflowNotFoundException()
            else:
                workflow.add_action_dynamic_output(workflow_action_id, data)
                self.uow.commit()

    def update_workflow_action_dynamic_output(
        self,
        workflow_id: str,
        workflow_action_id: str,
        workflow_dynamic_output_id: str,
        data: UpdateWorkflowActionDynamicOutput,
    ) -> None:
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            if not workflow:
                raise WorkflowNotFoundException()
            else:
                workflow.update_action_dynamic_output(
                    workflow_action_id, workflow_dynamic_output_id, data
                )
                self.uow.commit()

    def delete_workflow_action_dynamic_output(
        self,
        workflow_id: str,
        workflow_action_id: str,
        workflow_dynamic_output_id: str,
    ) -> None:
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            if not workflow:
                raise WorkflowNotFoundException()
            else:
                workflow.delete_action_dynamic_output(
                    workflow_action_id, workflow_dynamic_output_id
                )
                self.uow.commit()

    def list_workflow_action_inputs(
        self, workflow_id: str, workflow_action_id: str
    ) -> List[WorkflowActionInputView]:
        with self.uow:
            self.uow.workflows.check_workflow_exists(workflow_id)
            self.uow.workflows.check_workflow_action_exists(workflow_action_id)
            return self.uow.workflows.list_workflow_action_inputs_view(
                workflow_action_id
            )

    def list_workflow_action_outputs(
        self, workflow_id: str, workflow_action_id: str
    ) -> Sequence[WorkflowActionOutputView]:
        with self.uow:
            self.uow.workflows.check_workflow_exists(workflow_id)
            self.uow.workflows.check_workflow_action_exists(workflow_action_id)
            return self.uow.workflows.list_workflow_action_outputs_view(
                workflow_action_id
            )

    def search_workflow_action_outputs(
        self,
        workflow_id: str,
        with_type: Optional[str] = None,
        accessible_from: Optional[str] = None,
    ) -> Sequence[WorkflowActionOutputView]:
        """Method to search actions outputs in whole a workflow"""
        with self.uow:
            self.uow.workflows.check_workflow_exists(workflow_id)

            in_workflow_actions_filter = None
            if accessible_from is not None:
                in_workflow_actions_filter = (
                    self.uow.workflows.list_previous_workflow_actions_view(
                        accessible_from
                    )
                )

            with_type_filter = None
            if with_type is not None:
                with_type_filter = ActionIOType(with_type)

            return self.uow.workflows.search_workflow_action_outputs_view(
                workflow_id,
                with_type=with_type_filter,
                in_workflow_actions=in_workflow_actions_filter,
            )

    async def upload_static_file_input(
        self,
        workflow_id: str,
        workflow_action_id: str,
        workflow_action_input_id: str,
        filename: str,
        extension: str,
        file_io: BinaryIO,
        file_size: float,
    ) -> None:
        filename_to_generate = f"{filename}.{extension}" if extension else filename
        with self.uow:
            workflow: Workflow = self.uow.workflows.get_workflow(workflow_id)
            path: str = self.filestore_service.generate_file_path(filename_to_generate)

            await self.filestore_service.write(path, file_io)
            workflow.add_input_file(
                workflow_action_id,
                workflow_action_input_id,
                filename,
                extension,
                path,
                file_size,
            )

            workflow.check_all()
            self.uow.commit()

    def update_workflow_action_links(
        self, workflow_id: str, action_links: UpdateWorkflowActionLinks
    ) -> None:
        with self.uow:
            workflow: Optional[Workflow] = self.uow.workflows.get_workflow(workflow_id)
            if workflow is None:
                raise WorkflowNotFoundException()
            current_action_links = workflow.get_links_from_workflow_action(
                action_links.action_id
            )
            for current_link in current_action_links:
                workflow.delete_action_link(current_link.id)
            for next_action_id in action_links.next_actions_ids:
                workflow.add_action_link(action_links.action_id, next_action_id)
            workflow.sort_actions()
            workflow.check_all()
            self.uow.commit()

    def set_workflow_action_constraints(
        self, workflow_id: str, action_id: str, constraints: WorkflowActionConstraints
    ) -> None:
        with self.uow:
            workflow: Optional[Workflow] = self.uow.workflows.get_workflow(workflow_id)
            if workflow is None:
                raise WorkflowNotFoundException()
            workflow.set_action_constraints(action_id, constraints)
            self.uow.commit()

    def set_workflow_action_objectives(
        self, workflow_id: str, action_id: str, objectives: WorkflowActionObjectives
    ) -> None:
        with self.uow:
            workflow: Optional[Workflow] = self.uow.workflows.get_workflow(workflow_id)
            if workflow is None:
                raise WorkflowNotFoundException()
            workflow.set_action_objectives(action_id, objectives)
            self.uow.commit()
