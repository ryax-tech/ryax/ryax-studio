# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
import os
from typing import Any, Dict, List, Optional

from ryax.studio.domain.common.unit_of_work import IUnitOfWork
from ryax.studio.domain.services.filestore_service import IFilestoreService
from ryax.studio.domain.workflow.entities.workflow_action_input import (
    WorkflowActionInput,
)
from ryax.studio.domain.workflow.entities.workflow_result import WorkflowResult
from ryax.studio.domain.workflow.services.workflow_export_service import (
    IWorkflowExportService,
)
from ryax.studio.domain.workflow.services.workflow_import_service import (
    IWorkflowImportService,
)
from ryax.studio.domain.workflow.views.workflow_action_version_view import (
    WorkflowActionVersion,
)
from ryax.studio.domain.workflow.views.workflow_error_view import WorkflowErrorView
from ryax.studio.domain.workflow.views.workflow_list_view import WorkflowListView
from ryax.studio.domain.workflow.views.workflow_result_view import WorkflowResultView
from ryax.studio.domain.workflow.views.workflow_view import (
    WorkflowExtendedView,
    WorkflowView,
)
from ryax.studio.domain.workflow.workflow_exceptions import (
    NotAllKeysAreUniqueInResults,
    SchemaValidWorkflowInvalidException,
    WorkflowActionIONotFoundException,
    WorkflowHasNoTriggerException,
    WorkflowNotDeletableException,
    WorkflowNotFoundException,
    WrongStatusFilterException,
)
from ryax.studio.domain.workflow.workflow_factory import WorkflowFactory
from ryax.studio.domain.workflow.workflow_values import (
    CopyWorkflowData,
    CreateWorkflowData,
    CreateWorkflowResultData,
    WorkflowDeploymentStatus,
    WorkflowInformations,
    WorkflowValidityStatus,
)
from ryax.studio.infrastructure.messaging.utils.publisher import MessagingPublisher


class WorkflowService:
    """Workflow application service"""

    def __init__(
        self,
        uow: IUnitOfWork,
        factory: WorkflowFactory,
        messaging_publisher: MessagingPublisher,
        workflow_export_service: IWorkflowExportService,
        workflow_import_service: IWorkflowImportService,
        filestore_service: IFilestoreService,
    ) -> None:
        self.uow = uow
        self.factory = factory
        self.messaging_publisher = messaging_publisher
        self.workflow_export_service = workflow_export_service
        self.workflow_import_service = workflow_import_service
        self.filestore_service = filestore_service

    def list_workflows(
        self,
        current_project_id: str,
        search: Optional[str] = None,
        deployment_status_filter_value: Optional[str] = None,
    ) -> list[WorkflowListView]:
        """Method to list workflows"""
        with self.uow:
            if deployment_status_filter_value is not None:
                if WorkflowDeploymentStatus.has_value(deployment_status_filter_value):
                    deployment_status_filter = WorkflowDeploymentStatus(
                        deployment_status_filter_value
                    )
                else:
                    raise WrongStatusFilterException()
            else:
                deployment_status_filter = None
            workflows = self.uow.workflows.list_workflows(
                current_project_id, search, deployment_status_filter
            )

            converted_workflows: list[WorkflowListView] = []
            for workflow in workflows:
                # Get the endpoint prefix for this workflow
                trigger_name = None
                endpoint = None
                parameters: list[dict] = []
                try:
                    trigger = workflow.get_trigger_action()
                    # TODO replace this by a check on the endpoint in the addon parameters
                    endpoint = trigger.endpoint
                    trigger_name = trigger.get_name()

                    # Only list the inputs not null inputs with non default values
                    for input in trigger.get_all_inputs():
                        if input.has_file_value():
                            parameters.append(
                                {
                                    "name": input.display_name,
                                    "value": str(input.file_value),
                                }
                            )
                        if (
                            input.has_static_value()
                            and input.static_value != input.default_value
                            and not input.type.is_a_secret()
                        ):
                            parameters.append(
                                {
                                    "name": input.display_name,
                                    "value": input.static_value,
                                }
                            )
                except WorkflowHasNoTriggerException:
                    pass

                all_categories: set[str] = set()
                for action in workflow.get_actions():
                    for category in action.categories:
                        all_categories.add(str(category))

                sorted_categories = list(all_categories)
                sorted_categories.sort()

                converted_workflows.append(
                    WorkflowListView(
                        id=workflow.id,
                        name=workflow.name,
                        description=workflow.description,
                        validity_status=workflow.validity_status,
                        deployment_status=workflow.deployment_status,
                        has_form=workflow.has_form,
                        endpoint_prefix=endpoint,
                        categories=sorted_categories,
                        trigger=trigger_name,
                        parameters=parameters,
                    )
                )
            return converted_workflows

    def get_workflow(self, workflow_id: str) -> WorkflowView:
        """Method to get a workflow"""
        with self.uow:
            workflow_view = self.uow.workflows.get_workflow_view(workflow_id)
        return workflow_view

    def list_workflow_results(self, workflow_id: str) -> list[WorkflowResultView]:
        results = []
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            for result in workflow.results:
                workflow_result_info = workflow.get_workflow_action_result_info(
                    result.workflow_action_io_id
                )
                results.append(
                    WorkflowResultView(
                        key=result.key,
                        workflow_action_io_id=result.workflow_action_io_id,
                        description=f"Value from {'output' if result.is_an_output() else 'input'} \"{workflow_result_info.action_io_name}\" of action \"{workflow_result_info.action_name}\"",
                        type=workflow_result_info.action_type,
                    )
                )
        return results

    def overwrite_workflow_results(
        self, workflow_id: str, workflow_results: List[CreateWorkflowResultData]
    ) -> List[WorkflowResult]:
        # check the uniqueness of keys
        if len(workflow_results) != len({res.key for res in workflow_results}):
            raise NotAllKeysAreUniqueInResults
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            workflow_input_ids = [wf_input.id for wf_input in workflow.get_all_inputs()]
            workflow_output_ids = [
                wf_output.id for wf_output in workflow.get_all_outputs()
            ]
            if not all(
                workflow_result.workflow_action_io_id
                in workflow_input_ids + workflow_output_ids
                for workflow_result in workflow_results
            ):
                raise WorkflowActionIONotFoundException
            workflow.results = [
                WorkflowResult.create_from_data(
                    workflow_result,
                    is_an_output=workflow_result.workflow_action_io_id
                    in workflow_output_ids,
                )
                for workflow_result in workflow_results
            ]
            self.uow.commit()
            return workflow.results

    def get_workflow_extended(self, workflow_id: str) -> WorkflowExtendedView:
        """Method to get a workflow"""
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            workflow_view = WorkflowExtendedView.from_workflow(workflow)

            for action in workflow_view.actions:
                action.versions = [
                    WorkflowActionVersion(action.id, action.version)
                    for action in self.uow.actions.get_action_versions(action.action_id)
                ]
        return workflow_view

    def create_workflow(
        self, create_workflow_data: CreateWorkflowData, owner: str, project_id: str
    ) -> str:
        """Method to create a workflow"""
        with self.uow:
            new_workflow = self.factory.from_create_workflow_data(
                create_workflow_data, owner, project_id
            )
            new_workflow.check_all()
            self.uow.workflows.add_workflow(new_workflow)
            self.uow.commit()
            return new_workflow.id

    async def copy_workflow(
        self, copy_workflow_data: CopyWorkflowData, owner: str, project_id: str
    ) -> str:
        """Method to copy a workflow"""
        with self.uow:
            workflow = self.uow.workflows.get_workflow(copy_workflow_data.from_workflow)
            if workflow.project != project_id:
                raise WorkflowNotFoundException
            new_workflow = workflow.copy(copy_workflow_data, owner)

            for workflow_static_file in new_workflow.get_all_static_files():
                new_path: str = self.filestore_service.generate_file_path(
                    workflow_static_file.name
                )
                await self.filestore_service.copy_file(
                    workflow_static_file.path, new_path
                )
                workflow_static_file.update_path(new_path)

            new_workflow.check_all()
            self.uow.workflows.add_workflow(new_workflow)
            self.uow.commit()
            return new_workflow.id

    def update_workflow(
        self, workflow_id: str, workflow_informations: WorkflowInformations, owner: str
    ) -> None:
        """Method to update a workflow"""
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            workflow.update_informations(workflow_informations, owner)
            self.uow.commit()

    async def delete_workflow(self, workflow_id: str, authorization: str) -> None:
        """Method to delete a workflow"""
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            if not workflow.is_deletable():
                raise WorkflowNotDeletableException()

            all_static_files = workflow.get_all_static_files()
            for item in all_static_files:
                await self.filestore_service.remove_file(item.path)

            workflow.prepare_workflow_deleted_event()
            await self.messaging_publisher.publish(workflow.events)

            self.uow.workflows.delete_workflow(workflow)
            self.uow.commit()

    def get_workflow_errors(self, workflow_id: str) -> List[WorkflowErrorView]:
        """Method to retrieve all errors in a workflow"""
        with self.uow:
            self.uow.workflows.check_workflow_exists(workflow_id)
            return self.uow.workflows.get_workflow_errors(workflow_id)

    def get_workflow_schema(self) -> dict:
        return self.workflow_import_service.get_schema()

    def validate_workflow_schema(
        self, workflow_content: bytes, owner: str, project: str
    ) -> None:
        with self.uow:
            self.workflow_import_service.load_workflow_schema(workflow_content)
            self.workflow_import_service.check_workflow_schema_validity()
            actions_references = self.workflow_import_service.get_actions_references()
            actions = [
                self.uow.actions.get_action_by_name_version_project(
                    item[0], item[1], project
                )
                for item in actions_references
            ]
            workflow_file_metadata: Dict[str, Any] = {}
            workflow = self.workflow_import_service.process(
                owner, actions, workflow_file_metadata, project
            )
            workflow.check_all()
            if not workflow.validity_status == WorkflowValidityStatus.VALID:
                raise SchemaValidWorkflowInvalidException()

    async def deploy_workflow(self, workflow_id: str) -> None:
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            workflow.deploy()
            await self.messaging_publisher.publish(workflow.events)
            self.uow.commit()

    def deploy_workflow_success(self, workflow_id: str) -> None:
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            workflow.deploy_workflow_success()
            self.uow.commit()

    def undeploy_workflow_success(
        self, workflow_id: str, error: Optional[str] = None
    ) -> None:
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            if not error:
                workflow.undeploy_success()
            else:
                workflow.undeploy_with_error(error)
            self.uow.commit()

    async def stop_deploy_workflow(self, workflow_id: str, graceful: bool) -> None:
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            workflow.undeploy(graceful)
            await self.messaging_publisher.publish(workflow.events)
            self.uow.commit()

    async def import_workflow(
        self, workflow_content: bytes, owner: str, project: str
    ) -> str:
        with self.uow:
            self.workflow_import_service.load_workflow_package(workflow_content)
            self.workflow_import_service.check_workflow_schema_validity()
            actions_references = self.workflow_import_service.get_actions_references()
            actions = [
                self.uow.actions.get_action_by_name_version_project(
                    item[0], item[1], project
                )
                for item in actions_references
            ]
            workflow_file_metadata: Dict[str, Any] = {}
            workflow_files: List[
                Dict[str, Any]
            ] = self.workflow_import_service.get_workflow_io_files()
            for file in workflow_files:
                path = self.filestore_service.generate_file_path(file["name"])
                workflow_file_metadata[file["name"]] = {
                    "path": path,
                    "size": file["size"],
                }

            workflow = self.workflow_import_service.process(
                owner, actions, workflow_file_metadata, project
            )
            for file in workflow_files:
                filename = file["name"]
                await self.filestore_service.upload_file(
                    workflow_file_metadata[filename]["path"],
                    file["content"],
                    file["size"],
                )
            workflow.project = project
            workflow.check_all()
            workflow.sort_actions()
            self.uow.workflows.add_workflow(workflow)
            self.uow.commit()
            return workflow.id

    async def export_workflow(self, workflow_id: str) -> str:
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)

            # Create workflow zipfile
            path_to_workflow_zip = (
                self.workflow_export_service.generate_workflow_zip_file()
            )

            # Load static files
            workflow_all_inputs: List[WorkflowActionInput] = workflow.get_all_inputs()
            all_inputs_files = {}
            input_file_data: bytes
            for workflow_input in workflow_all_inputs:
                if workflow_input.file_value is not None:
                    input_file_value_path = workflow_input.file_value.path
                    input_file_data = await self.filestore_service.get_file(
                        input_file_value_path
                    )
                    all_inputs_files[input_file_value_path] = input_file_data
                elif workflow_input.project_variable_value is not None and (
                    workflow_input.project_variable_value.type.is_file()
                ):
                    assert workflow_input.project_variable_value.file_path is not None
                    project_variable_file_path = (
                        workflow_input.project_variable_value.file_path
                    )
                    input_file_data = await self.filestore_service.get_file(
                        project_variable_file_path
                    )
                    all_inputs_files[project_variable_file_path] = input_file_data
            # Create packaging and set it in zipfile with static files
            self.workflow_export_service.export_workflow(workflow, all_inputs_files)
            return os.path.abspath(path_to_workflow_zip)

    def overwrite_workflow_endpoint(
        self, workflow_id: str, new_path: Optional[str]
    ) -> Optional[str]:
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            trigger = workflow.get_trigger_action()
            if trigger.endpoint == new_path:
                return trigger.endpoint

            self.uow.workflows.check_if_endpoint_exists(
                new_path, project_id=workflow.project
            )
            trigger.update_endpoint(new_path)
            self.uow.commit()
            return trigger.endpoint

    def get_workflow_endpoint(self, workflow_id: str) -> Optional[str]:
        with self.uow:
            workflow = self.uow.workflows.get_workflow(workflow_id)
            try:
                trigger = workflow.get_trigger_action()
                return trigger.endpoint
            except WorkflowHasNoTriggerException:
                return None
