# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from dataclasses import dataclass
from typing import List, Optional, BinaryIO

from ryax.studio.domain.common.unit_of_work import IUnitOfWork
from ryax.studio.domain.project_variables.project_variable_entities import (
    ProjectVariable,
    ProjectVariableTransactionData,
)
from ryax.studio.domain.project_variables.project_variable_exceptions import (
    ProjectVariableIsInUseError,
)
from ryax.studio.domain.services.filestore_service import IFilestoreService


@dataclass
class ProjectVariableService:
    """User object application service"""

    uow: IUnitOfWork
    filestore_service: IFilestoreService

    def list(
        self, current_project_id: str, type_search: Optional[str]
    ) -> List[ProjectVariable]:
        with self.uow:
            return self.uow.project_variables.list(current_project_id, type_search)

    def get(self, project_variable_id: str, current_project_id: str) -> ProjectVariable:
        with self.uow:
            return self.uow.project_variables.get(
                project_variable_id, current_project_id
            )

    def add(self, data: ProjectVariableTransactionData) -> str:
        with self.uow:
            new_project_variable = ProjectVariable.from_add_project_variable_data(data)
            self.uow.project_variables.add(new_project_variable)
            self.uow.commit()
            return new_project_variable.id

    async def update(
        self,
        project_variable_id: str,
        data: ProjectVariableTransactionData,
        current_project_id: str,
    ) -> None:
        with self.uow:
            project_variable = self.uow.project_variables.get(
                project_variable_id, current_project_id
            )
            # Only allow to update var if workflow is updatable
            linked_workflow = self.uow.project_variables.get_linked_workflows(
                project_variable
            )
            for workflow in linked_workflow:
                if not workflow.is_updatable():
                    raise ProjectVariableIsInUseError(workflows=linked_workflow)

            if project_variable.type.value != data.type:
                raise ValueError("User Object type cannot be changed")
            if (
                project_variable.type.is_file()
                and project_variable.file_path is not None
                and project_variable.file_path != data.file_path
            ):
                # Remove old file if it is updated
                await self.filestore_service.remove_file(project_variable.file_path)

            project_variable.update(data)
            self.uow.commit()

    async def delete(self, project_variable_id: str, current_project_id: str) -> None:
        with self.uow:
            project_variable = self.uow.project_variables.get(
                project_variable_id, current_project_id
            )
            # Only allow to remove var if workflow is updatable
            linked_workflow = self.uow.project_variables.get_linked_workflows(
                project_variable
            )
            for workflow in linked_workflow:
                if not workflow.is_updatable():
                    raise ProjectVariableIsInUseError(workflows=linked_workflow)

            self.uow.project_variables.delete(project_variable)
            # Clean previous file if it was changed
            if (
                project_variable.type.is_file()
                and project_variable.file_path is not None
            ):
                await self.filestore_service.remove_file(project_variable.file_path)

            # Update workflows status after deletion
            for workflow in linked_workflow:
                workflow.check_all()
            self.uow.commit()

    async def _write_file(
        self,
        filename: str,
        extension: str,
        file_io: BinaryIO,
        data: ProjectVariableTransactionData,
    ) -> None:
        filename_to_generate = f"{filename}.{extension}" if extension else filename
        project_variable_path: str = self.filestore_service.generate_file_path(
            filename_to_generate
        )
        data.file_path = project_variable_path
        await self.filestore_service.write(data.file_path, file_io)
        data.value = filename_to_generate

    async def add_with_file(
        self,
        filename: str,
        extension: str,
        file_io: BinaryIO,
        data: ProjectVariableTransactionData,
    ) -> str:
        with self.uow:
            await self._write_file(filename, extension, file_io, data)
            new_project_variable = ProjectVariable.from_add_project_variable_data(data)
            self.uow.project_variables.add(new_project_variable)
            self.uow.commit()
            return new_project_variable.id

    async def update_with_file(
        self,
        filename: str,
        extension: str,
        file_io: BinaryIO,
        data: ProjectVariableTransactionData,
        project_variable_id: str,
    ) -> None:
        with self.uow:
            # Check that the object exists before writing it
            self.get(project_variable_id, data.project_id)

            await self._write_file(filename, extension, file_io, data)
            await self.update(project_variable_id, data, data.project_id)
            self.uow.commit()
