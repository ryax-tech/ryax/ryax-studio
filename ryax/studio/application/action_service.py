# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
from typing import List, Optional

from ryax.studio.domain.action.action import Action
from ryax.studio.domain.action.action_category import ActionCategory
from ryax.studio.domain.action.action_events import ActionReadyEvent
from ryax.studio.domain.action.action_exceptions import (
    ActionNotDeletableException,
    ActionNotFoundException,
)
from ryax.studio.domain.action.action_factory import ActionFactory
from ryax.studio.domain.action.views.action import (
    ActionExtendedView,
    ActionVersionView,
    ActionView,
)
from ryax.studio.domain.common.event_publisher import IEventPublisher
from ryax.studio.domain.common.unit_of_work import IUnitOfWork
from ryax.studio.domain.logo.logo import Logo
from ryax.studio.domain.services.assets_storage_service import IAssetsStorageService


class ActionService:
    """Action application service"""

    def __init__(
        self,
        uow: IUnitOfWork,
        factory: ActionFactory,
        assets_storage_service: IAssetsStorageService,
        event_publisher: IEventPublisher,
    ) -> None:
        self.uow = uow
        self.factory = factory
        self.assets_storage_service = assets_storage_service
        self.event_publisher = event_publisher

    def get_actions(
        self, current_project_id: str, search: str = "", category: Optional[str] = None
    ) -> List[ActionView]:
        """The two parameters search and category are used as action filters"""
        with self.uow:
            return self.uow.actions.get_actions_views(
                current_project_id, search, category
            )

    def get_action(self, action_id: str) -> Action:
        with self.uow:
            return self.uow.actions.get_action(action_id)

    def get_action_extended_view(self, action_id: str) -> ActionExtendedView:
        with self.uow:
            action = self.uow.actions.get_action(action_id)
            action_view = ActionExtendedView.from_action(action)
            action_view.versions = ActionVersionView.from_action_list(
                self.list_action_versions(action_id)
            )
            return action_view

    def add_action(self, event: ActionReadyEvent) -> str:
        with self.uow:
            new_action = self.factory.from_action_ready_event(event)
            category: Optional[ActionCategory]
            for item in event.categories:
                category = self.uow.actions.get_action_category_by_name(item)
                if not category:
                    new_action.add_new_category(item)
                else:
                    new_action.categories.append(category)
            self.uow.actions.add_action(new_action)
            self.uow.commit()
            return new_action.id

    def get_logo(self, action_id: str) -> Logo:
        with self.uow:
            logo = None
            try:
                action = self.uow.actions.get_action(action_id)
                logo = action.logo
            except ActionNotFoundException:
                pass
            if not logo:
                logo = self.assets_storage_service.get_default_logo()
            return logo

    def list_action_versions(self, action_id: str) -> List[Action]:
        with self.uow:
            return self.uow.actions.get_action_versions(action_id)

    async def delete_action(self, action_id: str) -> None:
        with self.uow:
            action = self.uow.actions.get_action(action_id)
            workflows = self.uow.workflows.list_workflows_where_action_used(action_id)
            if len(workflows) > 0:
                raise ActionNotDeletableException(workflows=workflows)
            else:
                self.uow.actions.delete_action(action)
                action.add_delete_event()
                await self.event_publisher.publish(action.events)
                self.uow.commit()

    def list_categories(self) -> List[ActionCategory]:
        with self.uow:
            return self.uow.actions.list_categories()
