# Ryax Repository

This repository is part of [Ryax](https://github.com/RyaxTech/ryax).

Ryax studio (code source) management microservice.
This microservice allow to design workflows using module blocks.

## Requirements

Assume that you have following dependencies installed on your system :

* Python 3.7 ([Installation Guide](https://wiki.python.org/moin/BeginnersGuide/))
* Poetry ([Installation Guide](https://python-poetry.org/docs/#installation))
* Docker ([Install Guide](https://docs.docker.com/get-docker/))

## Quick start

1. Open terminal in project and run ``docker-compose up`` to start development services.
2. Open other terminal in project and run ``poetry install`` to install dependencies.
3. Run ``poetry shell`` to activate the virtualenv.
4. Run ``export $(grep -v '^#' default.env | xargs)`` to load environment definition from file.
5. Run ``python main.py`` to start application.


## Linting

To run linter on code, use following command :

```bash
poetry run ./lint.sh
```

You can use the `-f` option to apply the autoformat :

```bash
poetry run ./lint.sh -f
```

### Setup git pre commit hook

It is recommended to create a pre-commit hook on git and make it executable:

```bash
cp ./pre-commit.sh ./.git/hooks/pre-commit
chmod +x ./.git/hooks/pre-commit
```

## Virtual environment shell

To open a shell in the virtual environment, by running following commands : 
1. Run ``poetry shell`` to activate the virtualenv.
2. Run ``export $(grep -v '^#' default.env | xargs)`` to load environment definition from file.

## Testing

**IMPORTANT :** Commands should be run in a virtual environment shell to use defined environment variables.

**IMPORTANT :** Docker compose should be started when running tests 

Use the following command to run tests:

```bash
pytest tests/
```

Use the following command to run tests with coverage:

```bash
pytest --cov=studio tests/
```

## Database migration (Alembic)

**IMPORTANT :** Commands should be run in a virtual environment shell to use defined environment variables.

Use the following command to generate database migration script:

```bash
alembic revision -m "<script_migration_message>" --autogenerate
```

Use the following command to migrate database (upgrade database model schema):

```bash
alembic upgrade head
```

Migration scripts are generated in ``./migrations/version`` directory.

## Message compilation (Protobuf)

Use the following command to compile protobuf message definitions (.proto file):

```bash
 python -m grpc_tools.protoc \
  -I ./ \
  --proto_path=. \
  --python_out=. \
  --mypy_out=. \
  ryax/studio/infrastructure/messaging/messages/*.proto
```
