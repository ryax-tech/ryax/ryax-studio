#!/usr/bin/env bash

clean_up() {
    set +e
    echo "Cleaning testing environment..."
    docker compose down -v
}
trap clean_up EXIT

# Export variables define in .env file
set -a
source default.env
set +a

# Start services
docker compose up -d

# Wait for services up
sleep 3

# Run tests and  export coverage
pytest --cov ./ryax --cov-report xml:coverage.xml --cov-report term --cov-config=.coveragerc --cov-branch -ra $@
